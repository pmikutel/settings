import { takeLatest, put, select } from 'redux-saga/effects';
import reportError from 'report-error';
import { map, pipe, mergeAll, prop, path, defaultTo, ifElse, always, isNil } from 'ramda';
import { reset as resetForm } from 'redux-form/immutable';
import { delay } from 'redux-saga';

import { ProductTypes, ProductActions } from './product.redux';
import { shopifyApi, hasErrors } from '../../services/shopify/api';
import api from '../../services/api';
import { selectActiveVariantId } from './product.selectors';
import { decode, encode, mapIdForProducts } from '../../utils/shopifyUrls';
import { extractNode } from '../../utils/shopifyData';

export function* request(product) {
  try {
    const response = yield shopifyApi.post('/', `{
    node(id: "${encode(product.id, 'product')}")
      {
        id,
        ... on Product {
          title,
          productType,
          handle,
          collections(first: 1){
            edges {
              node {
                title,
                products(first:5) {
                  edges {
                    node {
                      title,
                      id,
                      productType,
                      handle,
                      variants(first: 1) {
                        edges {
                          node {
                            title,
                            id,
                            price,
                            compareAtPrice,
                            image {
                              originalSrc
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          variants(first: 5) {
            edges {
              node {
                price,
                title,
                id,
                compareAtPrice,
                selectedOptions {
                  name
                  value
                },
                image {
                  originalSrc
                }
              }
            }
          }
        }
      }
    }`);

    const { title, id, productType, handle, variants: { edges }, collections } = response.data.data.node;
    const collectionTitle = path(['edges', 0, 'node', 'title'], collections);
    const collectionProducts = path(['edges', 0, 'node', 'products', 'edges'], collections);
    const refactorCollections = (data) => mapIdForProducts(extractNode(data));
    const data = {
      activeVariantId: product.variantId,
      variants: mapIdForProducts(extractNode(edges)),
      collectionTitle: defaultTo('', collectionTitle),
      collections: ifElse(isNil, always({}), refactorCollections)(collectionProducts),
      product: {
        title,
        id: decode(id),
        productType,
        handle,
      },
    };

    yield put(ProductActions.success(data));
  } catch (e) {
    yield put(ProductActions.failure(e));
    yield reportError(e);
  }
}

function* success({ data: variants }) {
  try {
    const variantid = yield select(selectActiveVariantId);
    yield put(ProductActions.metafieldsRequest(variantid || variants.variants[0].id));
  } catch (e) {
    yield reportError(e);
  }
}


function* metafieldsRequest({ id }) {
  try {
    const decodedId = encode(id, 'variant');
    const response = yield api.get(`/metafields/variant/${decodedId}`);

    if (hasErrors(response)) {
      yield put(ProductActions.failure(response));
      return;
    }

    const data = pipe(prop('data'), map(({ key, value }) => ({ [key]: value })), mergeAll)(response);

    yield put(ProductActions.setActiveVariantId(id));
    yield put(ProductActions.metafieldsSuccess(data));
  } catch (e) {
    yield put(ProductActions.metafieldsFailure(e));
    yield reportError(e);
  }
}

export function* customNewsletterRequest({ data }) {
  try {
    const variantid = yield select(selectActiveVariantId);
    yield api.post('/newsletter/custom', {
      ...data.toJS(),
      'merge_fields': {
        'VARIANTID': variantid,
      },
    });
    yield put(ProductActions.customNewsletterSuccess());
  } catch (e) {
    yield put(ProductActions.customNewsletterFailure(e));
    yield reportError(e);
  }
}

export function* customNewsletterSuccess() {
  try {
    yield delay(2000);
    yield put(resetForm('customNewsletter'));
    yield put(ProductActions.customNewsletterReset());
  } catch (e) {
    yield put(ProductActions.customNewsletterFailure(e));
    yield reportError(e);
  }
}

export default function* productSaga() {
  yield takeLatest(ProductTypes.REQUEST, request);
  yield takeLatest(ProductTypes.METAFIELDS_REQUEST, metafieldsRequest);
  yield takeLatest(ProductTypes.CUSTOM_NEWSLETTER_REQUEST, customNewsletterRequest);
  yield takeLatest(ProductTypes.CUSTOM_NEWSLETTER_SUCCESS, customNewsletterSuccess);
  yield takeLatest(ProductTypes.SUCCESS, success);
}
