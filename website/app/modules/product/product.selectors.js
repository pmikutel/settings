import { createSelector } from 'reselect';

const selectProductDomain = state => state.get('product');

export const selectVariantsItems = createSelector(
  selectProductDomain,
  state => state.getIn(['variants', 'items']),
);

export const selectIsFetching = createSelector(
  selectProductDomain,
  state => state.get('isFetching'),
);

export const selectCollectionTitle = createSelector(
  selectProductDomain,
  state => state.getIn(['collections', 'title']),
);

export const selectAllCollectionsItems = createSelector(
  selectProductDomain,
  state => state.getIn(['collections', 'items']),
);

export const selectProduct = createSelector(
  selectProductDomain,
  state => state.get('data'),
);

export const selectProductId = createSelector(
  selectProduct,
  state => state.get('id'),
);

export const selectCollections = createSelector(
  selectAllCollectionsItems, selectProductId,
  (items, id) => items.filter(item => item.get('id') !== id),
);

export const selectActiveVariantId = createSelector(
  selectProductDomain,
  state => state.get('activeVariantId'),
);

export const selectActiveVariant = createSelector(
  selectVariantsItems, selectActiveVariantId,
  (items, id) => items.find(item => item.get('id') === id),
);

export const selectVariants = createSelector(
  selectVariantsItems, selectActiveVariantId,
  (items, id) => items.filter(item => item.get('id') !== id),
);

export const selectMetafields = createSelector(
  selectProductDomain,
  state => state.get('metafields'),
);

export const selectCustomNewsletterSuccess = createSelector(
  selectProductDomain,
  state => state.getIn(['customNewsletter', 'isSuccess']),
);

export const selectCustomNewsletterError = createSelector(
  selectProductDomain,
  state => state.getIn(['customNewsletter', 'isError']),
);

export const selectCustomNewsletterFetching = createSelector(
  selectProductDomain,
  state => state.getIn(['customNewsletter', 'isFetching']),
);
