import { expect } from 'chai';
import { Record, fromJS, Map } from 'immutable';

import { reducer, ProductActions } from '../product.redux';


describe('Product: redux', () => {
  const ProductRecord = new Record({
    isFetching: false,
    data: Map(),
    variants: fromJS({
      items: Map(),
    }),
    collections: fromJS({
      items: Map(),
      title: '',
    }),
    activeVariantId: '',
    metafields: Map(),
    error: null,
    customNewsletter: fromJS({
      isFetching: false,
      isSuccess: false,
      isError: false,
      error: null,
    }),
  });

  const state = new ProductRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on REQUEST', () => {
      const expectedState = new ProductRecord().set('isFetching', true);

      expect(reducer(state, ProductActions.request()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SUCCESS', () => {
      const data = [{ product: {}, variants: [], collectionsTitle: '', collections: [], activeVariantId: 1 }];

      const expectedState = new ProductRecord()
        .set('isFetching', false)
        .set('activeVariantId', fromJS(data.activeVariantId))
        .set('data', fromJS(data.product))
        .setIn(['variants', 'items'], fromJS(data.variants))
        .setIn(['collections', 'title'], fromJS(data.collectionsTitle))
        .setIn(['collections', 'items'], fromJS(data.collections));

      expect(reducer(state, ProductActions.success(data)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new ProductRecord()
        .set('isFetching', false)
        .set('error', fromJS(error));

      expect(reducer(state, ProductActions.failure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on METAFIELDS_REQUEST', () => {
      const expectedState = new ProductRecord().set('isFetching', true);

      expect(reducer(state, ProductActions.metafieldsRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on METAFIELDS_SUCCESS', () => {
      const data = [{}];

      const expectedState = new ProductRecord()
        .set('isFetching', false)
        .set('metafields', fromJS(data));

      expect(reducer(state, ProductActions.metafieldsSuccess(data)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on METAFIELDS_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new ProductRecord()
        .set('isFetching', false)
        .set('error', fromJS(error));

      expect(reducer(state, ProductActions.metafieldsFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on CUSTOM_NEWSLETTER_REQUEST', () => {
      const expectedState = new ProductRecord()
        .setIn(['customNewsletter', 'isFetching'], true)
        .setIn(['customNewsletter', 'isSuccess'], false)
        .setIn(['customNewsletter', 'isError'], false)
        .setIn(['customNewsletter', 'error'], null);

      expect(reducer(state, ProductActions.customNewsletterRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on CUSTOM_NEWSLETTER_SUCCESS', () => {
      const expectedState = new ProductRecord()
        .setIn(['customNewsletter', 'isFetching'], false)
        .setIn(['customNewsletter', 'isSuccess'], true)
        .setIn(['customNewsletter', 'isError'], false)
        .setIn(['customNewsletter', 'error'], null);

      expect(reducer(state, ProductActions.customNewsletterSuccess()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on CUSTOM_NEWSLETTER_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new ProductRecord()
        .setIn(['customNewsletter', 'isFetching'], false)
        .setIn(['customNewsletter', 'isSuccess'], false)
        .setIn(['customNewsletter', 'isError'], true)
        .setIn(['customNewsletter', 'error'], fromJS(error));

      expect(reducer(state, ProductActions.customNewsletterFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on CUSTOM_NEWSLETTER_RESET', () => {
      const expectedState = new ProductRecord()
        .setIn(['customNewsletter', 'isFetching'], false)
        .setIn(['customNewsletter', 'isSuccess'], false)
        .setIn(['customNewsletter', 'isError'], false)
        .setIn(['customNewsletter', 'error'], null);

      expect(reducer(state, ProductActions.customNewsletterReset()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SET_ACTIVE_VARIANT_ID', () => {
      const id = { id: 123 };

      const expectedState = new ProductRecord()
        .set('activeVariantId', fromJS(id));

      expect(reducer(state, ProductActions.setActiveVariantId(id)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
