import { expect } from 'chai';
import { fromJS, Map, Record } from 'immutable';

import {
  selectVariantsItems,
  selectCollectionTitle,
  selectAllCollectionsItems,
  selectProduct,
  selectProductId,
  selectCollections,
  selectActiveVariantId,
  selectActiveVariant,
  selectVariants,
  selectMetafields,
  selectCustomNewsletterSuccess,
  selectCustomNewsletterError,
  selectCustomNewsletterFetching,
} from '../product.selectors';

describe('Product: selectors', () => {
  const ProductRecord = new Record({
    isFetching: false,
    data: Map(),
    variants: fromJS({
      items: Map(),
    }),
    collections: fromJS({
      items: Map(),
      title: '',
    }),
    activeVariantId: '',
    metafields: Map(),
    error: null,
    customNewsletter: fromJS({
      isFetching: false,
      isSuccess: false,
      isError: false,
      error: null,
    }),
  });

  const initialState = fromJS({
    product: new ProductRecord(),
  });

  describe('selectVariantsItems', () => {
    const items = [{ id: 1 }, { id: 2 }];

    const state = initialState.setIn(['product', 'variants', 'items'], fromJS(items));

    it('should select variants items', () => {
      expect(selectVariantsItems(state).toJS()).to.deep.equal(items);
    });
  });

  describe('selectCollectionTitle', () => {
    const title = { title: 'title' };

    const state = initialState.setIn(['product', 'collections', 'title'], fromJS(title));

    it('should select collection title', () => {
      expect(selectCollectionTitle(state).toJS()).to.deep.equal(title);
    });
  });

  describe('selectAllCollectionsItems', () => {
    const items = [{ id: 1 }, { id: 2 }];

    const state = initialState.setIn(['product', 'collections', 'items'], fromJS(items));

    it('should select all collections items', () => {
      expect(selectAllCollectionsItems(state).toJS()).to.deep.equal(items);
    });
  });

  describe('selectProduct', () => {
    const data = { title: 'title', id: 1 };
    const state = initialState.setIn(['product', 'data'], fromJS(data));

    it('should select product', () => {
      expect(selectProduct(state).toJS()).to.deep.equal(data);
    });
  });

  describe('selectProductId', () => {
    const id = { id: 1 };
    const state = initialState.setIn(['product', 'data', 'id'], fromJS(id));

    it('should select product id', () => {
      expect(selectProductId(state).toJS()).to.deep.equal(id);
    });
  });

  describe('selectCollections', () => {
    const id = 1;
    const items = [{ id: 1 }, { id: 2 }];
    const data = [{ id: 2 }];

    const state = initialState
      .setIn(['product', 'data', 'id'], fromJS(id))
      .setIn(['product', 'collections', 'items'], fromJS(items));

    it('should collections', () => {
      expect(selectCollections(state).toJS()).to.deep.equal(data);
    });
  });

  describe('selectActiveVariantId', () => {
    const id = 1;
    const state = initialState.setIn(['product', 'activeVariantId'], id);

    it('should select active variant id', () => {
      expect(selectActiveVariantId(state)).to.deep.equal(id);
    });
  });


  describe('selectActiveVariant', () => {
    const id = 1;
    const items = [{ id: 1 }, { id: 2 }];
    const data = { id: 1 };

    const state = initialState
      .setIn(['product', 'activeVariantId'], id)
      .setIn(['product', 'variants', 'items'], fromJS(items));

    it('should active variant', () => {
      expect(selectActiveVariant(state).toJS()).to.deep.equal(data);
    });
  });

  describe('selectVariants', () => {
    const id = 1;
    const items = [{ id: 1 }, { id: 2 }];
    const data = [{ id: 2 }];

    const state = initialState
      .setIn(['product', 'activeVariantId'], id)
      .setIn(['product', 'variants', 'items'], fromJS(items));

    it('should variants', () => {
      expect(selectVariants(state).toJS()).to.deep.equal(data);
    });
  });

  describe('selectMetafields', () => {
    const data = {};
    const state = initialState.setIn(['product', 'metafields'], data);

    it('should select metafields', () => {
      expect(selectMetafields(state)).to.deep.equal(data);
    });
  });

  describe('selectCustomNewsletterSuccess', () => {
    const isSuccess = true;
    const state = initialState.setIn(['product', 'customNewsletter', 'isSuccess'], isSuccess);

    it('should select custom newsletter success', () => {
      expect(selectCustomNewsletterSuccess(state)).to.deep.equal(isSuccess);
    });
  });

  describe('selectCustomNewsletterError', () => {
    const isError = true;
    const state = initialState.setIn(['product', 'customNewsletter', 'isError'], isError);

    it('should select custom newsletter failure', () => {
      expect(selectCustomNewsletterError(state)).to.deep.equal(isError);
    });
  });

  describe('selectCustomNewsletterFetching', () => {
    const isFetching = true;
    const state = initialState.setIn(['product', 'customNewsletter', 'isFetching'], isFetching);

    it('should select custom newsletter fetching', () => {
      expect(selectCustomNewsletterFetching(state)).to.deep.equal(isFetching);
    });
  });
});
