import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { fromJS, Map, Record } from 'immutable';
import { identity } from 'ramda';

import { ProductActions, ProductTypes } from '../product.redux';
import productSaga from '../product.sagas';
import api from '../../../services/api';

describe('Product: saga', () => {
  const ProductRecord = new Record({
    isFetching: false,
    data: Map(),
    variants: fromJS({
      items: Map(),
    }),
    collections: fromJS({
      items: Map(),
      title: '',
    }),
    activeVariantId: '',
    metafields: Map(),
    error: null,
    customNewsletter: fromJS({
      isFetching: false,
      isSuccess: false,
      isError: false,
      error: null,
    }),
  });

  const getSagaTester = ({ product = new ProductRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: fromJS({
        product,
      }),
      reducers: identity,
    });

    sagaTester.start(productSaga);
    return sagaTester;
  };

  describe('when request', () => {
    const response = {
      data: {
        data: {
          node: {
            title: 'title',
            id: 'MQ==',
            productType: 'typeValue',
            handle: 'handle',
            variants: {
              edges: [{
                node: {
                  title: 'variantTitle',
                  image: {
                    originalSrc: 'src',
                  },
                },
              }],
            },
            collections: {
              edges: [{
                node: {
                  title: 'collectionTitle',
                  products: {
                    edges: [{
                      node: {
                        title: 'collectionProductTitle',
                      },
                    }],
                  },
                },
              }],
            },
          },
        },
      },
    };

    it('should fetch product and dispatch successful action', async () => {
      const sagaTester = getSagaTester({});

      api.post.mockImplementation(() => Promise.resolve(response));

      sagaTester.dispatch(ProductActions.request(1, 123));

      const action = await sagaTester.waitFor(ProductTypes.SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should pass proper data to success', async () => {
      const data = {
        activeVariantId: 123,
        variants: [{ 'image': { 'originalSrc': 'src' }, 'title': 'variantTitle' }],
        collectionTitle: 'collectionTitle',
        collections: [{ 'title': 'collectionProductTitle' }],
        product: {
          handle: 'handle',
          title: 'title',
          id: '1',
          productType: 'typeValue',
        },
      };

      const sagaTester = getSagaTester({});

      api.post.mockImplementation(() => Promise.resolve(response));

      sagaTester.dispatch(ProductActions.request(1, 123));

      const action = await sagaTester.waitFor(ProductTypes.SUCCESS);
      expect(expect(action.data).to.deep.equal(data));
    });
  });

  it('should fetch product and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});

    api.post.mockImplementation(() => {
      throw new Error('Test error');
    });

    sagaTester.dispatch(ProductActions.request());

    const action = await sagaTester.waitFor(ProductTypes.FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch custom newsletter and dispatch successful action', async () => {
    const sagaTester = getSagaTester({
      product: new ProductRecord()
        .set('activeVariantId', '123'),
    });

    api.post.mockImplementation(() => Promise.resolve());

    sagaTester.dispatch(ProductActions.customNewsletterRequest(fromJS({ 'email_address': 'email' })));

    const action = await sagaTester.waitFor(ProductTypes.CUSTOM_NEWSLETTER_SUCCESS);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch custom newsletter and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});
    api.post.mockImplementation(() => {
      throw new Error('Test error');
    });

    sagaTester.dispatch(ProductActions.customNewsletterRequest());

    const action = await sagaTester.waitFor(ProductTypes.CUSTOM_NEWSLETTER_FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should success custom newsletter and dispatch reset action', async () => {
    const sagaTester = getSagaTester({
      product: new ProductRecord()
        .set('activeVariantId', '123'),
    });

    sagaTester.dispatch(ProductActions.customNewsletterSuccess());

    const action = await sagaTester.waitFor(ProductTypes.CUSTOM_NEWSLETTER_RESET);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch metafields and dispatch successful action', async () => {
    const sagaTester = getSagaTester({});

    api.get.mockImplementation(() => Promise.resolve(
      {
        data: [{
          'key': 'care',
          'value': 'Something',
        }],
      }
    ));

    sagaTester.dispatch(ProductActions.metafieldsRequest(1));

    const action = await sagaTester.waitFor(ProductTypes.METAFIELDS_SUCCESS);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch metafields and dispatch set active variant id action', async () => {
    const sagaTester = getSagaTester({});

    api.get.mockImplementation(() => Promise.resolve(
      {
        data: [{
          'key': 'care',
          'value': 'Something',
        }],
      }
    ));

    sagaTester.dispatch(ProductActions.metafieldsRequest(1));

    const action = await sagaTester.waitFor(ProductTypes.SET_ACTIVE_VARIANT_ID);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch metafields and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});
    api.get.mockImplementation(() => {
      throw new Error('Test error');
    });

    sagaTester.dispatch(ProductActions.metafieldsRequest(1));

    const action = await sagaTester.waitFor(ProductTypes.METAFIELDS_FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });
});
