import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, Map } from 'immutable';

export const { Types: ProductTypes, Creators: ProductActions } = createActions({
  request: ['id', 'variantId'],
  success: ['data'],
  failure: ['error'],
  metafieldsRequest: ['id'],
  metafieldsSuccess: ['data'],
  metafieldsFailure: ['error'],
  setActiveVariantId: ['id'],
  customNewsletterRequest: ['data'],
  customNewsletterSuccess: [''],
  customNewsletterFailure: ['error'],
  customNewsletterReset: [''],
  customNewsletterClearError: [],
}, { prefix: 'PRODUCT_' });

const ProductRecord = new Record({
  isFetching: false,
  data: Map(),
  variants: fromJS({
    items: {},
  }),
  collections: fromJS({
    items: {},
    title: '',
  }),
  activeVariantId: '',
  metafields: Map(),
  error: null,
  customNewsletter: fromJS({
    isFetching: false,
    isSuccess: false,
    isError: false,
    error: null,
  }),
});

export const INITIAL_STATE = new ProductRecord({});

const request = (state = INITIAL_STATE) => state
  .set('isFetching', true);

const success = (state = INITIAL_STATE, {
  data: {
    product,
    variants,
    collectionsTitle,
    collections,
    activeVariantId,
  },
}) => state
  .set('isFetching', false)
  .set('activeVariantId', fromJS(activeVariantId))
  .set('data', fromJS(product))
  .setIn(['variants', 'items'], fromJS(variants))
  .setIn(['collections', 'title'], fromJS(collectionsTitle))
  .setIn(['collections', 'items'], fromJS(collections));

const failure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));

const metafieldsRequest = (state = INITIAL_STATE) => state
  .set('isFetching', true);

const metafieldsSuccess = (state = INITIAL_STATE, { data }) => {
  return state
    .set('isFetching', false)
    .set('metafields', fromJS(data));
};

const metafieldsFailure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));

const setActiveVariantId = (state = INITIAL_STATE, { id }) => state
  .set('activeVariantId', fromJS(id));

const customNewsletterRequest = (state = INITIAL_STATE) => state
  .setIn(['customNewsletter', 'isFetching'], true)
  .setIn(['customNewsletter', 'isSuccess'], false)
  .setIn(['customNewsletter', 'isError'], false)
  .setIn(['customNewsletter', 'error'], null);

const customNewsletterFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['customNewsletter', 'isFetching'], false)
  .setIn(['customNewsletter', 'isSuccess'], false)
  .setIn(['customNewsletter', 'isError'], true)
  .setIn(['customNewsletter', 'error'], fromJS(error));

const customNewsletterSuccess = (state = INITIAL_STATE) => state
  .setIn(['customNewsletter', 'isFetching'], false)
  .setIn(['customNewsletter', 'isSuccess'], true)
  .setIn(['customNewsletter', 'isError'], false)
  .setIn(['customNewsletter', 'error'], null);

const customNewsletterReset = (state = INITIAL_STATE) => state
  .setIn(['customNewsletter', 'isFetching'], false)
  .setIn(['customNewsletter', 'isSuccess'], false)
  .setIn(['customNewsletter', 'isError'], false)
  .setIn(['customNewsletter', 'error'], null);

export const reducer = createReducer(INITIAL_STATE, {
  [ProductTypes.REQUEST]: request,
  [ProductTypes.SUCCESS]: success,
  [ProductTypes.FAILURE]: failure,
  [ProductTypes.METAFIELDS_REQUEST]: metafieldsRequest,
  [ProductTypes.METAFIELDS_SUCCESS]: metafieldsSuccess,
  [ProductTypes.METAFIELDS_FAILURE]: metafieldsFailure,
  [ProductTypes.SET_ACTIVE_VARIANT_ID]: setActiveVariantId,
  [ProductTypes.CUSTOM_NEWSLETTER_REQUEST]: customNewsletterRequest,
  [ProductTypes.CUSTOM_NEWSLETTER_SUCCESS]: customNewsletterSuccess,
  [ProductTypes.CUSTOM_NEWSLETTER_FAILURE]: customNewsletterFailure,
  [ProductTypes.CUSTOM_NEWSLETTER_RESET]: customNewsletterReset,
});
