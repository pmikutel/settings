import { createSelector } from 'reselect';


const selectStep2Domain = state => state.get('step2');

export const selectData = createSelector(
  selectStep2Domain,
  state => state.get('data'),
);
