import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, Map } from 'immutable';

export const { Types: Step2Types, Creators: Step2Actions } = createActions({
  updateData: ['data'],
  resetData: [],
}, { prefix: 'STEP_2_' });

export const Step2Record = new Record({
  data: Map(),
}, 'step2');

export const INITIAL_STATE = new Step2Record({});

const updateData = (state = INITIAL_STATE, { data = {} }) => state
  .set('data', fromJS(data));

const resetData = (state = INITIAL_STATE) => state
  .set('data', Map());

export const reducer = createReducer(INITIAL_STATE, {
  [Step2Types.UPDATE_DATA]: updateData,
  [Step2Types.RESET_DATA]: resetData,
});
