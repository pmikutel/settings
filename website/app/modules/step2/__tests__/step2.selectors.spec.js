import { expect } from 'chai';
import { fromJS, Map, Record } from 'immutable';

import { selectData } from '../step2.selectors';


describe('Step2: selectors', () => {
  const Step2Record = new Record({
    data: Map(),
  });

  const BlogRecord = new Record({
    articles: [],
  });

  const initialState = fromJS({
    step2: new Step2Record(),
    blog: new BlogRecord(),
  });

  describe('selectData', () => {
    const data = {
      prop: 'value',
    };

    const state = initialState.setIn(['step2', 'data'], data);

    it('should select data', () => {
      expect(selectData(state)).to.deep.equal(data);
    });
  });
});
