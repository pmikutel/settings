import { expect } from 'chai';
import { Map, Record, fromJS } from 'immutable';

import { reducer, Step2Actions } from '../step2.redux';

describe('Step2: redux', () => {
  const Step2Record = new Record({
    data: Map(),
  });

  const state = new Step2Record();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on UPDATE_DATA', () => {
      const data = {
        prop: 'value',
      };

      const expectedState = new Step2Record()
        .set('data', fromJS(data));

      expect(reducer(state, Step2Actions.updateData(data)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on RESET_DATA', () => {
      const data = {
        prop: 'value',
      };

      const previousState = new Step2Record()
        .set('data', data);

      const expectedState = new Step2Record()
        .set('data', Map());

      expect(reducer(previousState, Step2Actions.resetData()).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
