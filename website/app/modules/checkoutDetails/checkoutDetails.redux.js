import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, Map } from 'immutable';

export const { Types: CheckoutDetailsTypes, Creators: CheckoutDetailsActions } = createActions({
  updateData: ['data'],
  clearData: [''],
}, { prefix: 'CHECKOUT_DETAILS_' });

export const CheckoutDetailsRecord = new Record({
  data: Map(),
}, 'checkoutDetails');

export const INITIAL_STATE = new CheckoutDetailsRecord({});

const updateData = (state = INITIAL_STATE, { data = {} }) => state
  .set('data', fromJS(data));

const clearData = (state = INITIAL_STATE) => state
  .set('data', Map());

export const reducer = createReducer(INITIAL_STATE, {
  [CheckoutDetailsTypes.UPDATE_DATA]: updateData,
  [CheckoutDetailsTypes.CLEAR_DATA]: clearData,
});
