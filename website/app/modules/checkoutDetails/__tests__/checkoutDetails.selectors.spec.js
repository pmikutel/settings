import { expect } from 'chai';
import { fromJS, Map, Record } from 'immutable';

import { selectData } from '../checkoutDetails.selectors';


describe('CheckoutDetails: selectors', () => {
  const CheckoutDetailsRecord = new Record({
    data: Map(),
  });

  const initialState = fromJS({
    costsDetails: new CheckoutDetailsRecord(),
  });

  describe('selectData', () => {
    const data = {
      prop: 'value',
    };

    const state = initialState.setIn(['checkoutDetails', 'data'], data);

    it('should select data', () => {
      expect(selectData(state)).to.deep.equal(data);
    });
  });
});
