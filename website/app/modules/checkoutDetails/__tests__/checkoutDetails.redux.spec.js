import { expect } from 'chai';
import { Map, Record, fromJS } from 'immutable';

import { reducer, CheckoutDetailsActions } from '../checkoutDetails.redux';

describe('CheckoutDetails: redux', () => {
  describe('reducer', () => {
    it('should return initial state', () => {
      const CheckoutDetailsRecord = new Record({
        data: Map(),
      });

      const state = new CheckoutDetailsRecord();

      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on UPDATE_DATA', () => {
      const CheckoutDetailsRecord = new Record({
        data: Map(),
      });

      const state = new CheckoutDetailsRecord();
      const data = {
        prop: 'value',
      };

      const expectedState = new CheckoutDetailsRecord()
        .set('data', fromJS(data));

      expect(reducer(state, CheckoutDetailsActions.updateData(data)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should clear data on CLEAR_DATA', () => {
      const data = {
        prop: 'value',
      };
      const emptyData = {};
      const CheckoutDetailsRecord = new Record({
        data: data,
      });
      const state = new CheckoutDetailsRecord();

      const expectedState = new CheckoutDetailsRecord()
        .set('data', fromJS(emptyData));

      expect(reducer(state, CheckoutDetailsActions.clearData()).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
