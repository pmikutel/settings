import { createSelector } from 'reselect';


const selectCheckoutDetailsDomain = state => state.get('checkoutDetails');

export const selectData = createSelector(
  selectCheckoutDetailsDomain,
  state => state.get('data'),
);
