import { createActions, createReducer } from 'reduxsauce';
import { Record } from 'immutable';

export const { Types: ModalsTypes, Creators: ModalsActions } = createActions({
  openModal: ['modal', 'isPurchaseFlow', 'isProjectFlow'],
  closeModal: [''],
}, { prefix: 'MODALS_' });

const ModalsRecord = new Record({
  modal: '',
  isPurchaseFlow: false,
  isProjectFlow: false,
});

export const INITIAL_STATE = new ModalsRecord({});

export const openModal = (state = INITIAL_STATE, { modal, isPurchaseFlow, isProjectFlow }) => state
  .set('modal', modal)
  .set('isProjectFlow', isProjectFlow)
  .set('isPurchaseFlow', isPurchaseFlow);

export const closeModal = (state = INITIAL_STATE) => state
  .set('modal', '')
  .set('isProjectFlow', false)
  .set('isPurchaseFlow', false);

export const reducer = createReducer(INITIAL_STATE, {
  [ModalsTypes.OPEN_MODAL]: openModal,
  [ModalsTypes.CLOSE_MODAL]: closeModal,
});

