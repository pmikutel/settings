import { expect } from 'chai';
import { fromJS } from 'immutable';

import { selectModal, selectIsPurchaseFlow, selectIsProjectFlow } from '../modals.selectors';

describe('Modals: selectors', () => {
  const modal = '';
  const isPurchaseFlow = false;
  const isProjectFlow = false;

  const mockedState = fromJS({
    modals: {
      modal,
      isPurchaseFlow,
      isProjectFlow,
    },
  });

  describe('selectModal', () => {
    it('should select modal', () => {
      expect(selectModal(mockedState)).to.equal(modal);
    });
  });

  describe('selectIsPurchaseFlow', () => {
    it('should select is purchase flow', () => {
      expect(selectIsPurchaseFlow(mockedState)).to.equal(isPurchaseFlow);
    });
  });

  describe('selectIsProjectFlow', () => {
    it('should select is project flow', () => {
      expect(selectIsProjectFlow(mockedState)).to.equal(isPurchaseFlow);
    });
  });
});
