import { expect } from 'chai';
import { fromJS } from 'immutable';

import { reducer as modalsReducer, ModalsActions, ModalsTypes } from '../modals.redux';

describe('Modals: redux', () => {
  const state = fromJS({
    modal: '',
    isPurchaseFlow: false,
    isProjectFlow: false,
  });

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(modalsReducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should return state on unknown action', () => {
      expect(modalsReducer(state, { type: 'unknown-action' }).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on OPEN_MODAL', () => {
      const modal = 'en';
      const isPurchaseFlow = true;
      const isProjectFlow = true;
      const expectedState = state
        .set('modal', modal)
        .set('isPurchaseFlow', true)
        .set('isProjectFlow', true);
      const action = { modal, isPurchaseFlow, isProjectFlow, type: ModalsTypes.OPEN_MODAL };
      expect(modalsReducer(state, action).toJS()).to.deep.equal(expectedState.toJS());
    });
  });

  describe('openModal', () => {
    it('should return correct type', () => {
      expect(ModalsActions.openModal().type).to.equal(ModalsTypes.OPEN_MODAL);
    });

    it('should return proper payload', () => {
      const modal = 'login';
      expect(ModalsActions.openModal(modal).modal).to.deep.equal(modal);
    });
  });

  describe('closeModal', () => {
    it('should return correct type', () => {
      expect(ModalsActions.closeModal().type).to.equal(ModalsTypes.CLOSE_MODAL);
    });

    it('should set initial data', () => {
      expect(modalsReducer(state, ModalsActions.closeModal()).toJS())
        .to.deep.equal({ modal: '', isPurchaseFlow: false, isProjectFlow: false });
    });
  });
});
