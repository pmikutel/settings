import { createSelector } from 'reselect';

const selectModalsDomain = state => state.get('modals');

export const selectModal = createSelector(
  selectModalsDomain,
  state => state.get('modal'),
);

export const selectIsPurchaseFlow = createSelector(
  selectModalsDomain,
  state => state.get('isPurchaseFlow'),
);


export const selectIsProjectFlow = createSelector(
  selectModalsDomain,
  state => state.get('isProjectFlow'),
);
