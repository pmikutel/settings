import { pipe, toPairs, map, defaultTo, when, complement, isNil, filter, join } from 'ramda';

export const parseAttributesObjectToShopifyAttributesString = pipe(
  value => defaultTo({})(value),
  toPairs,
  map(([key, value]) => `{key:"${key}",value:"${value}"}`),
  value => `[${value}]`,
);

export const parseLineItemsArrayToShopifyLineItemsArray = pipe(
  value => defaultTo([])(value),
  map(({ variantId, quantity }) => `{variantId:"${variantId}",quantity:${quantity}}`),
  value => `[${value}]`,
);

export const parseStringToShopifyString = pipe(
  value => defaultTo('')(value),
  value => `"${value}"`,
);

export const parseObjectToShopifyObject = pipe(
  value => defaultTo({})(value),
  toPairs,
  map(([key, value]) => `${key}:"${value}"`),
  array => `{${array}}`
);

const executeWhenIsDefined = (data, fn) => when(complement(isNil), fn)(data);

export const parseCheckoutObjectToString = (object = {}) => {
  const customAttributes = executeWhenIsDefined(
    object.customAttributes,
    parseAttributesObjectToShopifyAttributesString,
  );
  const lineItems = executeWhenIsDefined(object.lineItems, parseLineItemsArrayToShopifyLineItemsArray);

  const note = executeWhenIsDefined(object.note, parseStringToShopifyString);
  const email = executeWhenIsDefined(object.email, parseStringToShopifyString);
  const shippingAddress = executeWhenIsDefined(object.shippingAddress, parseObjectToShopifyObject);
  const allowPartialAddresses = executeWhenIsDefined(object.allowPartialAddresses, defaultTo(false));

  const data = {
    allowPartialAddresses,
    customAttributes,
    lineItems,
    note,
    email,
    shippingAddress,
  };

  return pipe(
    filter(complement(isNil)),
    toPairs,
    map(([key, value]) => `${key}:${value}`),
    join(','),
    string => `{${string}}`,
  )(data);
};
