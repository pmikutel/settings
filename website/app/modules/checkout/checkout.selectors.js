import { createSelector } from 'reselect';
import { encode } from '../../utils/shopifyUrls';
import { selectActiveVariantId } from '../product/product.selectors';

const selectCheckoutDomain = state => state.get('checkout');

export const selectCheckoutId = createSelector(
  selectCheckoutDomain,
  state => state.get('currentCheckoutId'),
);

export const selectIsPurchaseFlow = createSelector(
  selectCheckoutDomain,
  state => state.get('isPurchaseFlow'),
);

export const selectCheckoutData = createSelector(
  selectCheckoutDomain,
  state => state.get('data'),
);

export const selectProjectCheckoutData = createSelector(
  selectCheckoutDomain,
  state => state.get('projectData'),
);

export const selectProjectCheckoutItems = createSelector(
  selectCheckoutDomain,
  state => state.get('projectItems'),
);

export const selectCustomAttributes = createSelector(
  selectProjectCheckoutData,
  state => state.get('customAttributes'),
);

export const selectUpdateAttributes = createSelector(
  selectCheckoutDomain,
  state => state.get('updateAttributes'),
);

export const selectUpdateAttributesFetching = createSelector(
  selectUpdateAttributes,
  state => state.get('isFetching'),
);

export const selectUpdateAttributesSuccess = createSelector(
  selectUpdateAttributes,
  state => state.get('isSuccess'),
);

export const selectProjectCheckoutId = createSelector(
  selectProjectCheckoutData,
  state => state.get('id'),
);

export const selectIsProductInProject = createSelector(
  selectProjectCheckoutItems, selectActiveVariantId,
  (items, id) => items.find(item => item.getIn(['variant', 'id']).includes(encode(id, 'variant'))),
);
