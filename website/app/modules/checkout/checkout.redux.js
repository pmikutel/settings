import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, Map, List } from 'immutable';

export const { Types: CheckoutTypes, Creators: CheckoutActions } = createActions({
  fetchRequest: ['checkoutId', 'isProject'],
  fetchSuccess: ['data'],
  fetchFailure: ['error'],
  fetchProjectSuccess: ['data', 'items'],
  createProject: ['data'],
  createRequest: ['data', 'isProject'],
  createSuccess: ['checkoutId'],
  createFailure: ['error'],
  customerAssociateRequest: ['checkoutId'],
  customerAssociateSuccess: ['checkoutId'],
  customerAssociateFailure: ['checkoutId'],
  customerEmailUpdateRequest: ['checkoutId', 'email'],
  customerAddressUpdateRequest: ['address', 'checkoutId'],
  customerAttributesUpdateRequest: ['address', 'checkoutId'],
  customerShippingLineUpdateRequest: ['checkoutId', 'line'],
  customerProjectLineItemAddRequest: ['item'],
  customerProjectLineItemRemoveRequest: ['id'],
  customerProjectLineItemRemoveSuccess: ['id'],
  customerDetailsUpdateSuccess: ['checkoutId', 'isProject'],
  customerDetailsUpdateFailure: ['checkoutId'],
  customerProjectUpdateSuccess: ['checkoutId'],
  updateAttributesRequest: ['attributes', 'redirect'],
  updateAttributesSuccess: ['customAttributes', 'redirect'],
  updateAttributesFailure: ['error'],
  updateAttributesReset: [''],
  setPurchaseFlow: ['isPurchaseFlow'],
}, { prefix: 'CHECKOUT_' });

export const CheckoutRecord = new Record({
  currentCheckoutId: null,
  data: Map(),
  projectData: Map(),
  projectItems: List(),
  updateAttributes: fromJS({
    isFetching: false,
    isSuccess: false,
    error: false,
  }),
  isFetching: false,
  isCreating: false,
  isUpdating: false,
  fetchError: null,
  createError: null,
  isPurchaseFlow: false,
}, 'checkout');

export const INITIAL_STATE = new CheckoutRecord({});

const fetchRequest = (state = INITIAL_STATE) => state
  .set('fetchError', null)
  .set('isFetching', true);

const fetchSuccess = (state = INITIAL_STATE, { data }) => state
  .set('currentCheckoutId', data.id)
  .set('isFetching', false)
  .set('data', fromJS(data));

const fetchFailure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', true)
  .set('fetchError', fromJS(error));

const createRequest = (state = INITIAL_STATE) => state
  .set('createError', null)
  .set('isCreating', true);

const createSuccess = (state = INITIAL_STATE) => state
  .set('isCreating', false);

const createFailure = (state = INITIAL_STATE, { error }) => state
  .set('isCreating', true)
  .set('createError', fromJS(error));

const fetchProjectSuccess = (state = INITIAL_STATE, { data, items }) => state
  .set('isFetching', false)
  .set('projectData', fromJS(data))
  .set('projectItems', fromJS(items));

const updateAttributesRequest = (state = INITIAL_STATE) => state
  .setIn(['updateAttributes', 'isFetching'], true);

const updateAttributesSuccess = (state = INITIAL_STATE, { customAttributes }) => state
  .setIn(['projectData', 'customAttributes'], fromJS(customAttributes))
  .setIn(['updateAttributes', 'isFetching'], false)
  .setIn(['updateAttributes', 'isSuccess'], true);

const updateAttributesReset = (state = INITIAL_STATE) => state
  .setIn(['updateAttributes', 'isSuccess'], false);

const updateAttributesFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['updateAttributes', 'isFetching'], false)
  .setIn(['updateAttributes', 'error'], fromJS(error));

const customerProjectLineItemRemoveSuccess = (state = INITIAL_STATE, { id }) => state
  .set('projectItems', state.get('projectItems').filter(item => item.get('id') !== id));

const setPurchaseFlow = (state = INITIAL_STATE, { isPurchaseFlow }) => state
  .set('isPurchaseFlow', isPurchaseFlow);

export const reducer = createReducer(INITIAL_STATE, {
  [CheckoutTypes.FETCH_REQUEST]: fetchRequest,
  [CheckoutTypes.FETCH_SUCCESS]: fetchSuccess,
  [CheckoutTypes.FETCH_FAILURE]: fetchFailure,
  [CheckoutTypes.FETCH_PROJECT_SUCCESS]: fetchProjectSuccess,
  [CheckoutTypes.CREATE_REQUEST]: createRequest,
  [CheckoutTypes.CREATE_SUCCESS]: createSuccess,
  [CheckoutTypes.CREATE_FAILURE]: createFailure,
  [CheckoutTypes.UPDATE_ATTRIBUTES_REQUEST]: updateAttributesRequest,
  [CheckoutTypes.UPDATE_ATTRIBUTES_SUCCESS]: updateAttributesSuccess,
  [CheckoutTypes.UPDATE_ATTRIBUTES_RESET]: updateAttributesReset,
  [CheckoutTypes.UPDATE_ATTRIBUTES_FAILURE]: updateAttributesFailure,
  [CheckoutTypes.CUSTOMER_PROJECT_LINE_ITEM_REMOVE_SUCCESS]: customerProjectLineItemRemoveSuccess,
  [CheckoutTypes.SET_PURCHASE_FLOW]: setPurchaseFlow,
});
