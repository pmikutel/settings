import { takeLatest, put, select, take } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import reportError from 'report-error';
import {
  pipe,
  path,
  isEmpty,
  either,
  complement,
  both,
  isNil,
  evolve,
  map,
  mergeAll,
  merge,
  defaultTo,
  prop,
  filter,
} from 'ramda';
import { push } from 'react-router-redux';
import { fromJS } from 'immutable';

import { CheckoutActions, CheckoutTypes } from './checkout.redux';
import { UserTypes, UserActions } from '../user/user.redux';
import {
  selectIsLoggedIn,
  selectSessionAccessToken,
  selectProfileData,
  selectProjectId,
  selectUserEmail,
} from '../user/user.selectors';
import { selectSelectedStyle, selectSelectedStyleName } from '../step1/step1.selectors';
import { selectData } from '../step2/step2.selectors';
import { selectCustomAttributes, selectIsPurchaseFlow, selectCheckoutData } from './checkout.selectors';
import { shopifyApi, hasErrors } from '../../services/shopify/api';
import { parseCheckoutObjectToString } from './checkout.helpers';
import { ModalsActions } from '../modals/modals.redux';
import { extractNode } from '../../utils/shopifyData';
import api from '../../services/api';

const hasCheckoutCreateErrors = pipe(
  path(['data', 'data', 'checkoutCreate', 'userErrors']),
  complement(isEmpty),
);

const hasNoData = pipe(
  path(['data', 'data']),
  isNil,
);

function* getPlanUrl({ data: attachment } = {}, oldData) {
  try {
    if (!attachment) {
      return oldData;
    }

    const userEmail = yield select(selectUserEmail);
    const newFloorPlan = yield api.post('/blogs/13635452972', {
      title: `Floor plan - ${userEmail}`,
      published: false,
      image: {
        attachment,
      },
    });

    return path(['data', 'image', 'src'])(newFloorPlan);
  } catch (e) {
    return reportError(e);
  }
}

export function* createProject({ data = false }) {
  try {
    const selectedStyle = yield select(selectSelectedStyle);
    const selectedStyleName = yield select(selectSelectedStyleName);
    const selectedData = yield select(selectData);
    const attributes = data || selectedData;

    if (selectedStyle && attributes.size > 0) {
      const { floorPlan, ...other } = attributes.toJS();
      const floorPlanData = yield getPlanUrl(floorPlan, '');

      yield put(CheckoutActions.createRequest({
        customAttributes: {
          selectedStyle,
          selectedStyleName,
          isProject: true,
          ...other,
          floorPlanName: pipe(prop('name'), defaultTo(''))(floorPlan),
          floorPlanData,
        },
      }, true));
    }
  } catch (e) {
    yield reportError(e);
  }
}

export function* createRequest({ data, isProject = false }) {
  try {
    const isLoggedIn = yield select(selectIsLoggedIn);

    if (!isLoggedIn) {
      yield put(CheckoutActions.setPurchaseFlow(true));
      yield put(ModalsActions.openModal('login', true));
      yield take(UserTypes.LOGIN_SUCCESS);
    }
    const isPurchaseFlow = yield select(selectIsPurchaseFlow);

    if (isPurchaseFlow) {
      yield put(CheckoutActions.setPurchaseFlow(false));
      return;
    }

    const input = parseCheckoutObjectToString(data);

    const response = yield shopifyApi.post('/', `mutation {
      checkoutCreate(input: ${input}) {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    if (either(hasErrors, hasCheckoutCreateErrors)(response)) {
      yield put(CheckoutActions.createFailure(response.data));
      return;
    }

    const id = path(['data', 'data', 'checkoutCreate', 'checkout', 'id'])(response);

    if (!isProject) {
      yield put(push('/purchase/costs'));
    } else {
      yield put(UserActions.metafieldsRequest(true));
    }

    yield put(CheckoutActions.createSuccess(id));
  } catch (e) {
    yield reportError(e);
  }
}

export function* updateAttributesRequest({ attributes, redirect = false }) {
  try {
    const { floorPlan, ...otherNew } = attributes.toJS();
    const checkoutId = yield select(selectProjectId);
    const reduxOldAttributes = yield select(selectCustomAttributes);
    const oldAttributes = reduxOldAttributes || fromJS({});
    const {
      selectedStyle: oldSelectSelectedStyle,
      selectedStyleName: oldSelectSelectedStyleName,
      floorPlanData,
      floorPlanName,
      ...otherOld
    } = oldAttributes.toJS();
    const reduxSelectedStyle = yield select(selectSelectedStyle);
    const reduxSelectedStyleName = yield select(selectSelectedStyleName);
    const selectedStyle = reduxSelectedStyle || oldSelectSelectedStyle;
    const selectedStyleName = reduxSelectedStyleName || oldSelectSelectedStyleName;
    const mergedOther = merge({ ...otherOld })({ ...otherNew });

    const floorPlanUrl = yield getPlanUrl(floorPlan, floorPlanData);

    const customAttributes = {
      ...mergedOther,
      selectedStyle,
      selectedStyleName,
      floorPlanName: pipe(prop('name'), defaultTo(floorPlanName))(floorPlan),
      floorPlanData: floorPlanUrl,
    };

    const input = parseCheckoutObjectToString({ customAttributes });

    const response = yield shopifyApi.post('/', `mutation {
      checkoutAttributesUpdate(checkoutId: "${checkoutId}", input: ${input}) {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    const userError = path(['data', 'checkoutAttributesUpdate', 'userErrors', 0, 'message'])(response.data);

    if (hasErrors(response) || userError) {
      yield put(CheckoutActions.updateAttributesFailure(response));
      return;
    }
    yield put(CheckoutActions.updateAttributesSuccess(customAttributes, redirect));
  } catch (e) {
    yield put(CheckoutActions.updateAttributesFailure(e));
    yield reportError(e);
  }
}

export function* updateAttributesSuccess({ redirect }) {
  try {
    const checkoutId = yield select(selectProjectId);
    if (redirect) {
      yield put(push(`/project/board/${checkoutId}`));
    }
    yield delay(3000);
    yield put(CheckoutActions.updateAttributesReset());
  } catch (e) {
    yield put(CheckoutActions.updateAttributesFailure(e));
  }
}

export function* createSuccess({ checkoutId }) {
  try {
    yield put(CheckoutActions.customerAssociateRequest(checkoutId));
  } catch (e) {
    yield reportError(e);
  }
}

export function* fetchRequest({ checkoutId, isProject = false }) {
  try {
    const response = yield shopifyApi.post('/', `{
      node(id: "${checkoutId}") {
        ... on Checkout {
          id,
          note,
          email,
          customAttributes {
            key,
            value
          },
          currencyCode,
          createdAt,
          completedAt,
          orderStatusUrl,
          paymentDue,
          ready,
          requiresShipping,
          webUrl,
          totalPrice,
          totalTax,
          availableShippingRates {
            ready,
            shippingRates {
              price,
              title,
              handle
            }
          },
          lineItems(first: 10) {
            edges {
              ... on CheckoutLineItemEdge {
                node {
                  ... on CheckoutLineItem {
                    id,
                    quantity,
                    title,
                    variant {
                      id,
                      image {
                        src
                      },
                      price,
                      title,
                      compareAtPrice,
                      selectedOptions {
                        name,
                        value
                      }
                      product {
                        description,
                        productType,
                        id,
                        handle,
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }`);

    if (both(hasErrors, hasNoData)(response)) {
      yield put(CheckoutActions.fetchFailure(response.data));
      return;
    }

    const data = pipe(
      path(['data', 'data', 'node']),
      evolve({
        customAttributes: pipe(
          map(({ key, value }) => ({ [key]: value })),
          mergeAll,
        ),
      }),
    )(response);

    if (isProject) {
      const projectItems = pipe(
        path(['lineItems', 'edges']),
        filter((item) => complement(isNil)(item.node.variant)),
      )(data);
      const projectItemsExtracted = projectItems ? extractNode(projectItems) : [];
      yield put(CheckoutActions.fetchProjectSuccess(data, projectItemsExtracted));
    } else {
      yield put(CheckoutActions.fetchSuccess(data, isProject));
    }
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerAssociateRequest({ checkoutId }) {
  try {
    const isLoggedIn = yield select(selectIsLoggedIn);

    if (!isLoggedIn) {
      yield put(CheckoutActions.customerAssociateFailure(checkoutId));
    }

    const accessToken = yield select(selectSessionAccessToken);
    const response = yield shopifyApi.post('/', `mutation {
      checkoutCustomerAssociate(checkoutId: "${checkoutId}", customerAccessToken: "${accessToken}") {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    const id = path(['data', 'data', 'checkoutCustomerAssociate', 'checkout', 'id'])(response);
    yield put(CheckoutActions.customerAssociateSuccess(id));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerAssociateSuccess({ checkoutId }) {
  try {
    yield put(CheckoutActions.fetchRequest(checkoutId));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerAssociateFailure({ checkoutId }) {
  try {
    yield put(CheckoutActions.fetchRequest(checkoutId));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerEmailUpdateRequest({ checkoutId, email }) {
  try {
    const response = yield shopifyApi.post('/', `mutation {
      checkoutEmailUpdate(checkoutId: "${checkoutId}", email: "${email}") {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    const id = path(['data', 'data', 'checkoutEmailUpdate', 'checkout', 'id'])(response);

    yield put(CheckoutActions.customerDetailsUpdateSuccess(id));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerDetailsUpdateSuccess({ checkoutId }) {
  try {
    yield put(CheckoutActions.fetchRequest(checkoutId));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerDetailsUpdateFailure({ checkoutId }) {
  try {
    yield put(CheckoutActions.fetchRequest(checkoutId));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerShippingLineUpdateRequest({ checkoutId, line }) {
  try {
    const response = yield shopifyApi.post('/', `mutation {
      checkoutShippingLineUpdate(checkoutId: "${checkoutId}", shippingRateHandle: "${line}") {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    const id = path(['data', 'data', 'checkoutEmailUpdate', 'checkout', 'id'])(response);

    yield put(CheckoutActions.customerDetailsUpdateSuccess(id));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerAddressUpdateRequest({ checkoutId, address }) {
  try {
    const userData = yield select(selectProfileData);
    const checkoutData = yield select(selectCheckoutData);
    const destination = '/purchase/payment';
    //Address2 is dedicated to get proper data for calculator
    const address2 = `
      ${address.get('floor')};
      ${address.get('lift')};
      ${address.get('when')};
      ${checkoutData.get('totalPrice')}
    `;

    const response = yield shopifyApi.post('/', `mutation {
      checkoutShippingAddressUpdate(shippingAddress: {
        country: "United Kingdom",
        firstName: "${userData.get('firstName')}",
        lastName: "${userData.get('lastName')}",
        city: "${address.get('postal_town')}",
        zip: "${address.get('postal_code')}",
        address1: "${address.get('number')} ${address.get('route')}",
        phone: "${address.get('phone')}",
        company: "${userData.get('lastName')}",
        address2: "${address2}"
      }, checkoutId: "${checkoutId}") {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    const id = path(['data', 'data', 'checkoutShippingAddressUpdate', 'checkout', 'id'])(response);
    yield put(CheckoutActions.customerDetailsUpdateSuccess(id));

    yield take(CheckoutTypes.FETCH_SUCCESS);

    if (location.pathname !== destination) {
      yield put(push(destination));
    }
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerAttributesUpdateRequest({ checkoutId, address }) {
  try {
    yield shopifyApi.post('/', `mutation {
      checkoutAttributesUpdate(checkoutId: "${checkoutId}", input: {customAttributes: [
        {
          key: "floor",
          value: "${address.get('floor')}"
        },
        {
          key: "lift?",
          value: "${address.get('lift')}"
        },
        {
          key: "delivery",
          value: "${address.get('when')}"
        }
      ]}) {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerProjectLineItemAddRequest({ item }) {
  try {
    const isLoggedIn = yield select(selectIsLoggedIn);

    if (!isLoggedIn) {
      yield put(ModalsActions.openModal('login'));
      yield take(UserTypes.METAFIELDS_SUCCESS);
    }

    const projectId = yield select(selectProjectId);

    const response = yield shopifyApi.post('/', `mutation {
      checkoutLineItemsAdd(lineItems: [{
        quantity: ${item.quantity},
        variantId: "${item.variantId}",
      }], checkoutId: "${projectId}") {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    const id = path(['data', 'data', 'checkoutLineItemsAdd', 'checkout', 'id'])(response);

    yield put(CheckoutActions.customerProjectUpdateSuccess(id));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerProjectLineItemRemoveRequest({ id }) {
  try {
    const projectId = yield select(selectProjectId);

    yield shopifyApi.post('/', `mutation {
      checkoutLineItemsRemove(
        checkoutId: "${projectId}",
        lineItemIds: "${id}",
        ) {
        userErrors {
          field
          message
        }
        checkout {
          id
        }
      }
    }`);

    yield put(CheckoutActions.customerProjectLineItemRemoveSuccess(id));
  } catch (e) {
    yield reportError(e);
  }
}

export function* customerProjectUpdateSuccess({ checkoutId }) {
  try {
    yield put(CheckoutActions.fetchRequest(checkoutId, true));
  } catch (e) {
    yield reportError(e);
  }
}

export default function* loginSaga() {
  yield takeLatest(CheckoutTypes.CREATE_PROJECT, createProject);
  yield takeLatest(CheckoutTypes.CREATE_REQUEST, createRequest);
  yield takeLatest(CheckoutTypes.CREATE_SUCCESS, createSuccess);
  yield takeLatest(CheckoutTypes.FETCH_REQUEST, fetchRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_ASSOCIATE_REQUEST, customerAssociateRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_ASSOCIATE_SUCCESS, customerAssociateSuccess);
  yield takeLatest(CheckoutTypes.CUSTOMER_ASSOCIATE_FAILURE, customerAssociateFailure);
  yield takeLatest(CheckoutTypes.CUSTOMER_EMAIL_UPDATE_REQUEST, customerEmailUpdateRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_ADDRESS_UPDATE_REQUEST, customerAddressUpdateRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_ATTRIBUTES_UPDATE_REQUEST, customerAttributesUpdateRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_SHIPPING_LINE_UPDATE_REQUEST, customerShippingLineUpdateRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_PROJECT_LINE_ITEM_ADD_REQUEST, customerProjectLineItemAddRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_PROJECT_LINE_ITEM_REMOVE_REQUEST, customerProjectLineItemRemoveRequest);
  yield takeLatest(CheckoutTypes.CUSTOMER_DETAILS_UPDATE_SUCCESS, customerDetailsUpdateSuccess);
  yield takeLatest(CheckoutTypes.CUSTOMER_DETAILS_UPDATE_FAILURE, customerDetailsUpdateFailure);
  yield takeLatest(CheckoutTypes.CUSTOMER_PROJECT_UPDATE_SUCCESS, customerProjectUpdateSuccess);
  yield takeLatest(CheckoutTypes.UPDATE_ATTRIBUTES_REQUEST, updateAttributesRequest);
  yield takeLatest(CheckoutTypes.UPDATE_ATTRIBUTES_SUCCESS, updateAttributesSuccess);
}
