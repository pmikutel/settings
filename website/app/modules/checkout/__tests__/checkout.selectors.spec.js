import { expect } from 'chai';
import { fromJS, List, Map, Record } from 'immutable';
import { encode } from '../../../utils/shopifyUrls';

import {
  selectCheckoutId,
  selectCheckoutData,
  selectProjectCheckoutData,
  selectCustomAttributes,
  selectProjectCheckoutId,
  selectUpdateAttributes,
  selectUpdateAttributesFetching,
  selectUpdateAttributesSuccess,
  selectProjectCheckoutItems,
  selectIsPurchaseFlow,
  selectIsProductInProject,
} from '../checkout.selectors';

describe('Checkout : selectors', () => {
  const CheckoutRecord = new Record({
    currentCheckoutId: null,
    data: Map(),
    projectData: Map(),
    projectItems: List(),
    updateAttributes: fromJS({
      isFetching: false,
      isSuccess: false,
      error: false,
    }),
    isFetching: false,
    isPurchaseFlow: false,
    isCreating: false,
    isUpdating: false,
    fetchError: null,
    createError: null,
  });

  const initialState = fromJS({
    checkout: new CheckoutRecord(),
  });

  describe('selectCheckoutData', () => {
    const items = {
      item1: {
        id: '1',
      },
      item2: {
        id: '2',
      },
    };

    const state = initialState.setIn(['checkout', 'data'], fromJS(items));

    it('should select products items', () => {
      expect(selectCheckoutData(state).toJS()).to.deep.equal(items);
    });
  });

  describe('selectCheckoutId', () => {
    const id = 1;

    const state = initialState.setIn(['checkout', 'currentCheckoutId'], id);

    it('should select checkout ID', () => {
      expect(selectCheckoutId(state)).to.deep.equal(id);
    });
  });

  describe('selectProjectCheckoutItems', () => {
    const item = fromJS({ item1: { id: '1' } });

    const state = initialState.setIn(['checkout', 'projectItems'], fromJS(item));

    it('should select checkout project items', () => {
      expect(selectProjectCheckoutItems(state)).to.deep.equal(item);
    });
  });

  describe('selectProjectCheckoutData', () => {
    const item = fromJS({ item1: { id: '1' } });

    const state = initialState.setIn(['checkout', 'projectData'], fromJS(item));

    it('should select checkout project items', () => {
      expect(selectProjectCheckoutData(state)).to.deep.equal(item);
    });
  });

  describe('selectCustomAttributes', () => {
    const item = fromJS({ item1: { id: '1' } });

    const state = initialState.setIn(['checkout', 'projectData', 'customAttributes'], fromJS(item));

    it('should select custom attributes', () => {
      expect(selectCustomAttributes(state)).to.deep.equal(item);
    });
  });

  describe('selectProjectCheckoutId', () => {
    const id = 1;

    const state = initialState.setIn(['checkout', 'projectData', 'id'], fromJS(id));

    it('should select project checkout id', () => {
      expect(selectProjectCheckoutId(state)).to.deep.equal(id);
    });
  });

  describe('selectUpdateAttributes', () => {
    const item = fromJS({ item1: { id: '1' } });

    const state = initialState.setIn(['checkout', 'updateAttributes'], fromJS(item));

    it('should select update attributes', () => {
      expect(selectUpdateAttributes(state)).to.deep.equal(item);
    });
  });

  describe('selectUpdateAttributesFetching', () => {
    const isFetching = true;

    const state = initialState.setIn(['checkout', 'updateAttributes', 'isFetching'], isFetching);

    it('should select update attributes fetching', () => {
      expect(selectUpdateAttributesFetching(state)).to.deep.equal(isFetching);
    });
  });

  describe('selectUpdateAttributesSuccess', () => {
    const isSuccess = true;

    const state = initialState.setIn(['checkout', 'updateAttributes', 'isSuccess'], isSuccess);

    it('should select update attributes success', () => {
      expect(selectUpdateAttributesSuccess(state)).to.deep.equal(isSuccess);
    });
  });

  describe('selectIsPurchaseFlow', () => {
    const isPurchaseFlow = true;

    const state = initialState.setIn(['checkout', 'isPurchaseFlow'], isPurchaseFlow);

    it('should select update attributes success', () => {
      expect(selectIsPurchaseFlow(state)).to.equal(isPurchaseFlow);
    });
  });

  describe('selectIsProductInProject', () => {
    const projectItems = [{ variant: { id: encode('1', 'variant') } }];

    const state = initialState
      .setIn(['checkout', 'projectItems'], fromJS(projectItems))
      .setIn(['product', 'activeVariantId'], 1);

    it('should select is product in project', () => {
      expect(selectIsProductInProject(state)).to.deep.equal(fromJS(projectItems[0]));
    });
  });
});
