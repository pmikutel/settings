import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { Record, fromJS, Map, List } from 'immutable';
import { identity } from 'ramda';
import { CALL_HISTORY_METHOD } from 'react-router-redux';

import { CheckoutActions, CheckoutTypes } from '../checkout.redux';
import checkoutSaga from '../checkout.sagas';
import { shopifyApi } from '../../../services/shopify/api';
import api from '../../../services/api';

describe('Checkout: saga', () => {
  const CheckoutRecord = new Record({
    currentCheckoutId: 1,
    data: Map(),
    projectItems: List(),
    projectData: fromJS({
      customAttributes: {
        something: 'something',
        floorPlanName: 'floorPlanName',
        selectedStyle: 'oldStyle',
        selectedStyleName: 'oldStyleName',
      },
      id: 1,
    }),
    updateAttributes: fromJS({
      isFetching: false,
      isSuccess: false,
      error: false,
    }),
    isFetching: false,
    isCreating: false,
    isUpdating: false,
    isPurchaseFlow: false,
    fetchError: null,
    createError: null,
  });

  const Step1Record = new Record({
    selectedStyle: null,
    selectedStyleName: null,
  });

  const Step2Record = new Record({
    data: Map(),
  });

  const UserRecord = new Record({
    session: fromJS({
      data: {
        accessToken: 'access-token',
      },
      isFetching: false,
      error: null,
    }),
    profile: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
  });

  const getSagaTester = ({
    checkout = new CheckoutRecord(),
    step1 = new Step1Record(),
    step2 = new Step2Record(),
    user = new UserRecord(),
  }) => {
    const sagaTester = new SagaTester({
      initialState: fromJS({
        checkout,
        step1,
        step2,
        user,
      }),
      reducers: identity,
    });

    sagaTester.start(checkoutSaga);
    return sagaTester;
  };

  describe('createProject', () => {
    it('should dispatch createRequest action when data is filled', async () => {
      const selectedStyle = 'style';
      const selectedStyleName = 'styleName';
      const step2Data = {
        floorPlan: {
          name: 'floor-plan-name',
          data: 'floor-plan-data',
        },
        prop: 'value',
      };

      const sagaTester = getSagaTester({
        step1: new Step1Record()
          .set('selectedStyle', selectedStyle)
          .set('selectedStyleName', selectedStyleName),
        step2: new Step2Record().set('data', fromJS(step2Data)),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({}));
      sagaTester.dispatch(CheckoutActions.createProject());

      const action = await sagaTester.waitFor(CheckoutTypes.CREATE_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch createRequest action with new data', async () => {
      const selectedStyle = 'style';
      const selectedStyleName = 'styleName';
      const step2Data = {};
      const data = {
        floorPlan: {
          name: 'floor-plan-name',
          data: 'floor-plan-data',
        },
        prop: 'value',
      };

      const receivedData = {
        customAttributes: {
          selectedStyle: 'style',
          selectedStyleName: 'styleName',
          floorPlanData: 'src.jpg',
          floorPlanName: 'floor-plan-name',
          isProject: true,
          prop: 'value',
        },
      };

      const sagaTester = getSagaTester({
        step1: new Step1Record()
          .set('selectedStyle', selectedStyle)
          .set('selectedStyleName', selectedStyleName),
        step2: new Step2Record().set('data', fromJS(step2Data)),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({}));
      api.post.mockReturnValue(Promise.resolve({ data: { image: { src: 'src.jpg' } } }));
      sagaTester.dispatch(CheckoutActions.createProject(fromJS(data)));

      const action = await sagaTester.waitFor(CheckoutTypes.CREATE_REQUEST);
      expect(expect(action.data).to.deep.equal(receivedData));
    });

    it('should not dispatch createRequest action when style is not selected', async () => {
      const step2Data = {
        floorPlan: {
          name: 'floor-plan-name',
          data: 'floor-plan-data',
        },
        prop: 'value',
      };

      const sagaTester = getSagaTester({
        step2: new Step2Record().set('data', fromJS(step2Data)),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({}));
      sagaTester.dispatch(CheckoutActions.createProject());

      expect(sagaTester.getCalledActions()).not.to.contains(CheckoutActions.createRequest());
    });
  });

  describe('createRequest', () => {
    it('should dispatch createSuccess action when request is successful', async () => {
      const data = {
        email: 'test@test.pl',
        customAttributes: {
          prop: 'value',
        },
      };

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({ data: { data: { checkoutCreate: { userErrors: [] } } } }));
      sagaTester.dispatch(CheckoutActions.createRequest(data));

      const action = await sagaTester.waitFor(CheckoutTypes.CREATE_SUCCESS);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch createFailure action when response has userErrors', async () => {
      const data = {
        email: 'test@test.pl',
        customAttributes: {
          prop: 'value',
        },
      };

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: { data: { checkoutCreate: { userErrors: ['error'] } } },
      }));

      sagaTester.dispatch(CheckoutActions.createRequest(data));

      const action = await sagaTester.waitFor(CheckoutTypes.CREATE_FAILURE);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch createFailure action when response has errors', async () => {
      const data = {
        email: 'test@test.pl',
        customAttributes: {
          prop: 'value',
        },
      };

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: { data: { checkoutCreate: { userErrors: [] } }, errors: ['error'] },
      }));

      sagaTester.dispatch(CheckoutActions.createRequest(data));

      const action = await sagaTester.waitFor(CheckoutTypes.CREATE_FAILURE);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch setPurchaseFlow action with true when is not project flow', async () => {
      const data = {
        email: 'test@test.pl',
        customAttributes: {
          prop: 'value',
        },
      };

      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], null),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: { data: { checkoutCreate: { userErrors: [] } }, errors: ['error'] },
      }));

      sagaTester.dispatch(CheckoutActions.createRequest(data, false));

      const action = await sagaTester.waitFor(CheckoutTypes.SET_PURCHASE_FLOW);
      expect(action.isPurchaseFlow).to.equal(true);
    });
  });

  describe('createSuccess', () => {
    it('should dispatch customerAssociateRequest action with checkoutId', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({});

      sagaTester.dispatch(CheckoutActions.createSuccess(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_ASSOCIATE_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('fetchRequest', () => {
    it('should dispatch fetchSuccess action when request is successful', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({ data: { data: {} } }));
      sagaTester.dispatch(CheckoutActions.fetchRequest(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.FETCH_SUCCESS);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch fetchProjectSuccess action when project request is successful', async () => {
      const checkoutId = 'checkout-id';
      const isProject = true;

      const sagaTester = getSagaTester({});
      const data = { data: { data: { node: { lineItems: { edges: [{ node: { variant: {} } }] } } } } };

      shopifyApi.post.mockReturnValue(Promise.resolve(data));
      sagaTester.dispatch(CheckoutActions.fetchRequest(checkoutId, isProject));

      const action = await sagaTester.waitFor(CheckoutTypes.FETCH_PROJECT_SUCCESS);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch fetchFailure action when response has no data and has errors', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          errors: ['error'],
        },
      }));

      sagaTester.dispatch(CheckoutActions.fetchRequest(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.FETCH_FAILURE);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerAssociateRequest', () => {
    it('should dispatch customerAssociateSuccess action when request is successful and user is logged in', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: { data: { checkoutCustomerAssociate: { checkout: { id: checkoutId } } } },
      }));
      sagaTester.dispatch(CheckoutActions.customerAssociateRequest(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_ASSOCIATE_SUCCESS);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch customerAssociateFailure action when user is not logged in', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({
        user: new UserRecord({
          session: fromJS({
            data: {},
            isFetching: false,
            error: null,
          }),
        }),
      });

      sagaTester.dispatch(CheckoutActions.customerAssociateRequest(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_ASSOCIATE_FAILURE);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerAssociateSuccess', () => {
    it('should dispatch fetchRequest', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({});

      sagaTester.dispatch(CheckoutActions.customerAssociateSuccess(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.FETCH_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerAssociateFailure', () => {
    it('should dispatch fetchRequest', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({});

      sagaTester.dispatch(CheckoutActions.customerAssociateFailure(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.FETCH_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerEmailUpdateRequest', () => {
    it('should dispatch customerAssociateSuccess action when request is successful', async () => {
      const checkoutId = 'checkout-id';
      const email = 'email@gmail.com';

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerEmailUpdateRequest(checkoutId, email));

      const action = await sagaTester.waitFor(CheckoutTypes.FETCH_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerEmailUpdateRequest', () => {
    it('should dispatch customerEmailUpdateRequest action with checkoutId and email', async () => {
      const checkoutId = 'checkout-id';
      const email = 'email@gmail.com';

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerEmailUpdateRequest(checkoutId, email));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_EMAIL_UPDATE_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerAddressUpdateRequest', () => {
    it('should dispatch customerAddressUpdateRequest action with checkoutId and address details', async () => {
      const checkoutId = 'checkout-id';
      const address = fromJS({
        address: 'postal_town',
        code: 'postal_code',
        route: 'route',
        phone: '123 321 123',
        company: 'company name',
        floor: '2',
        lift: 'yes',
        when: 'morning',
        totalPrice: '200',
      });

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerAddressUpdateRequest(address, checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_ADDRESS_UPDATE_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch push action to payment location', async () => {
      const checkoutId = 'checkout-id';
      const address = fromJS({
        address: 'postal_town',
        code: 'postal_code',
        route: 'route',
        phone: '123 321 123',
        company: 'company name',
      });

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerAddressUpdateRequest(address, checkoutId));

      const action = await sagaTester.waitFor(CALL_HISTORY_METHOD);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerShippingLineUpdateRequest', () => {
    it('should dispatch customerShippingLineUpdateRequest action with checkoutId and line', async () => {
      const checkoutId = 'checkout-id';
      const line = 'host';

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerShippingLineUpdateRequest(checkoutId, line));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_SHIPPING_LINE_UPDATE_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerAttributesUpdateRequest', () => {
    it('should dispatch customerAttributesUpdateRequest action with checkoutId and address', async () => {
      const checkoutId = 'checkout-id';
      const address = fromJS({
        address: 'postal_town',
        code: 'postal_code',
        route: 'route',
        phone: '123 321 123',
        company: 'company name',
        floor: '2',
        lift: 'yes',
        when: 'morning',
      });

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerAttributesUpdateRequest(address, checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_ATTRIBUTES_UPDATE_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerProjectLineItemAddRequest', () => {
    it('should dispatch customerProjectLineItemAddRequest action with checkoutId and item', async () => {
      const checkoutId = 'checkout-id';
      const item = [{ quantity: 1, variantId: '123' }];

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerProjectLineItemAddRequest(checkoutId, item));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_PROJECT_LINE_ITEM_ADD_REQUEST);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('customerProjectLineItemRemoveRequest', () => {
    it('should dispatch customerProjectLineItemRemoveSuccess action with checkoutId', async () => {
      const checkoutId = 'checkout-id';

      const sagaTester = getSagaTester({});
      sagaTester.dispatch(CheckoutActions.customerProjectLineItemRemoveRequest(checkoutId));

      const action = await sagaTester.waitFor(CheckoutTypes.CUSTOMER_PROJECT_LINE_ITEM_REMOVE_SUCCESS);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('updateAttributesRequest', () => {
    it('should dispatch updateAttributesSuccess', async () => {
      const attributes = fromJS({
        floorPlan: 'floorPlan',
      });

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {},
      }));

      sagaTester.dispatch(CheckoutActions.updateAttributesRequest(attributes));

      const action = await sagaTester.waitFor(CheckoutTypes.UPDATE_ATTRIBUTES_SUCCESS);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch updateAttributesSuccess with new attributes', async () => {
      const attributes = fromJS({
        floorPlan: { name: 'name', data: 'data' },
      });

      const selectedStyle = 'style';
      const selectedStyleName = 'styleName';

      const customAttributes = {
        something: 'something',
        floorPlanName: 'name',
        floorPlanData: 'src.jpg',
        selectedStyle: 'style',
        selectedStyleName: 'styleName',
      };

      const sagaTester = getSagaTester({
        step1: new Step1Record()
          .set('selectedStyle', selectedStyle)
          .set('selectedStyleName', selectedStyleName),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {},
      }));
      api.post.mockReturnValue(Promise.resolve({ data: { image: { src: 'src.jpg' } } }));

      sagaTester.dispatch(CheckoutActions.updateAttributesRequest(attributes));

      const action = await sagaTester.waitFor(CheckoutTypes.UPDATE_ATTRIBUTES_SUCCESS);
      expect(action.customAttributes).to.deep.equal(customAttributes);
    });

    it('should dispatch updateAttributesSuccess with new attributes and old selected style', async () => {
      const attributes = fromJS({
        floorPlan: { name: 'name', data: 'data' },
      });

      const customAttributes = {
        something: 'something',
        floorPlanName: 'name',
        floorPlanData: 'src.jpg',
        selectedStyle: 'oldStyle',
        selectedStyleName: 'oldStyleName',
      };

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {},
      }));
      api.post.mockReturnValue(Promise.resolve({ data: { image: { src: 'src.jpg' } } }));

      const sagaTester = getSagaTester({});

      sagaTester.dispatch(CheckoutActions.updateAttributesRequest(attributes));

      const action = await sagaTester.waitFor(CheckoutTypes.UPDATE_ATTRIBUTES_SUCCESS);
      expect(action.customAttributes).to.deep.equal(customAttributes);
    });

    it('should dispatch customerDetailsUpdateFailure action if user error exist', async () => {
      const attributes = fromJS({
        floorPlan: 'floorPlan',
      });

      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: { data: { checkoutAttributesUpdate: { userErrors: [{ message: 'message' }] } } },
      }));

      sagaTester.dispatch(CheckoutActions.updateAttributesRequest(attributes));

      const action = await sagaTester.waitFor(CheckoutTypes.UPDATE_ATTRIBUTES_FAILURE);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('updateAttributesSuccess', () => {
    it('should dispatch updateAttributesReset', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(CheckoutActions.updateAttributesSuccess());

      const action = await sagaTester.waitFor(CheckoutTypes.UPDATE_ATTRIBUTES_RESET);
      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });
});
