import { expect } from 'chai';

import {
  parseAttributesObjectToShopifyAttributesString,
  parseLineItemsArrayToShopifyLineItemsArray,
  parseStringToShopifyString,
  parseObjectToShopifyObject,
  parseCheckoutObjectToString,
} from '../checkout.helpers';

describe('Checkout: helpers', () => {
  describe('parseObjectToShopifyAttributes', () => {
    it('should return array string with key and value when data is defined', () => {
      const object = {
        'key-1': 'value-1',
        'key-2': 'value-2',
      };

      expect(parseAttributesObjectToShopifyAttributesString(object)).to.equal('[' +
        '{key:"key-1",value:"value-1"},' +
        '{key:"key-2",value:"value-2"}' +
        ']');
    });

    it('should return empty array string when data is not defined', () => {
      expect(parseAttributesObjectToShopifyAttributesString()).to.equal('[]');
    });
  });

  describe('parseLineItemsArrayToShopifyLineItemsArray', () => {
    it('should return array string when data is defined', () => {
      const array = [
        { variantId: 'variant-id-1', quantity: 2 },
        { variantId: 'variant-id-2', quantity: 3 },
      ];

      expect(parseLineItemsArrayToShopifyLineItemsArray(array)).to.equal('[' +
        '{variantId:"variant-id-1",quantity:2},' +
        '{variantId:"variant-id-2",quantity:3}' +
      ']');
    });

    it('should return empty array string when data is not defined', () => {
      expect(parseLineItemsArrayToShopifyLineItemsArray()).to.equal('[]');
    });
  });

  describe('parseStringToShopifyString', () => {
    it('should return string with quotes', () => {
      const string = 'string';

      expect(parseStringToShopifyString(string)).to.equal('"string"');
    });

    it('should return quotes when string is not defined', () => {
      expect(parseStringToShopifyString()).to.equal('""');
    });
  });

  describe('parseObjectToShopifyObject', () => {
    it('should return object string when data is defined', () => {
      const object = {
        'key-1': 'value-1',
        'key-2': 'value-2',
      };

      expect(parseObjectToShopifyObject(object)).to.equal('{' +
        'key-1:"value-1",' +
        'key-2:"value-2"' +
        '}');
    });

    it('should return empty object string when data is not defined', () => {
      expect(parseObjectToShopifyObject()).to.equal('{}');
    });
  });

  describe('parseCheckoutObjectToString', () => {
    it('should return parsed object without values, when data object is empty', () => {
      const object = {};

      expect(parseCheckoutObjectToString(object)).to.equal('{}');
    });

    it('should return parsed object with values, when data object is not empty', () => {
      const object = {
        allowPartialAddresses: true,
        customAttributes: { 'key-1': 'value-1', 'key-2': 'value-2' },
        lineItems: [
          { variantId: 'variant-id-1', quantity: 2 },
          { variantId: 'variant-id-2', quantity: 3 },
        ],
        note: 'note',
        email: 'email',
        shippingAddress: {
          'field-1': 'value-1',
          'field-2': 'value-2',
        },
      };

      expect(parseCheckoutObjectToString(object)).to.equal('{' +
        'allowPartialAddresses:true,' +
        'customAttributes:[' +
          '{key:"key-1",value:"value-1"},' +
          '{key:"key-2",value:"value-2"}' +
        '],' +
        'lineItems:[' +
          '{variantId:"variant-id-1",quantity:2},' +
          '{variantId:"variant-id-2",quantity:3}' +
        '],' +
        'note:"note",' +
        'email:"email",' +
        'shippingAddress:{' +
          'field-1:"value-1",' +
          'field-2:"value-2"' +
        '}' +
      '}');
    });
  });
});
