import { expect } from 'chai';
import { Record, fromJS, Map, List } from 'immutable';

import { reducer, CheckoutActions } from '../checkout.redux';

describe('Checkout: redux', () => {
  const CheckoutRecord = new Record({
    currentCheckoutId: null,
    data: Map(),
    projectData: Map(),
    projectItems: List(),
    updateAttributes: fromJS({
      isFetching: false,
      isSuccess: false,
      error: false,
    }),
    isFetching: false,
    isCreating: false,
    isUpdating: false,
    isPurchaseFlow: false,
    fetchError: null,
    createError: null,
  });

  const state = new CheckoutRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on FETCH_REQUEST', () => {
      const expectedState = new CheckoutRecord()
        .set('fetchError', null)
        .set('isFetching', true);

      expect(reducer(state, CheckoutActions.fetchRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FETCH_SUCCESS', () => {
      const data = { prop: 'value' };

      const expectedState = new CheckoutRecord()
        .set('currentCheckoutId', data.id)
        .set('isFetching', false)
        .set('data', fromJS(data));

      expect(reducer(state, CheckoutActions.fetchSuccess(data)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FETCH_PROJECT_SUCCESS', () => {
      const data = { prop: 'value' };
      const items = [{ prop: 'value' }];

      const expectedState = new CheckoutRecord()
        .set('isFetching', false)
        .set('projectData', fromJS(data))
        .set('projectItems', fromJS(items));

      expect(reducer(state, CheckoutActions.fetchProjectSuccess(data, items)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FETCH_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new CheckoutRecord()
        .set('isFetching', true)
        .set('fetchError', fromJS(error));

      expect(reducer(state, CheckoutActions.fetchFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on CREATE_REQUEST', () => {
      const expectedState = new CheckoutRecord()
        .set('createError', null)
        .set('isCreating', true);

      expect(reducer(state, CheckoutActions.createRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on CREATE_SUCCESS', () => {
      const data = { prop: 'value' };

      const expectedState = new CheckoutRecord()
        .set('isCreating', false);

      expect(reducer(state, CheckoutActions.createSuccess(data)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on CREATE_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new CheckoutRecord()
        .set('isCreating', true)
        .set('createError', fromJS(error));

      expect(reducer(state, CheckoutActions.createFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_ATTRIBUTES_REQUEST', () => {
      const expectedState = new CheckoutRecord()
        .setIn(['updateAttributes', 'isFetching'], true);

      expect(reducer(state, CheckoutActions.updateAttributesRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_ATTRIBUTES_SUCCESS', () => {
      const customAttributes = { prop: 'value' };

      const expectedState = new CheckoutRecord()
        .setIn(['projectData', 'customAttributes'], fromJS(customAttributes))
        .setIn(['updateAttributes', 'isFetching'], false)
        .setIn(['updateAttributes', 'isSuccess'], true);

      expect(reducer(state, CheckoutActions.updateAttributesSuccess(customAttributes)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_ATTRIBUTES_RESET', () => {
      const expectedState = new CheckoutRecord()
        .setIn(['updateAttributes', 'isSuccess'], false);

      expect(reducer(state, CheckoutActions.updateAttributesReset()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_ATTRIBUTES_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new CheckoutRecord()
        .setIn(['updateAttributes', 'isFetching'], false)
        .setIn(['updateAttributes', 'error'], fromJS(error));

      expect(reducer(state, CheckoutActions.updateAttributesFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should remove item on CUSTOMER_PROJECT_LINE_ITEM_REMOVE_SUCCESS', () => {
      const initialState = state.merge({
        projectItems: fromJS([{ id: 1 }, { id: 2 }]),
      });

      const id = 1;
      const expectedItems = fromJS([{ id: 2 }]);

      const expectedState = new CheckoutRecord()
        .set('projectItems', expectedItems);

      expect(reducer(initialState, CheckoutActions.customerProjectLineItemRemoveSuccess(id)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SET_PURCHASE_FLOW', () => {
      const expectedState = new CheckoutRecord()
        .set('isPurchaseFlow', true);

      expect(reducer(state, CheckoutActions.setPurchaseFlow(true)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
