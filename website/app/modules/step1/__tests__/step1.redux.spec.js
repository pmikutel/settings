import { expect } from 'chai';
import { Record } from 'immutable';

import { reducer, Step1Actions } from '../step1.redux';

describe('Step1: redux', () => {
  const Step1Record = new Record({
    selectedStyle: null,
    selectedStyleName: '',
  });

  const state = new Step1Record();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on CHANGE_STYLE', () => {
      const style = 'test-style';
      const styleName = 'test-style-name';

      const expectedState = new Step1Record()
        .set('selectedStyle', style)
        .set('selectedStyleName', styleName);

      expect(reducer(state, Step1Actions.changeStyle(style, styleName)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on RESET_STYLE', () => {
      const style = 'test-style';
      const styleName = 'test-style-name';

      const previousState = new Step1Record()
        .set('selectedStyle', style)
        .set('selectedStyleName', styleName);

      const expectedState = new Step1Record()
        .set('selectedStyle', null)
        .set('selectedStyleName', '');

      expect(reducer(previousState, Step1Actions.resetStyle()).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
