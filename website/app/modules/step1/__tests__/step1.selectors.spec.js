import { expect } from 'chai';
import { fromJS, Record } from 'immutable';

import { selectSelectedStyle, selectSelectedStyleName } from '../step1.selectors';

describe('Step1: selectors', () => {
  const Step1Record = new Record({
    selectedStyle: null,
    selectedStyleName: '',
  });

  const initialState = fromJS({
    step1: new Step1Record(),
  });

  describe('selectSelectedStyle', () => {
    const style = 'test-style';

    const state = initialState.setIn(['step1', 'selectedStyle'], style);

    it('should select selected style', () => {
      expect(selectSelectedStyle(state)).to.equal(style);
    });
  });

  describe('selectSelectedStyleName', () => {
    const name = 'test-style-name';

    const state = initialState.setIn(['step1', 'selectedStyleName'], name);

    it('should select selected style name', () => {
      expect(selectSelectedStyleName(state)).to.equal(name);
    });
  });
});
