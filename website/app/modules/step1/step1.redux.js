import { createActions, createReducer } from 'reduxsauce';
import { Record } from 'immutable';

export const { Types: Step1Types, Creators: Step1Actions } = createActions({
  changeStyle: ['style', 'name'],
  resetStyle: [],
}, { prefix: 'STEP_1_' });

export const Step1Record = new Record({
  selectedStyle: null,
  selectedStyleName: '',
}, 'step1');

export const INITIAL_STATE = new Step1Record({});

const changeStyle = (state = INITIAL_STATE, { style, name }) => state
  .set('selectedStyle', style)
  .set('selectedStyleName', name);

const resetStyle = (state = INITIAL_STATE) => state
  .set('selectedStyle', null)
  .set('selectedStyleName', '');

export const reducer = createReducer(INITIAL_STATE, {
  [Step1Types.CHANGE_STYLE]: changeStyle,
  [Step1Types.RESET_STYLE]: resetStyle,
});
