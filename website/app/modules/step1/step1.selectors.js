import { createSelector } from 'reselect';

const selectStep1Domain = state => state.get('step1');


export const selectSelectedStyle = createSelector(
  selectStep1Domain,
  state => state.get('selectedStyle'),
);

export const selectSelectedStyleName = createSelector(
  selectStep1Domain,
  state => state.get('selectedStyleName'),
);
