import { takeLatest, put } from 'redux-saga/effects';
import reportError from 'report-error';

import {
  path, prop, pipe, ifElse, always, isEmpty, pathSatisfies, is, merge, last, equals,
} from 'ramda';

import { ProductsTypes, ProductsActions } from './products.redux';
import { shopifyApi, hasErrors } from '../../services/shopify/api';
import { normalizeProducts } from '../../services/shopify/serializers';
import {
  extractNode,
  flattenVariants,
  flattenImages,
  flattenSelectedOptions,
  extractDeepOptions,
} from '../../utils/shopifyData';

export function* request({ productType = '', collection = '', lastItemId = '', clear = true, tag = '', limit = 6 }) {
  if (equals(clear, true)) {
    yield put(ProductsActions.clearData());
  }

  try {
    const productTypeQuery = productType ? `, query:"product_type:${productType}"` : '';
    const tagQuery = tag ? `, query:"tag:${tag}"` : '';
    const pagination = ifElse(
      isEmpty,
      always(''),
      always(`, after: "${lastItemId}"`))(lastItemId);

    const productDetailsQuery = `
      products (first:${limit}${tagQuery}${productTypeQuery}${pagination}) {
        pageInfo{
          hasNextPage
        },
        edges {
          cursor,
          node {
            id,
            title,
            description,
            descriptionHtml,
            vendor,
            productType,
            handle,
            images (first: 1) {
              edges{
                node{
                  id,
                  src,
                }
              }
            },
            variants(first: 1) {
              edges {
                node {
                  id,
                  title,
                  available,
                  compareAtPrice,
                  weight,
                  price,
                  selectedOptions {
                    name
                    value
                  },
                  image {
                    id,
                    src,
                  },
                }
              }
            },
            options {
              id,
              name,
              values
            }
          }
        }
      }
    `;

    const collectionFilter = `
      collections(first: 1, query: "${collection}") {
      edges {
        node {
          id,
          title,
          image {
            originalSrc
          }
          ${productDetailsQuery}
          }
        }
      }
    `;

    const mainQuery = ifElse(isEmpty, always(productDetailsQuery), always(collectionFilter))(collection);

    const finalQuery = `{
      shop {
        ${mainQuery}
      }
    }`;

    const response = yield shopifyApi.post('/', finalQuery);
    if (hasErrors(response)) {
      yield put(ProductsActions.failure(response));
      return;
    }

    const pathToRegularProducts = ['data', 'data', 'shop', 'products'];
    const pathToCollectionProducts = ['data', 'data', 'shop', 'collections', 'edges', 0, 'node', 'products'];
    const getPath = ifElse(
      pathSatisfies(is(Object), pathToRegularProducts),
      always(pathToRegularProducts),
      always(pathToCollectionProducts))(response);
    const getProducts = pipe(path(getPath), prop('edges'));

    const parseProducts = pipe(
      getProducts,
      extractNode,
      flattenVariants,
      flattenImages,
      flattenSelectedOptions,
      extractDeepOptions
    );
    const parsedProducts = parseProducts(response);
    const normalizedProducts = normalizeProducts(parsedProducts);

    const lastProductCursor = pipe(getProducts, last, prop('cursor'))(response);
    const hasNextPage = pipe(path(getPath), path(['pageInfo', 'hasNextPage']))(response);
    const normalizedProductsWithMeta = merge(normalizedProducts, { lastProductCursor, hasNextPage });

    yield put(ProductsActions.success(normalizedProductsWithMeta));
  } catch (e) {
    yield put(ProductsActions.failure(e));
    yield reportError(e);
  }
}


export default function* productsSaga() {
  yield takeLatest(ProductsTypes.REQUEST, request);
}
