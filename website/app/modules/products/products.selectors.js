import { createSelector } from 'reselect';

const selectProductsDomain = state => state.get('products');

export const selectProductsKeys = createSelector(
  selectProductsDomain,
  state => state.get('keys'),
);

export const selectProductsItems = createSelector(
  selectProductsDomain,
  state => state.get('items'),
);

export const selectLastProductCursor = createSelector(
  selectProductsDomain,
  state => state.get('lastProductCursor'),
);

export const selectHasNextPage = createSelector(
  selectProductsDomain,
  state => state.get('hasNextPage'),
);
