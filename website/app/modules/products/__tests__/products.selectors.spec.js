import { expect } from 'chai';
import { fromJS, List, Map, Record } from 'immutable';

import {
  selectProductsItems,
  selectProductsKeys,
  selectLastProductCursor,
  selectHasNextPage,
} from '../products.selectors';

describe('Product: selectors', () => {
  const ProductsRecord = new Record({
    isFetching: false,
    items: Map(),
    keys: List(),
    error: null,
  });

  const initialState = fromJS({
    product: new ProductsRecord(),
  });

  describe('selectProductsItems', () => {
    const items = {
      item1: { id: 'item1' },
      item2: { id: 'item2' },
    };

    const state = initialState.setIn(['products', 'items'], fromJS(items));

    it('should select products items', () => {
      expect(selectProductsItems(state).toJS()).to.deep.equal(items);
    });
  });

  describe('selectProductsKeys', () => {
    const keys = {
      item1: { id: 'item1' },
      item2: { id: 'item2' },
    };

    const state = initialState.setIn(['products', 'keys'], fromJS(keys));

    it('should select products items', () => {
      expect(selectProductsKeys(state).toJS()).to.deep.equal(keys);
    });
  });

  describe('selectLastProductCursor', () => {
    const state = initialState.setIn(['products', 'lastProductCursor'], 'testCursor');

    it('should select products last cursor', () => {
      expect(selectLastProductCursor(state)).to.deep.equal('testCursor');
    });
  });

  describe('selectHasNextPage', () => {
    const state = initialState.setIn(['products', 'hasNextPage'], true);

    it('should select hasNextPage from products', () => {
      expect(selectHasNextPage(state)).to.deep.equal(true);
    });
  });
});
