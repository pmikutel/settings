import { expect } from 'chai';
import { Record, fromJS, Map, List } from 'immutable';

import { reducer, ProductsActions } from '../products.redux';

describe('Product: redux', () => {
  const ProductsRecord = new Record({
    isFetching: false,
    items: Map(),
    keys: List(),
    lastProductCursor: '',
    hasNextPage: false,
    error: null,
  });

  const state = new ProductsRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on REQUEST', () => {
      const expectedState = new ProductsRecord().set('isFetching', true);

      expect(reducer(state, ProductsActions.request()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SUCCESS from first request', () => {
      const entities = { 0: { id: 'entity1' } };
      const result = [{ id: 'result1' }];
      const lastProductCursor = 'testCursor';
      const hasNextPage = false;

      const expectedState = new ProductsRecord()
        .set('isFetching', false)
        .set('items', fromJS(entities))
        .set('keys', fromJS(result))
        .set('lastProductCursor', lastProductCursor)
        .set('hasNextPage', hasNextPage);

      expect(reducer(state, ProductsActions.success({ entities, result, lastProductCursor, hasNextPage })).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SUCCESS from second request', () => {
      const entitiesFromFirstRequest = { 0: { id: 'entity1' } };
      const entitiesFromSecondRequest = { 1: { id: 'entity2' } };
      const expectedEntities = { 0: { id: 'entity1' }, 1: { id: 'entity2' } };
      const resultFromFirstRequest = [{ id: 'result1' }];
      const resultFromSecondRequest = [{ id: 'result2' }];
      const expectedResult = [{ id: 'result1' }, { id: 'result2' }];
      const lastProductCursorFromFirstRequest = 'testCursor1';
      const lastProductCursorFromSecondRequest = 'testCursor2';
      const hasNextPageFromFirstRequest = true;
      const hasNextPageFromSecondRequest = false;

      const previousState = new ProductsRecord()
        .set('isFetching', false)
        .set('items', fromJS(entitiesFromFirstRequest))
        .set('keys', fromJS(resultFromFirstRequest))
        .set('lastProductCursor', lastProductCursorFromFirstRequest)
        .set('hasNextPage', hasNextPageFromFirstRequest);

      const expectedState = new ProductsRecord()
        .set('isFetching', false)
        .set('items', fromJS(expectedEntities))
        .set('keys', fromJS(expectedResult))
        .set('lastProductCursor', lastProductCursorFromSecondRequest)
        .set('hasNextPage', hasNextPageFromSecondRequest);

      expect(reducer(previousState, ProductsActions.success({
        entities: entitiesFromSecondRequest,
        result: resultFromSecondRequest,
        lastProductCursor: lastProductCursorFromSecondRequest,
        hasNextPage: hasNextPageFromSecondRequest,
      })).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new ProductsRecord()
        .set('isFetching', false)
        .set('error', fromJS(error));

      expect(reducer(state, ProductsActions.failure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should clear data on CLEAR_DATA', () => {
      const previousState = new ProductsRecord()
        .set('isFetching', false)
        .set('items', fromJS({ data: 'dummyItems' }))
        .set('keys', fromJS(['dummyId']))
        .set('lastProductCursor', 'dummy cursor')
        .set('hasNextPage', true);

      const expectedState = new ProductsRecord()
        .set('isFetching', false)
        .set('items', fromJS({}))
        .set('keys', fromJS([]))
        .set('lastProductCursor', '')
        .set('hasNextPage', false);

      expect(reducer(previousState, ProductsActions.clearData()).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
