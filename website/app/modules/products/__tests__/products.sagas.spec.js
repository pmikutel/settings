import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { List, Map, Record } from 'immutable';

import { ProductsActions, ProductsTypes } from '../products.redux';
import productsSaga from '../products.sagas';
import { shopifyApi } from '../../../services/shopify/api';

describe('Products: saga', () => {
  const ProductsRecord = new Record({
    isFetching: false,
    items: Map(),
    keys: List(),
    lastProductCursor: '',
    hasNextPage: false,
    error: null,
  });

  const getSagaTester = ({ product = new ProductsRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: {
        product,
      },
    });

    sagaTester.start(productsSaga);
    return sagaTester;
  };

  const variantsData = {
    edges: [
      {
        node: {
          id: 'variantId',
          images: { id: 'imageId' },
          selectedOptions: [
            { name: 'optionName', value: 'optionValue' },
            { name: 'Options', value: 'sizeValue;deliveryValue' },
          ],
        },
      },
    ],
  };

  const imageData = {
    edges: [
      {
        node: { id: 'imageId' },
      },
    ],
  };

  const productsData = {
    data: {
      data: {
        shop: {
          products: {
            edges: [
              {
                cursor: 'cursor',
                node: {
                  id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzI4ODc4NTQ5ODE1Ng==',
                  images: imageData,
                  variants: variantsData,
                },
              },
            ],
            pageInfo: {
              hasNextPage: false,
            },
          },
        },
      },
    },
  };

  it('should fetch products and dispatch successful action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockImplementation(() => Promise.resolve(productsData));

    sagaTester.dispatch(ProductsActions.request());

    const action = await sagaTester.waitFor(ProductsTypes.SUCCESS);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should pass properly parsed products data to success', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockImplementation(() => Promise.resolve(productsData));

    sagaTester.dispatch(ProductsActions.request());

    const action = await sagaTester.waitFor(ProductsTypes.SUCCESS);

    expect(expect(action.data).to.deep.equal({
      entities: {
        image: {
          imageId: {
            id: 'imageId',
          },
        },
        products: {
          288785498156: {
            id: '288785498156',
            images: ['imageId'],
            variants: [
              {
                optionname: 'optionValue',
                size: 'sizeValue',
                delivery: 'deliveryValue',
                id: 'variantId',
                images: {
                  id: 'imageId',
                },
                options: 'sizeValue;deliveryValue',
                selectedOptions: [
                  { name: 'optionName', value: 'optionValue' },
                  { name: 'Options', value: 'sizeValue;deliveryValue' },
                ],
              },
            ],
          },
        },
      },
      result: ['288785498156'],
      hasNextPage: false,
      lastProductCursor: 'cursor',
    }));
  });

  it('should fetch products and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockImplementationOnce(() => {
      throw new Error('Test error');
    });

    sagaTester.dispatch(ProductsActions.request());

    const action = await sagaTester.waitFor(ProductsTypes.FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });
});
