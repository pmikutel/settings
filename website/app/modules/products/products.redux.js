import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, List, Map } from 'immutable';
import { mergeDeepRight, insertAll } from 'ramda';

export const { Types: ProductsTypes, Creators: ProductsActions } = createActions({
  request: ['productType', 'collection', 'lastItemId', 'clear', 'tag', 'limit'],
  success: ['data'],
  failure: ['error'],
  clearData: [''],
}, { prefix: 'PRODUCTS_' });

const ProductsRecord = new Record({
  isFetching: false,
  items: Map(),
  keys: List(),
  lastProductCursor: '',
  hasNextPage: false,
  error: null,
});

export const INITIAL_STATE = new ProductsRecord({});

const request = (state = INITIAL_STATE) => state
  .set('isFetching', true);

const success = (state = INITIAL_STATE, { data: { entities, result, lastProductCursor, hasNextPage } }) => {
  const items = state.get('items');
  const newEntities = mergeDeepRight(items.toJS(), entities);
  const keys = state.get('keys');
  const newResult = insertAll(0, keys.toJS(), result);

  return state
    .set('isFetching', false)
    .set('items', fromJS(newEntities))
    .set('keys', fromJS(newResult))
    .set('lastProductCursor', lastProductCursor)
    .set('hasNextPage', hasNextPage);
};

const failure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));

const clearData = (state = INITIAL_STATE) => state
  .set('items', fromJS({}))
  .set('keys', fromJS([]))
  .set('lastProductCursor', '')
  .set('hasNextPage', false);

export const reducer = createReducer(INITIAL_STATE, {
  [ProductsTypes.REQUEST]: request,
  [ProductsTypes.SUCCESS]: success,
  [ProductsTypes.FAILURE]: failure,
  [ProductsTypes.CLEAR_DATA]: clearData,
});
