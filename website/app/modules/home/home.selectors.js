import { createSelector } from 'reselect';

const selectHomeDomain = state => state.get('home');

export const selectNewsletterSuccess = createSelector(
  selectHomeDomain,
  state => state.getIn(['newsletter', 'isSuccess']),
);

export const selectNewsletterError = createSelector(
  selectHomeDomain,
  state => state.getIn(['newsletter', 'isError']),
);


export const selectNewsletterFetching = createSelector(
  selectHomeDomain,
  state => state.getIn(['newsletter', 'isFetching']),
);
