import { takeLatest, put } from 'redux-saga/effects';
import reportError from 'report-error';
import { reset as resetForm } from 'redux-form/immutable';
import { delay } from 'redux-saga';

import { HomeTypes, HomeActions } from './home.redux';
import api from '../../services/api';

export function* newsletterRequest({ data }) {
  try {
    yield api.post('/newsletter/home', data);
    yield put(HomeActions.newsletterSuccess());
  } catch (e) {
    yield put(HomeActions.newsletterFailure(e));
    yield reportError(e);
  }
}

export function* newsletterSuccess() {
  try {
    yield delay(2000);
    yield put(resetForm('newsletter'));
    yield put(HomeActions.newsletterReset());
  } catch (e) {
    yield put(HomeActions.newsletterFailure(e));
    yield reportError(e);
  }
}

export default function* homeSaga() {
  yield takeLatest(HomeTypes.NEWSLETTER_REQUEST, newsletterRequest);
  yield takeLatest(HomeTypes.NEWSLETTER_SUCCESS, newsletterSuccess);
}
