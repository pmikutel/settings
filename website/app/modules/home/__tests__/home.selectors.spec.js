import { expect } from 'chai';
import { fromJS, Record } from 'immutable';

import {
  selectNewsletterError,
  selectNewsletterSuccess,
  selectNewsletterFetching,
} from '../home.selectors';

describe('Home: selectors', () => {
  const HomeRecord = new Record({
    newsletter: fromJS({
      isFetching: false,
      isSuccess: false,
      error: null,
    }),
  });

  const initialState = fromJS({
    home: new HomeRecord(),
  });

  describe('selectNewsletterSuccess', () => {
    it('should select success status true', () => {
      const state = initialState.setIn(['home', 'newsletter', 'isSuccess'], true);
      expect(selectNewsletterSuccess(state)).to.deep.equal(true);
    });

    it('should select success status false', () => {
      const state = initialState.setIn(['home', 'newsletter', 'isSuccess'], false);
      expect(selectNewsletterSuccess(state)).to.deep.equal(false);
    });
  });

  describe('selectNewsletterError', () => {
    it('should select error status true', () => {
      const state = initialState.setIn(['home', 'newsletter', 'isError'], true);
      expect(selectNewsletterError(state)).to.deep.equal(true);
    });

    it('should select error status false', () => {
      const state = initialState.setIn(['home', 'newsletter', 'isError'], false);
      expect(selectNewsletterError(state)).to.deep.equal(false);
    });
  });

  describe('selectNewsletterFetching', () => {
    it('should select fetching status true', () => {
      const state = initialState.setIn(['home', 'newsletter', 'isFetching'], true);
      expect(selectNewsletterFetching(state)).to.deep.equal(true);
    });

    it('should select fetching status false', () => {
      const state = initialState.setIn(['home', 'newsletter', 'isFetching'], false);
      expect(selectNewsletterFetching(state)).to.deep.equal(false);
    });
  });
});
