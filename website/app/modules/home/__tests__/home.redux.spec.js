import { expect } from 'chai';
import { Record, fromJS } from 'immutable';

import { reducer, HomeActions } from '../home.redux';

describe('Home: redux', () => {
  const HomeRecord = new Record({
    newsletter: fromJS({
      isFetching: false,
      isSuccess: false,
      isError: false,
      error: null,
    }),
  });

  const state = new HomeRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on NEWSLETTER_REQUEST', () => {
      const expectedState = new HomeRecord()
        .setIn(['newsletter', 'isFetching'], true)
        .setIn(['newsletter', 'isSuccess'], false)
        .setIn(['newsletter', 'isError'], false)
        .setIn(['newsletter', 'error'], null);

      expect(reducer(state, HomeActions.newsletterRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on NEWSLETTER_SUCCESS', () => {
      const expectedState = new HomeRecord()
        .setIn(['newsletter', 'isFetching'], false)
        .setIn(['newsletter', 'isSuccess'], true)
        .setIn(['newsletter', 'isError'], false)
        .setIn(['newsletter', 'error'], null);

      expect(reducer(state, HomeActions.newsletterSuccess()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on NEWSLETTER_RESET', () => {
      const expectedState = new HomeRecord()
        .setIn(['newsletter', 'isFetching'], false)
        .setIn(['newsletter', 'isSuccess'], false)
        .setIn(['newsletter', 'isError'], false)
        .setIn(['newsletter', 'error'], null);

      expect(reducer(state, HomeActions.newsletterReset()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on NEWSLETTER_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new HomeRecord()
        .setIn(['newsletter', 'isFetching'], false)
        .setIn(['newsletter', 'isSuccess'], false)
        .setIn(['newsletter', 'isError'], true)
        .setIn(['newsletter', 'error'], fromJS(error));

      expect(reducer(state, HomeActions.newsletterFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
