import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { fromJS, Record } from 'immutable';
import api from '../../../services/api';

import { HomeActions, HomeTypes } from '../home.redux';
import homeSaga from '../home.sagas';

describe('Home: saga', () => {
  const HomeRecord = new Record({
    newsletter: fromJS({
      isFetching: false,
      isSuccess: false,
      error: null,
    }),
  });

  const getSagaTester = ({ home = new HomeRecord() }) => {
    const sagaTester = new SagaTester({ initialState: {
      home,
    } });

    sagaTester.start(homeSaga);
    return sagaTester;
  };

  it('should fetch newsletter and dispatch successful action', async () => {
    const sagaTester = getSagaTester({});
    api.post.mockImplementation(() => Promise.resolve());

    sagaTester.dispatch(HomeActions.newsletterRequest());

    const action = await sagaTester.waitFor(HomeTypes.NEWSLETTER_SUCCESS);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should success newsletter and dispatch reset action', async () => {
    const sagaTester = getSagaTester({});
    api.post.mockImplementation(() => Promise.resolve());

    sagaTester.dispatch(HomeActions.newsletterSuccess());

    const action = await sagaTester.waitFor(HomeTypes.NEWSLETTER_RESET);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch newsletter and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});
    api.post.mockImplementation(() => {
      throw new Error('Test error');
    });

    sagaTester.dispatch(HomeActions.newsletterRequest());

    const action = await sagaTester.waitFor(HomeTypes.NEWSLETTER_FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });
});
