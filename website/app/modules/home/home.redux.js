import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS } from 'immutable';

export const { Types: HomeTypes, Creators: HomeActions } = createActions({
  newsletterRequest: ['data'],
  newsletterSuccess: [''],
  newsletterFailure: ['error'],
  newsletterReset: [''],
}, { prefix: 'HOME_' });

const HomeRecord = new Record({
  newsletter: fromJS({
    isFetching: false,
    isSuccess: false,
    isError: false,
    error: null,
  }),
});

export const INITIAL_STATE = new HomeRecord({});

const newsletterRequest = (state = INITIAL_STATE) => state
  .setIn(['newsletter', 'isFetching'], true)
  .setIn(['newsletter', 'isSuccess'], false)
  .setIn(['newsletter', 'isError'], false)
  .setIn(['newsletter', 'error'], null);

const newsletterFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['newsletter', 'isFetching'], false)
  .setIn(['newsletter', 'isSuccess'], false)
  .setIn(['newsletter', 'isError'], true)
  .setIn(['newsletter', 'error'], fromJS(error));

const newsletterSuccess = (state = INITIAL_STATE) => state
  .setIn(['newsletter', 'isFetching'], false)
  .setIn(['newsletter', 'isSuccess'], true)
  .setIn(['newsletter', 'isError'], false)
  .setIn(['newsletter', 'error'], null);

const newsletterReset = (state = INITIAL_STATE) => state
  .setIn(['newsletter', 'isFetching'], false)
  .setIn(['newsletter', 'isSuccess'], false)
  .setIn(['newsletter', 'isError'], false)
  .setIn(['newsletter', 'error'], null);

export const reducer = createReducer(INITIAL_STATE, {
  [HomeTypes.NEWSLETTER_REQUEST]: newsletterRequest,
  [HomeTypes.NEWSLETTER_SUCCESS]: newsletterSuccess,
  [HomeTypes.NEWSLETTER_FAILURE]: newsletterFailure,
  [HomeTypes.NEWSLETTER_RESET]: newsletterReset,
});
