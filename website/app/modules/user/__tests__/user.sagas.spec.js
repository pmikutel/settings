import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { Record, fromJS } from 'immutable';
import { identity } from 'ramda';
import moment from 'moment';
import { CALL_HISTORY_METHOD } from 'react-router-redux';

import { UserActions, UserTypes } from '../user.redux';
import { ModalsTypes } from '../../modals/modals.redux';
import { CheckoutTypes, CheckoutActions } from '../../checkout/checkout.redux';
import registerSaga from '../user.sagas';
import { shopifyApi } from '../../../services/shopify/api';
import api from '../../../services/api';
import { RegisterTypes } from '../../register/register.redux';

describe('User: saga', () => {
  const UserRecord = new Record({
    session: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    profile: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    updateUser: fromJS({
      isFetching: false,
      isSuccess: false,
      error: false,
    }),
    metafields: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    isProjectFlow: false,
  });

  const RegisterRecord = new Record({
    createProject: true,
  });

  const ModalsRecord = new Record({
    modal: '',
  });

  const getSagaTester = ({ user = new UserRecord(), modals = new ModalsRecord(), register = new RegisterRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: fromJS({
        user,
        modals,
        register,
      }),
      reducers: identity,
    });

    sagaTester.start(registerSaga);
    return sagaTester;
  };

  describe('loginRequest', () => {
    it('should dispatch loginSuccess action when login is successful', async () => {
      const data = fromJS({ email: 'email', password: 'password' });
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerAccessTokenCreate: {
              customerAccessToken: 'access-token',
              userErrors: [],
            },
          },
        },
      }));

      sagaTester.dispatch(UserActions.loginRequest(data));

      const action = await sagaTester.waitFor(UserTypes.LOGIN_SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch loginFailure action when login has errors', async () => {
      const data = fromJS({ email: 'email', password: 'password' });
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          errors: ['error'],
          data: {
            customerAccessTokenCreate: {
              customerAccessToken: 'access-token',
              userErrors: [],
            },
          },
        },
      }));

      sagaTester.dispatch(UserActions.loginRequest(data));

      const action = await sagaTester.waitFor(UserTypes.LOGIN_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch loginFailure action when customerAccessTokenCreate has errors', async () => {
      const data = fromJS({ email: 'email', password: 'password' });
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerAccessTokenCreate: {
              userErrors: ['error'],
            },
          },
        },
      }));

      sagaTester.dispatch(UserActions.loginRequest(data));

      const action = await sagaTester.waitFor(UserTypes.LOGIN_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('loginSuccess', () => {
    it('should dispatch profileRequest action', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(UserActions.loginSuccess());

      const action = await sagaTester.waitFor(UserTypes.PROFILE_REQUEST);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch closeModal action', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(UserActions.loginSuccess());

      const action = await sagaTester.waitFor(ModalsTypes.CLOSE_MODAL);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('profileRequest', () => {
    it('should dispatch profileSuccess action when request is successful', async () => {
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customer: {
              firstName: 'first-name',
              lastName: 'last-name',
              userErrors: [],
            },
          },
        },
      }));

      sagaTester.dispatch(UserActions.profileRequest());

      const action = await sagaTester.waitFor(UserTypes.PROFILE_SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch profileFailure action when data has errors', async () => {
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          errors: ['error'],
          data: {
            customer: {
              firstName: 'first-name',
              lastName: 'last-name',
              userErrors: [],
            },
          },
        },
      }));

      sagaTester.dispatch(UserActions.profileRequest());

      const action = await sagaTester.waitFor(UserTypes.PROFILE_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('checkSession', () => {
    it('should dispatch logoutRequest action when token expired and user is logged in', async () => {
      const expiresAt = moment().subtract(1, 'minutes').toISOString();
      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], 'access-token')
          .setIn(['session', 'data', 'expiresAt'], expiresAt),
      });

      sagaTester.dispatch(UserActions.checkSession());

      const action = await sagaTester.waitFor(UserTypes.LOGOUT_REQUEST);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should not dispatch logoutRequest action when user is not logged in', async () => {
      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], null),
      });

      sagaTester.dispatch(UserActions.checkSession());

      expect(sagaTester.getCalledActions()).not.to.contains(UserActions.logoutRequest());
    });
  });

  describe('logoutRequest', () => {
    it('should dispatch logoutSuccess action when request is successful', async () => {
      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], 'access-token'),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({}));

      sagaTester.dispatch(UserActions.logoutRequest());

      const action = await sagaTester.waitFor(UserTypes.LOGOUT_SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch logoutFailure action when data has errors', async () => {
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          errors: ['error'],
        },
      }));

      sagaTester.dispatch(UserActions.logoutRequest());

      const action = await sagaTester.waitFor(UserTypes.LOGOUT_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('metafieldsRequest', () => {
    it('should dispatch metafieldsSuccess action when request is successful', async () => {
      const sagaTester = getSagaTester({});

      api.get.mockReturnValue(Promise.resolve({
        data: [{ key: 'project', value: 'val-1' }],
      }));

      sagaTester.dispatch(UserActions.metafieldsRequest());

      const action = await sagaTester.waitFor(UserTypes.METAFIELDS_SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch metafieldsRequest action when project not returned and createProject is set', async () => {
      const sagaTester = getSagaTester({});

      api.get.mockReturnValue(Promise.resolve({
        data: [{ key: 'project', value: 'val-1' }],
      }));

      sagaTester.dispatch(UserActions.metafieldsRequest());

      const action = await sagaTester.waitFor(UserTypes.METAFIELDS_REQUEST);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch push to url location when project returned and createProject is set', async () => {
      const sagaTester = getSagaTester({});

      api.get.mockReturnValue(Promise.resolve({
        data: [{ key: 'project', value: 'val-1' }],
      }));

      sagaTester.dispatch(UserActions.metafieldsRequest());

      const action = await sagaTester.waitFor(CALL_HISTORY_METHOD);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch resetShouldCreateProject when project returned and createProject is set', async () => {
      const sagaTester = getSagaTester({});

      api.get.mockReturnValue(Promise.resolve({
        data: [{ key: 'project', value: 'val-1' }],
      }));

      sagaTester.dispatch(UserActions.metafieldsRequest());

      const action = await sagaTester.waitFor(RegisterTypes.RESET_SHOULD_CREATE_PROJECT);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch metafieldsFailure action when data has errors', async () => {
      const sagaTester = getSagaTester({});

      api.get.mockReturnValue(Promise.reject({ error: 'error' }));

      sagaTester.dispatch(UserActions.metafieldsRequest());

      const action = await sagaTester.waitFor(UserTypes.METAFIELDS_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('metafieldsSuccess', () => {
    it('should dispatch createProject action when project is not defined', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(UserActions.metafieldsSuccess({ project: null }, true));

      const action = await sagaTester.waitFor(CheckoutTypes.CREATE_PROJECT);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should not dispatch createProject action when project is defined', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(UserActions.metafieldsSuccess({ project: 'project-id' }));

      expect(sagaTester.getCalledActions()).not.to.contains(CheckoutActions.createProject());
    });
  });

  describe('updateUserRequest', () => {
    it('should dispatch loginRequest action when password exist', async () => {
      const data = fromJS({
        name: 'name',
        company: 'company',
        email: 'email',
        password: 'password',
      });

      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], 'access-token'),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({}));

      sagaTester.dispatch(UserActions.updateUserRequest(data));

      const action = await sagaTester.waitFor(UserTypes.LOGIN_REQUEST);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch profileRequest action when password does not exist', async () => {
      const data = fromJS({
        name: 'name',
        company: 'company',
        email: 'email',
      });

      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], 'access-token'),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({}));

      sagaTester.dispatch(UserActions.updateUserRequest(data));

      const action = await sagaTester.waitFor(UserTypes.PROFILE_REQUEST);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch updateUserSuccess action', async () => {
      const data = fromJS({
        name: 'name',
        company: 'company',
        email: 'email',
      });

      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], 'access-token'),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({}));

      sagaTester.dispatch(UserActions.updateUserRequest(data));

      const action = await sagaTester.waitFor(UserTypes.UPDATE_USER_SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch updateUserFailure action if user errors exist', async () => {
      const data = fromJS({
        name: 'name',
        company: 'company',
        email: 'email',
      });

      const sagaTester = getSagaTester({
        user: new UserRecord()
          .setIn(['session', 'data', 'accessToken'], 'access-token'),
      });

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: { data: { customerUpdate: { userErrors: [{ message: 'message' }] } } },
      }));

      sagaTester.dispatch(UserActions.updateUserRequest(data));

      const action = await sagaTester.waitFor(UserTypes.UPDATE_USER_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('updateUserSuccess', () => {
    it('should dispatch updateUserReset action', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(UserActions.updateUserSuccess());

      const action = await sagaTester.waitFor(UserTypes.UPDATE_USER_RESET);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });
});
