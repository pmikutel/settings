import { expect } from 'chai';
import { fromJS, Record } from 'immutable';
import moment from 'moment';

import {
  selectIsTokenExpired,
  selectIsLoggedIn,
  selectSessionAccessToken,
  selectSessionData,
  selectSessionExpiresAt,
  selectProfileData,
  selectLoginError,
  selectUserCompanyName,
  selectProjectId,
  selectUpdateUser,
  selectUpdateUserFetching,
  selectUpdateUserSuccess,
  selectUserEmail,
  selectIsProjectFlow,
} from '../user.selectors';

describe('User: selectors', () => {
  const UserRecord = new Record({
    session: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    profile: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    updateUser: fromJS({
      isFetching: false,
      isSuccess: false,
      error: false,
    }),
    metafields: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    isProjectFlow: false,
  }, 'user');

  const initialState = fromJS({
    user: new UserRecord(),
  });

  describe('selectSessionData', () => {
    it('should select session data', () => {
      const data = { prop: 'value' };
      const state = initialState.setIn(['user', 'session', 'data'], data);

      expect(selectSessionData(state)).to.deep.equal(data);
    });
  });

  describe('selectIsProjectFlow', () => {
    it('should select is project flow', () => {
      const data = true;
      const state = initialState.setIn(['user', 'isProjectFlow'], data);

      expect(selectIsProjectFlow(state)).to.deep.equal(data);
    });
  });

  describe('selectProfileData', () => {
    it('should select profile data', () => {
      const data = { prop: 'value' };
      const state = initialState.setIn(['user', 'profile', 'data'], data);

      expect(selectProfileData(state)).to.deep.equal(data);
    });
  });

  describe('selectUserEmail', () => {
    it('should select user email', () => {
      const email = 'email@email.com';
      const state = initialState.setIn(['user', 'profile', 'data', 'email'], email);

      expect(selectUserEmail(state)).to.equal(email);
    });
  });

  describe('selectLoginError', () => {
    it('should select login error as false if it is empty', () => {
      expect(selectLoginError(initialState)).to.equal(false);
    });

    it('should select login error as true if it is not empty', () => {
      const state = initialState.setIn(['user', 'session', 'error'], 'dummy error');

      expect(selectLoginError(state)).to.equal(true);
    });
  });

  describe('selectSessionAccessToken', () => {
    it('should select session data accessToken values', () => {
      const accessToken = 'access-token';
      const state = initialState.setIn(['user', 'session', 'data', 'accessToken'], accessToken);

      expect(selectSessionAccessToken(state)).to.equal(accessToken);
    });
  });

  describe('selectIsLoggedIn', () => {
    it('should return true when accessToken is defined', () => {
      const accessToken = 'access-token';
      const state = initialState.setIn(['user', 'session', 'data', 'accessToken'], accessToken);

      expect(selectIsLoggedIn(state)).to.equal(true);
    });

    it('should return false when accessToken is defined', () => {
      const accessToken = null;
      const state = initialState.setIn(['user', 'session', 'data', 'accessToken'], accessToken);

      expect(selectIsLoggedIn(state)).to.equal(false);
    });
  });

  describe('selectSessionExpiresAt', () => {
    it('should select session data expiresAt value', () => {
      const expiresAt = 'expires-at';
      const state = initialState.setIn(['user', 'session', 'data', 'expiresAt'], expiresAt);

      expect(selectSessionExpiresAt(state)).to.equal(expiresAt);
    });
  });

  describe('selectIsTokenExpired', () => {
    it('should return true when current date is after expiresAt', () => {
      const expiresAt = moment().subtract(1, 'minutes').toISOString();
      const state = initialState.setIn(['user', 'session', 'data', 'expiresAt'], expiresAt);

      expect(selectIsTokenExpired(state)).to.equal(true);
    });

    it('should return false when current date is before expiresAt', () => {
      const expiresAt = moment().add(1, 'days').toISOString();
      const state = initialState.setIn(['user', 'session', 'data', 'expiresAt'], expiresAt);

      expect(selectIsTokenExpired(state)).to.equal(false);
    });
  });

  describe('selectUserCompanyName', () => {
    it('should select profile data lastName value', () => {
      const lastName = 'example company name';
      const state = initialState.setIn(['user', 'profile', 'data', 'lastName'], lastName);

      expect(selectUserCompanyName(state)).to.equal(lastName);
    });
  });

  describe('selectProjectId', () => {
    it('should select project id', () => {
      const id = '123';
      const state = initialState.setIn(['user', 'metafields', 'data', 'project'], id);

      expect(selectProjectId(state)).to.equal(id);
    });
  });

  describe('selectUpdateUser', () => {
    it('should select update user', () => {
      const item = fromJS({ item1: { id: '1' } });

      const state = initialState.setIn(['user', 'updateUser'], item);

      expect(selectUpdateUser(state)).to.equal(item);
    });
  });

  describe('selectUpdateUserFetching', () => {
    it('should select update user fetching', () => {
      const isFetching = true;

      const state = initialState.setIn(['user', 'updateUser', 'isFetching'], isFetching);

      expect(selectUpdateUserFetching(state)).to.equal(isFetching);
    });
  });

  describe('selectUpdateUserSuccess', () => {
    it('should select update user success', () => {
      const isSuccess = true;

      const state = initialState.setIn(['user', 'updateUser', 'isSuccess'], isSuccess);

      expect(selectUpdateUserSuccess(state)).to.equal(isSuccess);
    });
  });
});
