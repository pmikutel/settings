import { expect } from 'chai';
import { Record, fromJS } from 'immutable';

import { reducer, UserActions } from '../user.redux';

describe('User: redux', () => {
  const UserRecord = new Record({
    session: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    profile: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    updateUser: fromJS({
      isFetching: false,
      isSuccess: false,
      error: false,
    }),
    metafields: fromJS({
      data: {},
      isFetching: false,
      error: null,
    }),
    isProjectFlow: false,
  });

  const state = new UserRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on LOGIN_REQUEST', () => {
      const expectedState = new UserRecord()
        .setIn(['session', 'isFetching'], true)
        .setIn(['session', 'error'], null);

      expect(reducer(state, UserActions.loginRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on LOGIN_SUCCESS', () => {
      const sessionData = { prop: 'value' };

      const expectedState = new UserRecord()
        .setIn(['session', 'isFetching'], false)
        .setIn(['session', 'data'], fromJS(sessionData));

      expect(reducer(state, UserActions.loginSuccess(sessionData)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on LOGIN_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new UserRecord()
        .setIn(['session', 'isFetching'], false)
        .setIn(['session', 'error'], fromJS(error));

      expect(reducer(state, UserActions.loginFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on LOGIN_RESET', () => {
      const expectedState = new UserRecord()
        .setIn(['session', 'error'], null);

      expect(reducer(state, UserActions.loginReset()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on LOGOUT_REQUEST', () => {
      const expectedState = new UserRecord()
        .setIn(['session', 'isFetching'], true)
        .setIn(['session', 'error'], null);

      expect(reducer(state, UserActions.logoutRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on LOGOUT_SUCCESS', () => {
      const expectedState = new UserRecord();

      expect(reducer(state, UserActions.logoutSuccess()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on LOGOUT_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new UserRecord()
        .setIn(['session', 'error'], fromJS(error));

      expect(reducer(state, UserActions.logoutFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should clear error on CLEAR_ERROR', () => {
      const error = { error: 'error' };

      const previousState = new UserRecord()
        .setIn(['session', 'error'], fromJS(error));

      const expectedState = new UserRecord()
        .setIn(['session', 'error'], null);

      expect(reducer(previousState, UserActions.clearError()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on PROFILE_REQUEST', () => {
      const expectedState = new UserRecord()
        .setIn(['profile', 'isFetching'], true)
        .setIn(['profile', 'error'], null);

      expect(reducer(state, UserActions.profileRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on PROFILE_SUCCESS', () => {
      const data = { prop: 'value' };

      const expectedState = new UserRecord()
        .setIn(['profile', 'isFetching'], false)
        .setIn(['profile', 'data'], fromJS(data));

      expect(reducer(state, UserActions.profileSuccess(data)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on PROFILE_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new UserRecord()
        .setIn(['profile', 'isFetching'], false)
        .setIn(['profile', 'error'], fromJS(error));

      expect(reducer(state, UserActions.profileFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on METAFIELDS_REQUEST', () => {
      const expectedState = new UserRecord()
        .setIn(['metafields', 'isFetching'], true)
        .setIn(['metafields', 'error'], null);

      expect(reducer(state, UserActions.metafieldsRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on METAFIELDS_SUCCESS', () => {
      const metafieldsData = { prop: 'value' };

      const expectedState = new UserRecord()
        .setIn(['metafields', 'isFetching'], false)
        .setIn(['metafields', 'data'], fromJS(metafieldsData));

      expect(reducer(state, UserActions.metafieldsSuccess(metafieldsData)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on METAFIELDS_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new UserRecord()
        .setIn(['metafields', 'isFetching'], false)
        .setIn(['metafields', 'error'], fromJS(error));

      expect(reducer(state, UserActions.metafieldsFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_USER_REQUEST', () => {
      const expectedState = new UserRecord()
        .setIn(['updateUser', 'isFetching'], true);

      expect(reducer(state, UserActions.updateUserRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_USER_SUCCESS', () => {
      const expectedState = new UserRecord()
        .setIn(['updateUser', 'isFetching'], false)
        .setIn(['updateUser', 'isSuccess'], true);

      expect(reducer(state, UserActions.updateUserSuccess()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_USER_RESET', () => {
      const expectedState = new UserRecord()
        .setIn(['updateUser', 'isSuccess'], false);

      expect(reducer(state, UserActions.updateUserReset()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on UPDATE_USER_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new UserRecord()
        .setIn(['updateUser', 'isFetching'], false)
        .setIn(['updateUser', 'error'], fromJS(error));

      expect(reducer(state, UserActions.updateUserFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
