import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS } from 'immutable';

export const { Types: UserTypes, Creators: UserActions } = createActions({
  loginRequest: ['data', 'isPurchaseFlow', 'isProjectFlow'],
  loginSuccess: ['sessionData'],
  loginFailure: ['error'],
  loginReset: [''],
  logoutRequest: [''],
  logoutSuccess: [''],
  logoutFailure: ['error'],
  clearError: [],
  profileRequest: [''],
  profileSuccess: ['data'],
  profileFailure: ['error'],
  metafieldsRequest: ['isProject'],
  metafieldsSuccess: ['data', 'isNotProject'],
  metafieldsFailure: ['error'],
  checkSession: [],
  updateUserRequest: ['data'],
  updateUserSuccess: [''],
  updateUserFailure: ['error'],
  updateUserReset: [''],
}, { prefix: 'USER_' });

export const UserRecord = new Record({
  session: fromJS({
    data: {},
    isFetching: false,
    error: null,
  }),
  profile: fromJS({
    data: {},
    isFetching: false,
    error: null,
  }),
  updateUser: fromJS({
    isFetching: false,
    isSuccess: false,
    error: false,
  }),
  metafields: fromJS({
    data: {},
    isFetching: false,
    error: null,
  }),
  isProjectFlow: false,
}, 'user');

export const INITIAL_STATE = new UserRecord({});

const loginRequest = (state = INITIAL_STATE, { isProjectFlow = false }) => state
  .setIn(['session', 'isFetching'], true)
  .set('isProjectFlow', isProjectFlow)
  .setIn(['session', 'error'], null);

const loginSuccess = (state = INITIAL_STATE, { sessionData }) => state
  .setIn(['session', 'isFetching'], false)
  .setIn(['session', 'data'], fromJS(sessionData));

const loginFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['session', 'isFetching'], false)
  .setIn(['session', 'error'], fromJS(error));

const loginReset = (state = INITIAL_STATE) => state
  .setIn(['session', 'error'], null);

const logoutRequest = (state = INITIAL_STATE) => state
  .setIn(['session', 'isFetching'], true)
  .set('isProjectFlow', false)
  .setIn(['session', 'error'], null);

const logoutSuccess = () => INITIAL_STATE;

const logoutFailure = (state, { error }) => INITIAL_STATE
  .setIn(['session', 'error'], fromJS(error));

const clearError = (state = INITIAL_STATE) => state
  .setIn(['session', 'error'], null);

const profileRequest = (state = INITIAL_STATE) => state
  .setIn(['profile', 'isFetching'], true)
  .setIn(['profile', 'error'], null);

const profileSuccess = (state = INITIAL_STATE, { data }) => state
  .setIn(['profile', 'isFetching'], false)
  .setIn(['profile', 'data'], fromJS(data));

const profileFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['profile', 'isFetching'], false)
  .setIn(['profile', 'error'], fromJS(error));

const metafieldsRequest = (state = INITIAL_STATE) => state
  .setIn(['metafields', 'isFetching'], true)
  .setIn(['metafields', 'error'], null);

const metafieldsSuccess = (state = INITIAL_STATE, { data }) => state
  .set('isProjectFlow', false)
  .setIn(['metafields', 'isFetching'], false)
  .setIn(['metafields', 'data'], fromJS(data));

const metafieldsFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['metafields', 'isFetching'], false)
  .setIn(['metafields', 'error'], fromJS(error));

const updateUserRequest = (state = INITIAL_STATE) => state
  .setIn(['updateUser', 'isFetching'], true);

const updateUserSuccess = (state = INITIAL_STATE) => state
  .setIn(['updateUser', 'isFetching'], false)
  .setIn(['updateUser', 'isSuccess'], true);

const updateUserReset = (state = INITIAL_STATE) => state
  .setIn(['updateUser', 'isSuccess'], false);

const updateUserFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['updateUser', 'isFetching'], false)
  .setIn(['updateUser', 'error'], fromJS(error));

export const reducer = createReducer(INITIAL_STATE, {
  [UserTypes.LOGIN_REQUEST]: loginRequest,
  [UserTypes.LOGIN_SUCCESS]: loginSuccess,
  [UserTypes.LOGIN_FAILURE]: loginFailure,
  [UserTypes.LOGIN_RESET]: loginReset,
  [UserTypes.LOGOUT_REQUEST]: logoutRequest,
  [UserTypes.LOGOUT_SUCCESS]: logoutSuccess,
  [UserTypes.LOGOUT_FAILURE]: logoutFailure,
  [UserTypes.CLEAR_ERROR]: clearError,
  [UserTypes.PROFILE_REQUEST]: profileRequest,
  [UserTypes.PROFILE_SUCCESS]: profileSuccess,
  [UserTypes.PROFILE_FAILURE]: profileFailure,
  [UserTypes.METAFIELDS_REQUEST]: metafieldsRequest,
  [UserTypes.METAFIELDS_SUCCESS]: metafieldsSuccess,
  [UserTypes.METAFIELDS_FAILURE]: metafieldsFailure,
  [UserTypes.UPDATE_USER_REQUEST]: updateUserRequest,
  [UserTypes.UPDATE_USER_SUCCESS]: updateUserSuccess,
  [UserTypes.UPDATE_USER_RESET]: updateUserReset,
  [UserTypes.UPDATE_USER_FAILURE]: updateUserFailure,
});
