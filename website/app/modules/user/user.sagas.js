import { delay } from 'redux-saga';
import { takeLatest, put, select } from 'redux-saga/effects';
import reportError from 'report-error';
import { push } from 'react-router-redux';
import { pipe, path, isEmpty, either, complement, map, mergeAll, prop } from 'ramda';

import { UserTypes, UserActions } from './user.redux';
import {
  selectSessionAccessToken,
  selectIsLoggedIn,
  selectIsTokenExpired,
  selectProfileData,
  selectIsProjectFlow,
} from './user.selectors';
import { selectCreateProject } from '../register/register.selectors';
import { RegisterActions } from '../register/register.redux';
import { ModalsActions } from '../modals/modals.redux';
import { shopifyApi, hasErrors } from '../../services/shopify/api';
import api from '../../services/api';
import { CheckoutActions } from '../checkout/checkout.redux';

const hasAccessTokenCreateErrors = pipe(
  path(['data', 'data', 'customerAccessTokenCreate', 'userErrors']),
  complement(isEmpty),
);

export function* loginRequest({ data, isPurchaseFlow = false }) {
  try {
    yield put(CheckoutActions.setPurchaseFlow(isPurchaseFlow));
    const response = yield shopifyApi.post('/', `mutation {
      customerAccessTokenCreate(input: {
        email: "${data.get('email')}",
        password: "${data.get('password')}"
      }) {
        userErrors {
          field
          message
        }
        customerAccessToken {
          accessToken
          expiresAt
        }
      }
    }`);

    if (either(hasErrors, hasAccessTokenCreateErrors)(response)) {
      yield put(UserActions.loginFailure(response.data));
      return;
    }

    const session = path(['data', 'data', 'customerAccessTokenCreate', 'customerAccessToken'])(response);

    yield put(UserActions.loginSuccess(session));
  } catch (e) {
    yield reportError(e);
  }
}

export function* loginSuccess() {
  try {
    yield put(UserActions.profileRequest());
    yield put(ModalsActions.closeModal());
  } catch (e) {
    yield reportError(e);
  }
}

export function* profileRequest() {
  try {
    const accessToken = yield select(selectSessionAccessToken);

    const response = yield shopifyApi.post('/', `{
      customer(customerAccessToken: "${accessToken}") {
        id,
        firstName,
        lastName,
        email,
        orders(first: 10, reverse: true) {
          edges {
            node {
              orderNumber,
              shippingAddress {
                address1,
                address2,
                city,
                zip,
                country,
              },
              totalPrice,
              processedAt,
            }
          }
        }
      }
    }`);

    if (hasErrors(response)) {
      yield put(UserActions.profileFailure(response.data));
      return;
    }

    const customer = path(['data', 'data', 'customer'])(response);

    if (!customer) {
      yield put(UserActions.logoutRequest());
      return;
    }

    yield put(UserActions.profileSuccess(customer));
  } catch (e) {
    yield reportError(e);
  }
}

export function* profileSuccess() {
  try {
    yield put(UserActions.metafieldsRequest(false));
  } catch (e) {
    yield reportError(e);
  }
}

export function* metafieldsRequest({ isProject }) {
  try {
    const profile = yield select(selectProfileData);
    const isProjectFlow = yield select(selectIsProjectFlow);
    const shouldCreateProject = yield select(selectCreateProject);
    const checkProjectData = isProject || shouldCreateProject;
    const response = yield api.get(`/metafields/customer/${profile.get('id')}`);

    const data = pipe(
      prop('data'),
      map(({ key, value }) => ({ [key]: value })),
      mergeAll,
    )(response);

    if (checkProjectData && !data.project) {
      yield delay(500);
      yield put(UserActions.metafieldsSuccess(data, !isProject));
      yield put(UserActions.metafieldsRequest(true));
    }

    if (checkProjectData && data.project) {
      yield put(push(`/project/board/${data.project}`));
      yield put(RegisterActions.resetShouldCreateProject());
    }

    if (data.project && isProjectFlow) {
      yield put(push(`/project/board/${data.project}`));
      yield put(ModalsActions.openModal('project'));
    }

    if (!data.project && isProjectFlow) {
      yield put(UserActions.metafieldsSuccess(data, true));
    }

    yield put(UserActions.metafieldsSuccess(data, false));
  } catch (e) {
    yield put(UserActions.metafieldsFailure(e));
    yield reportError(e);
  }
}

export function* updateUserRequest({ data }) {
  try {
    const accessToken = yield select(selectSessionAccessToken);
    const password = data.get('password') ? `password: "${data.get('password')}"` : '';

    const response = yield shopifyApi.post('/', `mutation {
      customerUpdate(
        customerAccessToken: "${accessToken}"
        customer: {
          firstName: "${data.get('name')}",
          lastName: "${data.get('company')}",
          email: "${data.get('email')}",
          ${password}
        }
      ) {
        userErrors {
          field
          message
        }
      }
    }`);

    const userError = path(['data', 'customerUpdate', 'userErrors', 0, 'message'])(response.data);

    if (hasErrors(response) || userError) {
      yield put(UserActions.updateUserFailure(response.data));
      return;
    }

    if (password) {
      yield put(UserActions.loginRequest(data, false));
    } else {
      yield put(UserActions.profileRequest());
    }
    yield put(UserActions.updateUserSuccess(data));
  } catch (e) {
    yield put(UserActions.updateUserFailure(data));
    yield reportError(e);
  }
}

export function* updateUserSuccess() {
  try {
    yield delay(3000);
    yield put(UserActions.updateUserReset());
  } catch (e) {
    yield put(UserActions.updateUserFailure(e));
    yield reportError(e);
  }
}

export function* checkSession() {
  try {
    const isLoggedIn = yield select(selectIsLoggedIn);
    const isTokenExpired = yield select(selectIsTokenExpired);

    if (isLoggedIn && isTokenExpired) {
      yield put(UserActions.logoutRequest());
      return;
    }

    if (isLoggedIn) {
      yield put(UserActions.profileRequest());
    }
  } catch (e) {
    yield reportError(e);
  }
}

export function* logoutRequest() {
  try {
    const accessToken = yield select(selectSessionAccessToken);

    const response = yield shopifyApi.post('/', `mutation {
      customerAccessTokenDelete(customerAccessToken: "${accessToken}") {
        userErrors {
          field
          message
        }
        deletedAccessToken
        deletedCustomerAccessTokenId
      }
    }`);

    if (hasErrors(response)) {
      yield put(UserActions.logoutFailure(response.data));
      return;
    }

    yield put(UserActions.logoutSuccess());
    yield put(push('/'));
  } catch (e) {
    yield reportError(e);
  }
}

export function* metafieldsSuccess({ data: { project }, isNotProject }) {
  try {
    if (!project && isNotProject) {
      yield put(CheckoutActions.createProject());
    }
  } catch (e) {
    yield reportError(e);
  }
}

export default function* loginSaga() {
  yield takeLatest(UserTypes.LOGIN_REQUEST, loginRequest);
  yield takeLatest(UserTypes.LOGIN_SUCCESS, loginSuccess);
  yield takeLatest(UserTypes.PROFILE_REQUEST, profileRequest);
  yield takeLatest(UserTypes.PROFILE_SUCCESS, profileSuccess);
  yield takeLatest(UserTypes.METAFIELDS_REQUEST, metafieldsRequest);
  yield takeLatest(UserTypes.METAFIELDS_SUCCESS, metafieldsSuccess);
  yield takeLatest(UserTypes.CHECK_SESSION, checkSession);
  yield takeLatest(UserTypes.LOGOUT_REQUEST, logoutRequest);
  yield takeLatest(UserTypes.UPDATE_USER_REQUEST, updateUserRequest);
  yield takeLatest(UserTypes.UPDATE_USER_SUCCESS, updateUserSuccess);
}
