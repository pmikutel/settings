import { createSelector } from 'reselect';
import { complement, isNil } from 'ramda';
import moment from 'moment';

const selectUserDomain = state => state.get('user');

export const selectSessionData = createSelector(
  selectUserDomain,
  state => state.getIn(['session', 'data']),
);

export const selectIsProjectFlow = createSelector(
  selectUserDomain,
  state => state.get('isProjectFlow'),
);

export const selectProfileData = createSelector(
  selectUserDomain,
  state => state.getIn(['profile', 'data']),
);

export const selectUserEmail = createSelector(
  selectProfileData,
  state => state.get('email'),
);

export const selectLoginError = createSelector(
  selectUserDomain,
  state => complement(isNil)(state.getIn(['session', 'error'])),
);

export const selectSessionAccessToken = createSelector(
  selectSessionData,
  state => state.get('accessToken'),
);

export const selectSessionExpiresAt = createSelector(
  selectSessionData,
  state => state.get('expiresAt', moment()),
);

export const selectIsLoggedIn = createSelector(
  selectSessionAccessToken,
  complement(isNil),
);

export const selectIsTokenExpired = createSelector(
  selectSessionExpiresAt,
  date => moment().isSameOrAfter(date)
);

export const selectUserCompanyName = createSelector(
  selectProfileData,
  state => state.get('lastName')
);

export const selectProjectId = createSelector(
  selectUserDomain,
  state => state.getIn(['metafields', 'data', 'project'])
);

export const selectUpdateUser = createSelector(
  selectUserDomain,
  state => state.get('updateUser'),
);

export const selectUpdateUserFetching = createSelector(
  selectUpdateUser,
  state => state.get('isFetching'),
);

export const selectUpdateUserSuccess = createSelector(
  selectUpdateUser,
  state => state.get('isSuccess'),
);
