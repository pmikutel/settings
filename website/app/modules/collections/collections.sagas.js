import { takeLatest, put } from 'redux-saga/effects';
import reportError from 'report-error';
import { path, pipe } from 'ramda';

import { CollectionsTypes, CollectionsActions } from './collections.redux';
import { BlogActions } from '../blog/blog.redux';
import { hasErrors, shopifyApi } from '../../services/shopify/api';
import {
  extractDeepOptions,
  extractNode,
  flattenSelectedOptions,
  flattenVariants,
} from '../../utils/shopifyData';
import { groupProducts } from './collections.helpers';

export function* request({ count = 20 }) {
  try {
    const response = yield shopifyApi.post('/', `{
      shop {
        collections(first: ${count}) {
          edges {
            node {
              id,
              title,
              image {
                originalSrc
              },
            }
          }
        }
      }
    }`);

    if (hasErrors(response)) {
      yield put(CollectionsActions.failure(response));
      return;
    }

    const result = path(['data', 'data', 'shop', 'collections', 'edges'], response);

    yield put(CollectionsActions.success(extractNode(result)));
  } catch (error) {
    yield put(CollectionsActions.failure(error));
    yield reportError(error);
  }
}

export function* singleRequest({ id }) {
  try {
    const response = yield shopifyApi.post('/', `{
      node(id: "${id}") {
        id,
        ... on Collection {
          title,
          products(first:100) {
            edges {
              node {
                id,
                title,
                tags,
                description,
                productType,
                handle,
                variants(first: 1) {
                  edges {
                    node {
                      title,
                      id,
                      compareAtPrice,
                      image {
                        originalSrc
                      },
                      price,
                      selectedOptions {
                        name
                        value
                      },
                    }
                  }
                }
              }
            }
          }
        }
      }
    }`);

    if (hasErrors(response)) {
      yield put(CollectionsActions.failure(response));
      return;
    }

    const collectionTitle = path(['data', 'data', 'node', 'title'])(response);

    yield put(BlogActions.request(collectionTitle));

    const flattenResult = pipe(
      path(['data', 'data', 'node', 'products', 'edges']),
      extractNode,
      flattenVariants,
      flattenSelectedOptions,
      extractDeepOptions,
    )(response);

    const groupedResults = groupProducts(flattenResult);

    yield put(CollectionsActions.singleSuccess(groupedResults));
  } catch (error) {
    yield put(CollectionsActions.singleFailure(error));
    yield reportError(error);
  }
}

export default function* collections() {
  yield takeLatest(CollectionsTypes.REQUEST, request);
  yield takeLatest(CollectionsTypes.SINGLE_REQUEST, singleRequest);
}
