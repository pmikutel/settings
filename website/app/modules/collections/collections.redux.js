import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, Map } from 'immutable';

export const { Types: CollectionsTypes, Creators: CollectionsActions } = createActions({
  request: ['count'],
  success: ['data'],
  failure: ['error'],
  singleRequest: ['id'],
  singleSuccess: ['data'],
  singleFailure: ['error'],
}, { prefix: 'COLLECTIONS_' });

const CollectionsRecord = new Record({
  isFetching: false,
  items: Map(),
  error: null,
  single: Map(),
});

export const INITIAL_STATE = new CollectionsRecord({});

const request = (state = INITIAL_STATE) => state
  .set('isFetching', true);

const success = (state = INITIAL_STATE, { data }) => state
  .set('isFetching', false)
  .set('items', fromJS(data));

const failure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));

const singleRequest = (state = INITIAL_STATE) => state
  .set('isFetching', true);

const singleSuccess = (state = INITIAL_STATE, { data }) => state
  .set('isFetching', false)
  .set('single', fromJS(data));

const singleFailure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));

export const reducer = createReducer(INITIAL_STATE, {
  [CollectionsTypes.REQUEST]: request,
  [CollectionsTypes.SUCCESS]: success,
  [CollectionsTypes.FAILURE]: failure,
  [CollectionsTypes.SINGLE_REQUEST]: singleRequest,
  [CollectionsTypes.SINGLE_SUCCESS]: singleSuccess,
  [CollectionsTypes.SINGLE_FAILURE]: singleFailure,
});
