import { expect } from 'chai';
import { fromJS, Map, Record } from 'immutable';

import { selectCollections, selectCollectionsWithImage, selectSingleCollection } from '../collections.selectors';

describe('Collections: selectors', () => {
  const CollectionsRecord = new Record({
    items: Map(),
  });

  const initialState = fromJS({
    product: new CollectionsRecord(),
  });

  describe('selectCollections', () => {
    const items = {
      item1: { id: 'item1', title: 'title' },
      item2: { id: 'item2', title: 'title 2' },
    };

    const state = initialState.setIn(['collections', 'items'], fromJS(items));

    it('should select collections items', () => {
      expect(selectCollections(state).toJS()).to.deep.equal(items);
    });
  });

  describe('selectCollectionsWithImage', () => {
    const items = {
      item1: { id: 'item1', title: 'title' },
      item2: { id: 'item2', title: 'title 2' },
      item3: { id: 'item2', title: 'title 2', image: { originalSrc: 'image.jpg' } },
    };

    const expectedItems = { item3: { id: 'item2', title: 'title 2', image: { originalSrc: 'image.jpg' } } };

    const state = initialState.setIn(['collections', 'items'], fromJS(items));

    it('should select collections with image', () => {
      expect(selectCollectionsWithImage(state).toJS()).to.deep.equal(expectedItems);
    });
  });

  describe('selectSingleCollection', () => {
    const items = { id: 'item1', title: 'title' };

    const state = initialState.setIn(['collections', 'single'], fromJS(items));

    it('should select single collection', () => {
      expect(selectSingleCollection(state).toJS()).to.deep.equal(items);
    });
  });
});
