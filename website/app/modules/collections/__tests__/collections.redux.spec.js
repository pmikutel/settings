import { expect } from 'chai';
import { Record, fromJS, Map } from 'immutable';

import { reducer, CollectionsActions } from '../collections.redux';

describe('Collections: redux', () => {
  const CollectionsRecord = new Record({
    isFetching: false,
    items: Map(),
    error: null,
    single: Map(),
  });

  const state = new CollectionsRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on REQUEST', () => {
      const expectedState = new CollectionsRecord().set('isFetching', true);

      expect(reducer(state, CollectionsActions.request()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SUCCESS', () => {
      const items = [{ id: 'item1' }, { id: 'item2' }];

      const expectedState = new CollectionsRecord()
        .set('isFetching', false)
        .set('items', items);

      expect(reducer(state, CollectionsActions.success(items)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new CollectionsRecord()
        .set('isFetching', false)
        .set('error', fromJS(error));

      expect(reducer(state, CollectionsActions.failure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
