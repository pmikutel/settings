import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { Map, Record } from 'immutable';

import { CollectionsActions, CollectionsTypes } from '../collections.redux';
import collectionsSaga from '../collections.sagas';
import { shopifyApi } from '../../../services/shopify/api';

describe('Collections: saga', () => {
  const CollectionsRecord = new Record({
    isFetching: false,
    items: Map(),
    error: null,
  });

  const getSagaTester = ({ collections = new CollectionsRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: {
        collections,
      },
    });

    sagaTester.start(collectionsSaga);
    return sagaTester;
  };

  it('should fetch collections and dispatch successful action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockReturnValue(Promise.resolve({
      data: {
        data: {
          shop: {
            collections: {
              edges: [],
            },
          },
        },
      },
    }));

    sagaTester.dispatch(CollectionsActions.request());

    const action = await sagaTester.waitFor(CollectionsTypes.SUCCESS);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch collections and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockReturnValue(Promise.resolve({
      data: {
        errors: ['error'],
      },
    }));

    sagaTester.dispatch(CollectionsActions.request());

    const action = await sagaTester.waitFor(CollectionsTypes.FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });
});
