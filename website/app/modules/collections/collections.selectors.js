import { createSelector } from 'reselect';
import { isNil, complement } from 'ramda';

const selectCollectionsDomain = state => state.get('collections');

export const selectCollections = createSelector(
  selectCollectionsDomain,
  state => state.get('items'),
);

export const selectCollectionsWithImage = createSelector(
  selectCollections,
  (items) => items.filter(item => complement(isNil)(item.getIn(['image', 'originalSrc']))),
);

export const selectSingleCollection = createSelector(
  selectCollectionsDomain,
  state => state.get('single'),
);
