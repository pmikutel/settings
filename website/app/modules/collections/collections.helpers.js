import {
  groupBy,
  path,
  evolve,
  pipe,
  intersection,
  prop,
  filter,
  map,
  flatten,
  complement,
  isEmpty,
  pathEq,
  take,
} from 'ramda';

const arrayPriceRange = ['Bronze', 'Silver', 'Gold'];

export const groupProducts = pipe(
  groupBy(path(['variants', 0, 'delivery'])),
  map(pipe(
    map(evolve({
      tags: intersection(arrayPriceRange),
    })),
    filter(pipe(prop('tags'), complement(isEmpty))),
    data => arrayPriceRange.map(key => data.filter(pathEq(['tags', 0], key))),
    flatten,
    take(10),
  )),
);

