import { all, fork } from 'redux-saga/effects';
import homeSaga from './home/home.sagas';
import productsSaga from './products/products.sagas';
import productSaga from './product/product.sagas';
import registerSaga from './register/register.sagas';
import userSaga from './user/user.sagas';
import checkoutSaga from './checkout/checkout.sagas';
import blogSaga from './blog/blog.sagas';
import collectionsSaga from './collections/collections.sagas';
import productTypesSaga from './productTypes/productTypes.sagas';
import resetPasswordSaga from './resetPassword/resetPassword.sagas';

export default function* rootSaga() {
  yield all([
    fork(homeSaga),
    fork(registerSaga),
    fork(productsSaga),
    fork(productSaga),
    fork(userSaga),
    fork(checkoutSaga),
    fork(blogSaga),
    fork(collectionsSaga),
    fork(productTypesSaga),
    fork(resetPasswordSaga),
  ]);
}
