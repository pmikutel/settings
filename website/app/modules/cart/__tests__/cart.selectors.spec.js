import { expect } from 'chai';
import { fromJS, Map, Record } from 'immutable';

import { selectCartItems, selectCompletedCartItems } from '../cart.selectors';

describe('Cart: selectors', () => {
  const CartRecord = new Record({
    completedItems: Map(),
    items: Map(),
  });

  const initialState = fromJS({
    items: new CartRecord(),
  });

  describe('selectCartItems', () => {
    const items = {
      item1: {
        id: '111',
        variantId: '1111',
        title: 'title 1',
        image: 'path 1',
        type: 'type 1',
        size: 'size 1',
        delivery: 'date 1',
        quantity: 1,
        price: 10030,
      },
      item2: {
        id: '222',
        variantId: '2222',
        title: 'title 2',
        image: 'path 2',
        type: 'type 2',
        size: 'size 2',
        delivery: 'date 2',
        quantity: 2,
        price: 100,
      },
    };

    const state = initialState
      .setIn(['cart', 'items'], fromJS(items));

    it('should select products items', () => {
      expect(selectCartItems(state).toJS()).to.deep.equal(items);
    });
  });

  describe('selectCompletedCartItems', () => {
    const items = {
      item1: {
        id: '111',
        variantId: '1111',
        title: 'title 1',
        image: 'path 1',
        type: 'type 1',
        size: 'size 1',
        delivery: 'date 1',
        quantity: 1,
        price: 10030,
      },
      item2: {
        id: '222',
        variantId: '2222',
        title: 'title 2',
        image: 'path 2',
        type: 'type 2',
        size: 'size 2',
        delivery: 'date 2',
        quantity: 2,
        price: 100,
      },
    };

    const state = initialState.setIn(['cart', 'completedItems'], fromJS(items));

    it('should select completedItems products items', () => {
      expect(selectCompletedCartItems(state).toJS()).to.deep.equal(items);
    });
  });
});
