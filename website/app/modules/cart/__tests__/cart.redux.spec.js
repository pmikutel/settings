import { expect } from 'chai';
import { Record, fromJS, Map } from 'immutable';

import { reducer as cartReducer, CartTypes } from '../cart.redux';

describe('Cart: redux', () => {
  const CartRecord = new Record({
    items: Map(),
    completedItems: Map(),
    itemAdded: false,
    lastAdded: Map(),
  });

  const state = new CartRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(cartReducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set proper data in state on REMOVE_ITEM', () => {
      const initialState = state.merge({
        items: [{ id: 1, variantId: 1, title: 'test 1' }],
      });

      const expectedState = state.merge({
        items: [],
      });

      const action = { id: 1, variantId: 1, type: CartTypes.REMOVE_ITEM };
      expect(cartReducer(initialState, action).toJS()).to.deep.equal(expectedState.toJS());
    });

    it('should update proper item on UPDATE_ITEM', () => {
      const data = { id: 2, variantId: 2, title: 'test 3' };

      const initialState = state.merge({
        items: [{ id: 1, variantId: 1, title: 'test 1' }, { id: 2, variantId: 2, title: 'test 2' }],
      });

      const expectedState = state.merge({
        items: [{ id: 1, variantId: 1, title: 'test 1' }, { id: 2, variantId: 2, title: 'test 3' }],
      });

      const action = { data, type: CartTypes.UPDATE_ITEM };
      expect(cartReducer(initialState, action).toJS()).to.deep.equal(expectedState.toJS());
    });

    describe('clearCart', () => {
      it('should remove all items from items and move them into completedItems', () => {
        const initialState = state.merge({
          items: [
            { id: 1, variantId: 1, title: 'test 1' },
            { id: 2, variantId: 2, title: 'test 2' },
          ],
        });

        const expectedState = state.merge({
          items: Map(),
          completedItems: fromJS(initialState.items),
        });

        const action = { type: CartTypes.CLEAR_CART };
        expect(cartReducer(initialState, action).toJS()).to.deep.equal(expectedState.toJS());
      });
    });


    describe('addItem', () => {
      it('should set proper data in state', () => {
        const data = { id: 1, variantId: 1, title: 'test 1', quantity: 1 };
        const initialState = state.merge({
          items: {},
          itemAdded: true,
        });

        const expectedState = state.merge({
          items: {
            1: data,
          },
          lastAdded: data,
          itemAdded: true,
        });

        const action = { data, type: CartTypes.ADD_ITEM };
        expect(cartReducer(initialState, action).toJS()).to.deep.equal(expectedState.toJS());
      });

      it('should update quantity when the same item passed', () => {
        const data = { id: 1, variantId: 1, title: 'test 1', quantity: 2 };
        const expectedData = { id: 1, variantId: 1, title: 'test 1', quantity: 4 };
        const initialState = state.setIn(['items', data.id], fromJS(data));
        const expectedState = state.setIn(['items', data.id], fromJS(expectedData));

        const action = { data, type: CartTypes.ADD_ITEM };

        expect(cartReducer(initialState, action).getIn(['items', 1, 'quantity']))
          .to.equal(expectedState.getIn(['items', 1, 'quantity']));
      });
    });
  });
});
