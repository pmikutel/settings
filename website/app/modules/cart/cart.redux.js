import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, Map } from 'immutable';

export const { Types: CartTypes, Creators: CartActions } = createActions({
  addItem: ['data'],
  updateItem: ['data'],
  removeItem: ['id'],
  clearCart: [''],
  clearItemAdded: [''],
}, { prefix: 'CART_' });

export const CartRecord = new Record({
  completedItems: Map(),
  items: Map(),
  itemAdded: false,
  lastAdded: Map(),
}, 'cart');

export const INITIAL_STATE = new CartRecord({});

const addItem = (state = INITIAL_STATE, { data = {} }) => {
  const items = state.get('items');

  items.map((item) => {
    if (item.get('variantId') === data.variantId) {
      const newQuantity = item.get('quantity') + data.quantity;
      data.quantity = newQuantity;
    }
  });

  return state
    .setIn(['items', data.variantId], fromJS(data))
    .set('itemAdded', true)
    .set('lastAdded', fromJS(data));
};

const updateItem = (state = INITIAL_STATE, { data = {} }) => {
  const items = state.get('items');

  return state.merge({
    items: items.map(item => item.get('variantId') === data.variantId ? fromJS(data) : item),
  });
};

const removeItem = (state = INITIAL_STATE, { id }) => state.
  update('items', (items) => items.filter((item) => item.get('variantId') !== id));

const clearCart = (state = INITIAL_STATE) => state
  .set('completedItems', state.get('items'))
  .set('items', Map());

const clearItemAdded = (state = INITIAL_STATE) => state
  .set('itemAdded', false)
  .set('lastAdded', Map());

export const reducer = createReducer(INITIAL_STATE, {
  [CartTypes.ADD_ITEM]: addItem,
  [CartTypes.REMOVE_ITEM]: removeItem,
  [CartTypes.UPDATE_ITEM]: updateItem,
  [CartTypes.CLEAR_CART]: clearCart,
  [CartTypes.CLEAR_ITEM_ADDED]: clearItemAdded,
});
