import { createSelector } from 'reselect';

const selectCartDomain = state => state.get('cart');

export const selectCartItems = createSelector(
  selectCartDomain,
  state => state.get('items'),
);

export const selectCompletedCartItems = createSelector(
  selectCartDomain,
  state => state.get('completedItems'),
);

export const selectShowCartNotification = createSelector(
  selectCartDomain,
  state => state.get('itemAdded'),
);

export const selectLastAddedItem = createSelector(
  selectCartDomain,
  state => state.get('lastAdded'),
);

export const selectItemsNumber = createSelector(
  selectCartDomain,
  state => {
    let result = 0;
    state.get('items').map(item => {
      result = result + parseInt(item.get('quantity'), 0);
    });

    return result;
  }
);
