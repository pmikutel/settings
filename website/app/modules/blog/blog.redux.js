import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, List } from 'immutable';

export const { Types: BlogTypes, Creators: BlogActions } = createActions({
  request: ['query', 'articlesNumber'],
  success: ['data'],
  failure: ['error'],
}, { prefix: 'BLOG_' });

const BlogRecord = new Record({
  isFetching: false,
  articles: List(),
  error: null,
});

export const INITIAL_STATE = new BlogRecord({});

const request = (state = INITIAL_STATE) => state
  .set('isFetching', true);

const success = (state = INITIAL_STATE, { data }) => state
  .set('isFetching', false)
  .set('articles', fromJS(data));

const failure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));


export const reducer = createReducer(INITIAL_STATE, {
  [BlogTypes.REQUEST]: request,
  [BlogTypes.SUCCESS]: success,
  [BlogTypes.FAILURE]: failure,
});
