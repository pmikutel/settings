import { takeLatest, put } from 'redux-saga/effects';
import reportError from 'report-error';
import { path } from 'ramda';
import { BlogTypes, BlogActions } from './blog.redux';
import { shopifyApi, hasErrors } from '../../services/shopify/api';
import { extractNode } from '../../utils/shopifyData';

export function* request({ query, articlesNumber = 1 }) {
  try {
    const response = yield shopifyApi.post('/', `{
    shop {
        blogs(first: 10, query:"${query}"){
          edges{
            node{
              id,
              title,
              articles(first: ${articlesNumber}) {
                edges {
                  node {
                    id,
                    tags,
                    image {
                      originalSrc
                    },
                    title,
                    contentHtml
                  }
                }
              }
            }
          }
        }
      }
    }`);

    if (hasErrors(response)) {
      yield put(BlogActions.failure(response));
      return;
    }

    const articles = path(['data', 'shop', 'blogs', 'edges', 0, 'node', 'articles', 'edges'], response.data);

    yield put(BlogActions.success(extractNode(articles)));
  } catch (e) {
    yield put(BlogActions.failure(e));
    yield reportError(e);
  }
}

export default function* blogSaga() {
  yield takeLatest(BlogTypes.REQUEST, request);
}
