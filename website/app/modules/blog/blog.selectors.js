import { createSelector } from 'reselect';
import { complement, isNil, pipe, when } from 'ramda';

const selectBlogDomain = state => state.get('blog');

export const selectArticles = createSelector(
  selectBlogDomain,
  state => state.get('articles'),
);

export const selectImage = createSelector(
  selectArticles,
  pipe(
    articles => articles.find(item => item.get('tags').includes('image')),
    when(
      complement(isNil),
      item => item.getIn(['image', 'originalSrc']),
    )
  )
);

export const selectArticleImage = createSelector(
  selectArticles,
  state => state.getIn([0, 'image', 'originalSrc']),
);
