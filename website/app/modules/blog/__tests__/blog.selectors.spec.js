import { expect } from 'chai';
import { fromJS, Record, List } from 'immutable';

import { selectArticles, selectImage, selectArticleImage } from '../blog.selectors';


describe('Blog: selectors', () => {
  const BlogRecord = new Record({
    isFetching: false,
    articles: List(),
    error: null,
  });

  const initialState = fromJS({
    blog: new BlogRecord(),
  });

  describe('selectArticles', () => {
    const articles = [{ originalSrc: 'test.jpg' }];

    const state = initialState.setIn(['blog', 'articles'], fromJS(articles));

    it('should select articles', () => {
      expect(selectArticles(state).toJS()).to.deep.equal(articles);
    });
  });

  describe('selectArticleImage', () => {
    const image = 'test.jpg';

    const state = initialState.setIn(['blog', 'articles', 0, 'image', 'originalSrc'], image);

    it('should select article image', () => {
      expect(selectArticleImage(state)).to.deep.equal(image);
    });
  });

  describe('selectImage', () => {
    const articles = [{
      tags: ['image'],
      image: {
        originalSrc: 'test.png',
      },
    }];

    const state = initialState.setIn(['blog', 'articles'], fromJS(articles));

    it('should select selected style', () => {
      expect(selectImage(state)).to.equal('test.png');
    });
  });
});
