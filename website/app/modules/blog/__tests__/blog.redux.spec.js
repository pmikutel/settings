import { expect } from 'chai';
import { Record, fromJS } from 'immutable';

import { reducer, BlogActions } from '../blog.redux';

describe('Blog: redux', () => {
  const BlogRecord = new Record({
    isFetching: false,
    articles: [],
    error: null,
  });

  const state = new BlogRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on REQUEST', () => {
      const expectedState = new BlogRecord().set('isFetching', true);

      expect(reducer(state, BlogActions.request()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SUCCESS', () => {
      const articles = [{ originalSrc: 'test.jpg' }];

      const expectedState = new BlogRecord()
        .set('isFetching', false)
        .set('articles', fromJS(articles));

      expect(reducer(state, BlogActions.success(articles)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new BlogRecord()
        .set('isFetching', false)
        .set('error', fromJS(error));

      expect(reducer(state, BlogActions.failure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
