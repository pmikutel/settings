import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { Map, Record } from 'immutable';

import { BlogActions, BlogTypes } from '../blog.redux';
import blogSaga from '../blog.sagas';
import { shopifyApi } from '../../../services/shopify/api';

describe('Blog: saga', () => {
  const BlogRecord = new Record({
    isFetching: false,
    data: Map(),
    error: null,
  });

  const getSagaTester = ({ blog = new BlogRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: {
        blog,
      },
    });

    sagaTester.start(blogSaga);
    return sagaTester;
  };

  it('should fetch blog and dispatch successful action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockImplementation(() => Promise.resolve(
      { data: { data: { shop: { blogs: { edges: [{ node: { articles: { edges: [{ title: 'title' }] } } }] } } } } },
    ));

    sagaTester.dispatch(BlogActions.request(''));

    const action = await sagaTester.waitFor(BlogTypes.SUCCESS);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch blog and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockReturnValue(Promise.resolve({
      data: {
        errors: ['error'],
      },
    }));


    sagaTester.dispatch(BlogActions.request(''));

    const action = await sagaTester.waitFor(BlogTypes.FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });
});
