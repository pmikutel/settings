import { combineReducers } from 'redux-immutable';

import { reducer as form } from 'redux-form/immutable';
import { reducer as route } from './router/router.redux';
import { reducer as locales } from './locales/locales.redux';
import { reducer as home } from './home/home.redux';
import { reducer as products } from './products/products.redux';
import { reducer as product } from './product/product.redux';
import { reducer as modals } from './modals/modals.redux';
import { reducer as user, UserRecord } from './user/user.redux';
import { reducer as register } from './register/register.redux';
import { reducer as step1, Step1Record } from './step1/step1.redux';
import { reducer as step2, Step2Record } from './step2/step2.redux';
import { reducer as checkoutDetails, CheckoutDetailsRecord } from './checkoutDetails/checkoutDetails.redux';
import { reducer as cart, CartRecord } from './cart/cart.redux';
import { reducer as checkout } from './checkout/checkout.redux';
import { reducer as blog } from './blog/blog.redux';
import { reducer as collections } from './collections/collections.redux';
import { reducer as productTypes } from './productTypes/productTypes.redux';
import { reducer as resetPassword } from './resetPassword/resetPassword.redux';

export const records = [Step1Record, Step2Record, CartRecord, UserRecord, CheckoutDetailsRecord];

export default function createReducer() {
  return combineReducers({
    route,
    locales,
    home,
    products,
    product,
    form,
    modals,
    register,
    step1,
    step2,
    checkoutDetails,
    cart,
    user,
    checkout,
    collections,
    productTypes,
    blog,
    resetPassword,
  });
}
