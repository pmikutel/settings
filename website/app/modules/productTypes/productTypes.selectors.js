import { createSelector } from 'reselect';

const selectProductTypesDomain = state => state.get('productTypes');

export const selectProductTypes = createSelector(
  selectProductTypesDomain,
  state => state.get('items'),
);
