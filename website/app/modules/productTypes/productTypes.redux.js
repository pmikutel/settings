import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS, Map } from 'immutable';

export const { Types: ProductTypesTypes, Creators: ProductTypesActions } = createActions({
  request: [''],
  success: ['data'],
  failure: ['error'],
}, { prefix: 'PRODUCT_TYPES_' });

const ProductTypesRecord = new Record({
  isFetching: false,
  items: Map(),
  error: null,
});

export const INITIAL_STATE = new ProductTypesRecord({});

const request = (state = INITIAL_STATE) => state
  .set('isFetching', true);

const success = (state = INITIAL_STATE, { data }) => state
  .set('isFetching', false)
  .set('items', fromJS(data));

const failure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));

export const reducer = createReducer(INITIAL_STATE, {
  [ProductTypesTypes.REQUEST]: request,
  [ProductTypesTypes.SUCCESS]: success,
  [ProductTypesTypes.FAILURE]: failure,
});
