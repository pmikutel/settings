import { takeLatest, put } from 'redux-saga/effects';
import reportError from 'report-error';
import { path } from 'ramda';

import { ProductTypesTypes, ProductTypesActions } from './productTypes.redux';
import { hasErrors, shopifyApi } from '../../services/shopify/api';

export function* request({ count = 100 }) {
  try {
    const response = yield shopifyApi.post('/', `{
      shop {
        productTypes(first: ${count}) {
          edges {
            node
          }
        }
      }
    }`);
    if (hasErrors(response)) {
      yield put(ProductTypesActions.failure(response.data));
      return;
    }

    const result = path(['data', 'data', 'shop', 'productTypes', 'edges'], response);

    yield put(ProductTypesActions.success(result));
  } catch (e) {
    yield put(ProductTypesActions.failure(e));
    yield reportError(e);
  }
}


export default function* collections() {
  yield takeLatest(ProductTypesTypes.REQUEST, request);
}
