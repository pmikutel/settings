import { expect } from 'chai';
import { fromJS, Map, Record } from 'immutable';

import { selectProductTypes } from '../productTypes.selectors';

describe('ProductTypes: selectors', () => {
  const ProductTypesRecord = new Record({
    items: Map(),
  });

  const initialState = fromJS({
    product: new ProductTypesRecord(),
  });

  describe('selectProductTypes', () => {
    const items = {
      item1: { id: 'item1', title: 'title' },
      item2: { id: 'item2', title: 'title 2' },
    };

    const state = initialState.setIn(['productTypes', 'items'], fromJS(items));

    it('should select productTypes items', () => {
      expect(selectProductTypes(state).toJS()).to.deep.equal(items);
    });
  });
});
