import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { Map, Record } from 'immutable';

import { ProductTypesActions, ProductTypesTypes } from '../productTypes.redux';
import productTypesSaga from '../productTypes.sagas';
import { shopifyApi } from '../../../services/shopify/api';

describe('ProductTypes: saga', () => {
  const ProductTypesRecord = new Record({
    isFetching: false,
    items: Map(),
    error: null,
  });

  const getSagaTester = ({ productTypes = new ProductTypesRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: {
        productTypes,
      },
    });

    sagaTester.start(productTypesSaga);
    return sagaTester;
  };

  it('should fetch productTypes and dispatch successful action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockReturnValue(Promise.resolve({
      data: {
        data: {
          shop: {
            productTypes: {
              edges: [],
            },
          },
        },
      },
    }));

    sagaTester.dispatch(ProductTypesActions.request());

    const action = await sagaTester.waitFor(ProductTypesTypes.SUCCESS);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });

  it('should fetch productTypes and dispatch failure action', async () => {
    const sagaTester = getSagaTester({});

    shopifyApi.post.mockReturnValue(Promise.resolve({
      data: {
        errors: ['error'],
      },
    }));

    sagaTester.dispatch(ProductTypesActions.request());

    const action = await sagaTester.waitFor(ProductTypesTypes.FAILURE);

    expect(sagaTester.getCalledActions()).to.contains(action);
  });
});
