import { expect } from 'chai';
import { Record, fromJS, Map } from 'immutable';

import { reducer, ProductTypesActions } from '../productTypes.redux';

describe('ProductTypes: redux', () => {
  const ProductTypesRecord = new Record({
    isFetching: false,
    items: Map(),
    error: null,
  });

  const state = new ProductTypesRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on REQUEST', () => {
      const expectedState = new ProductTypesRecord().set('isFetching', true);

      expect(reducer(state, ProductTypesActions.request()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SUCCESS', () => {
      const items = [{ id: 'item1' }, { id: 'item2' }];

      const expectedState = new ProductTypesRecord()
        .set('isFetching', false)
        .set('items', items);

      expect(reducer(state, ProductTypesActions.success(items)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new ProductTypesRecord()
        .set('isFetching', false)
        .set('error', fromJS(error));

      expect(reducer(state, ProductTypesActions.failure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
