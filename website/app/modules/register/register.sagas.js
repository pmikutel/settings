import { takeLatest, put, select } from 'redux-saga/effects';
import reportError from 'report-error';
import { pipe, path, isEmpty, either, complement } from 'ramda';
import { push } from 'react-router-redux';

import { RegisterTypes, RegisterActions } from './register.redux';
import { UserActions } from '../user/user.redux';
import { selectIsPurchaseFlow } from '../checkout/checkout.selectors';
import { shopifyApi, hasErrors } from '../../services/shopify/api';

const hasCustomerErrors = pipe(
  path(['data', 'data', 'customerCreate', 'userErrors']),
  complement(isEmpty),
);

export function* request({ data, url }) {
  try {
    let createProject = true;
    const isPurchaseFlow = yield select(selectIsPurchaseFlow);
    const response = yield shopifyApi.post('/', `mutation {
      customerCreate(input: {
        firstName: "${data.get('fullName')}",
        email: "${data.get('email')}",
        password: "${data.get('password')}",
        lastName: "${data.get('companyName')}",
      }) {
        userErrors {
          field
          message
        }
        customer {
          id
        }
      }
    }`);

    if (either(hasErrors, hasCustomerErrors)(response)) {
      yield put(RegisterActions.failure(response.data));
      return;
    }

    url = isPurchaseFlow ? '/purchase/start' : url;

    if (url !== '/project/board') {
      yield put(push(url));
      createProject = false;
    }

    yield put(UserActions.loginRequest(data, isPurchaseFlow));
    yield put(RegisterActions.success(createProject));
  } catch (e) {
    yield reportError(e);
  }
}


export default function* registerSaga() {
  yield takeLatest(RegisterTypes.REQUEST, request);
}
