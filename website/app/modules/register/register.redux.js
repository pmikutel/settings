import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS } from 'immutable';

export const { Types: RegisterTypes, Creators: RegisterActions } = createActions({
  request: ['data', 'url'],
  success: ['shouldCreateProject'],
  failure: ['error'],
  clearError: [],
  resetShouldCreateProject: [],
}, { prefix: 'REGISTER_' });

export const RegisterRecord = new Record({
  isFetching: false,
  error: null,
  createProject: false,
}, 'register');

export const INITIAL_STATE = new RegisterRecord({});

const request = (state = INITIAL_STATE) => state
  .set('isFetching', true)
  .set('error', null);

const success = (state = INITIAL_STATE, { shouldCreateProject = false }) => state
  .set('isFetching', false)
  .set('createProject', shouldCreateProject);

const failure = (state = INITIAL_STATE, { error }) => state
  .set('isFetching', false)
  .set('error', fromJS(error));

const clearError = (state = INITIAL_STATE) => state
  .set('error', null);

const resetShouldCreateProject = (state = INITIAL_STATE) => state
  .set('createProject', false);

export const reducer = createReducer(INITIAL_STATE, {
  [RegisterTypes.REQUEST]: request,
  [RegisterTypes.SUCCESS]: success,
  [RegisterTypes.FAILURE]: failure,
  [RegisterTypes.CLEAR_ERROR]: clearError,
  [RegisterTypes.RESET_SHOULD_CREATE_PROJECT]: resetShouldCreateProject,
});
