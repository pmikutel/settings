import { createSelector } from 'reselect';
import { complement, isNil } from 'ramda';

const selectRegisterDomain = state => state.get('register');

export const selectRegisterProcessing = createSelector(
  selectRegisterDomain,
  state => state.get('isFetching'),
);

export const selectRegisterError = createSelector(
  selectRegisterDomain,
  state => complement(isNil)(state.get('error')),
);

export const selectCreateProject = createSelector(
  selectRegisterDomain,
  state => state.get('createProject')
);
