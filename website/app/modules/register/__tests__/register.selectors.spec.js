import { expect } from 'chai';
import { fromJS, Record } from 'immutable';

import {
  selectRegisterError,
} from '../register.selectors';

describe('Register: selectors', () => {
  const RegisterRecord = new Record({
    error: null,
  });

  const initialState = fromJS({
    register: new RegisterRecord(),
  });

  describe('selectRegisterError', () => {
    it('should select register error as false if it is empty', () => {
      expect(selectRegisterError(initialState)).to.equal(false);
    });

    it('should select register error as true if it is not empty', () => {
      const state = initialState.setIn(['register', 'error'], 'dummy error');

      expect(selectRegisterError(state)).to.equal(true);
    });
  });
});
