import { expect } from 'chai';
import { Record, fromJS } from 'immutable';

import { reducer, RegisterActions } from '../register.redux';

describe('Register: redux', () => {
  const RegisterRecord = new Record({
    isFetching: false,
    error: null,
    createProject: false,
  });

  const state = new RegisterRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on REQUEST', () => {
      const expectedState = new RegisterRecord()
        .set('isFetching', true)
        .set('error', null);

      expect(reducer(state, RegisterActions.request()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on SUCCESS', () => {
      const expectedState = new RegisterRecord()
        .set('isFetching', false);

      expect(reducer(state, RegisterActions.success()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on FAILURE', () => {
      const error = 'error';

      const expectedState = new RegisterRecord()
        .set('isFetching', false)
        .set('error', fromJS(error));

      expect(reducer(state, RegisterActions.failure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should clear error on CLEAR_ERROR', () => {
      const error = 'error';

      const previousState = new RegisterRecord()
        .set('error', fromJS(error));

      const expectedState = new RegisterRecord()
        .set('error', null);

      expect(reducer(previousState, RegisterActions.clearError()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should reset create project on RESET_SHOULD_CREATE_PROJECT', () => {
      const createProject = true;

      const previousState = new RegisterRecord()
        .set('createProject', fromJS(createProject));

      const expectedState = new RegisterRecord()
        .set('createProject', false);

      expect(reducer(previousState, RegisterActions.resetShouldCreateProject()).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
