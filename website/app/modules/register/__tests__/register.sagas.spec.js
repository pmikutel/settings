import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { Record, fromJS } from 'immutable';
import { CALL_HISTORY_METHOD } from 'react-router-redux';
import { identity } from 'ramda';

import { RegisterActions, RegisterTypes } from '../register.redux';
import { UserActions, UserTypes } from '../../user/user.redux';
import registerSaga from '../register.sagas';
import { shopifyApi } from '../../../services/shopify/api';

describe('Register: saga', () => {
  const RegisterRecord = new Record({
    isFetching: false,
    error: null,
  });

  const CheckoutRecord = new Record({
    isPurchaseFlow: false,
  });

  const getSagaTester = ({ register = new RegisterRecord(), checkout = new CheckoutRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: fromJS({
        register,
        checkout,
      }),
      reducers: identity,
    });

    sagaTester.start(registerSaga);
    return sagaTester;
  };

  describe('request', () => {
    const customer = fromJS({
      email: 'test-email',
      password: 'test-password',
      firstName: 'test-full-name',
      lastName: 'test-company-name',
    });

    const url = '/';

    it('should fetch register data and dispatch successful action', async () => {
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerCreate: {
              userErrors: [],
            },
          },
        },
      }));

      sagaTester.dispatch(RegisterActions.request(customer, url));

      const action = await sagaTester.waitFor(RegisterTypes.SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch push to url location', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(RegisterActions.request(customer, url));

      const action = await sagaTester.waitFor(CALL_HISTORY_METHOD);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should login user', async () => {
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerCreate: {
              userErrors: [],
            },
          },
        },
      }));

      sagaTester.dispatch(UserActions.loginRequest(customer, url));

      const action = await sagaTester.waitFor(UserTypes.LOGIN_REQUEST);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should fetch register data and dispatch failure action when errors are defined', async () => {
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          errors: ['error'],
          data: {
            customerCreate: {
              userErrors: [],
            },
          },
        },
      }));

      sagaTester.dispatch(RegisterActions.request(customer, url));

      const action = await sagaTester.waitFor(RegisterTypes.FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should fetch register data and dispatch failure action when user errors are defined', async () => {
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerCreate: {
              userErrors: ['error'],
            },
          },
        },
      }));

      sagaTester.dispatch(RegisterActions.request(customer, url));

      const action = await sagaTester.waitFor(RegisterTypes.FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });
});
