import { createSelector } from 'reselect';

const selectUserDomain = state => state.get('resetPassword');

export const selectRemindError = createSelector(
  selectUserDomain,
  state => state.getIn(['remind', 'error']),
);

export const selectResetError = createSelector(
  selectUserDomain,
  state => state.getIn(['reset', 'error']),
);

export const selectRemindSuccess = createSelector(
  selectUserDomain,
  state => state.getIn(['remind', 'success']),
);
