import SagaTester from 'redux-saga-tester';
import { expect } from 'chai';
import { Record, fromJS } from 'immutable';
import { identity } from 'ramda';
import { CALL_HISTORY_METHOD } from 'react-router-redux';

import { ResetPasswordActions, ResetPasswordTypes } from '../resetPassword.redux';
import { ModalsTypes } from '../../modals/modals.redux';
import registerSaga from '../resetPassword.sagas';
import { shopifyApi } from '../../../services/shopify/api';

describe('ResetPassword: saga', () => {
  const ResetPasswordRecord = new Record({
    remind: fromJS({
      isFetching: false,
      success: false,
      error: '',
    }),
    reset: fromJS({
      isFetching: false,
      success: false,
      error: false,
    }),
  });

  const getSagaTester = ({ resetPassword = new ResetPasswordRecord() }) => {
    const sagaTester = new SagaTester({
      initialState: fromJS({
        resetPassword,
      }),
      reducers: identity,
    });

    sagaTester.start(registerSaga);
    return sagaTester;
  };

  describe('remindRequest', () => {
    it('should dispatch remindSuccess action when remind is successful', async () => {
      const data = fromJS({ email: 'email' });
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerRecover: {
              userErrors: {},
            },
          },
        },
      }));

      sagaTester.dispatch(ResetPasswordActions.remindRequest(data));

      const action = await sagaTester.waitFor(ResetPasswordTypes.REMIND_SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch remindFailure action when remind have errors', async () => {
      const data = fromJS({ email: 'email' });
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          errors: [
            {
              message: 'message',
            },
          ],
        },
      }));

      sagaTester.dispatch(ResetPasswordActions.remindRequest(data));

      const action = await sagaTester.waitFor(ResetPasswordTypes.REMIND_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch remindFailure action when remind have user errors', async () => {
      const data = fromJS({ email: 'email' });
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerRecover: {
              userErrors: [{
                message: 'message',
              }],
            },
          },
        },
      }));

      sagaTester.dispatch(ResetPasswordActions.remindRequest(data));

      const action = await sagaTester.waitFor(ResetPasswordTypes.REMIND_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('resetRequest', () => {
    it('should dispatch resetSuccess action when remind is successful', async () => {
      const data = fromJS({ password: 'password' });
      const id = 'id';
      const token = 'token';
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerReset: {
              userErrors: {},
            },
          },
        },
      }));

      sagaTester.dispatch(ResetPasswordActions.resetRequest(data, id, token));

      const action = await sagaTester.waitFor(ResetPasswordTypes.RESET_SUCCESS);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch resetFailure action when remind have errors', async () => {
      const data = fromJS({ password: 'password' });
      const id = 'id';
      const token = 'token';
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          errors: [
            {
              message: 'message',
            },
          ],
        },
      }));

      sagaTester.dispatch(ResetPasswordActions.remindRequest(data, id, token));

      const action = await sagaTester.waitFor(ResetPasswordTypes.REMIND_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch resetFailure action when remind have user errors', async () => {
      const data = fromJS({ password: 'password' });
      const id = 'id';
      const token = 'token';
      const sagaTester = getSagaTester({});

      shopifyApi.post.mockReturnValue(Promise.resolve({
        data: {
          data: {
            customerRecover: {
              userErrors: [{
                message: 'message',
              }],
            },
          },
        },
      }));

      sagaTester.dispatch(ResetPasswordActions.remindRequest(data, id, token));

      const action = await sagaTester.waitFor(ResetPasswordTypes.REMIND_FAILURE);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });

  describe('resetSuccess', () => {
    it('should dispatch push to home location', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(ResetPasswordActions.resetSuccess());

      const action = await sagaTester.waitFor(CALL_HISTORY_METHOD);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });

    it('should dispatch open login modal', async () => {
      const sagaTester = getSagaTester({});

      sagaTester.dispatch(ResetPasswordActions.resetSuccess());

      const action = await sagaTester.waitFor(ModalsTypes.OPEN_MODAL);

      expect(sagaTester.getCalledActions()).to.contains(action);
    });
  });
});
