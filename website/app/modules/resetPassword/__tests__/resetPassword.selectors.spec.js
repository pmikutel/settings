import { expect } from 'chai';
import { fromJS, Record } from 'immutable';

import {
  selectRemindError,
  selectRemindSuccess,
  selectResetError,
} from '../resetPassword.selectors';


describe('ResetPassword: selectors', () => {
  const ResetPasswordRecord = new Record({
    remind: fromJS({
      isFetching: false,
      success: false,
      error: '',
    }),
    reset: fromJS({
      isFetching: false,
      success: false,
      error: false,
    }),
  });

  const initialState = fromJS({
    resetPassword: new ResetPasswordRecord(),
  });

  describe('selectRemindError', () => {
    it('should select remind error', () => {
      const error = true;
      const state = initialState.setIn(['resetPassword', 'remind', 'error'], error);

      expect(selectRemindError(state)).to.deep.equal(error);
    });
  });

  describe('selectRemindSuccess', () => {
    it('should select remind success', () => {
      const success = true;
      const state = initialState.setIn(['resetPassword', 'remind', 'success'], success);

      expect(selectRemindSuccess(state)).to.deep.equal(success);
    });
  });

  describe('selectResetError', () => {
    it('should select reset error', () => {
      const error = true;
      const state = initialState.setIn(['resetPassword', 'reset', 'error'], error);

      expect(selectResetError(state)).to.deep.equal(error);
    });
  });
});
