import { expect } from 'chai';
import { Record, fromJS } from 'immutable';

import { reducer, ResetPasswordActions } from '../resetPassword.redux';

describe('ResetPassword: redux', () => {
  const ResetPasswordRecord = new Record({
    remind: fromJS({
      isFetching: false,
      success: false,
      error: '',
    }),
    reset: fromJS({
      isFetching: false,
      success: false,
      error: false,
    }),
  });

  const state = new ResetPasswordRecord();

  describe('reducer', () => {
    it('should return initial state', () => {
      expect(reducer(undefined, {}).toJS()).to.deep.equal(state.toJS());
    });

    it('should set data on REMIND_REQUEST', () => {
      const expectedState = new ResetPasswordRecord()
        .setIn(['remind', 'isFetching'], true)
        .setIn(['remind', 'error'], '');

      expect(reducer(state, ResetPasswordActions.remindRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on REMIND_SUCCESS', () => {
      const remindData = { prop: 'value' };

      const expectedState = new ResetPasswordRecord()
        .setIn(['remind', 'isFetching'], false)
        .setIn(['remind', 'success'], true);

      expect(reducer(state, ResetPasswordActions.remindSuccess(remindData)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on REMIND_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new ResetPasswordRecord()
        .setIn(['remind', 'isFetching'], false)
        .setIn(['remind', 'error'], fromJS(error));

      expect(reducer(state, ResetPasswordActions.remindFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on REMIND_RESET', () => {
      const expectedState = new ResetPasswordRecord()
        .setIn(['remind', 'isFetching'], false)
        .setIn(['remind', 'error'], '');

      expect(reducer(state, ResetPasswordActions.remindReset()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on RESET_REQUEST', () => {
      const expectedState = new ResetPasswordRecord()
        .setIn(['reset', 'isFetching'], true)
        .setIn(['reset', 'error'], false);

      expect(reducer(state, ResetPasswordActions.resetRequest()).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on RESET_SUCCESS', () => {
      const remindData = { prop: 'value' };

      const expectedState = new ResetPasswordRecord()
        .setIn(['reset', 'isFetching'], false)
        .setIn(['reset', 'success'], true);

      expect(reducer(state, ResetPasswordActions.resetSuccess(remindData)).toJS())
        .to.deep.equal(expectedState.toJS());
    });

    it('should set data on RESET_FAILURE', () => {
      const error = { error: 'error' };

      const expectedState = new ResetPasswordRecord()
        .setIn(['reset', 'isFetching'], false)
        .setIn(['reset', 'error'], true);

      expect(reducer(state, ResetPasswordActions.resetFailure(error)).toJS())
        .to.deep.equal(expectedState.toJS());
    });
  });
});
