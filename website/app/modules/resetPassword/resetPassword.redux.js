import { createActions, createReducer } from 'reduxsauce';
import { Record, fromJS } from 'immutable';

export const { Types: ResetPasswordTypes, Creators: ResetPasswordActions } = createActions({
  remindRequest: ['data'],
  remindSuccess: [''],
  remindFailure: ['error'],
  remindReset: [''],
  resetRequest: ['data', 'id', 'token'],
  resetSuccess: [''],
  resetFailure: ['error'],
}, { prefix: 'RESET_PASSWORD_' });

export const ResetPasswordRecord = new Record({
  remind: fromJS({
    isFetching: false,
    success: false,
    error: '',
  }),
  reset: fromJS({
    isFetching: false,
    success: false,
    error: false,
  }),
}, 'resetPassword');

export const INITIAL_STATE = new ResetPasswordRecord({});

const remindRequest = (state = INITIAL_STATE) => state
  .setIn(['remind', 'isFetching'], true)
  .setIn(['remind', 'error'], '');

const remindSuccess = (state = INITIAL_STATE) => state
  .setIn(['remind', 'isFetching'], false)
  .setIn(['remind', 'success'], true);

const remindFailure = (state = INITIAL_STATE, { error }) => state
  .setIn(['remind', 'isFetching'], false)
  .setIn(['remind', 'error'], fromJS(error));

const remindReset = (state = INITIAL_STATE) => state
  .setIn(['remind', 'success'], false)
  .setIn(['remind', 'error'], '');

const resetRequest = (state = INITIAL_STATE) => state
  .setIn(['reset', 'isFetching'], true)
  .setIn(['reset', 'error'], false);

const resetSuccess = (state = INITIAL_STATE) => state
  .setIn(['reset', 'isFetching'], false)
  .setIn(['reset', 'success'], true);

const resetFailure = (state = INITIAL_STATE) => state
  .setIn(['reset', 'isFetching'], false)
  .setIn(['reset', 'error'], true);


export const reducer = createReducer(INITIAL_STATE, {
  [ResetPasswordTypes.REMIND_REQUEST]: remindRequest,
  [ResetPasswordTypes.REMIND_SUCCESS]: remindSuccess,
  [ResetPasswordTypes.REMIND_FAILURE]: remindFailure,
  [ResetPasswordTypes.REMIND_RESET]: remindReset,
  [ResetPasswordTypes.RESET_REQUEST]: resetRequest,
  [ResetPasswordTypes.RESET_SUCCESS]: resetSuccess,
  [ResetPasswordTypes.RESET_FAILURE]: resetFailure,
});
