import { takeLatest, put } from 'redux-saga/effects';
import reportError from 'report-error';
import { path } from 'ramda';
import { push } from 'react-router-redux';

import { ResetPasswordActions, ResetPasswordTypes } from './resetPassword.redux';
import { shopifyApi } from '../../services/shopify/api';
import { ModalsActions } from '../modals/modals.redux';

export function* remindRequest({ data }) {
  try {
    const response = yield shopifyApi.post('/', `mutation {
      customerRecover (email: "${data.get('email')}"){
        userErrors {
          field,
          message
        } 
      }
    }`);
    const error = path(['errors', 0, 'message'])(response.data);
    const userError = path(['data', 'customerRecover', 'userErrors', 0, 'message'])(response.data);

    if (error || userError) {
      yield put(ResetPasswordActions.remindFailure(error || userError));
      return;
    }

    yield put(ResetPasswordActions.remindSuccess(response));
  } catch (e) {
    yield put(ResetPasswordActions.remindFailure(e));
    yield reportError(e);
  }
}

export function* resetRequest({ data, id, token }) {
  try {
    const response = yield shopifyApi.post('/', `mutation {
      customerReset (id: "${id}", input: {
        resetToken: "${token}",
        password: "${data.get('password')}"
      }){
        userErrors {
          field,
          message
        }
        customer {
          id
        }
      }
    }`);

    const error = path(['errors', 0, 'message'])(response.data);
    const userError = path(['data', 'customerReset', 'userErrors', 0, 'message'])(response.data);

    if (error || userError) {
      yield put(ResetPasswordActions.resetFailure());
      return;
    }

    yield put(ResetPasswordActions.resetSuccess(response));
  } catch (e) {
    yield put(ResetPasswordActions.resetFailure(e));
    yield reportError(e);
  }
}

export function* resetSuccess() {
  try {
    yield put(ModalsActions.openModal('login'));
    yield put(push('/'));
  } catch (e) {
    yield reportError(e);
  }
}

export default function* resetPasswordSaga() {
  yield takeLatest(ResetPasswordTypes.REMIND_REQUEST, remindRequest);
  yield takeLatest(ResetPasswordTypes.RESET_REQUEST, resetRequest);
  yield takeLatest(ResetPasswordTypes.RESET_SUCCESS, resetSuccess);
}
