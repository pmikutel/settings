import { identity } from 'ramda';
require('es5-shim');
require('es5-shim/es5-sham');
//eslint-disable-next-line import/first
import BrowserDetection from './utils/browserDetection';

const detection = new BrowserDetection();

window.onload = () => {
  detection.checkSupport(identity, () => {
    document.documentElement.className += ' unsupported';
  });

  document.documentElement.className += ` device-${detection.deviceType}`;

  if (detection.isInAppBrowser) {
    document.documentElement.className += ' in-app-browser';
  }
};
