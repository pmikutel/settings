import React, { PureComponent } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import App from './app.container';
import Home from './home';
import User from './user';
import Project from './project';
import Products from './products';
import Purchase from './purchase';
import Terms from './terms';
import Privacy from './privacy';
import About from './about';
import NotFound from './notFound';
import Reset from './resetPassword';

export class RootContainer extends PureComponent {
  render() {
    return (
      <Switch>
        <Route path="/">
          <App>
            <Switch>
              <Route exact path="/" component={Home} />

              <Route path="/user" component={User} />
              <Route path="/project" component={Project} />
              <Route path="/products" component={Products} />
              <Route path="/purchase" component={Purchase} />
              <Route path="/reset" component={Reset} />

              <Route path="/about" component={About} />
              <Route path="/terms" component={Terms} />
              <Route path="/privacy" component={Privacy} />

              <Route component={NotFound} />
            </Switch>
          </App>
        </Route>
      </Switch>
    );
  }
}

export default withRouter(RootContainer);
