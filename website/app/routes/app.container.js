import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router-dom';

import { App } from './app.component';
import { selectLocalesLanguage } from '../modules/locales/locales.selectors';
import { selectModal, selectIsPurchaseFlow, selectIsProjectFlow } from '../modules/modals/modals.selectors';
import { LocalesActions } from '../modules/locales/locales.redux';
import { ModalsActions } from '../modules/modals/modals.redux';
import { UserActions } from '../modules/user/user.redux';
import { selectShowCartNotification } from '../modules/cart/cart.selectors';

const mapStateToProps = createStructuredSelector({
  language: selectLocalesLanguage,
  modal: selectModal,
  isPurchaseFlow: selectIsPurchaseFlow,
  isProjectFlow: selectIsProjectFlow,
  showCartNotification: selectShowCartNotification,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  setLanguage: LocalesActions.setLanguage,
  openModal: ModalsActions.openModal,
  closeModal: ModalsActions.closeModal,
  checkSession: UserActions.checkSession,
}, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
