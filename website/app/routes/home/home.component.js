import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { always, ifElse, prop } from 'ramda';

import Header from '../../components/header';
import { Footer } from '../../components/footer/footer.component';

import { FindInspiration } from './components/findInspiration/findInspiration.component';
import { Process } from './components/process/process.component';
import { BestSellers } from './components/bestSellers/bestSellers.component';
import { Video } from './components/video/video.component';
import { Newsletter } from '../../components/newsletter/newsletter.component';
import { HeroSlider } from '../../components/heroSlider/heroSlider.component';

import { sliderRowStyles } from './home.styles';
import { Grid, Components } from '../../theme';

export class Home extends PureComponent {
  static propTypes = {
    productRequest: PropTypes.func.isRequired,
    productsItems: PropTypes.object.isRequired,
    productsKeys: PropTypes.object.isRequired,
    handleNewsletterSubmit: PropTypes.func.isRequired,
    newsletterSuccess: PropTypes.bool.isRequired,
    newsletterError: PropTypes.bool.isRequired,
    newsletterFetching: PropTypes.bool.isRequired,
    newsletterReset: PropTypes.func.isRequired,
    addItem: PropTypes.func.isRequired,
    addProjectItem: PropTypes.func.isRequired,
    blogRequest: PropTypes.func.isRequired,
    articles: PropTypes.object.isRequired,
    projectId: PropTypes.string,
  };

  componentDidMount() {
    this.props.productRequest('', '', '', true, 'Shop our latest', 2);
    this.props.blogRequest('Home', 4);
  }

  renderBestsellers = () => ifElse(
    prop('size'),
    () =>
      <BestSellers
        items={this.props.productsItems}
        keys={this.props.productsKeys}
        addItem={this.props.addItem}
        addProjectItem={this.props.addProjectItem}
        projectId={this.props.projectId}
      />,
    always(null))(this.props.productsItems);

  render = () => (
    <div>
      <Helmet title="Homepage" />

      <Header />

      <Grid.Section>
        <Grid.Row extraStyles={sliderRowStyles}>
          <HeroSlider data={this.props.articles} />
        </Grid.Row>
        <FindInspiration />
      </Grid.Section>

      <Process />

      <Grid.Section>
        <Components.Dots
          width="50%"
          height="1888"
          top="0"
          mobileTop="0"
          mobileHeight="1511"
          mobileWidth="64%"
        />
        {this.renderBestsellers()}
        <Video />
        <Newsletter
          onSubmit={this.props.handleNewsletterSubmit}
          isSuccess={this.props.newsletterSuccess}
          isError={this.props.newsletterError}
          clearError={this.props.newsletterReset}
          isFetching={this.props.newsletterFetching}
        />
      </Grid.Section>

      <Footer />
    </div>
  );
}
