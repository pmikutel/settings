import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../../theme';

const wrapperDesktopStyles = css`
  padding: 250px 0 0;
  margin-bottom: 0;
`;

const wrapperWideStyles = css`
  margin-bottom: 200px;
`;

export const Wrapper = styled.section`
  position: relative;
  padding: 65px 20px 0 20px;
  margin-bottom: 65px;

  ${Media.desktop(wrapperDesktopStyles)}
  ${Media.desktopWide(wrapperWideStyles)}
`;

const titleDesktopStyles = css`
  margin-bottom: 0;
  line-height: 94px;
`;

export const Title = styled.h1`
  position: relative;
  z-index: 2;
  line-height: 33px;
  margin-bottom: 75px;

  ${Media.desktop(titleDesktopStyles)}
`;

const infoBoxDesktopStyles = css`
  line-height: 50px;
  max-width: 420px;
  margin: 125px 10px 40px 39%;
  padding: 0;
`;

export const InfoBox = styled.div`
  color: ${Variables.colorWarmGrey};
  font-size: 22px;
  line-height: 46px;
  margin: 0 auto 40px;
  text-align: left;
  padding-left: 16.66%;
  width: 100%;
  box-sizing: border-box;

  ${Media.desktop(infoBoxDesktopStyles)}
`;

export const descriptionDesktopStyles = css`
  margin-bottom: 120px;
`;

export const Description = styled.p`
  margin-bottom: 45px;
`;

export const MasonryGridWrapper = styled.div`
  position: relative;
  top: -35px;
`;
