import { defineMessages } from 'react-intl';

export default defineMessages({
  cta: {
    id: 'bestSellers.cta',
    defaultMessage: 'See all products',
  },
  description: {
    id: 'bestSellers.description',
    defaultMessage: 'Discover design led furniture, keep on top of our latest deals and whats on trend.',
  },
  title: {
    id: 'bestSellers.title',
    defaultMessage: 'SHOP OUR LATEST',
  },
});
