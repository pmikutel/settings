import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import messages from './bestSellers.messages';

import {
  Wrapper,
  Title,
  InfoBox,
  Description,
  MasonryGridWrapper,
} from './bestSellers.styles';

import { ProductItem } from '../../../../components/productItem/productItem.component';
import { MasonryGrid } from '../../../../components/masonryGrid/masonryGrid.component';
import { Components, Grid } from '../../../../theme';
import { decode } from '../../../../utils/shopifyUrls';
import { WindowScroll } from '../../../../components/windowScroll/windowScroll.component';


export class BestSellersComponent extends PureComponent {
  static propTypes = {
    items: PropTypes.object.isRequired,
    keys: PropTypes.object.isRequired,
    intl: PropTypes.object.isRequired,
    addItem: PropTypes.func.isRequired,
    addProjectItem: PropTypes.func.isRequired,
    projectId: PropTypes.string,
  };

  state = {
    scroll: {},
  };

  get items() {
    return this.props.keys.toJS().map(key => {
      const product = this.props.items.getIn(['products', key]);
      const image = this.props.items.getIn(['image', product.getIn(['images', 0])]);
      const firstVariant = product.getIn(['variants', 0]).toJS();

      return ({
        id: product.get('id'),
        title: product.get('title'),
        description: product.get('descriptionHtml'),
        image: image.get('src'),
        variant: firstVariant,
        variantId: decode(firstVariant.id),
        size: firstVariant.size,
        price: firstVariant.price,
        wide: firstVariant.wide,
        delivery: firstVariant.delivery,
        handle: product.get('handle'),
      });
    });
  }

  renderItems = () => this.items.map((item, index) => (
    <ProductItem
      arrayLength={this.items.length}
      key={index}
      index={index}
      id={item.id}
      image={item.image}
      title={item.title}
      description={item.description}
      variantId={item.variantId}
      size={item.size}
      price={item.price}
      compareAtPrice={item.variant.compareAtPrice}
      wide={item.wide}
      delivery={item.delivery}
      variantTitle={item.variant.title}
      productHandle={item.handle}
      url={`/products/${item.handle}/${item.id}/variant/${item.variantId}`}
      addCartItem={this.props.addItem}
      addProjectItem={this.props.addProjectItem}
      projectId={this.props.projectId}
      scroll={this.state.scroll}
    />
  ));

  render = () => (
    <Wrapper>
      <WindowScroll onScroll={(e) => this.setState({ scroll: e })} />
      <Grid.Column offset={3} columns={11} mobileOffset={1} mobileColumns={5}>
        <Title><FormattedMessage {...messages.title} /></Title>
      </Grid.Column>
      <MasonryGridWrapper>
        <MasonryGrid>
          <InfoBox>
            <Description><FormattedMessage {...messages.description} /></Description>
            <Components.ActionLink extraStyles={Components.smallButtonExtraStyles} to="/products/all">
              <FormattedMessage {...messages.cta} />
              <Components.Arrow />
            </Components.ActionLink>
          </InfoBox>

          {this.renderItems()}
        </MasonryGrid>
      </MasonryGridWrapper>
    </Wrapper>
  );
}

export const BestSellers = injectIntl(BestSellersComponent);
