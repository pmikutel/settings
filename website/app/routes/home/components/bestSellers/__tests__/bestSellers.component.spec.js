import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import 'jest-styled-components';
import { identity } from 'ramda';

import { intlMock } from '../../../../../utils/testing';
import { BestSellersComponent as BestSellers } from '../bestSellers.component';

describe('<BestSellers />', () => {
  const defaultProps = {
    keys: fromJS([]),
    items: fromJS({}),
    intl: intlMock(),
    addItem: identity,
    addProjectItem: identity,
  };

  const component = (props = {}) => <BestSellers {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
