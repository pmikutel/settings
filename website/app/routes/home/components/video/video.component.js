import React, { PureComponent } from 'react';
import YouTube from 'react-youtube';

import {
  Wrapper,
  VideoOverLay,
  PlayButton,
  PlayIcon,
  YouTubeWrapper,
} from './video.styles';

const VIDEO_ID = 'OjN-uFxPe7E';

export class Video extends PureComponent {
  state = {
    playerInstance: {},
    isOverlayVisible: true,
  };

  savePlayerInstance = (event) => {
    this.setState({ playerInstance: event.target });
  };

  playClip = () => {
    this.setState({ isOverlayVisible: false });
    this.state.playerInstance.playVideo();
  };

  render = () => {
    const opts = {
      height: '630px',
      width: '100%',
      playerVars: {
        autoplay: 0,
        showinfo: 0,
        rel: 0,
      },
    };

    return (
      <Wrapper>
        <VideoOverLay isVisible={this.state.isOverlayVisible}>
          <PlayButton onClick={this.playClip}>
            <PlayIcon />
          </PlayButton>
        </VideoOverLay>
        <YouTubeWrapper isVisible={this.state.isOverlayVisible}>
          <YouTube
            videoId={VIDEO_ID}
            opts={opts}
            onReady={this.savePlayerInstance}
          />
        </YouTubeWrapper>
      </Wrapper>
    );
  };
}


