import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Media } from '../../../../theme';
import Sprite from '../../../../utils/sprites';

export const PlayIcon = styled.i`
  display: block;

  ${Sprite.responsive('play')};
`;

export const PlayButton = styled.button`
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-47%) translateY(-50%);
`;

export const VideoOverLay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1;
  display: ${ifElse(propEq('isVisible', true), always('block'), always('none'))};
`;

export const YouTubeWrapper = styled.div`
  transition: filter 2s ease-in-out;
  filter: ${ifElse(propEq('isVisible', true), always('grayscale(100%)'), always('grayscale(0%)'))};
`;

const wrapperDesktopStyles = css`
  margin-bottom: 185px;
`;

export const Wrapper = styled.section`
  margin-bottom: 160px;
  position: relative;

  ${Media.desktop(wrapperDesktopStyles)}
`;
