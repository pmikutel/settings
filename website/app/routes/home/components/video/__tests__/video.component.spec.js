import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';

import { Video } from '../video.component';

describe('<Video />', () => {
  const component = (props = {}) => <Video {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
