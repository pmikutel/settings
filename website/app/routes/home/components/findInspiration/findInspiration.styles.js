import styled, { css } from 'styled-components';
import { prop } from 'ramda';
import { Media, Variables } from '../../../../theme';

const wrapperDesktopStyles = css`
  padding: 0;
  margin-bottom: 0;
`;

export const Wrapper = styled.section`
  position: relative;
  margin-bottom: 65px;
  z-index: 1;

  ${Media.desktop(wrapperDesktopStyles)}
`;

const titleDesktopStyles = css`
  margin-bottom: 0;
  line-height: 94px;
`;

export const Title = styled.h1`
  position: relative;
  z-index: 1;
  line-height: 33px;
  margin-bottom: 75px;

  ${Media.desktop(titleDesktopStyles)}
`;

const infoBoxDesktopStyles = css`
  line-height: 50px;
  max-width: 420px;
  margin: 125px 10px 40px 40%;
  padding: 0;
`;

export const InfoBox = styled.div`
  color: ${Variables.colorWarmGrey};
  font-size: 22px;
  line-height: 46px;
  text-align: left;
  padding-left: 16.66%;
  width: calc(100% - 32px);
  box-sizing: border-box;
  margin: 80px auto 50px;

  ${Media.desktop(infoBoxDesktopStyles)}
`;

export const descriptionDesktopStyles = css`
  margin-bottom: 120px;
`;

export const Description = styled.p`
  margin-bottom: 45px;
`;

export const MasonryGridWrapper = styled.div`
  position: relative;
  top: -35px;
`;

export const SliderWrapper = styled.div`
  height: 500px;
`;

export const Image = styled.div`
  background: url("${prop('src')}") center;
  background-size: cover;
  height: 500px;
`;
