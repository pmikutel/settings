import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import { ifElse, prop } from 'ramda';
import { MainSlider } from '../../../../components/slider/slider.component';

import messages from './findInspiration.messages';
import { WindowResize } from '../../../../components/windowResize/windowResize.component';

import {
  Wrapper,
  Title,
  InfoBox,
  Description,
  MasonryGridWrapper,
  SliderWrapper,
  Image,
} from './findInspiration.styles';

import { MasonryGrid } from '../../../../components/masonryGrid/masonryGrid.component';
import { Components, Grid } from '../../../../theme';
import { ProductImage } from './components/productImage.component';

import industrialWideImage from './assets/industrial-wide.png';
import industrialImage from './assets/industrial.png';
import japaneseImage from './assets/japanese.png';
import minimalImage from './assets/minimal.png';

const arrayImages = [industrialWideImage, industrialImage, japaneseImage, minimalImage];

const MOBILE_BREAKPOINT = 1024;

export class FindInspirationComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
  };

  state = {
    mobile: window.innerWidth < MOBILE_BREAKPOINT,
  };

  onResizeHandler = (evt) => {
    this.setState({
      mobile: evt.currentTarget.innerWidth < MOBILE_BREAKPOINT,
    });
  };

  renderGrid = () => ifElse(
    prop('mobile'),
    () =>
      <MasonryGridWrapper>
        <SliderWrapper>
          <MainSlider inspire noAutoPlay>
            {this.renderMobileItems()}
          </MainSlider>
        </SliderWrapper>
        {this.renderInfoBox()}
      </MasonryGridWrapper>,
    () =>
      <MasonryGridWrapper>
        <MasonryGrid nrOfNonImageElements={1}>
          {this.renderInfoBox()}
          <ProductImage id={1} src={japaneseImage} />
          <ProductImage id={2} src={industrialWideImage} />
          <ProductImage id={3} src={minimalImage} />
          <ProductImage id={4} src={industrialImage} />
        </MasonryGrid>
      </MasonryGridWrapper>,
  )(this.state);

  renderInfoBox = () => (
    <InfoBox>
      <Description><FormattedMessage {...messages.description} /></Description>
      <Components.ActionLink extraStyles={Components.smallButtonExtraStyles} to="/project/step1">
        <FormattedMessage {...messages.cta} />
        <Components.Arrow />
      </Components.ActionLink>
    </InfoBox>
  );

  renderMobileItems = () => arrayImages.map((item, index) => <Image key={index} id={index} src={item} />);

  render = () => (
    <Wrapper>
      <WindowResize onResize={this.onResizeHandler} />
      <Grid.Row>
        <Grid.Column offset={3} columns={11} mobileOffset={1} mobileColumns={5}>
          <Title><FormattedMessage {...messages.title} /></Title>
        </Grid.Column>
      </Grid.Row>
      {this.renderGrid()}
    </Wrapper>
  );
}

export const FindInspiration = injectIntl(FindInspirationComponent);


