import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'findInspiration.title',
    defaultMessage: 'We inspire the brave',
  },
  description: {
    id: 'findInspiration.description',
    defaultMessage: 'Explore our styles, tells us what inspires you and start building your project with us.',
  },
  cta: {
    id: 'findInspiration.cta',
    defaultMessage: 'Find inspiration',
  },
});
