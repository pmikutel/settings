import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { ProductImage } from '../productImage.component';

describe('<ProductImage />', () => {
  const defaultProps = {
    src: 'test.jpg',
  };

  const component = (props = {}) => <ProductImage {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
