import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Media } from '../../../../../theme';

const tabletImageWrapperStyles = css`
  padding: 0 10px;
  text-align: ${ifElse(propEq('isLeft', false), always('left'), always('right'))};
  margin-top: 0;
`;

export const ImageWrapper = styled.div`
  padding: ${ifElse(propEq('isEven', false), always('0 0 0 37px'), always('0 37px 0 0'))};
  margin-top: -80px;
  opacity: 0;
  ${Media.tablet(tabletImageWrapperStyles)};
`;

const imageTabletStyles = css`
  margin-top: 0;
  width: auto;
  max-width: 350px;
`;

const imageDesktopStyles = css`
  margin-top: 0;
  width: auto;
  max-width: 460px;
`;

const imageDesktopWideStyles = css`
  margin-top: 0;
  width: auto;
  max-width: 100%;
`;

export const Image = styled.img`
  position: relative;
  width: 100%;

  ${Media.tablet(imageTabletStyles)}
  ${Media.desktop(imageDesktopStyles)}
  ${Media.desktopWide(imageDesktopWideStyles)}
`;
