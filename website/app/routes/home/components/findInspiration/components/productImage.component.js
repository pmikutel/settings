import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';

import { ImageWrapper, Image } from './productImage.styles';
import { animateItemImage } from '../../../../../utils/animations';

export class ProductImage extends PureComponent {
  static propTypes = {
    src: PropTypes.string.isRequired,
    id: PropTypes.any,
    isLeft: PropTypes.any,
  };

  state = {
    animated: false,
    initialized: false,
  };

  componentDidMount() {
    this.setState({ initialized: true });
  }

  onVisibleHandler = (isVisible) => {
    if (this.state.initialized && isVisible) {
      animateItemImage(this.itemElement, 1, 0, () => {
        this.setState({ animated: true });
      });
    }
  };

  handleItemRef = (ref) => (this.itemElement = ref);

  isEven = () => this.props.id % 2 === 0;

  itemElement = null;

  render = () => {
    const isEven = this.isEven();
    const isLeft = this.props.isLeft;
    return (
      <ImageWrapper innerRef={this.handleItemRef} isEven={isEven} isLeft={isLeft}>
        <VisibilitySensor onChange={this.onVisibleHandler} active={!this.state.animated} />

        <Image src={this.props.src} />
      </ImageWrapper>
    );
  };
}
