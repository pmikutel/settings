import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';

import { intlMock } from '../../../../../utils/testing';
import { FindInspirationComponent as FindInspiration } from '../findInspiration.component';

describe('<FindInspiration />', () => {
  const defaultProps = {
    intl: intlMock(),
  };

  const component = (props = {}) => <FindInspiration {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
