import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'process.title',
    defaultMessage: 'We Partner with you',
  },
  installTitle: {
    id: 'process.installTitle',
    defaultMessage: 'install',
  },
  costTitle: {
    id: 'process.costTitle',
    defaultMessage: 'cost',
  },
  planTitle: {
    id: 'process.planTitle',
    defaultMessage: 'plan',
  },
  inspireTitle: {
    id: 'process.inspireTitle',
    defaultMessage: 'inspire',
  },
  inspire: {
    id: 'process.inspire',
    // eslint-disable-next-line
    defaultMessage: 'Getting inspired is the first step to creating an amazing workspace. Select the right products to express your unique style.',
  },
  cost: {
    id: 'process.cost',
    // eslint-disable-next-line
    defaultMessage: 'Beautiful furniture from the manufacturers to your office with no hidden costs, tailored to your budget and your requirements.',
  },
  plan: {
    id: 'process.plan',
    // eslint-disable-next-line
    defaultMessage: 'Our team of talented space planners and curators can assess your space and help you bring it to life.',
  },
  install: {
    id: 'process.install',
    // eslint-disable-next-line
    defaultMessage: 'Making sure your furniture is a perfect fit for you. Arriving on schedule, at a time that is convenient for you.',
  },
});
