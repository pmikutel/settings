import styled, { css } from 'styled-components';
import { ifElse, always, propEq } from 'ramda';
import { Media, Variables } from '../../../../theme';

const wrapperDesktopStyles = css`
  margin-top: -235px;
`;

export const Wrapper = styled.section`
  background-image: linear-gradient(180deg, #fff104, ${Variables.colorBarbiePink});
  color: ${Variables.colorPureWhite};
  position: relative;

  ${Media.desktop(wrapperDesktopStyles)}
`;

const titleDesktopStyles = css`
  line-height: 94px;
  margin-bottom: 230px;
  letter-spacing: 33px;
  max-width: 1075px;
`;

export const Title = styled.h1`
  position: relative;
  text-transform: uppercase;
  letter-spacing: 16px;
  line-height: 33px;
  margin-bottom: 105px;

  ${Media.desktop(titleDesktopStyles)}
`;

const processWrapperDesktopStyles = css`
  padding: 360px 0 180px;
`;

export const processWrapperStyles = css`
  padding: 105px 15px 5px 0;

  ${Media.desktop(processWrapperDesktopStyles)}
`;

const processStepDesktopStyles = css`
  flex-direction: row;
  margin-bottom: 230px;
`;

export const processStepStyles = css`
  position: relative;
  margin-bottom: 150px;
  flex-direction: column;

  ${Media.desktop(processStepDesktopStyles)}
`;

const stepInfoDesktopStyles = css`
  order: 0;
`;

export const stepInfoStyles = css`
  position: relative;
  order: 1;

  ${Media.desktop(stepInfoDesktopStyles)}
`;

const stepCounterDesktopStyles = css`
  font-size: 500px;
  top: -50px;
  left: -50px;
`;

export const StepCounter = styled.span`
  font-size: 300px;
  opacity: 0.2;
  letter-spacing: -4px;
  position: absolute;
  top: -220px;
  left: 0;

  ${Media.desktop(stepCounterDesktopStyles)}
`;

const stepTitleDesktopStyles = css`
  letter-spacing: 24px;
`;

export const StepTitle = styled.h2`
  position: relative;
  top: 60%;
  letter-spacing: 13px;
  text-transform: uppercase;

  ${Media.desktop(stepTitleDesktopStyles)}
`;

export const StepDescription = styled.p`
  position: relative;
  top: 60%;
  line-height: 36px;
  max-width: 460px;
`;


const imageDesktopStyles = css`
  margin-bottom: 0;
  width: auto;
  max-width: 400px;
  left: 0;
`;

const imageDesktopWideStles = css`
  max-width: none;
`;

export const Image = styled.img`
  margin-bottom: 110px;
  width: 100%;
  object-fit: contain;
  position: relative;

  left: ${ifElse(propEq('left', true), always(-10), always(10))}%;

  ${Media.desktop(imageDesktopStyles)}
  ${Media.desktopWide(imageDesktopWideStles)}
`;

const imageWrapperDesktopStyles = css`
  display: flex;
`;

export const imageWrapperStyles = css`
  display: block;
  position: relative;
  z-index: 1;

  ${Media.desktop(imageWrapperDesktopStyles)}
`;
