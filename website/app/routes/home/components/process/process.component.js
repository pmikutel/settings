import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';

import messages from './process.messages';

import { Grid, Components } from '../../../../theme';
import {
  Title,
  Wrapper,
  StepCounter,
  StepTitle,
  StepDescription,
  Image,
  processWrapperStyles,
  processStepStyles,
  stepInfoStyles,
  imageWrapperStyles,
} from './process.styles';

import image01 from './assets/01.jpg';
import image02 from './assets/02.jpg';
import image03 from './assets/03.jpg';
import image04 from './assets/04.jpg';

export class Process extends PureComponent {
  render = () => (
    <Wrapper>
      <Grid.Section extraStyles={processWrapperStyles}>
        <Components.Dots
          width="50%"
          height="1408"
          top="-672"
          mobileTop="-113"
          mobileHeight="602"
          mobileWidth="64%"
          right
          mobileRight
        />
        <Grid.Row>
          <Grid.Column offset={1} columns={12} mobileOffset={0} mobileColumns={5}>
            <Title><FormattedMessage {...messages.title} /></Title>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={processStepStyles}>
          <Grid.Column offset={3} columns={5} extraStyles={stepInfoStyles}>
            <StepCounter>01</StepCounter>
            <StepTitle><FormattedMessage {...messages.inspireTitle} /></StepTitle>
            <StepDescription><FormattedMessage {...messages.inspire} /></StepDescription>
          </Grid.Column>

          <Grid.Column offset={1} columns={6} extraStyles={imageWrapperStyles}>
            <Image src={image01} alt="01" />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={processStepStyles}>
          <Grid.Column offset={1} columns={6} extraStyles={imageWrapperStyles}>
            <Image left={true} src={image02} alt="02" />
          </Grid.Column>

          <Grid.Column offset={1} columns={5} extraStyles={stepInfoStyles}>
            <StepCounter>02</StepCounter>
            <StepTitle><FormattedMessage {...messages.costTitle} /></StepTitle>
            <StepDescription><FormattedMessage {...messages.cost} /></StepDescription>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={processStepStyles}>
          <Grid.Column offset={3} columns={5} extraStyles={stepInfoStyles}>
            <StepCounter>03</StepCounter>
            <StepTitle><FormattedMessage {...messages.planTitle} /></StepTitle>
            <StepDescription><FormattedMessage {...messages.plan} /></StepDescription>
          </Grid.Column>

          <Grid.Column offset={1} columns={6} extraStyles={imageWrapperStyles}>
            <Image src={image03} alt="03" />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={processStepStyles}>
          <Grid.Column offset={1} columns={6} extraStyles={imageWrapperStyles}>
            <Image left={true} src={image04} alt="04" />
          </Grid.Column>

          <Grid.Column offset={1} columns={5} extraStyles={stepInfoStyles}>
            <StepCounter>04</StepCounter>
            <StepTitle><FormattedMessage {...messages.installTitle} /></StepTitle>
            <StepDescription><FormattedMessage {...messages.install} /></StepDescription>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    </Wrapper>
  );
}


