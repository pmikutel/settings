import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';

import { Process } from '../process.component';

describe('<Process />', () => {
  const component = (props = {}) => <Process {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
