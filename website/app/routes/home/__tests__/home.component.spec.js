import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { identity } from 'ramda';

import { Home } from '../home.component';

describe('<MainSlider />', () => {
  const defaultProps = {
    productRequest: identity,
    productsKeys: {},
    productsItems: {},
    handleNewsletterSubmit: identity,
    newsletterReset: identity,
    newsletterSuccess: true,
    newsletterError: false,
    newsletterFetching: false,
    addItem: identity,
    addProjectItem: identity,
    blogRequest: identity,
    projectId: '',
    articles: {},
  };

  const component = (props = {}) => <Home {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
