import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../home.container';
import { HomeActions } from '../../../modules/home/home.redux';
import { ProductsActions } from '../../../modules/products/products.redux';
import { CartActions } from '../../../modules/cart/cart.redux';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { BlogActions } from '../../../modules/blog/blog.redux';

describe('Home: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call ProductsActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).productRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ProductsActions.request());
    });

    it('should call HomeActions.newsletterRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).handleNewsletterSubmit();

      expect(dispatch.firstCall.args[0]).to.deep.equal(HomeActions.newsletterRequest());
    });

    it('should call CartActions.addItem', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CartActions.addItem());
    });

    it('should call CheckoutActions.customerProjectLineItemAddRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addProjectItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.customerProjectLineItemAddRequest());
    });

    it('should call BlogActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).blogRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(BlogActions.request());
    });
  });
});
