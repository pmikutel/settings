import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Home } from './home.component';
import { HomeActions } from '../../modules/home/home.redux';
import {
  selectNewsletterSuccess,
  selectNewsletterError,
  selectNewsletterFetching,
} from '../../modules/home/home.selectors';
import { selectLocalesLanguage } from '../../modules/locales/locales.selectors';
import { CartActions } from '../../modules/cart/cart.redux';
import { CheckoutActions } from '../../modules/checkout/checkout.redux';
import { BlogActions } from '../../modules/blog/blog.redux';
import { selectArticles } from '../../modules/blog/blog.selectors';
import { ProductsActions } from '../../modules/products/products.redux';
import { selectProjectId } from '../../modules/user/user.selectors';
import { selectProductsItems, selectProductsKeys } from '../../modules/products/products.selectors';

const mapStateToProps = createStructuredSelector({
  language: selectLocalesLanguage,
  productsItems: selectProductsItems,
  productsKeys: selectProductsKeys,
  newsletterSuccess: selectNewsletterSuccess,
  newsletterError: selectNewsletterError,
  newsletterFetching: selectNewsletterFetching,
  articles: selectArticles,
  projectId: selectProjectId,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  productRequest: ProductsActions.request,
  handleNewsletterSubmit: HomeActions.newsletterRequest,
  newsletterReset: HomeActions.newsletterReset,
  addItem: CartActions.addItem,
  addProjectItem: CheckoutActions.customerProjectLineItemAddRequest,
  blogRequest: BlogActions.request,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
