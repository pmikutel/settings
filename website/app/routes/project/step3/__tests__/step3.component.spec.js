import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { Step3Component as Step3 } from '../step3.component';
import { intlMock } from '../../../../utils/testing';


describe('<Step3 />', () => {
  const defaultProps = {
    intl: intlMock(),
    register: identity,
    openModal: identity,
    clearError: identity,
    registerIsProcessing: false,
    registerError: false,
  };

  const component = (props = {}) => <Step3 {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
