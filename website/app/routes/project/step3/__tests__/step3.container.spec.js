import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../step3.container';
import { RegisterActions } from '../../../../modules/register/register.redux';

describe('Step3: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call RegisterActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).register();

      expect(dispatch).to.have.been.calledWith(RegisterActions.request());
    });
  });
});
