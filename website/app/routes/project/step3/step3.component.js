import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';

import { RegisterForm } from '../../../components/registerForm/registerForm.component';
import { UnderlineTitle } from '../../../components/underlineTitle/underlineTitle.component';
import { InfoBox } from '../../../components/infoBox/infoBox.component';

import messages from './step3.messages';

import {
  recommendCopyStyles,
  wrapperStyles,
  headerStyles,
  rowStyles,
  infoRowStyles,
  infoBoxStyles,
  Description,
  titleColumnStyles,
} from './step3.styles';

import { Grid, Components } from '../../../theme';

export class Step3Component extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    register: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    registerIsProcessing: PropTypes.bool.isRequired,
    registerError: PropTypes.bool.isRequired,
    clearError: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.clearError();
  }

  render = () => (
    <div>
      <Grid.Section extraStyles={wrapperStyles}>
        <Grid.Row extraStyles={rowStyles}>
          <Grid.Row extraStyles={infoRowStyles}>
            <Grid.Column offset={1} columns={1}>
              <Components.ActionLink to="/project/step2" extraStyles={Components.buttonArrowStyles}>
                <Components.Arrow left />
              </Components.ActionLink>
            </Grid.Column>

            <Grid.Column extraStyles={infoBoxStyles} columns={2}>
              <InfoBox copy="3/3" smallInfoBox />
            </Grid.Column>
          </Grid.Row>

          <Grid.Row extraStyles={headerStyles}>
            <Grid.Column
              extraStyles={recommendCopyStyles}
              offset={1}
              columns={15}
            >
              <FormattedMessage {...messages.mobileCopy} />
            </Grid.Column>

            <Grid.Column offset={1} columns={15} extraStyles={titleColumnStyles}>
              <UnderlineTitle
                title={this.props.intl.formatMessage(messages.title)}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid.Row>
        <Grid.Row>
          <Description>
            {this.props.intl.formatMessage(messages.description)}
            <Components.LinkButton onClick={() => this.props.openModal('login', false, true)}>
              <FormattedMessage {...messages.logIn} />
            </Components.LinkButton>
          </Description>
        </Grid.Row>
      </Grid.Section>
      <RegisterForm
        onSubmit={(data) => this.props.register(data, '/project/board')}
        registerIsProcessing={this.props.registerIsProcessing}
        registerError={this.props.registerError}
      />
    </div>
  );
}

export const Step3 = injectIntl(Step3Component);

