import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Step3 } from './step3.component';
import { RegisterActions } from '../../../modules/register/register.redux';
import { ModalsActions } from '../../../modules/modals/modals.redux';
import { selectRegisterError, selectRegisterProcessing } from '../../../modules/register/register.selectors';

const mapStateToProps = createStructuredSelector({
  registerError: selectRegisterError,
  registerIsProcessing: selectRegisterProcessing,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  register: RegisterActions.request,
  openModal: ModalsActions.openModal,
  clearError: RegisterActions.clearError,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Step3));
