
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'step3.title',
    defaultMessage: 'Register',
  },
  mobileCopy: {
    id: 'step3.mobileCopy',
    defaultMessage: 'Register to save your project and get our recommandations',
  },
  description: {
    id: 'step3.description',
    defaultMessage: 'Create an account with White Bear to save your details and start building your space.\n' +
    'Already have an account?',
  },
  logIn: {
    id: 'step3.logIn',
    defaultMessage: 'Sign in here',
  },
});
