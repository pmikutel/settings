import styled, { css } from 'styled-components';
import { Variables, Media } from '../../../theme';

export const wrapperStyles = css`
  display: flex;
  flex-direction: column;
`;

const recommendCopyTabletStyles = css`
  display: none;
`;

export const recommendCopyStyles = css`
  color: ${Variables.colorPureBlack};
  font-size: 18px;
  line-height: 36px;
  padding-top: 45px;
  display: block;

  ${Media.tablet(recommendCopyTabletStyles)};
`;

const headerTabletStyles = css`
  flex-direction: column;
  width: 100%;
  margin-left: 0;
`;

export const headerStyles = css`
  display: flex;
  flex-direction: column;
  margin-top: 81px;
  width: calc(100% - 54px);
  margin-left: 38px;

  ${Media.tablet(headerTabletStyles)};
`;

const rowDesktopStyles = css`
  flex-direction: column;
`;

export const rowStyles = css`
  width: 100%;
  flex-direction: column-reverse;

  ${Media.desktop(rowDesktopStyles)};
`;

const infoRowDesktopStyles = css`
  flex-direction: row;
`;

export const infoRowStyles = css`
  flex-direction: column-reverse;

  ${Media.desktop(infoRowDesktopStyles)}
`;

const infoBoxDesktopStyles = css`
  margin-top: 0;
  margin-bottom: 0;
  left: auto;
`;

export const infoBoxStyles = css`
  position: relative;
  margin-top: 107px;
  margin-bottom: 55px;
  left: -16px;

  ${Media.desktop(infoBoxDesktopStyles)}
`;

export const descriptionDesktopStyles = css`
  padding: 0;
`;

export const descriptionTabletStyles = css`
  padding: 0 18px 0 32px;
`;

export const Description = styled.div`
  max-width: 800px;
  margin: 39px auto 79px;
  font-size: 22px;
  line-height: 46px;
  display: block;
  padding-left: 41px;

  color: ${Variables.colorWarmGrey};

  ${Media.tablet(descriptionTabletStyles)};
  ${Media.desktop(descriptionDesktopStyles)};
`;

const titleColumnDesktopStyles = css`
  padding-left: 0;
`;

const titleColumnTabletStyles = css`
  padding-left: 16px;
`;

export const titleColumnStyles = css`
  padding-left: 0;

  ${Media.tablet(titleColumnTabletStyles)};
  ${Media.desktop(titleColumnDesktopStyles)};
`;
