import { prop } from 'ramda';
import styled, { css } from 'styled-components';
import { Media, Typography } from '../../../theme';
import { colorPureBlack } from '../../../theme/variables';

export const Wrapper = styled.div`
  position: relative;
`;

const headerRowDesktopStyles = css`
  margin: 0 0 263px;
`;

export const headerRowStyles = css`  
  width: 100%;
  position: relative;
  margin-top: 155px;

  ${Media.desktop(headerRowDesktopStyles)};
`;

export const backButtonExtraStyles = css`
  padding-left: 82px;
  width: 244px;
`;

export const shareButtonStyles = css`
  width: 184px;
`;

export const backButtonArrowExtraStyles = css`
  left: 30px;
`;

export const sliderWrapperStyles = css`
  overflow: hidden;
`;

const buttonColumnDesktopStyles = css`
  flex-direction: row;
  justify-content: space-between;
  position: absolute;
  top: 46px;
  z-index: 2;
  display: flex;
`;

export const buttonColumnStyles = css`
  flex-direction: column;
  display: none;
  
  ${Media.desktop(buttonColumnDesktopStyles)};
`;

export const buttonRowStyles = css`
  z-index: 2;
`;

const articleImageDesktopStyles = css`
  height: 768px;
  margin-bottom: 0;
`;

export const ArticleImage = styled.div`
  background-image: url(${prop('image')});
  height: 348px;
  width: 100%;
  background-size: cover;
  margin-bottom: 77px;
  
  ${Media.desktop(articleImageDesktopStyles)};
`;

const copyDesktopStyles = css`
  position: absolute;
  top: 287px;
  left: 94px;
  max-width: calc(100% - 94px);
  z-index: 2;
`;

export const Copy = styled.div`
  padding: 0 35px 37px;
  ${Media.desktop(copyDesktopStyles)};
`;

export const SubTitle = styled.div`
  font-size: 18px;
  line-height: 36px;
  color: ${colorPureBlack};
  ${Typography.fontLyonLight};
`;

const titleDesktopStyles = css`
  letter-spacing: 45px;
`;

export const Title = styled.h1`
  letter-spacing: 16px;
  overflow-wrap: break-word;
  
  ${Media.desktop(titleDesktopStyles)};
`;

export const headerColumnStyles = css`
  position: relative;
`;
