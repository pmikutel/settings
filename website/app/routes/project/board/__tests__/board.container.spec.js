import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../board.container';
import { CheckoutActions } from '../../../../modules/checkout/checkout.redux';
import { ProductsActions } from '../../../../modules/products/products.redux';
import { CartActions } from '../../../../modules/cart/cart.redux';
import { CollectionsActions } from '../../../../modules/collections/collections.redux';
import { ModalsActions } from '../../../../modules/modals/modals.redux';


describe('Board: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call CheckoutActions.fetchRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).fetchProject();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.fetchRequest());
    });

    it('should call ProductsActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).productsRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ProductsActions.request());
    });

    it('should call CollectionsActions.singleRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).collectionRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CollectionsActions.singleRequest());
    });

    it('should call CheckoutActions.customerProjectLineItemRemoveRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).removeProjectItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.customerProjectLineItemRemoveRequest());
    });

    it('should call CartActions.addItem', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CartActions.addItem());
    });

    it('should call CheckoutActions.customerProjectLineItemAddRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addProjectItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.customerProjectLineItemAddRequest());
    });

    it('should call ModalsActions.openModal', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).openModal();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ModalsActions.openModal());
    });
  });
});
