import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';
import { intlMock } from '../../../../utils/testing';

import { BoardComponent as Board } from '../board.component';

describe('<Board />', () => {
  const defaultProps = {
    fetchProject: identity,
    projectData: fromJS({}),
    projectId: '12312',
    match: { params: { id: '11' } },
    collectionRequest: identity,
    addItem: identity,
    removeProjectItem: identity,
    productsRequest: identity,
    addProjectItem: identity,
    openModal: identity,
    projectItems: fromJS({}),
    productsItems: fromJS({}),
    singleCollection: fromJS({}),
    articleImage: '',
    companyName: '',
    isLoggedIn: true,
    intl: intlMock(),
    cartItems: fromJS({}),
  };

  const component = (props = {}) => <Board {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
