import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { always, ifElse, prop, both } from 'ramda';

import messages from './board.messages';
import { Grid, Components } from '../../../theme';
import { AnimatedText } from '../../../components/animatedText/animatedText.component';
import { Saved } from './components/saved/saved.component';
import { Book } from './components/book/book.component';
import { Recommended } from './components/recommended/recommended.component';
import { ProductSlider } from '../../../components/productSlider/productSlider.component';
import { StickyButton } from '../../../components/stickyButton/stickyButton.component';
import { WindowScroll } from '../../../components/windowScroll/windowScroll.component';

import {
  Wrapper,
  headerRowStyles,
  backButtonExtraStyles,
  backButtonArrowExtraStyles,
  sliderWrapperStyles,
  buttonColumnStyles,
  shareButtonStyles,
  buttonRowStyles,
  ArticleImage,
  Copy,
  SubTitle,
  Title,
  headerColumnStyles,
} from './board.styles';


export class BoardComponent extends PureComponent {
  static propTypes = {
    match: PropTypes.object.isRequired,
    fetchProject: PropTypes.func.isRequired,
    collectionRequest: PropTypes.func.isRequired,
    addItem: PropTypes.func.isRequired,
    removeProjectItem: PropTypes.func.isRequired,
    productsRequest: PropTypes.func.isRequired,
    addProjectItem: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    projectData: PropTypes.object.isRequired,
    projectItems: PropTypes.object.isRequired,
    projectId: PropTypes.string.isRequired,
    productsItems: PropTypes.object.isRequired,
    singleCollection: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    articleImage: PropTypes.string,
    companyName: PropTypes.string,
    isLoggedIn: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
    cartItems: PropTypes.object.isRequired,
  };

  state = {
    copied: false,
    isProductAdded: false,
    scroll: {},
  };

  componentDidMount() {
    this.props.fetchProject(this.props.match.params.id, true);
    this.props.productsRequest();
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.projectItems.size > 0 &&
      this.props.cartItems.size > 0 &&
      !this.state.isProductAdded
    ) {
      nextProps.projectItems.map(projectItem => {
        this.props.cartItems.map((cartItem, index) => {
          if (projectItem.getIn(['variant', 'id']) === index) {
            this.setState({ isProductAdded: true });
          }
        });
      });
    }
  }

  handleCopySuccess = () => this.setState({ copied: true });

  addItem = (data) => {
    this.props.addItem(data);
    this.setState({ isProductAdded: true });
  };

  renderSlider = () => ifElse(
    prop('size'),
    () =>
      <ProductSlider
        items={this.props.productsItems.get('products')}
        title={this.props.intl.formatMessage(messages.slider)}
      />,
    always(null),
  )(this.props.productsItems);

  renderRecommended = () => ifElse(
    prop('size'),
    () =>
      <Recommended
        singleCollection={this.props.singleCollection}
        addItem={this.addItem}
        addProjectItem={this.props.addProjectItem}
        projectData={this.props.projectData}
        collectionRequest={this.props.collectionRequest}
        projectId={this.props.projectId}
        scroll={this.state.scroll}
      />,
    always(null),
  )(this.props.projectData.get('customAttributes'));

  renderChangeStyleButton = () => ifElse(
    both(prop('projectId'), prop('isLoggedIn')),
    () =>
      <Components.Button
        extraStyles={backButtonExtraStyles}
        onClick={() => this.props.openModal('project')}
        noActive
      >
        <Components.Arrow extraStyles={backButtonArrowExtraStyles} left />
        <FormattedMessage {...messages.change} />
      </Components.Button>,
    always(null),
  )(this.props);

  renderTitle = () => ifElse(
    both(prop('projectId'), prop('isLoggedIn')),
    () =>
      <Copy>
        <SubTitle>
          <FormattedMessage {...messages.project} />
        </SubTitle>
        <Title>{this.props.companyName}</Title>
      </Copy>,
    always(null),
  )(this.props);

  render = () => (
    <Wrapper>
      <WindowScroll onScroll={(e) => this.setState({ scroll: e })} />
      <Grid.Section>
        <Grid.Row extraStyles={buttonRowStyles}>
          <Grid.Column offset={1} columns={14} extraStyles={buttonColumnStyles}>
            <div>
              {this.renderChangeStyleButton()}
            </div>
            <CopyToClipboard text={window.location.href} onCopy={this.handleCopySuccess}>
              <Components.Button extraStyles={shareButtonStyles} noActive>
                <AnimatedText
                  text={this.props.intl.formatMessage(messages.share)}
                  successText={this.props.intl.formatMessage(messages.copied)}
                  waiting={this.state.copied}
                  callback={() => this.setState({ copied: false })}
                />
              </Components.Button>
            </CopyToClipboard>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={headerRowStyles}>
          <Grid.Column offset={3} columns={13} extraStyles={headerColumnStyles}>
            {this.renderTitle()}
            <ArticleImage image={this.props.articleImage} />
          </Grid.Column>
        </Grid.Row>

        <Saved
          projectItems={this.props.projectItems}
          addItem={this.addItem}
          removeProjectItem={this.props.removeProjectItem}
          projectId={this.props.projectId}
          scroll={this.state.scroll}
        />

        {this.renderRecommended()}
      </Grid.Section>
      <Book />
      <Grid.Section extraStyles={sliderWrapperStyles}>
        {this.renderSlider()}
      </Grid.Section>
      <StickyButton
        disabled={!this.state.isProductAdded}
        action={() => this.props.history.push('/purchase/cart')}
        copy={this.props.intl.formatMessage(messages.cart)}
      />
    </Wrapper>
  );
}

export const Board = injectIntl(BoardComponent);
