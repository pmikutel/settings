
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'board.title',
    defaultMessage: 'Project board page',
  },
  change: {
    id: 'board.change',
    defaultMessage: 'Change Your Style',
  },
  share: {
    id: 'board.share',
    defaultMessage: 'Share project',
  },
  copied: {
    id: 'board.copied',
    defaultMessage: 'Url copied',
  },
  slider: {
    id: 'board.slider',
    defaultMessage: 'KEEP BROWSING OUR PRODUCTS',
  },
  cart: {
    id: 'board.cart',
    defaultMessage: 'Review Basket',
  },
  project: {
    id: 'board.project',
    defaultMessage: 'Your project',
  },
});
