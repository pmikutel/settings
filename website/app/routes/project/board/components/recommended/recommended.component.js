import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { ifElse, prop, always, isNil } from 'ramda';

import { ProductItem } from '../../../../../components/productItem/productItem.component';
import { MasonryGrid } from '../../../../../components/masonryGrid/masonryGrid.component';
import { decode } from '../../../../../utils/shopifyUrls';

import messages from './recommended.messages';
import { deliveryArray, stripItemBeforeWide } from './recommended.helpers';

import { Grid } from '../../../../../theme';
import {
  Title,
  Description,
  Copy,
  DeliveryTitle,
  Number,
  columnNumberStyles,
  ContentWrapper,
  deliverRowStyles,
  DeliveryWrapper,
} from './recommended.styles';

export class RecommendedComponent extends PureComponent {
  static propTypes = {
    projectData: PropTypes.object.isRequired,
    addItem: PropTypes.func.isRequired,
    collectionRequest: PropTypes.func.isRequired,
    addProjectItem: PropTypes.func.isRequired,
    singleCollection: PropTypes.object.isRequired,
    projectId: PropTypes.string,
    intl: PropTypes.object.isRequired,
    scroll: PropTypes.object.isRequired,
  };

  componentDidMount() {
    const collection = this.props.projectData.getIn(['customAttributes', 'selectedStyle']);
    this.props.collectionRequest(collection);
  }

  printItems = (items, size) => items.map((item, index) => {
    const data = item.variants[0];

    return (
      <ProductItem
        arrayLength={size}
        key={index}
        index={index}
        id={decode(item.id)}
        image={data.image.originalSrc}
        title={item.title}
        description={item.description}
        variantId={decode(data.id)}
        lineItemId={item.id}
        size={data.size}
        price={data.price}
        compareAtPrice={data.compareAtPrice}
        wide={data.wide}
        delivery={data.delivery}
        variantTitle={data.title}
        addCartItem={this.props.addItem}
        addProjectItem={this.props.addProjectItem}
        projectId={this.props.projectId}
        productHandle={item.handle}
        url={`/products/${item.handle}/${decode(item.id)}/variant/${decode(data.id)}`}
        scroll={this.props.scroll}
      />
    );
  });

  renderDelivery = () => deliveryArray.map((item, index) => {
    const data = stripItemBeforeWide(this.props.singleCollection.toJS()[item]);

    return (
      ifElse(
        isNil,
        always(null),
        () =>
          <DeliveryWrapper key={index} zIndex={deliveryArray.length - index}>
            <Grid.Row extraStyles={deliverRowStyles}>
              <Grid.Column offset={3} columns={6}>
                <DeliveryTitle>
                  {this.props.intl.formatMessage(messages.estimatedTitle) + deliveryArray[index]}
                </DeliveryTitle>
              </Grid.Column>
              <Grid.Column offset={0} columns={7} extraStyles={columnNumberStyles}>
                <Number>
                  {this.props.intl.formatMessage(messages[`priceRange${index}`])}
                </Number>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column offset={1} columns={14}>
                <MasonryGrid>
                  {this.printItems(data, data.length)}
                </MasonryGrid>
              </Grid.Column>
            </Grid.Row>
          </DeliveryWrapper>,
      )(data));
  });

  renderContent = () => ifElse(
    prop('size'),
    () =>
      <ContentWrapper>
        {this.renderDelivery()}
      </ContentWrapper>,
    always(null),
  )(this.props.singleCollection);

  render = () => (
    <Grid.Section>
      <Grid.Row>
        <Grid.Column offset={3} columns={6}>
          <Title>
            <FormattedMessage {...messages.title} />
          </Title>
          <Description>
            <Copy>
              <FormattedMessage {...messages.description} />
            </Copy>
          </Description>
        </Grid.Column>
      </Grid.Row>
      {this.renderContent()}
    </Grid.Section>
  );
}

export const Recommended = injectIntl(RecommendedComponent);
