import styled, { css } from 'styled-components';
import { prop } from 'ramda';
import { Variables, Typography, Media } from '../../../../../theme';

const titleDesktopStyles = css`
  letter-spacing: 24px;
`;
export const Title = styled.h2`
  letter-spacing: 13px;
  margin-bottom: 56px;

  ${Media.desktop(titleDesktopStyles)};
`;

export const Copy = styled.p`
  margin: 0;
`;

const descriptionDesktopStyles = css`
  padding-right: 118px;
`;

export const Description = styled.div`
  line-height: 50px;
  color: ${Variables.colorWarmGrey};

  ${Media.desktop(descriptionDesktopStyles)};
`;

export const DeliveryTitle = styled.div`
  font-size: 22px;
  line-height: 50px;
  padding: 160px 0 17px;
  border-bottom: 1px solid ${Variables.colorWarmGrey};
  color: ${Variables.colorPureBlack};
  ${Typography.fontLyonLight};
`;

const numberDesktopStyles = css`
  font-size: 500px;
  position: absolute;
  top: -24px;
  left: 0;
  margin: 0;
  height: auto;
`;

export const Number = styled.div`
  font-size: 300px;
  line-height: 400px;
  ${Typography.fontLyonLight};
  color: ${Variables.colorWarmGrey};
  letter-spacing: -4px;
  opacity: 0.1;
  white-space: nowrap;
  margin-top: -100px;
  height: 295px;

  ${Media.desktop(numberDesktopStyles)};
`;

const columnNumberDesktopStyles = css`
  padding-bottom: 300px;
  position: absolute;
  right: 0;
  left: auto;
`;

const columnNumberDesktopWideStyles = css`
  padding-bottom: 200px;
`;

export const columnNumberStyles = css`
  position: relative;
  left: 50px;

  ${Media.desktopWide(columnNumberDesktopWideStyles)};
  ${Media.desktop(columnNumberDesktopStyles)};
`;

const contentWrapperDesktopStyles = css`
  padding-bottom: 285px;
`;

export const ContentWrapper = styled.div`
  padding-bottom: 172px;

  ${Media.desktop(contentWrapperDesktopStyles)};
`;

const deliverRowDesktopStyles = css`
  flex-direction: row;
  margin-bottom: 60px;
`;

export const deliverRowStyles = css`
  flex-direction: column;
  z-index: 10;

  ${Media.desktop(deliverRowDesktopStyles)};
`;

export const DeliveryWrapper = styled.div`
  z-index: ${prop('zIndex')};
  position: relative;
`;
