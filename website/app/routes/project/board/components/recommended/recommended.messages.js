import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'recommended.title',
    defaultMessage: 'RECOMMENDED PRODUCTS',
  },
  description: {
    id: 'recommended.description',
    defaultMessage: `Based on space requirements and your chosen style we have chosen a few products for you. Add your
    favorites to your basket.`,
  },
  estimated: {
    id: 'recommended.estimated',
    defaultMessage: 'Browse Products',
  },
  estimatedTitle: {
    id: 'recommended.estimatedTitle',
    defaultMessage: 'Estimated Delivery',
  },
  priceRange0: {
    id: 'recommended.estimatedTitle',
    defaultMessage: '1-2',
  },
  priceRange1: {
    id: 'recommended.estimatedTitle',
    defaultMessage: '3-4',
  },
  priceRange2: {
    id: 'recommended.estimatedTitle',
    defaultMessage: '5+',
  },
});
