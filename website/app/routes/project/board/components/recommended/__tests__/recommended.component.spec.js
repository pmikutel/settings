import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';
import { intlMock } from '../../../../../../utils/testing';

import { RecommendedComponent as Recommended } from '../recommended.component';

describe('<Recommended />', () => {
  const defaultProps = {
    projectData: fromJS({}),
    addItem: identity,
    collectionRequest: identity,
    addProjectItem: identity,
    singleCollection: fromJS({}),
    projectId: '',
    intl: intlMock(),
    scroll: {},
  };

  const component = (props = {}) => <Recommended {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
