import { map } from 'ramda';

export const deliveryArray = [' 1-2 weeks', ' 3-4 weeks', ' 5+ weeks'];

export const stripItemBeforeWide = (data) => {
  let result = [];
  let index = 0;

  if (!data || data.length < 3) {
    return data;
  }

  map(item => {
    const next = data[index + 1];
    const shouldSkip = next &&
      next.variants[0].wide &&
      next.variants[0].wide.toLowerCase() === 'yes' &&
      index % 2 === 0;
    index ++;

    if (!shouldSkip) {
      result.push(item);
    }
  }, data);

  return result;
};
