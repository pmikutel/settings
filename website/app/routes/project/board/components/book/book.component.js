import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';

import messages from './book.messages';

import { Grid } from '../../../../../theme';
import { Wrapper, Title, Description, Button } from './book.styles';
import { getTelLink } from '../../../../../utils/telephone';

export class BookComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
  };

  get email() {
    return `mailto:${this.props.intl.formatMessage(messages.mail)}`;
  }

  render = () => (
    <Wrapper>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={3} columns={10}>
            <Title>
              <FormattedMessage {...messages.title} />
            </Title>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column offset={5} columns={6}>
            <Description>
              <FormattedMessage {...messages.description} />
            </Description>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column offset={6} columns={4}>
            <Button href={getTelLink}>
              <FormattedMessage {...messages.buttonCall} />
            </Button>
            <Button href={this.email}>
              <FormattedMessage {...messages.buttonEmail} />
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    </Wrapper>
  );
}

export const Book = injectIntl(BookComponent);
