import styled, { css } from 'styled-components';
import { Variables, Typography, Media } from '../../../../../theme';


const wrapperDesktopStyles = css`
  padding: 181px 0 100px;
`;

export const Wrapper = styled.div`
  padding: 98px 0 90px;
  background-image: ${Variables.gradientCustom};
  color: ${Variables.colorPureWhite};
  text-align: center;
  
  ${Media.desktop(wrapperDesktopStyles)};
`;

const titleDesktopStyles = css`
  padding-bottom: 100px;
  letter-spacing: 24px;
`;

export const Title = styled.h2`
  ${Typography.fontGothamBook};
  letter-spacing: 13px;
  padding-bottom: 58px;
  
  ${Media.desktop(titleDesktopStyles)};
`;

const descriptionDesktopStyles = css`
  margin-bottom: 47px;
`;

export const Description = styled.p`
  ${Typography.fontLyonLight};
  font-size: 18px;
  line-height: 36px;
  margin: 0 0 40px;
  
  ${Media.desktop(descriptionDesktopStyles)};
`;

export const Button = styled.a`
  ${Typography.fontLyonLight};
  color: ${Variables.colorPureWhite};
  font-size: 18px;
  line-height: 36px;
  height: 64px;
  width: 100%;
  text-align: center;
  border: 2px solid ${Variables.colorPureWhite};
  display: flex;
  justify-content: center;
  align-items: center;
  max-width: 338px;
  margin: 0 auto 30px;
`;
