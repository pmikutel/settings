import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'book.title',
    defaultMessage: 'BOOK A SHOWROOM TOUR',
  },
  description: {
    id: 'book.description',
    defaultMessage: `Want to know what your products will look like in real life? Get in contact to book a showroom 
    tour.`,
  },
  buttonCall: {
    id: 'book.buttonCall',
    defaultMessage: 'Call Me',
  },
  buttonEmail: {
    id: 'book.buttonEmail',
    defaultMessage: 'Email Me',
  },
  telephone: {
    id: 'book.telephone',
    defaultMessage: '394854353454',
  },
  mail: {
    id: 'book.mail',
    defaultMessage: 'sales@whitebear.com',
  },
});
