import React from 'react';
import { shallow } from 'enzyme';
import { intlMock } from '../../../../../../utils/testing';

import { BookComponent as Book } from '../book.component';

describe('<Book />', () => {
  const defaultProps = {
    intl: intlMock(),
  };

  const component = (props = {}) => <Book {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
