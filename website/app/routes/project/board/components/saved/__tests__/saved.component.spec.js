import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';
import { intlMock } from '../../../../../../utils/testing';

import { SavedComponent as Saved } from '../saved.component';

describe('<Saved />', () => {
  const defaultProps = {
    projectItems: fromJS({}),
    addItem: identity,
    removeProjectItem: identity,
    projectId: '',
    intl: intlMock(),
    scroll: {},
  };

  const component = (props = {}) => <Saved {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
