import styled, { css } from 'styled-components';
import { Variables, Media } from '../../../../../theme';

const titleDesktopStyles = css`
  letter-spacing: 24px;
`;
export const Title = styled.h2`
  letter-spacing: 13px;
  margin-bottom: 56px;
  
  ${Media.desktop(titleDesktopStyles)};
`;

export const Copy = styled.p`
  margin: 0;
`;

const descriptionDesktopStyles = css`
  padding-right: 118px;
`;

export const Description = styled.div`
  line-height: 50px;
  color: ${Variables.colorWarmGrey};
  
  ${Media.desktop(descriptionDesktopStyles)};
`;

export const buttonStyles = css`
  margin-top: 57px;
  max-width: 244px;
  justify-content: center;
`;

export const wrapperStyles = css`
  padding-bottom: 153px;
`;
