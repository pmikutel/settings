import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'saved.title',
    defaultMessage: 'YOUR SAVED PRODUCTS',
  },
  description: {
    id: 'saved.description',
    defaultMessage: `You currently don’t have any saved products. Browse products and save them to your project for 
    later.`,
  },
  button: {
    id: 'saved.button',
    defaultMessage: 'Browse Products',
  },
});
