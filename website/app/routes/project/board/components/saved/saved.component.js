import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { ifElse, prop, always } from 'ramda';

import { ProductItem } from '../../../../../components/productItem/productItem.component';
import { MasonryGrid } from '../../../../../components/masonryGrid/masonryGrid.component';
import { flattenDeepSelectedOptions } from '../../../../../utils/shopifyData';
import { decode } from '../../../../../utils/shopifyUrls';

import messages from './saved.messages';

import { Grid, Components } from '../../../../../theme';
import { Title, Description, buttonStyles, Copy, wrapperStyles } from './saved.styles';

export class SavedComponent extends PureComponent {
  static propTypes = {
    projectItems: PropTypes.object.isRequired,
    addItem: PropTypes.func.isRequired,
    removeProjectItem: PropTypes.func.isRequired,
    projectId: PropTypes.string,
    intl: PropTypes.object.isRequired,
    scroll: PropTypes.object.isRequired,
  };

  printItems = () => this.props.projectItems.toJS().map((item, index) => {
    const data = flattenDeepSelectedOptions(item.variant);

    return (
      <ProductItem
        arrayLength={this.props.projectItems.size}
        key={index}
        id={decode(data.product.id)}
        index={index}
        image={data.image.src}
        title={item.title}
        description={data.product.description}
        variantId={decode(data.id)}
        lineItemId={item.id}
        size={data.size}
        price={data.price}
        compareAtPrice={data.compareAtPrice}
        wide={data.wide}
        delivery={data.delivery}
        variantTitle={data.title}
        addCartItem={this.props.addItem}
        removeProjectItem={this.props.removeProjectItem}
        projectId={this.props.projectId}
        productHandle={data.product.handle}
        url={`/products/${data.product.handle}/${decode(data.product.id)}/variant/${decode(data.id)}`}
        scroll={this.props.scroll}
      />
    );
  });

  renderEmptyContent = () => ifElse(
    prop('size'),
    always(null),
    () =>
      <Description>
        <Copy>
          <FormattedMessage {...messages.description} />
        </Copy>
        <Components.ActionLink to="/products/all" extraStyles={buttonStyles}>
          <FormattedMessage {...messages.button} />
        </Components.ActionLink>
      </Description>,
  )(this.props.projectItems);

  renderContent = () => ifElse(
    prop('size'),
    () =>
      <MasonryGrid>
        {this.printItems()}
      </MasonryGrid>,
    always(null),
  )(this.props.projectItems);

  render = () => (
    <Grid.Section extraStyles={wrapperStyles}>
      <Grid.Row>
        <Grid.Column offset={3} columns={6}>
          <Title>
            <FormattedMessage {...messages.title} />
          </Title>
          {this.renderEmptyContent()}
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column offset={1} columns={14}>
          {this.renderContent()}
        </Grid.Column>
      </Grid.Row>
    </Grid.Section>
  );
}

export const Saved = injectIntl(SavedComponent);
