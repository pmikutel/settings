import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Board } from './board.component';
import { selectProjectCheckoutData, selectProjectCheckoutItems } from '../../../modules/checkout/checkout.selectors';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { CollectionsActions } from '../../../modules/collections/collections.redux';
import { selectSingleCollection } from '../../../modules/collections/collections.selectors';
import { CartActions } from '../../../modules/cart/cart.redux';
import { selectProjectId, selectUserCompanyName, selectIsLoggedIn } from '../../../modules/user/user.selectors';
import { selectCartItems } from '../../../modules/cart/cart.selectors';
import { ProductsActions } from '../../../modules/products/products.redux';
import { selectProductsItems } from '../../../modules/products/products.selectors';
import { selectArticleImage } from '../../../modules/blog/blog.selectors';
import { ModalsActions } from '../../../modules/modals/modals.redux';

const mapStateToProps = createStructuredSelector({
  projectData: selectProjectCheckoutData,
  projectItems: selectProjectCheckoutItems,
  projectId: selectProjectId,
  productsItems: selectProductsItems,
  singleCollection: selectSingleCollection,
  articleImage: selectArticleImage,
  companyName: selectUserCompanyName,
  isLoggedIn: selectIsLoggedIn,
  cartItems: selectCartItems,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  productsRequest: ProductsActions.request,
  collectionRequest: CollectionsActions.singleRequest,
  fetchProject: CheckoutActions.fetchRequest,
  removeProjectItem: CheckoutActions.customerProjectLineItemRemoveRequest,
  addItem: CartActions.addItem,
  addProjectItem: CheckoutActions.customerProjectLineItemAddRequest,
  openModal: ModalsActions.openModal,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Board));
