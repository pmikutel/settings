import React, { PureComponent } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Helmet from 'react-helmet';

import Step1 from './step1/step1.container';
import Step2 from './step2/step2.container';
import Step3 from './step3/step3.container';
import Board from './board/board.container';

import Header from '../../components/header';
import { Footer } from '../../components/footer/footer.component';

export class Project extends PureComponent {
  static propTypes = {

  };

  renderRoutes = () => (
    <Switch>
      <Route path="/project/step1" component={Step1} />
      <Route path="/project/step2" component={Step2} />
      <Route path="/project/step3" component={Step3} />
      <Route path="/project/board/:id" component={Board} />

      <Redirect from="/project" to="/project/step1" />
    </Switch>
  );

  render() {
    return (
      <div>
        <Helmet
          title="Project"
        />

        <Header />

        {this.renderRoutes()}

        <Footer />
      </div>
    );
  }
}
