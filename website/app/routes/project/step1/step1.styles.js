import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../theme';

const contentBoxTextDesktopStyles = css`
  line-height: 50px;
  padding: 0 5px 0 236px;
`;

export const ContentBoxText = styled.div`
  display: inline-block;
  box-sizing: border-box;
  break-inside: avoid-column;
  color: ${Variables.colorWarmGrey};
  font-size: 22px;
  line-height: 46px;
  margin: 48px 0 70px;
  padding: 0 18px 0 57px;
  text-align: left;
  max-width: 650px;
  
  ${Media.desktop(contentBoxTextDesktopStyles)}
`;

export const sectionStyles = css`
  margin-top: 35px;
  padding-bottom: 150px;
`;

const wrapperDesktopStyles = css`
  padding-bottom: 100px;
`;

export const Wrapper = styled.div`
  position: relative;
  padding-bottom: 70px;
  
  ${Media.desktop(wrapperDesktopStyles)}
`;
