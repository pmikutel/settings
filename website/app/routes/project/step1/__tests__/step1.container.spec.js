import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../step1.container';
import { Step1Actions } from '../../../../modules/step1/step1.redux';
import { BlogActions } from '../../../../modules/blog/blog.redux';
import { CollectionsActions } from '../../../../modules/collections/collections.redux';

describe('Step1: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call Step1Actions.changeStyle', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).changeStyle();

      expect(dispatch).to.have.been.calledWith(Step1Actions.changeStyle());
    });

    it('should call BlogActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).blogRequest();

      expect(dispatch).to.have.been.calledWith(BlogActions.request());
    });

    it('should call CollectionsActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).collectionRequest();

      expect(dispatch).to.have.been.calledWith(CollectionsActions.request());
    });
  });
});
