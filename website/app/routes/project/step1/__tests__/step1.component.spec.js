import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { identity } from 'ramda';
import { fromJS } from 'immutable';
import { intlMock } from '../../../../utils/testing';

import { Step1Component as Step1 } from '../step1.component';


describe('<Step1 />', () => {
  const defaultProps = {
    intl: intlMock(),
    selectedStyle: '',
    changeStyle: identity,
    articleImage: '',
    blogRequest: identity,
    collectionRequest: identity,
    collections: {},
    history: {},
    isLoggedIn: false,
  };

  const component = (props = {}) => <Step1 {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const articleImage = 'test.jpg';
    const collections = fromJS([{
      title: 'title',
      id: 'id',
      image: { originalSrc: 'test.jpg' },
    }]);
    const wrapper = render({ articleImage, collections });

    global.expect(wrapper).toMatchSnapshot();
  });
});
