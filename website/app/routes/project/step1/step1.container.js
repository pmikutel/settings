import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Step1Actions } from '../../../modules/step1/step1.redux';
import { CollectionsActions } from '../../../modules/collections/collections.redux';
import { BlogActions } from '../../../modules/blog/blog.redux';
import { selectSelectedStyle } from '../../../modules/step1/step1.selectors';
import { selectImage } from '../../../modules/blog/blog.selectors';
import { selectCollectionsWithImage } from '../../../modules/collections/collections.selectors';
import { selectIsLoggedIn } from '../../../modules/user/user.selectors';
import { Step1 } from './step1.component';

const mapStateToProps = createStructuredSelector({
  selectedStyle: selectSelectedStyle,
  articleImage: selectImage,
  collections: selectCollectionsWithImage,
  isLoggedIn: selectIsLoggedIn,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  changeStyle: Step1Actions.changeStyle,
  blogRequest: BlogActions.request,
  collectionRequest: CollectionsActions.request,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Step1));
