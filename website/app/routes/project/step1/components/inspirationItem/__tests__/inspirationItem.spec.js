import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { spy } from 'sinon';
import { identity } from 'ramda';
import 'jest-styled-components';

import { InspirationItem as InspirationItem } from '../inspirationItem.component';
import { BoxInner } from '../inspirationItem.styles';

describe('<InspirationItem />', () => {
  const defaultProps = {
    active: '0',
    title: 'example title',
    id: '1',
    description: 'example description',
    backgroundImage: 'http://example.url',
    index: 1,
    arrayLength: 8,
    onSelect: identity,
  };

  const component = (props = {}) => <InspirationItem {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();
    global.expect(wrapper).toMatchSnapshot();
  });

  describe('InspirationItem after click', () => {
    it('should set active status if active is other item', () => {
      const onSelectSpy = spy();
      const wrapper = render({ onSelect: onSelectSpy });

      wrapper.find(BoxInner).simulate('click');

      expect(onSelectSpy).to.have.been.calledWith('1');
    });

    it('should set inactive status if active is current item', () => {
      const onSelectSpy = spy();
      const wrapper = render({ onSelect: onSelectSpy, active: '1' });

      wrapper.find(BoxInner).simulate('click');

      expect(onSelectSpy).to.have.been.calledWith(null);
    });
  });
});
