import styled, { css } from 'styled-components';
import { ifElse, always, propEq } from 'ramda';
import { Media, Variables } from '../../../../../theme';

const iconDoneDesktopStyles = css`
  height: 54px;
  width: 117px;
`;

export const IconDone = styled.div`
  backface-visibility: hidden;
  border: 18px solid;
  border-right: 0;
  border-top: 0;
  border-image-source: ${Variables.gradientButton};
  border-image-slice: 1;
  bottom: 0;
  height: 54px;
  left: 0;
  margin: auto;
  position: absolute;
  right: 0;
  top: -45px;
  transform: rotate(-45deg);
  width: 117px;

  ${Media.tablet(iconDoneDesktopStyles)}
`;

const tabletBoxOverlayStyles = css`
  width: calc(100% - 49px);
`;
const desktopBoxOverlayStyles = css`
  width: calc(100% - 65px);
`;

export const BoxOverlay = styled.div`
  background: rgba(0,0,0,0.5);
  display: ${ifElse(propEq('isActive', true), always('block'), always('none'))};
  height: 100%;
  position: absolute;
  width: calc(100% - 37px);
  
  ${Media.tablet(tabletBoxOverlayStyles)}
  ${Media.desktop(desktopBoxOverlayStyles)}
`;

const boxImageDesktopStyles = css`
  max-width: 100%;
`;

export const BoxImage = styled.img`
  max-width: 100%;
  pointer-events: none;
  user-select: none;

  ${Media.desktop(boxImageDesktopStyles)}
`;

const boxTitleTabletStyles = css`
  left: ${ifElse(propEq('isLeft', true), always('11px'), always('97%'))};
`;

const boxTitleDesktopWideStyles = css`
  left: ${ifElse(propEq('isLeft', true), always('11px'), always('98%'))};
`;

const boxTitleDesktopStyles = css`
  font-size: 22px;
  letter-spacing: 12px;
`;

export const BoxTitle = styled.div`
  bottom: 0;
  color: ${Variables.colorPureBlack};
  display: inline-block;
  font-family: ${Variables.gothamBookFontFamily};
  font-size: 14px;
  letter-spacing: 8px;
  position: absolute;
  text-transform: uppercase;
  transform: translateY(100%) rotate(-90deg);
  transform-origin: top left;
  backface-visibility: hidden;
  left: ${ifElse(propEq('isEven', true), always('96%'), always('18px'))};

  ${Media.tablet(boxTitleDesktopStyles)}
  ${Media.tablet(boxTitleTabletStyles)}
  ${Media.desktopWide(boxTitleDesktopWideStyles)}
`;

const boxInnerTabletStyles = css`
  max-width: 300px;
  padding: ${ifElse(propEq('isLeft', true), always('0 10px 0 39px'), always('0 39px 0 10px'))};
  float: ${ifElse(propEq('isLeft', true), always('right'), always('left'))};
`;

const boxInnerTabletLandscapeStyles = css`
  max-width: 350px
`;

const boxInnerDesktopStyles = css`
  padding: ${ifElse(propEq('isLeft', true), always('0 10px 0 55px'), always('0 55px 0 10px'))};
`;

const boxInnerDesktopWideStyles = css`
  max-width: 100%;
`;

export const BoxInner = styled.div`
  float: ${ifElse(propEq('isEven', true), always('left'), always('right'))};
  line-height: 0;
  position: relative;
  &:hover {
    cursor: pointer;
  }
  padding: ${ifElse(propEq('isEven', false), always('0 0 0 37px'), always('0 37px 0 0'))};

  ${Media.tablet(boxInnerTabletStyles)}
  ${Media.tabletLandscape(boxInnerTabletLandscapeStyles)}
  ${Media.desktop(boxInnerDesktopStyles)}
  ${Media.desktopWide(boxInnerDesktopWideStyles)}
`;

export const ContentBox = styled.div`
  display: block;
  width: 100%;
  break-inside: avoid-column;
  opacity: 0;

  &:hover ${BoxOverlay} {
    display: block;
  }
`;
