import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { prop, equals, pipe, divide, __ } from 'ramda';
import VisibilitySensor from 'react-visibility-sensor';

import { BoxOverlay, IconDone, BoxImage, BoxTitle, BoxInner, ContentBox } from './inspirationItem.styles';
import { animateItemImage } from '../../../../../utils/animations';

export class InspirationItem extends PureComponent {
  static propTypes = {
    active: PropTypes.string,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    backgroundImage: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    arrayLength: PropTypes.number.isRequired,
    onSelect: PropTypes.func.isRequired,
    isLeft: PropTypes.bool,
  };

  state = {
    animated: false,
    initialized: false,
  };

  componentDidMount() {
    this.setState({ initialized: true });
  }

  onVisibleHandler = (isVisible) => {
    if (this.state.initialized && isVisible) {
      animateItemImage(this.itemElement, 1, 0, () => {
        this.setState({ animated: true });
      });
    }
  };

  handleItemRef = (ref) => (this.itemElement = ref);

  itemElement = null;

  handleClick = (elementKey) => {
    if (elementKey !== this.props.active) {
      this.props.onSelect(elementKey, this.props.title);
      return;
    }

    this.props.onSelect(null);
  };

  isEven = () => this.props.index % 2 === 0;

  isFirstRight = () => pipe(
    prop('arrayLength'),
    divide(__, 2),
    equals(this.props.index)
  )(this.props);

  render() {
    const isEven = this.isEven();
    const isLeft = this.props.isLeft;

    return (
      <ContentBox innerRef={this.handleItemRef}>
        <VisibilitySensor onChange={this.onVisibleHandler} active={!this.state.animated} />

        <BoxInner
          onClick={() => this.handleClick(this.props.id)}
          isLeft={isLeft}
          isEven={isEven}
          isFirstRight={this.isFirstRight()}
        >
          <BoxOverlay isActive={this.props.active === this.props.id}>
            <IconDone />
          </BoxOverlay>
          <BoxImage isLeft={isLeft} isEven={isEven} src={this.props.backgroundImage} alt={this.props.title} />
          <BoxTitle isLeft={isLeft} isEven={isEven}>
            {this.props.title}
          </BoxTitle>
        </BoxInner>
      </ContentBox>
    );
  }
}
