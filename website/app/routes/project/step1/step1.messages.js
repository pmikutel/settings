
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'step1.title',
    defaultMessage: 'Step1 page',
  },
  next: {
    id: 'step1.next',
    defaultMessage: 'Next step',
  },
  heroTitle: {
    id: 'step1.heroTitle',
    defaultMessage: 'Pick Your Style',
  },
  heroSubtitle: {
    id: 'step1.heroSubtitle',
    defaultMessage: 'Inspiration',
  },
  contentText: {
    id: 'step1.contentText',
    defaultMessage: 'Getting inspired is the first step to creating an amazing workspace. Explore our styles, ' +
    'tell us what inspires you and start building your project.',
  },
});
