import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ifElse, prop, always } from 'ramda';

import { FormattedMessage, injectIntl } from 'react-intl';

import messages from './step1.messages';

import { StaticHeroImage } from '../../../components/staticHeroImage/staticHeroImage.component';
import { InspirationItem } from './components/inspirationItem/inspirationItem.component';
import { MasonryGrid } from '../../../components/masonryGrid/masonryGrid.component';

import { ContentBoxText, sectionStyles, Wrapper } from './step1.styles';
import { Grid } from '../../../theme';
import { StickyButton } from '../../../components/stickyButton/stickyButton.component';


export class Step1Component extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    selectedStyle: PropTypes.string,
    changeStyle: PropTypes.func.isRequired,
    articleImage: PropTypes.string,
    blogRequest: PropTypes.func.isRequired,
    collectionRequest: PropTypes.func.isRequired,
    collections: PropTypes.object,
    history: PropTypes.object.isRequired,
    isLoggedIn: PropTypes.bool,
  };

  componentDidMount() {
    this.props.blogRequest('step1');
    this.props.collectionRequest(100);
  }

  renderItems = () => this.props.collections.valueSeq().map((item, index) => (
    <InspirationItem
      active={this.props.selectedStyle}
      backgroundImage={item.getIn(['image', 'originalSrc'])}
      title={item.get('title')}
      key={item.get('id')}
      id={item.get('id')}
      index={index}
      arrayLength={this.props.collections.size}
      onSelect={this.props.changeStyle}
    />
  ));

  render = () => (
    <Wrapper>
      <StaticHeroImage
        heroImage={this.props.articleImage}
        title={this.props.intl.formatMessage(messages.heroTitle)}
        subtitle={this.props.intl.formatMessage(messages.heroSubtitle)}
        shouldShowStatus
        status={ifElse(prop('isLoggedIn'), always('1/2'), always('1/3'))(this.props)}
      />

      <Grid.Section extraStyles={sectionStyles}>
        <MasonryGrid containsElementsWithoutImages={true}>
          <ContentBoxText>
            <FormattedMessage {...messages.contentText} />
          </ContentBoxText>

          {this.renderItems()}
        </MasonryGrid>
      </Grid.Section>

      <StickyButton
        action={() => this.props.history.push('/project/step2')}
        disabled={!this.props.selectedStyle}
        copy={this.props.intl.formatMessage(messages.next)}
      />
    </Wrapper>
  );
}

export const Step1 = injectIntl(Step1Component);
