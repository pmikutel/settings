import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../step2.container';
import { Step2Actions } from '../../../../modules/step2/step2.redux';
import { BlogActions } from '../../../../modules/blog/blog.redux';

describe('Step2: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call Step2Actions.updateData', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateData();

      expect(dispatch).to.have.been.calledWith(Step2Actions.updateData());
    });

    it('should call BlogActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).blogRequest();

      expect(dispatch).to.have.been.calledWith(BlogActions.request());
    });
  });
});
