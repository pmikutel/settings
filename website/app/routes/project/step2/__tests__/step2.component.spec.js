import React from 'react';
import { expect } from 'chai';
import { spy } from 'sinon';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { Step2Component as Step2 } from '../step2.component';
import { Step2Form } from '../components/form/form.component';
import { intlMock } from '../../../../utils/testing';


describe('<Step2 />', () => {
  const defaultProps = {
    history: {
      push: identity,
    },
    intl: intlMock(),
    updateData: identity,
    data: fromJS({}),
    articleImage: '',
    projectId: '',
    blogRequest: identity,
    updateAttributes: identity,
    createProject: identity,
    isLoggedIn: false,
  };

  const component = (props = {}) => <Step2 {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('<Step2Form />', () => {
    it('should call updateData on submit when user is not logged', () => {
      const updateData = spy();
      const wrapper = render({ updateData });

      wrapper.find(Step2Form).simulate('submit');

      expect(updateData).to.have.been.called;
    });

    it('should change page to step3', () => {
      const history = { push: spy() };
      const wrapper = render({ history });

      wrapper.find(Step2Form).simulate('submit');

      expect(history.push).to.have.been.calledWith('/project/step3');
    });

    it('should update attributes when user is logged and project id exist', () => {
      const updateAttributes = spy();
      const wrapper = render({ isLoggedIn: true, projectId: '123', updateAttributes });

      wrapper.find(Step2Form).simulate('submit');

      expect(updateAttributes).to.have.been.called;
    });

    it('should create project when user is logged', () => {
      const createProject = spy();
      const wrapper = render({ isLoggedIn: true, createProject });

      wrapper.find(Step2Form).simulate('submit');

      expect(createProject).to.have.been.called;
    });
  });
})
;
