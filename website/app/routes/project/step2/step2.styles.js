import styled, { css } from 'styled-components';
import Media from '../../../theme/media';
import { Variables } from '../../../theme';

const descriptionDesktopStyles = css`
  margin-top: 78px;
  margin-bottom: -160px;
`;

export const Description = styled.p`
  color: ${Variables.colorWarmGrey};
  margin-top: 40px;
  margin-bottom: -50px;
  font-size: 22px;
  line-height: 2.27;
  max-width: 305px;
  
  ${Media.desktop(descriptionDesktopStyles)}
`;

const wrapperDesktopStyles = css`
  padding-bottom: 100px;
`;

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  position: relative;
  padding-bottom: 70px;
  
  ${Media.desktop(wrapperDesktopStyles)}  
`;
