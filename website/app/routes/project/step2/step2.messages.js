import { defineMessages } from 'react-intl';

export default defineMessages({
  description: {
    id: 'step2.description',
    defaultMessage: 'Tell us about your space, we will tell you what we think your space needs.',
  },
  title1: {
    id: 'spacePlan.title1',
    defaultMessage: 'YOUR COMPANY',
  },
  title2: {
    id: 'spacePlan.title2',
    defaultMessage: 'YOUR DATES',
  },
  title3: {
    id: 'spacePlan.title3',
    defaultMessage: 'YOUR SPACE',
  },
  submit: {
    id: 'spacePlan.submit',
    defaultMessage: 'Next step',
  },
  heroTitle: {
    id: 'step2.heroTitle',
    defaultMessage: 'Your space',
  },
  heroSubtitle: {
    id: 'step2.heroSubtitle',
    defaultMessage: 'About your space',
  },
});
