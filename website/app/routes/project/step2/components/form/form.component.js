import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import { reduxForm, Field } from 'redux-form/immutable';
import { ifElse, always, isNil, isEmpty, anyPass, is } from 'ramda';
import validate from 'validate.js/validate';

import { FilesList } from '../../../../../components/filesList/filesList.component';
import { SpaceSubtitle } from '../spaceSubtitle/spaceSubtitle.component';
import { Counter } from '../../../../../components/counter/counter.component';
import { Upload } from '../../../../../components/upload/upload.component';
import { Checkbox } from '../../../../../components/checkbox/checkbox.component';
import { RadioButtons } from '../../../../../components/radioButtons/radioButtons.component';
import { StickyButton } from '../../../../../components/stickyButton/stickyButton.component';
import { COMPANY_DESCRIPTIONS_RADIO_OPTIONS, DELIVERY_DATE_DESCRIPTIONS_RADIO_OPTIONS } from './form.helpers';
import messages from './form.messages';

import {
  Form,
  contentColumnStyles,
  negativeMarginRowStyles,
  FormField,
  inputStyles,
  fieldRowStyles,
  sectionStyles,
  UploadDescription,
  uploadFileLabelStyles,
  filesListStyles,
} from './form.styles';
import { Grid, Forms } from '../../../../../theme';


export class Step2FormBase extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
  };

  renderTextField = ({ message, input, placeholder, meta: { touched, error }, errorText }) => (
    <FormField>
      <Forms.Label>
        <FormattedMessage {...message} />
      </Forms.Label>

      <Forms.Input
        errorStyle={touched && !!error}
        {...input}
        placeholder={placeholder}
        extraStyles={inputStyles}
      />

      {touched && error && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
    </FormField>
  );

  renderCounterField = ({ message, input, meta: { touched, invalid, dirty }, errorText }) => (
    <FormField>
      <Forms.Label>
        <FormattedMessage {...message} />
      </Forms.Label>

      <Counter {...input} max={9999} />

      {(dirty || touched) && invalid && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
    </FormField>
  );

  renderRadioField = ({ message, input, items }) => (
    <FormField>
      <Forms.Label>
        <FormattedMessage {...message} />
      </Forms.Label>

      <RadioButtons
        items={items}
        {...input}
      />
    </FormField>
  );

  renderUploadField = ({ message, input, supportedFiles, meta: { dirty, error }, errorText, checkMessage }) => (
    <FormField>
      <FilesList
        items={ifElse(
          anyPass([isNil, isEmpty, is(Boolean)]),
          always([]),
          always([input.value]),
        )(input.value)}
        listStyles={filesListStyles}
      />

      <Forms.Label extraStyles={uploadFileLabelStyles}>
        <FormattedMessage {...message} />
      </Forms.Label>

      <Upload {...input} supportedFiles={supportedFiles} />

      <UploadDescription>
        {this.props.intl.formatMessage(messages.uploadDescription)}
      </UploadDescription>

      <Checkbox
        label={this.props.intl.formatMessage(checkMessage)}
        {...input}
      />

      {dirty && error && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
    </FormField>
  );

  render = () => (
    <Form onSubmit={this.props.handleSubmit(this.props.onSubmit)} noValidate>
      <Grid.Section extraStyles={sectionStyles}>
        <Grid.Row>
          <Grid.Column columns={11} offset={3} extraStyles={contentColumnStyles}>
            <SpaceSubtitle
              number="01"
              title={this.props.intl.formatMessage(messages.title1)}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={negativeMarginRowStyles}>
          <Grid.Column columns={8} offset={6}>
            <Field
              name="whatDoes"
              type="text"
              placeholder={this.props.intl.formatMessage(messages.design)}
              message={messages.whatDoes}
              component={this.renderTextField}
              errorText={messages.whatDoesError}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={fieldRowStyles}>
          <Grid.Column columns={8} offset={6}>
            <Field
              name="peopleCount"
              message={messages.people}
              component={this.renderCounterField}
              errorText={messages.peopleError}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={fieldRowStyles}>
          <Grid.Column columns={8} offset={6}>
            <Field
              name="companyDescription"
              message={messages.describe}
              component={this.renderRadioField}
              items={COMPANY_DESCRIPTIONS_RADIO_OPTIONS}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column columns={11} offset={3} extraStyles={contentColumnStyles}>
            <SpaceSubtitle
              number="02"
              title={this.props.intl.formatMessage(messages.title2)}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={negativeMarginRowStyles}>
          <Grid.Column columns={8} offset={6}>
            <Field
              name="deliveryDate"
              message={messages.dateDescription}
              component={this.renderRadioField}
              items={DELIVERY_DATE_DESCRIPTIONS_RADIO_OPTIONS}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column columns={11} offset={3} extraStyles={contentColumnStyles}>
            <SpaceSubtitle
              number="03"
              title={this.props.intl.formatMessage(messages.title3)}
            />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={negativeMarginRowStyles}>
          <Grid.Column columns={8} offset={6}>
            <Field
              name="floorPlan"
              message={messages.uploadInputTitle}
              checkMessage={messages.checkLabel}
              supportedFiles={['jpg', 'jpeg', 'png']}
              component={this.renderUploadField}
              errorText={messages.uploadError}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>

      <StickyButton
        loading={this.props.loading}
        disabled={this.props.invalid}
        copy={this.props.intl.formatMessage(messages.submit)}
      />
    </Form>
  );
}

export const Step2Form = injectIntl(reduxForm({
  form: 'step2',
  validate: values => validate(values.toJS(), {
    whatDoes: {
      presence: {
        allowEmpty: false,
      },
    },
    peopleCount: {
      presence: {
        allowEmpty: false,
      },
      numericality: {
        greaterThan: 0,
      },
    },
    floorPlan: {
      presence: {
        allowEmpty: false,
      },
      exclusion: {
        within: [false],
      },
    },
  }),
})(Step2FormBase));
