import messages from './form.messages';

export const COMPANY_DESCRIPTIONS_RADIO_OPTIONS = [
  { id: 'start', message: messages.radioItemStart },
  { id: 'established', message: messages.radioItemEstablished },
  { id: 'industry', message: messages.radioItemIndustry },
];

export const DELIVERY_DATE_DESCRIPTIONS_RADIO_OPTIONS = [
  { id: 'low', message: messages.dateRadioItemFirst },
  { id: 'medium', message: messages.dateRadioItemSecond },
  { id: 'high', message: messages.dateRadioItemThird },
];
