import React from 'react';
import { expect } from 'chai';
import { spy } from 'sinon';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { Step2FormBase as Step2Form } from '../form.component';
import { intlMock } from '../../../../../../utils/testing';
import { StickyButton } from '../../../../../../components/stickyButton/stickyButton.component';

describe('<Step2Form />', () => {
  const defaultProps = {
    intl: intlMock(),
    handleSubmit: identity,
    onSubmit: identity,
    invalid: false,
    loading: false,
  };

  const component = (props = {}) => <Step2Form {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('<StickyButton />', () => {
    it('should call handleSubmit with onSubmit argument when user submit form', () => {
      const onSubmit = spy();
      const handleSubmit = spy();
      const wrapper = render({ onSubmit, handleSubmit });

      wrapper.find(StickyButton).simulate('click');

      expect(handleSubmit).to.have.been.calledWith(onSubmit);
    });
  });
});
