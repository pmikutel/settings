import { defineMessages } from 'react-intl';

export default defineMessages({
  description: {
    id: 'step2.description',
    defaultMessage: 'Tell us about your space, we will tell you what we think your space needs.',
  },
  title1: {
    id: 'step2.title1',
    defaultMessage: 'YOUR COMPANY',
  },
  title2: {
    id: 'step2.title2',
    defaultMessage: 'YOUR DATES',
  },
  title3: {
    id: 'step2.title3',
    defaultMessage: 'YOUR SPACE',
  },
  submit: {
    id: 'step2.submit',
    defaultMessage: 'Next step',
  },
  heroTitle: {
    id: 'step2.heroTitle',
    defaultMessage: 'Your space',
  },
  heroSubtitle: {
    id: 'step2.heroSubtitle',
    defaultMessage: 'About your space',
  },
  whatDoes: {
    id: 'step2.whatDoes',
    defaultMessage: 'What does your company do?',
  },
  whatDoesError: {
    id: 'step2.whatDoesError',
    defaultMessage: 'Please provide what does your company do',
  },
  design: {
    id: 'step2.design',
    defaultMessage: 'e.g. design, marketing',
  },
  people: {
    id: 'step2.people',
    defaultMessage: 'How many people are using the space?',
  },
  peopleError: {
    id: 'step2.peopleError',
    defaultMessage: 'People count should be greater than 0',
  },
  describe: {
    id: 'step2.describe',
    defaultMessage: 'How would you describe your company?',
  },
  radioItemStart: {
    id: 'step2.start',
    defaultMessage: 'Early days start up',
  },
  radioItemEstablished: {
    id: 'step2.established',
    defaultMessage: 'Established',
  },
  radioItemIndustry: {
    id: 'step2.industry',
    defaultMessage: 'Industry leader',
  },
  dateDescription: {
    id: 'step2.dateDescription',
    defaultMessage: 'When do you need it all to arrive?',
  },
  dateRadioItemFirst: {
    id: 'step2.dateRadioItemFirst',
    defaultMessage: 'Next 1-2 weeks',
  },
  dateRadioItemSecond: {
    id: 'step2.dateRadioItemSecond',
    defaultMessage: 'Next 3-4 weeks',
  },
  dateRadioItemThird: {
    id: 'step2.dateRadioItemThird',
    defaultMessage: '5+ weeks',
  },
  uploadInputTitle: {
    id: 'step2.uploadInputTitle',
    defaultMessage: 'Upload a floor plan',
  },
  uploadLabel: {
    id: 'step2.uploadLabel',
    defaultMessage: 'jpeg, png, eps',
  },
  uploadError: {
    id: 'step2.uploadError',
    defaultMessage: 'Please upload floor plan (max 2MB)',
  },
  uploadDescription: {
    id: 'step2.uploadDescription',
    defaultMessage: `We offer bespoke planning services. Upload your floorplan and our curators can help bring your 
    space to life.`,
  },
  checkLabel: {
    id: 'step2.checkLabel',
    defaultMessage: 'I’ll do it later',
  },
});
