import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../../../theme';

export const Form = styled.form`
  width: 100%;
`;

export const contentColumnStyles = css`
  flex-direction: column;
`;

export const FormField = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const inputDesktopStyles = css`
  max-width: 305px;
`;

export const inputStyles = css`
  max-width: 100%;
  
  ${Media.desktop(inputDesktopStyles)}
`;

const fieldRowDesktopStyles = css`
  margin-bottom: 70px;
`;

export const fieldRowStyles = css`
  margin-bottom: 40px;
  
  ${Media.desktop(fieldRowDesktopStyles)}
`;

const negativeMarginRowDesktopStyles = css`
  margin-top: -100px;
`;

export const negativeMarginRowStyles = css`
  position:relative;
  margin-top: -60px;
  
  ${Media.desktop(negativeMarginRowDesktopStyles)}
  
  ${fieldRowStyles}
`;

export const UploadDescription = styled.p`
  font-size: 18px;
  opacity: 0.9;
  line-height: 2;
  max-width: 520px;
  margin-bottom: 20px;
  color: ${Variables.colorWarmGrey};
`;

export const sectionDesktopStyles = css`
  padding-bottom: 60px;
`;

export const sectionStyles = css`
  padding-bottom: 90px;
  
  ${Media.desktop(sectionDesktopStyles)};
`;

export const uploadFileLabelStyles = css`
  margin-bottom: 15px;
`;

export const filesListStyles = css`
  margin-top: 30px;
`;
