import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Wrapper, Number, Title } from './spaceSubtitle.styles';

export class SpaceSubtitle extends PureComponent {
  static propTypes = {
    number: PropTypes.string,
    title: PropTypes.string,
  };

  render = () => (
    <Wrapper number={this.props.number}>
      <Number number={this.props.number}>
        {this.props.number}
      </Number>
      <Title number={this.props.number}>
        {this.props.title}
      </Title>
    </Wrapper>
  );
}


