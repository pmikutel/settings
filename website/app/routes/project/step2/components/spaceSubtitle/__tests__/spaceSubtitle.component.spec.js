import React from 'react';
import { shallow } from 'enzyme';

import { SpaceSubtitle } from '../spaceSubtitle.component';

describe('<SpaceSubtitle />', () => {
  const defaultProps = {
    title: 'Test',
    number: '03',
  };

  const component = (props = {}) => <SpaceSubtitle {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
