import styled, { css } from 'styled-components';
import { ifElse, propEq, always, T, cond } from 'ramda';
import { Media, Variables, Typography } from '../../../../../theme';

const isNumber2 = propEq('number', '02');
const isNumber3 = propEq('number', '03');

const wrapperDesktopStyles = css`
  padding-right: 90px;
`;

const wrapperRightDesktopStyles = css`
  padding-left: 90px;
`;

const wrapperRightStyles = css`
  padding-right: 0;
  padding-left: 0;
  
  ${Media.desktop(wrapperRightDesktopStyles)}
`;

const numberRightStyles = css`
  text-align: left;
`;

const getRightStyles = ifElse(isNumber2, always(wrapperRightStyles), always(''));
const getNumberRightStyles = ifElse(isNumber2, always(numberRightStyles), always(''));

export const Wrapper = styled.div`
  display: block;
  position: relative;
  margin-top: 40px;
  padding-right: 0;
  
  ${getRightStyles}
  
  ${Media.desktop(wrapperDesktopStyles)}
`;

const numberDesktopStyles = css`
  font-size: 500px;
  letter-spacing: -4px;
  opacity: 0.1;
`;

export const Number = styled.span`
  ${Typography.fontLyonLight};
  font-size: 300px;
  letter-spacing: -2.4px;
  opacity: 0.2;
  line-height: 1;
  color: ${Variables.colorWarmGrey};
  display: block;
  text-align: right;
  
  ${getNumberRightStyles}
  
  ${Media.desktop(numberDesktopStyles)}
`;

const titleOneDesktopStyles = css`
  bottom: 114px;
`;

const titleTwoDesktopStyles = css`
  right: 58px;
  left: auto;
  bottom: 149px;
`;

const titleThreeDesktopStyles = css`
  bottom: 142px;
`;

const getTitleStyles = cond([
  [isNumber2, always(titleTwoDesktopStyles)],
  [isNumber3, always(titleThreeDesktopStyles)],
  [T, always(titleOneDesktopStyles)],
]);

const titleDesktopStyles = css`
  font-size: 40px;
  letter-spacing: 24px;
  max-width: 100%;
  
  ${getTitleStyles}
`;

export const Title = styled.h2`
  ${Typography.fontGothamBook};
  position: absolute;
  font-size: 22px;
  letter-spacing: 13px;
  max-width: 300px;
  bottom: 85px;
  right: auto;
  left: 0;
    
  ${Media.desktop(titleDesktopStyles)}
`;
