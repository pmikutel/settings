import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Step2Actions } from '../../../modules/step2/step2.redux';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { selectData } from '../../../modules/step2/step2.selectors';
import { selectImage } from '../../../modules/blog/blog.selectors';
import { Step2 } from './step2.component';
import { BlogActions } from '../../../modules/blog/blog.redux';
import { selectIsLoggedIn, selectProjectId } from '../../../modules/user/user.selectors';

const mapStateToProps = createStructuredSelector({
  data: selectData,
  articleImage: selectImage,
  isLoggedIn: selectIsLoggedIn,
  projectId: selectProjectId,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  updateData: Step2Actions.updateData,
  blogRequest: BlogActions.request,
  updateAttributes: CheckoutActions.updateAttributesRequest,
  createProject: CheckoutActions.createProject,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Step2);
