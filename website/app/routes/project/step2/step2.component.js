import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { always, cond, ifElse, prop, propEq, T, pipe, isEmpty, both, complement, either, isNil } from 'ramda';

import { Step2Form } from './components/form/form.component';
import { StaticHeroImage } from '../../../components/staticHeroImage/staticHeroImage.component';

import messages from './step2.messages';

import { Description, Wrapper } from './step2.styles';
import { Grid } from '../../../theme';


export class Step2Component extends PureComponent {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    intl: PropTypes.object.isRequired,
    updateData: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    articleImage: PropTypes.string,
    projectId: PropTypes.string,
    blogRequest: PropTypes.func.isRequired,
    updateAttributes: PropTypes.func.isRequired,
    createProject: PropTypes.func.isRequired,
    isLoggedIn: PropTypes.bool,
  };

  state = {
    loading: false,
  };

  componentDidMount() {
    this.props.blogRequest('step2');
  }

  handleSubmit = data => {
    this.props.updateData(data);
    cond([
      [both(propEq('isLoggedIn', true), pipe(prop('projectId'), complement(either(isEmpty, isNil)))),
        () => {
          this.setState({ loading: true });
          this.props.updateAttributes(data, true);
        }],
      [propEq('isLoggedIn', true), () => {
        this.setState({ loading: true });
        this.props.createProject(data);
      }],
      [T, () => this.props.history.push('/project/step3')],
    ])(this.props);
  };

  render = () => (
    <Wrapper>
      <StaticHeroImage
        heroImage={this.props.articleImage}
        title={this.props.intl.formatMessage(messages.heroTitle)}
        subtitle={this.props.intl.formatMessage(messages.heroSubtitle)}
        shouldShowStatus
        status={ifElse(prop('isLoggedIn'), always('2/2'), always('2/3'))(this.props)}
        showBackIcon
      />

      <Grid.Section>
        <Grid.Row>
          <Grid.Column columns={4} offset={3} mobileOffset={1} mobileColumns={5}>
            <Description>
              {this.props.intl.formatMessage(messages.description)}
            </Description>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>

      <Step2Form
        loading={this.state.loading}
        onSubmit={this.handleSubmit}
        initialValues={this.props.data}
      />
    </Wrapper>
  );
}

export const Step2 = injectIntl(Step2Component);
