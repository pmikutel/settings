
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'notFound.header',
    defaultMessage: '404 ERROR',
  },
  description: {
    id: 'notFound.description',
    defaultMessage: 'Page Not Found',
  },
  info: {
    id: 'notFound.info',
    defaultMessage: 'It appears the page you were looking for doesn’t exist. Sorry about that.',
  },
  cta: {
    id: 'notFound.cta',
    defaultMessage: 'Back to Home',
  },
});
