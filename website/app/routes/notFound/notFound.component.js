import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import Helmet from 'react-helmet';

import { Wrapper, Header, Description, Logo, Info, LinkButtonWrapper, sectionStyles } from './notFound.styles';
import { Grid, Components } from '../../theme';

import messages from './notFound.messages';

export class NotFound extends PureComponent {
  render() {
    return (
      <Wrapper>
        <Helmet
          title="Not Found"
        />
        <Grid.Section extraStyles={sectionStyles}>
          <Logo to="/" />
          <Header>
            <FormattedMessage {...messages.header} />
          </Header>
          <Description>
            <FormattedMessage {...messages.description} />
          </Description>
          <Info>
            <FormattedMessage {...messages.info} />
          </Info>
          <LinkButtonWrapper>
            <Components.ActionLink extraStyles={Components.centeredButtonExtraStyles} to="/">
              <FormattedMessage {...messages.cta} />
            </Components.ActionLink>
          </LinkButtonWrapper>
        </Grid.Section>
      </Wrapper>
    );
  }
}
