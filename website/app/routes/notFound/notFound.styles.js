import styled, { css } from 'styled-components';
import * as RRD from 'react-router-dom';
import { Variables } from '../../theme';
import Sprite from '../../utils/sprites';

export const Wrapper = styled.div`
  display: inline-block;
  width: 100%;
  min-height: 100vh;
  background-color: ${Variables.colorBackgroundGray};
`;

export const Header = styled.h1`
  margin-bottom: 22px;
  text-align: center;
`;

export const Description = styled.p`
  line-height: 36px;
  text-align: center;
  margin: 0;
`;

export const Info = styled.p`
  text-align: center;
  margin: 0 0 30px;
`;

export const Logo = styled(RRD.Link)`
  ${Sprite.responsive('logo')};
  display: block;
  margin: 0 auto 151px;
`;

export const LinkButtonWrapper = styled.div`
  max-width: 225px;
  margin: 0 auto;
`;

export const sectionStyles = css`
  z-index: 1;
  padding: 65px 20px;
`;
