import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { All } from './all.component';
import { ProductsActions } from '../../../modules/products/products.redux';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { CartActions } from '../../../modules/cart/cart.redux';
import {
  selectProductsItems,
  selectProductsKeys,
  selectLastProductCursor,
  selectHasNextPage,
} from '../../../modules/products/products.selectors';
import { CollectionsActions } from '../../../modules/collections/collections.redux';
import { selectCollections } from '../../../modules/collections/collections.selectors';
import { ProductTypesActions } from '../../../modules/productTypes/productTypes.redux';
import { selectProductTypes } from '../../../modules/productTypes/productTypes.selectors';
import { BlogActions } from '../../../modules/blog/blog.redux';
import { selectArticles } from '../../../modules/blog/blog.selectors';
import { selectProjectId } from '../../../modules/user/user.selectors';

const mapStateToProps = createStructuredSelector({
  productsKeys: selectProductsKeys,
  productsItems: selectProductsItems,
  collectionsItems: selectCollections,
  productTypesItems: selectProductTypes,
  lastProductCursor: selectLastProductCursor,
  hasNextPage: selectHasNextPage,
  articles: selectArticles,
  projectId: selectProjectId,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  productsRequest: ProductsActions.request,
  addItem: CartActions.addItem,
  collectionsRequest: CollectionsActions.request,
  productTypesRequest: ProductTypesActions.request,
  addProjectItem: CheckoutActions.customerProjectLineItemAddRequest,
  blogRequest: BlogActions.request,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(All));
