import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl, FormattedMessage } from 'react-intl';
import { ifElse, always, propEq, pipe, filter, map, equals, isEmpty } from 'ramda';

import {
  InfoHeader,
  sliderRowStyles,
  selectRowStyles,
  selectColumnLeftStyles,
  selectColumnRightStyles,
  itemsRowStyles,
  loadMoreRowStyles,
} from './all.styles';

import { Grid, Components } from '../../../theme';

import { MasonryGrid } from '../../../components/masonryGrid/masonryGrid.component';
import { HeroSlider } from '../../../components/heroSlider/heroSlider.component';
import { ProductItem } from '../../../components/productItem/productItem.component';
import { ProductSelect } from '../../../components/productSelect/productSelect.component';
import { Loader } from '../../../components/loader/loader.component';
import { WindowScroll } from '../../../components/windowScroll/windowScroll.component';

import { decode } from '../../../utils/shopifyUrls';

import messages from './all.messages';
import BrowserDetection from '../../../utils/browserDetection';

const detection = new BrowserDetection();

/**
 * TODO
 * once API will allow to do filtering by both Collection and ProductType pls do following:
 * 1. In on<FilterType>Handler functions:
 * - remove setting state to empty string
 * - remove productRequests with only 2 params being used (third as '')
 * - uncomment productRequests which utilises all 3 params
 * 2. In render function:
 * - remove oppositeFilterState props and its handling in productSelect component (get option function)
 */

export class AllComponent extends PureComponent {
  static propTypes = {
    productsRequest: PropTypes.func.isRequired,
    addItem: PropTypes.func.isRequired,
    productsKeys: PropTypes.object.isRequired,
    productsItems: PropTypes.object.isRequired,
    collectionsRequest: PropTypes.func.isRequired,
    collectionsItems: PropTypes.object.isRequired,
    productTypesRequest: PropTypes.func.isRequired,
    productTypesItems: PropTypes.object.isRequired,
    lastProductCursor: PropTypes.string.isRequired,
    hasNextPage: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
    addProjectItem: PropTypes.func.isRequired,
    blogRequest: PropTypes.func.isRequired,
    articles: PropTypes.object.isRequired,
    projectId: PropTypes.string,
  };

  state = {
    productsType: '',
    collection: '',
    scroll: {},
  };

  componentDidMount() {
    ifElse(
      equals(true),
      () => this.props.productsRequest('', '', '', true, '', 20),
      () => this.props.productsRequest(),
    )(detection.isPrerenderingBot);
    this.props.collectionsRequest();
    this.props.productTypesRequest();
    this.props.blogRequest('Our Products', 4);
  }

  onSelectProductTypeHandler = (productsType) => {
    if (productsType === this.state.productsType) {
      return;
    }

    this.setState({
      productsType,
      collection: '',
    });

    this.props.productsRequest(productsType);
    // this.props.productsRequest(productsType, this.state.collection);
  };

  onSelectCollectionHandler = (collection) => {
    if (collection === this.state.collection) {
      return;
    }

    this.setState({
      productsType: '',
      collection,
    });

    this.props.productsRequest('', collection);
    // this.props.productsRequest(this.state.productsType, collection);
  };

  onLoadMoreHandler = () => {
    this.props.productsRequest(this.state.productsType, this.state.collection, this.props.lastProductCursor, false);
  };

  get items() {
    return this.props.productsKeys.toJS().map(key => {
      const product = this.props.productsItems.getIn(['products', key]);
      const image = this.props.productsItems.getIn(['image', product.getIn(['images', 0])]);
      const firstVariant = product.getIn(['variants', 0]).toJS();

      return ({
        id: product.get('id'),
        title: product.get('title'),
        description: product.get('descriptionHtml'),
        productType: product.get('productType'),
        image: image.get('src'),
        variant: firstVariant,
        variantId: decode(firstVariant.id),
        size: firstVariant.size,
        price: firstVariant.price,
        wide: firstVariant.wide,
        delivery: firstVariant.delivery,
        handle: product.get('handle'),
      });
    });
  }

  get collections() {
    return pipe(
      filter(item => !item.get('image')),
      map((item) => ({ value: item.get('title'), label: item.get('title') }))
    )(this.props.collectionsItems.toArray());
  }

  get productTypes() {
    const result = [];

    this.props.productTypesItems.map(item => {
      const title = item.get('node');

      if (title) {
        result.push({ value: title, label: title });
      }
    });

    return result;
  }

  renderItems = () => this.items.map((item, index) => (
    <ProductItem
      arrayLength={this.items.length}
      key={index}
      index={index}
      id={item.id}
      image={item.image}
      title={item.title}
      description={item.description}
      variantId={item.variantId}
      size={item.size}
      price={item.price}
      compareAtPrice={item.variant.compareAtPrice}
      wide={item.wide}
      delivery={item.delivery}
      productType={item.productType}
      variantTitle={item.variant.title}
      productHandle={item.handle}
      url={`/products/${item.handle}/${item.id}/variant/${item.variantId}`}
      addCartItem={this.props.addItem}
      addProjectItem={this.props.addProjectItem}
      projectId={this.props.projectId}
      scroll={this.state.scroll}
    />
  ));

  renderLoadMoreButton = () => ifElse(
    propEq('hasNextPage', true),
    always(
      <Grid.Row extraStyles={loadMoreRowStyles}>
        <Components.Button noActive onClick={this.onLoadMoreHandler}>
          <FormattedMessage
            {...messages.loadMore}
          />
        </Components.Button>
      </Grid.Row>
    ),
    always(''),
  )(this.props);

  renderContent = () => ifElse(
    isEmpty,
    () => <Loader />,
    () =>
      <MasonryGrid scroll>
        {this.renderItems()}
      </MasonryGrid>,
  )(this.items);

  render = () => (
    <div>
      <WindowScroll onScroll={(e) => this.setState({ scroll: e })} />
      <Grid.Section>
        <Grid.Row extraStyles={sliderRowStyles}>
          <HeroSlider data={this.props.articles} />
        </Grid.Row>

        <Grid.Row>
          <Grid.Column offset={2} columns={6}>
            <InfoHeader>
              <FormattedMessage {...messages.info} />
            </InfoHeader>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row extraStyles={selectRowStyles}>
          <Grid.Column
            columns={7}
            offset={1}
            mobileOffset={0}
            mobileColumns={6}
            extraStyles={selectColumnLeftStyles}
          >
            <ProductSelect
              value={this.state.productsType}
              options={this.productTypes}
              onUpdate={this.onSelectProductTypeHandler}
              placeholder={this.props.intl.formatMessage(messages.productTypes)}
            />
          </Grid.Column>
          <Grid.Column
            columns={7}
            mobileColumns={6}
            extraStyles={selectColumnRightStyles}
          >
            <ProductSelect
              value={this.state.collection}
              options={this.collections}
              onUpdate={this.onSelectCollectionHandler}
              placeholder={this.props.intl.formatMessage(messages.collections)}
              upper
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row extraStyles={itemsRowStyles}>
          <Grid.Column offset={1} columns={14}>
            {this.renderContent()}
          </Grid.Column>
        </Grid.Row>

        {this.renderLoadMoreButton()}
      </Grid.Section>
    </div>
  );
}

export const All = injectIntl(AllComponent);
