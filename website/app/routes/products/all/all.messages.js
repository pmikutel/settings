
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'all.title',
    defaultMessage: 'All page',
  },
  collections: {
    id: 'all.collections',
    defaultMessage: 'Room',
  },
  productTypes: {
    id: 'all.productTypes',
    defaultMessage: 'Category',
  },
  loadMore: {
    id: 'all.loadMore',
    defaultMessage: 'Load More',
  },
  info: {
    id: 'all.info',
    defaultMessage: 'Select the variant that fits your space. Bespoke options available.',
  },
  clear: {
    id: 'all.clear',
    defaultMessage: 'Clear filters',
  },
});
