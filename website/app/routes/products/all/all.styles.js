import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../theme';

export const sliderRowStyles = css`
  margin-bottom: -90px;
  width: 100%;
`;

export const selectRowStyles = css`
  margin-bottom: 60px;
  flex-wrap: wrap;
  z-index: 2;
`;

const selectColumn = css`
  box-sizing: border-box;
`;

const selectColumnLeftDesktopStyles = css`
  padding-right: 15px;
`;

export const selectColumnLeftStyles = css`
  z-index: 2;
  ${selectColumn}
  ${Media.desktop(selectColumnLeftDesktopStyles)}
`;

const selectColumnRightDesktopStyles = css`
  padding-left: 15px;
`;

export const selectColumnRightStyles = css`
  z-index: 1;
  ${selectColumn}
  ${Media.desktop(selectColumnRightDesktopStyles)}
`;

const itemsRowMobileStyles = css`
  width: 100%;
`;

export const itemsRowStyles = css`
  margin-bottom: 60px;
  ${Media.mobile(itemsRowMobileStyles)}
`;

const loadMoreRowDesktopStyles = css`
  margin-bottom: 140px;
`;

export const loadMoreRowStyles = css`
  margin-bottom: 90px;
  display: flex;
  align-items: center;
  flex-direction: column;
  
  ${Media.mobile(loadMoreRowDesktopStyles)}
`;

export const infoHeaderDesktopStyles = css`
  margin-bottom: 110px;
  padding: 0 0 0 60px;
`;

export const InfoHeader = styled.p`
  color: ${Variables.colorWarmGrey};
  margin-bottom: 70px;
  padding: 50px 50px 0 56px;

  ${Media.desktop(infoHeaderDesktopStyles)}
`;
