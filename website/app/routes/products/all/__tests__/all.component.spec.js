import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { fromJS } from 'immutable';
import { AllComponent as All } from '../all.component';

import { intlMock } from '../../../../utils/testing';

describe('<All />', () => {
  const defaultProps = {
    productsRequest: identity,
    addItem: identity,
    addProjectItem: identity,
    blogRequest: identity,
    articles: fromJS({}),
    productsKeys: fromJS([]),
    productsItems: fromJS({}),
    collectionsRequest: identity,
    collectionsItems: fromJS({}),
    productTypesRequest: identity,
    productTypesItems: fromJS({}),
    lastProductCursor: '',
    hasNextPage: true,
    projectId: '',
    intl: intlMock(),
  };

  const component = (props = {}) => <All {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
