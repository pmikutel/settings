import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../all.container';
import { ProductsActions } from '../../../../modules/products/products.redux';
import { CartActions } from '../../../../modules/cart/cart.redux';
import { ProductTypesActions } from '../../../../modules/productTypes/productTypes.redux';
import { CollectionsActions } from '../../../../modules/collections/collections.redux';
import { CheckoutActions } from '../../../../modules/checkout/checkout.redux';
import { BlogActions } from '../../../../modules/blog/blog.redux';

describe('All: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call ProductsActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).productsRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ProductsActions.request());
    });

    it('should call CartActions.addItem', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CartActions.addItem());
    });

    it('should call CollectionsActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).collectionsRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CollectionsActions.request());
    });

    it('should call ProductTypesActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).productTypesRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ProductTypesActions.request());
    });

    it('should call CheckoutActions.customerProjectLineItemAddRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addProjectItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.customerProjectLineItemAddRequest());
    });

    it('should call BlogActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).blogRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(BlogActions.request());
    });
  });
});
