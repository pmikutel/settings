import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { intlMock } from '../../../../../utils/testing';

import { ProductComponent as Product } from '../product.component';

describe('<Product />', () => {
  const defaultProps = {
    title: '',
    data: {
      title: 'title',
      variantId: '123',
      image: { originalSrc: 'originalSrc' },
    },
    metafields: fromJS({
      deliveryDate: 'low',
    }),
    addItem: identity,
    addProjectItem: identity,
    collectionTitle: '',
    intl: intlMock(),
    productId: '12',
    isProductInProject: fromJS({}),
    fetchProject: identity,
    removeProjectItem: identity,
  };

  const component = (props = {}) => <Product {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props), { disableLifecycleMethods: true });

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
