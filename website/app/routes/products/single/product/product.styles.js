import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Variables, Media, Typography } from '../../../../theme';

const textStyles = css`
  font-size: 18px;
  line-height: 36px;
`;

const imageColumnDesktopStyles = css`
  margin: 0;
  min-height: 100%;
`;

export const imageColumnStyles = css`
  max-width: 85%;
  margin-left: -16px;
  overflow: hidden;
  z-index: -1;

  ${Media.desktop(imageColumnDesktopStyles)}
`;

export const ImageWrapper = styled.div`
  ${Media.desktopWide(css`
    width: 100%;
  `)}
  
  ${Media.desktop(css`
    position: ${ifElse(propEq('isFixed', true), always('fixed'), always('absolute'))};
    ${ifElse(propEq('isFixed', true), always('top: 0;'), always('bottom: 0;'))};
    width: 50%;
    max-width: 720px;
  `)}
`;

export const Image = styled.img`
  width: 100%;
`;

const titleDesktopStyles = css`
  text-align: left;
  margin-top: 0;
  display: ${ifElse(propEq('mobile', true), always('none'), always('block'))};
  position: ${({ fixed }) => fixed ? 'fixed' : 'absolute'};
  top: ${({ fixed }) => fixed ? 'auto' : '0px'};
  transform: translateY(${({ fixed, parallaxHeight }) => fixed ? -36 : parallaxHeight}px);
`;

export const Title = styled.h3`
  ${textStyles};
  margin-top: 85px;
  text-align: center;
  display: ${ifElse(propEq('mobile', true), always('none'), always('block'))};

  ${Typography.fontLyonLight}

  ${Media.desktop(titleDesktopStyles)}
`;

const variantDesktopWideStyles = css`
  font-size: 80px;
`;

const variantDesktopStyles = css`
  position: relative;
  transform: translateX(${({ bigTitle }) => bigTitle ? -22 : 0}%) scale(${({ bigTitle }) => bigTitle ? 1 : 0.5});
  font-size: 60px;
  letter-spacing: 32px;
  padding-right: 20px;
  text-align: left;
  overflow-wrap: break-word;
  width: 130%;
  transform-origin: left top;
`;

export const Variant = styled.h1`
  text-align: center;
  
  ${Media.desktop(variantDesktopStyles)}
  ${Media.desktopWide(variantDesktopWideStyles)}
`;

const bounceContainerDesktopStyles = css`
  padding-top: 59px;
`;

export const BounceContainer = styled.div`
  position: relative;
  
  ${Media.desktop(bounceContainerDesktopStyles)}
`;

export const Details = styled.div``;

const descriptionTitleDesktopStyles = css`
  letter-spacing: 12px;
`;

export const DescriptionTitle = styled.h3`
  text-transform: uppercase;
  letter-spacing: 8px;

  ${Media.desktop(descriptionTitleDesktopStyles)}
`;

export const Description = styled.p`
 color: ${Variables.colorWarmGrey};
`;

export const sizeDesktopStyles = css`
  padding-top: 0;
  padding-bottom: 66px;
`;
export const Size = styled.div`
  padding-top: 80px;
  ${textStyles};
  padding-bottom: 31px;
  color: ${Variables.colorWarmGrey};

  ${Media.desktop(sizeDesktopStyles)}
`;

const deliveryDesktopStyles = css`
  padding-bottom: 66px;
`;

export const Delivery = styled.div`
  ${textStyles};
  padding: 10px 0 62px;

  ${Media.desktop(deliveryDesktopStyles)}
`;

export const priceDesktopWideStyles = css`
  text-align: right;
  letter-spacing: 24px;
`;

export const Price = styled.h2`
  letter-spacing: 13px;
  text-align: left;

  ${Media.desktopWide(priceDesktopWideStyles)}
`;

const usableRowDesktopWideStyles = css`
  margin-bottom: 97px;

  &:last-child {
    flex-direction: row;
  }
`;

export const UsableRow = styled.div`
  margin-bottom: 48px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;

  &:last-child {
    flex-direction: column;
  }

  ${Media.desktopWide(usableRowDesktopWideStyles)}
`;

const rowDesktoptyles = css`
  flex-direction: row;
  margin-top: -111px;
`;

export const rowStyles = css`
  flex-direction: column;
  position: relative;
  z-index: 1;

  ${Media.desktop(rowDesktoptyles)}
`;

export const MobileContent = styled.div`
  display: block;
  position: absolute;
  width: 100%;
  top: 0;
  left: 8px;

  ${Media.desktop(css`display:none;`)}
`;

export const DesktopContent = styled.div`
  display: none;

  ${Media.desktop(css`
    display:block;
    margin-top: 135px;
    position: relative;
    padding-top: 36px;
  `)}
`;

const buttonDesktopWideStyles = css`
  width: 245px;
  margin: 0;
`;

const buttonDesktopStyles = css`
  margin-bottom: 20px;
  margin-right: 0;
`;

export const buttonStyles = css`
  width: calc(100% - 4px);
  text-align: left;
  margin-bottom: 20px;
  margin-right: 0;

  ${Media.desktopWide(buttonDesktopWideStyles)}
  ${Media.desktop(buttonDesktopStyles)}
`;
