import { defineMessages } from 'react-intl';

export default defineMessages({
  details: {
    id: 'product.details',
    defaultMessage: 'Details',
  },
  material: {
    id: 'product.material',
    defaultMessage: 'Material',
  },
  care: {
    id: 'product.care',
    defaultMessage: 'Care',
  },
  delivery: {
    id: 'product.delivery',
    defaultMessage: 'Estimated delivery time: <strong> { time }</strong>',
  },
  addCart: {
    id: 'product.addCart',
    defaultMessage: 'Add to Basket',
  },
  addedCart: {
    id: 'product.addedCart',
    defaultMessage: 'Item added',
  },
  saveProject: {
    id: 'product.saveProject',
    defaultMessage: 'Save to Project',
  },
  savedProject: {
    id: 'product.savedProject',
    defaultMessage: 'Item Saved',
  },
  removeProject: {
    id: 'product.removeProject',
    defaultMessage: 'Remove from Project',
  },
  removedProject: {
    id: 'product.removedProject',
    defaultMessage: 'Item removed',
  },
  currency: {
    id: 'product.currency',
    defaultMessage: '£{ price }',
  },
  low: {
    id: 'product.low',
    defaultMessage: '1-2 weeks',
  },
  medium: {
    id: 'product.medium',
    defaultMessage: '3-4 weeks',
  },
  high: {
    id: 'products.high',
    defaultMessage: '5+ weeks',
  },
});
