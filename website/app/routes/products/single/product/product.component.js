import React, { PureComponent } from 'react';
import { always, cond, prop, T, isNil, complement, pipe, either, lt } from 'ramda';
import { StickyContainer, Sticky } from 'react-sticky';
import Helmet from 'react-helmet';

import PropTypes from 'prop-types';
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from 'react-intl';
import { Accordeon } from '../../../../components/accordeon/accordeon.component';
import { Counter } from '../../../../components/counter/counter.component';
import { encode } from '../../../../utils/shopifyUrls';
import { extractVariantTitle } from '../../../../utils/shopifyData';
import { AnimatedText } from '../../../../components/animatedText/animatedText.component';
import { getProductVarianAnimation } from '../../../../utils/animations';
import { WindowScroll } from '../../../../components/windowScroll/windowScroll.component';
import { isDesktop, isDesktopWide } from '../../../../theme/media';

import messages from './product.messages';

import {
  Image,
  Title,
  Variant,
  Details,
  DescriptionTitle,
  Description,
  Size,
  Delivery,
  rowStyles,
  UsableRow,
  Price,
  imageColumnStyles,
  MobileContent,
  DesktopContent,
  buttonStyles,
  BounceContainer,
  ImageWrapper,
} from './product.styles';

import { Grid, Components } from '../../../../theme';

const parallaxHeight = isDesktopWide() ? 97 : 87;

export class ProductComponent extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    data: PropTypes.object,
    intl: PropTypes.object.isRequired,
    metafields: PropTypes.object,
    addItem: PropTypes.func.isRequired,
    collectionTitle: PropTypes.string,
    productId: PropTypes.string,
    productHandle: PropTypes.string,
    addProjectItem: PropTypes.func.isRequired,
    projectId: PropTypes.string,
    isProductInProject: PropTypes.object,
    fetchProject: PropTypes.func.isRequired,
    removeProjectItem: PropTypes.func.isRequired,
  };

  state = {
    counter: 1,
    cartUpdated: false,
    projectId: null,
    bigTitle: true,
    fixedTitle: true,
  };

  componentDidMount() {
    window.prerenderReady = true;
    if (this.props.projectId) {
      this.props.fetchProject(this.props.projectId, true);
    }
    this.parallax = getProductVarianAnimation(this.variantRef, parallaxHeight);
    this.handleScroll();
  }

  setCounter = (counter) => {
    this.setState({ counter });
  };

  handleScroll = () => {
    if (!isDesktop()) {
      return;
    }
    const { scrollY } = window;
    const distance = Math.max(0, Math.min(parallaxHeight, scrollY));
    const progress = distance / parallaxHeight;
    this.parallax.progress(progress);
    this.setState({ fixedTitle: progress !== 1 });
  };

  get variantTitle() {
    return extractVariantTitle(this.props.data.title);
  }

  addCartItem = () => {
    const data = {
      id: this.props.productId,
      title: this.props.title,
      variantTitle: this.variantTitle,
      image: this.props.data.image.originalSrc,
      size: this.props.data.size,
      delivery: this.props.data.delivery,
      quantity: this.state.counter,
      price: this.props.data.price,
      variantId: this.encodedVariantId,
      productHandle: this.props.productHandle,
      counter: this.state.counter,
    };

    this.props.addItem(data);
    this.setState({ cartUpdated: true });
  };

  addProjectItem = () => {
    const data = {
      quantity: this.state.counter,
      variantId: this.encodedVariantId,
    };

    this.props.addProjectItem(data);
  };

  removeProjectItem = () => this.props.removeProjectItem(this.props.isProductInProject.get('id'));

  get encodedVariantId() {
    return encode(this.props.data.id, 'variant');
  }

  variantRef = null;

  handleVariantRef = ref => (this.variantRef = ref);

  renderProjectButton = () => cond([
    [pipe(prop('isProductInProject'), complement(isNil)), () =>
      <Components.Button extraStyles={buttonStyles} noActive onClick={this.removeProjectItem}>
        {this.props.intl.formatMessage(messages.removeProject)}
        <Components.Arrow />
      </Components.Button>,
    ],
    [prop('projectId'), () =>
      <Components.Button extraStyles={buttonStyles} noActive onClick={this.addProjectItem}>
        {this.props.intl.formatMessage(messages.saveProject)}
        <Components.Arrow />
      </Components.Button>,
    ],
    [T, always(null)],
  ])(this.props);

  renderAccordion = (title, content) => {
    if (content && content !== 'none') {
      return (
        <Accordeon title={title} description={content} />
      );
    }

    return null;
  };

  render = () => {
    const { data, metafields } = this.props;
    return (
      <Grid.Section>
        <WindowScroll onScroll={this.handleScroll} throttle={false} />
        <Helmet
          title={this.variantTitle}
          meta={[
            { name: 'description', content: metafields.get('shortDescription') },
            { property: 'og:title', content: this.variantTitle },
            { property: 'og:image', content: data.image.originalSrc },
            { property: 'og:description', content: metafields.get('shortDescription') },
            { itemprop: 'name', content: this.variantTitle },
            { itemprop: 'image', content: data.image.originalSrc },
            { itemprop: 'description', content: metafields.get('shortDescription') },
            { name: 'twitter:card', content: 'summary_large_image' },
          ]}
        />

        <Grid.Row extraStyles={rowStyles}>
          <Grid.Column
            columns={8}
            mobileColumns={6}
            mobileOffset={0}
            extraStyles={imageColumnStyles}
          >
            <StickyContainer style={{ height: '100%', display: 'flex', flex: 1 }}>
              <Sticky disableCompensation>
                {({ distanceFromBottom }) =>
                  <ImageWrapper isFixed={either(isNil, lt(0))(distanceFromBottom)}>
                    <Image src={data.image.originalSrc} />
                    <MobileContent>
                      <Title>{this.props.title}</Title>
                      <Variant>{this.variantTitle}</Variant>
                    </MobileContent>
                  </ImageWrapper>
                }
              </Sticky>
            </StickyContainer>
          </Grid.Column>
          <Grid.Column columns={6} mobileColumns={6} offset={1} mobileOffset={0}>
            <DesktopContent>
              <Title fixed={this.state.fixedTitle} parallaxHeight={parallaxHeight}>{this.props.title}</Title>
              <Variant innerRef={this.handleVariantRef}>{this.variantTitle}</Variant>
            </DesktopContent>
            <BounceContainer>
              <Size>{data.size}</Size>
              <Details>
                <DescriptionTitle><FormattedMessage{...messages.details} /></DescriptionTitle>
                <Description>{metafields.get('shortDescription')}</Description>
              </Details>
              {
                this.renderAccordion(
                  this.props.intl.formatMessage(messages.material),
                  metafields.get('material')
                )
              }
              {
                this.renderAccordion(
                  this.props.intl.formatMessage(messages.care),
                  metafields.get('care')
                )
              }
              <Delivery>
                <FormattedHTMLMessage
                  {...messages.delivery}
                  values={{ time: this.props.data.delivery }}
                />
              </Delivery>
              <UsableRow>
                <Counter
                  min={1}
                  max={9999}
                  value={this.state.counter}
                  onChange={this.setCounter}
                />
                <Price>
                  <FormattedMessage
                    {...messages.currency}
                    values={{ price: data.price * this.state.counter }}
                  />
                </Price>
              </UsableRow>
              <UsableRow>
                <Components.Button extraStyles={buttonStyles} noActive onClick={this.addCartItem}>
                  <AnimatedText
                    text={this.props.intl.formatMessage(messages.addCart)}
                    successText={this.props.intl.formatMessage(messages.addedCart)}
                    waiting={this.state.cartUpdated}
                    callback={() => this.setState({ cartUpdated: false })}
                  />
                  <Components.Arrow />
                </Components.Button>
                {this.renderProjectButton()}
              </UsableRow>
            </BounceContainer>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    );
  };
}

export const Product = injectIntl(ProductComponent);
