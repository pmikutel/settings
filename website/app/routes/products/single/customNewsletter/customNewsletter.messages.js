import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'custom.title',
    defaultMessage: 'want to make it yours?',
  },
  description: {
    id: 'custom.description',
    defaultMessage: `Custom finish is available. Submit your details if you would like to find out more about 
    customising this product and we will be in touch.`,
  },
});
