import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { intlMock } from '../../../../../utils/testing';

import { CustomNewsletterComponent as CustomNewsletter } from '../customNewsletter.component';


describe('<CustomNewsletter />', () => {
  const defaultProps = {
    customNewsletterRequest: identity,
    customNewsletterSuccess: false,
    customNewsletterError: false,
    customNewsletterFetching: false,
    clearError: identity,
    intl: intlMock(),
  };

  const component = (props = {}) => <CustomNewsletter {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
