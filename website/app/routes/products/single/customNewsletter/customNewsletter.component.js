import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import { CustomNewsletterForm } from './customNewsletterForm/customNewsletterForm.component';

import messages from './customNewsletter.messages';

import {
  Wrapper,
  Title,
  Description,
} from './customNewsletter.styles';

import { Grid } from '../../../../theme';
import { TryAgain } from '../../../../components/tryAgain/tryAgain.component';


export class CustomNewsletterComponent extends PureComponent {
  static propTypes = {
    customNewsletterRequest: PropTypes.func.isRequired,
    customNewsletterSuccess: PropTypes.bool.isRequired,
    customNewsletterError: PropTypes.bool.isRequired,
    customNewsletterFetching: PropTypes.bool.isRequired,
    clearError: PropTypes.func.isRequired,
    intl: PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.clearError();
  }

  render = () => (
    <Wrapper>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={3} columns={10}>
            <Title>{this.props.intl.formatMessage(messages.title)}</Title>
            <Description>{this.props.intl.formatMessage(messages.description)}</Description>
            <CustomNewsletterForm
              onSubmit={this.props.customNewsletterRequest}
              hasError={this.props.customNewsletterError}
              isSuccess={this.props.customNewsletterSuccess}
              isFetching={this.props.customNewsletterFetching}
            />
            <TryAgain isActive={this.props.customNewsletterError} />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    </Wrapper>

  );
}

export const CustomNewsletter = injectIntl(CustomNewsletterComponent);

