import styled, { css } from 'styled-components';
import { ifElse, propEq, always } from 'ramda';
import { Media, Typography, Variables } from '../../../../../theme';

const inputDesktopStyles = css`
  width: 100%;
`;

const inputErrorStyles = ifElse(
  propEq('hasError', true),
  () => css`
    border-color: ${Variables.colorRed};
    color: ${Variables.colorRed};
  `,
  always(''),
);

export const Input = styled.input`  
  width: 100%;
  color:  ${Variables.colorPureWhite};
  border-color:  ${Variables.colorPureWhite};
  ${Typography.fontLyonLight}; 
  
  &::placeholder {
    color: ${Variables.colorPureWhite};
  }
  
  &:-webkit-autofill {
    transition: background-color 5000s ease-in-out 0s;
    -webkit-text-fill-color: ${Variables.colorPureWhite};
  }
  
  ${Media.desktop(inputDesktopStyles)}
  ${inputErrorStyles}
`;

export const Form = styled.form`
  position: relative;
  width: 100%;
`;

export const SuccessIcon = styled.span`
  backface-visibility: hidden;
  border: 3px solid ${Variables.colorPureWhite};
  border-right: 0;
  border-top: 0;
  height: 14px;
  margin: auto;
  position: absolute;
  right: 19px;
  top: 19px;
  transform: rotate(-45deg);
  width: 25px;
`;

export const SubmitButton = styled.button`
  display: block;
  height: 2px;
  padding: 20px;
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: 20px;
  width: 18px;
  
  &:before {
    background: ${Variables.colorPureWhite};
    content: '';
    display: block;
    height: 2px;
    position: absolute;
    top: 19px;
    right: 1px;
    width: 43px;
  }
  
  &:after {
    border-right: 2px solid ${Variables.colorPureWhite};
    border-bottom: 2px solid ${Variables.colorPureWhite};
    content: '';
    display: inline-block;
    padding: 5px;
    position: absolute;
    right: 0;
    top: 14px;
    transform: rotate(-45deg);
  }
 `;
