import { defineMessages } from 'react-intl';

export default defineMessages({
  placeholder: {
    id: 'customNewsletterForm.placeholder',
    defaultMessage: 'Type your email here',
  },
});
