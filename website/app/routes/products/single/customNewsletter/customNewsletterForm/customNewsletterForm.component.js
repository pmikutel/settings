import React, { PureComponent } from 'react';
import { always, cond, propEq, T } from 'ramda';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form/immutable';
import { injectIntl } from 'react-intl';
import validate from 'validate.js';

import { DotsLoader } from '../../../../../components/dotsLoader/dotsLoader.component';
import messages from './customNewsletterForm.messages';
import { Input, Form, SuccessIcon, SubmitButton } from './customNewsletterForm.styles';

export class customNewsletterFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    isSuccess: PropTypes.bool.isRequired,
    hasError: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired,
    valid: PropTypes.bool,
    intl: PropTypes.object.isRequired,
  };

  renderInput = ({ input, type, placeholder, hasError, meta: { invalid, dirty, active } }) => (
    <Input
      hasError={hasError || (dirty && !active && invalid)}
      placeholder={placeholder}
      type={type}
      {...input}
      required
    />
  );

  renderIcon = () => cond([
    [propEq('isSuccess', true), always(<SuccessIcon />)],
    [propEq('isFetching', true), always(<DotsLoader />)],
    [T, always(<SubmitButton type="submit" disabled={!this.props.valid} />)],
  ])(this.props);

  render = () => (
    <Form onSubmit={this.props.handleSubmit} noValidate>
      <Field
        name="email_address"
        hasError={this.props.hasError}
        component={this.renderInput}
        placeholder={this.props.intl.formatMessage(messages.placeholder)}
      />
      {this.renderIcon()}
    </Form>
  );
}

export const CustomNewsletterForm = injectIntl(reduxForm({
  form: 'customNewsletter',
  validate: (values) => validate(values.toJS(), {
    'email_address': {
      email: true,
      presence: {
        allowEmpty: false,
      },
    },
  }),
})(customNewsletterFormComponent));
