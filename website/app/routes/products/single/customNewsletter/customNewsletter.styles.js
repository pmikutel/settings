import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../../theme';

const wrapperDesktopStyles = css`
  padding: 191px 0 226px;  
`;


export const Wrapper = styled.div`
  background-image: ${Variables.gradientCustom};
  padding: 98px 0 184px;
  
  ${Media.desktop(wrapperDesktopStyles)}
`;

const titleDesktopStyles = css`
  padding-bottom: 49px;
  letter-spacing: 24px;
  line-height: 1.2;
`;

export const Title = styled.h2`
  color: ${Variables.colorPureWhite};
  text-transform: uppercase;  
  text-align: center;
  margin: 0 auto;
  max-width: 690px;
  padding-bottom: 55px;
  letter-spacing: 13px;
  font-weight: 300;
  line-height: normal;
  
  ${Media.desktop(titleDesktopStyles)}  
`;

const descriptionDesktopStyles = css`
  padding-bottom: 50px;
`;

export const Description = styled.p`
  color: ${Variables.colorPureWhite};
  text-align: center;
  font-weight: 300;
  font-size: 18px;
  max-width: 560px;
  margin: 0 auto;
  padding-bottom: 145px;
  line-height: 2;
  
  ${Media.desktop(descriptionDesktopStyles)}
`;
