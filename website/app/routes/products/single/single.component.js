import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ifElse, pathEq, T, always, propEq, cond, prop, equals } from 'ramda';
import { injectIntl } from 'react-intl';

import { Product } from './product/product.component';
import { Variants } from './variants/variants.component';
import { CustomNewsletter } from './customNewsletter/customNewsletter.component';
import { ProductSlider } from '../../../components/productSlider/productSlider.component';
import { flattenDeepSelectedProductsOptions, flattenDeepSelectedOptions } from '../../../utils/shopifyData';

import { sliderWrapperStyles, EmptyTitle } from './single.styles';
import { Grid, Components } from '../../../theme';

import messages from './single.messages';
import { Loader } from '../../../components/loader/loader.component';


export class SingleComponent extends PureComponent {
  static propTypes = {
    match: PropTypes.object.isRequired,
    productRequest: PropTypes.func.isRequired,
    metafieldsRequest: PropTypes.func.isRequired,
    activeVariant: PropTypes.object,
    activeVariantId: PropTypes.string,
    variants: PropTypes.object,
    product: PropTypes.object,
    metafields: PropTypes.object,
    customNewsletterRequest: PropTypes.func.isRequired,
    customNewsletterSuccess: PropTypes.bool.isRequired,
    customNewsletterError: PropTypes.bool.isRequired,
    customNewsletterFetching: PropTypes.bool.isRequired,
    customNewsletterReset: PropTypes.func.isRequired,
    collectionTitle: PropTypes.string,
    collections: PropTypes.object,
    addItem: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
    addProjectItem: PropTypes.func.isRequired,
    projectId: PropTypes.string,
    isProductInProject: PropTypes.object,
    fetchProject: PropTypes.func.isRequired,
    removeProjectItem: PropTypes.func.isRequired,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  componentDidMount() {
    this.props.productRequest(this.props.match.params.id, this.props.match.params.activeVariantId);
  }

  componentWillUpdate(nextProps) {
    if (equals(this.props.match.params, nextProps.match.params)) {
      return;
    }
    this.props.productRequest(nextProps.match.params.id, nextProps.match.params.activeVariantId);
  }

  renderSlider = () => ifElse(
    prop('size'),
    () =>
      <ProductSlider
        title={this.props.intl.formatMessage(messages.sliderTitle)}
        items={this.props.collections}
      />,
    always(null),
  )(this.props.collections);

  renderLoader = () => (
    <Loader />
  );

  renderPage = () => (
    <div>
      <Product
        data={flattenDeepSelectedOptions(this.props.activeVariant.toJS())}
        metafields={this.props.metafields}
        title={this.props.product.get('title')}
        addItem={this.props.addItem}
        addProjectItem={this.props.addProjectItem}
        removeProjectItem={this.props.removeProjectItem}
        collectionTitle={this.props.collectionTitle}
        projectId={this.props.projectId}
        isProductInProject={this.props.isProductInProject}
        fetchProject={this.props.fetchProject}
        productHandle={this.props.product.get('handle')}
        productId={this.props.product.get('id')}
      />
      <Variants
        title={this.props.product.get('title')}
        variants={flattenDeepSelectedProductsOptions(this.props.variants.toJS())}
        metafieldsRequest={this.props.metafieldsRequest}
        metafields={this.props.metafields}
        productId={this.props.product.get('id')}
        productHandle={this.props.product.get('handle')}
        addItem={this.props.addItem}
        addProjectItem={this.props.addProjectItem}
        projectId={this.props.projectId}
      />
      <CustomNewsletter
        customNewsletterRequest={this.props.customNewsletterRequest}
        customNewsletterSuccess={this.props.customNewsletterSuccess}
        customNewsletterError={this.props.customNewsletterError}
        customNewsletterFetching={this.props.customNewsletterFetching}
        clearError={this.props.customNewsletterReset}
      />
      <Grid.Section extraStyles={sliderWrapperStyles}>
        {this.renderSlider()}
      </Grid.Section>
    </div>
  );

  renderEmptyPage = () => (
    <div>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={1} columns={14}>
            <EmptyTitle>
              {this.props.intl.formatMessage(messages.emptyTitle)}
            </EmptyTitle>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
      <Components.SubmitButton onClick={this.context.router.history.goBack}>
        {this.props.intl.formatMessage(messages.back)}
      </Components.SubmitButton>
    </div>
  );

  render = () => cond([
    [propEq('isFetching', true), this.renderLoader],
    [pathEq(['metafields', 'size'], 0), this.renderEmptyPage],
    [T, this.renderPage],
  ])(this.props);
}

export const Single = injectIntl(SingleComponent);

