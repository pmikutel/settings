import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Single } from './single.component';
import { ProductActions } from '../../../modules/product/product.redux';
import { CartActions } from '../../../modules/cart/cart.redux';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { selectIsProductInProject } from '../../../modules/checkout/checkout.selectors';
import { selectProjectId } from '../../../modules/user/user.selectors';
import {
  selectProduct,
  selectActiveVariant,
  selectVariants,
  selectMetafields,
  selectCustomNewsletterError,
  selectCustomNewsletterSuccess,
  selectCustomNewsletterFetching,
  selectCollectionTitle,
  selectCollections,
  selectIsFetching,
} from '../../../modules/product/product.selectors';


const mapStateToProps = createStructuredSelector({
  product: selectProduct,
  activeVariant: selectActiveVariant,
  variants: selectVariants,
  metafields: selectMetafields,
  customNewsletterError: selectCustomNewsletterError,
  customNewsletterSuccess: selectCustomNewsletterSuccess,
  customNewsletterFetching: selectCustomNewsletterFetching,
  collectionTitle: selectCollectionTitle,
  collections: selectCollections,
  isFetching: selectIsFetching,
  isProductInProject: selectIsProductInProject,
  projectId: selectProjectId,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  productRequest: ProductActions.request,
  metafieldsRequest: ProductActions.metafieldsRequest,
  customNewsletterRequest: ProductActions.customNewsletterRequest,
  customNewsletterReset: ProductActions.customNewsletterReset,
  addItem: CartActions.addItem,
  addProjectItem: CheckoutActions.customerProjectLineItemAddRequest,
  removeProjectItem: CheckoutActions.customerProjectLineItemRemoveRequest,
  fetchProject: CheckoutActions.fetchRequest,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Single));
