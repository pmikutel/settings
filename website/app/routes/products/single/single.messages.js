import { defineMessages } from 'react-intl';

export default defineMessages({
  sliderTitle: {
    id: 'single.sliderTitle',
    defaultMessage: 'RELATED PRODUCTS',
  },
  emptyTitle: {
    id: 'single.emptyTitle',
    defaultMessage: 'We don\'t have enough information about this product. Please come back later.',
  },
  back: {
    id: 'single.back',
    defaultMessage: 'Back',
  },
});
