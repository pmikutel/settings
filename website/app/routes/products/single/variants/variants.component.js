import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ifElse, isNil, always, pipe, prop } from 'ramda';
import { FormattedMessage, injectIntl } from 'react-intl';

import { ProductItem } from '../../../../components/productItem/productItem.component';
import { MasonryGrid } from '../../../../components/masonryGrid/masonryGrid.component';
import { WindowScroll } from '../../../../components/windowScroll/windowScroll.component';

import messages from './variants.messages';

import {
  Wrapper,
  Title,
  InfoBox,
  Description,
  MasonryGridWrapper,
  rowStyles,
  linkStyles,
} from './variants.styles';

import { Grid, Components } from '../../../../theme';


export class VariantsComponent extends PureComponent {
  static propTypes = {
    variants: PropTypes.array,
    metafields: PropTypes.object,
    metafieldsRequest: PropTypes.func.isRequired,
    productId: PropTypes.string,
    productHandle: PropTypes.string,
    productType: PropTypes.string,
    title: PropTypes.string,
    intl: PropTypes.object.isRequired,
    addItem: PropTypes.func.isRequired,
    addProjectItem: PropTypes.func.isRequired,
    projectId: PropTypes.string,
    scroll: PropTypes.object,
  };

  state = {
    scroll: {},
  };

  renderVariant = () => this.props.variants.map((variant, index) => (
    <ProductItem
      arrayLength={this.props.variants.length}
      id={this.props.productId}
      key={index}
      index={index}
      variantId={variant.id}
      image={variant.image.originalSrc}
      title={this.props.title}
      price={variant.price}
      compareAtPrice={variant.compareAtPrice}
      description={this.props.metafields.get('shortDescription')}
      size={variant.size}
      wide={variant.wide}
      delivery={variant.delivery}
      variantTitle={variant.title}
      productHandle={this.props.productHandle}
      url={`/products/${this.props.productHandle}/${this.props.productId}/variant/${variant.id}`}
      addCartItem={this.props.addItem}
      addProjectItem={this.props.addProjectItem}
      projectId={this.props.projectId}
      showVariantTitle
    />
  ));

  renderVariants = () => (
    <Grid.Section>
      <WindowScroll onScroll={(e) => this.setState({ scroll: e })} />
      <Components.Dots
        width="49%"
        height="1312"
        top="-461"
        mobileBottom="0"
        mobileHeight="602"
        mobileWidth="64%"
        mobileRight
      />
      <Grid.Row extraStyles={rowStyles}>
        <Grid.Column offset={1} columns={14}>
          <Wrapper>
            <Title><FormattedMessage {...messages.title} /></Title>

            <MasonryGridWrapper>
              <MasonryGrid>
                <InfoBox>
                  <Description><FormattedMessage {...messages.description} /></Description>
                  <Components.ActionLink extraStyles={linkStyles} desktop noActive to="/products/all">
                    <FormattedMessage {...messages.link} />
                    <Components.Arrow />
                  </Components.ActionLink>
                </InfoBox>

                {this.renderVariant()}

              </MasonryGrid>
              <Components.ActionLink extraStyles={linkStyles} noActive to="/products/all">
                <FormattedMessage {...messages.link} />
                <Components.Arrow />
              </Components.ActionLink>
            </MasonryGridWrapper>
          </Wrapper>
        </Grid.Column>
      </Grid.Row>
    </Grid.Section>
  );

  render = () => pipe(
    prop('variants'),
    ifElse(
      isNil,
      always(null),
      this.renderVariants,
    )
  )(this.props);
}

export const Variants = injectIntl(VariantsComponent);
