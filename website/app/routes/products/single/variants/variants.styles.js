import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Media, Variables } from '../../../../theme';

const MOBILE_PADDING_LEFT = '35px';

const wrapperDesktopStyles = css`
  padding: 104px 0 0;
  margin-bottom: 200px;
`;

export const Wrapper = styled.section`
  position: relative;
  padding: 32px 20px 0 20px;
  margin-bottom: 30px;

  ${Media.desktop(wrapperDesktopStyles)}
`;

const titleMobileStyles = css`
  font-size: 19px;
`;

const titleDesktopStyles = css`
  max-width: 960px;
  margin-left: 0;
  margin-bottom: 0;
  padding-left: 0;
  font-size: 28px;
`;

const titleDesktopWideStyles = css`
  margin-left: 95px;
  font-size: 80px;
  line-height: 94px;
`;

export const Title = styled.h1`
  margin: 0 auto 35px;
  position: relative;
  z-index: 2;
  line-height: 33px;
  padding-left: ${MOBILE_PADDING_LEFT};

  ${Media.mobile(titleMobileStyles)};
  ${Media.desktop(titleDesktopStyles)};
  ${Media.desktopWide(titleDesktopWideStyles)};
`;

const infoBoxDesktopStyles = css`
  line-height: 50px;
  max-width: 390px;
  margin: 55px 0 60px 55px;
  padding-left: 0;
`;

const infoBoxDesktopWideStyles = css`
  line-height: 50px;
  max-width: 420px;
  margin: 55px 0 60px 95px;
  padding-left: 0;
`;

export const InfoBox = styled.div`
  color: ${Variables.colorWarmGrey};
  font-size: 22px;
  line-height: 46px;
  margin-bottom: 20px;
  text-align: left;
  padding-left: ${MOBILE_PADDING_LEFT};

  ${Media.desktop(infoBoxDesktopStyles)}
  ${Media.desktopWide(infoBoxDesktopWideStyles)}
`;

export const Description = styled.p`
  color: ${Variables.colorWarmGrey};
  margin-bottom: 70px;
`;

export const MasonryGridWrapper = styled.div`
  width: 100%;
  margin: 0 auto;
  position: relative;
  top: -35px;
`;

export const rowStyles = css`
  width: 100%;
`;

const linkDesktopStyles = css`
  max-width: 198px;
  margin: 20px 0 0;
  display: ${ifElse(propEq('desktop', true), always('block'), always('none'))};
`;

export const linkStyles = css`
  max-width: 175px;
  margin: 60px auto 0;
  text-align: left;
  display: ${ifElse(propEq('desktop', true), always('none'), always('block'))};

  ${Media.desktop(linkDesktopStyles)};
`;
