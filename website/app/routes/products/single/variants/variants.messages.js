import { defineMessages } from 'react-intl';

export default defineMessages({
  link: {
    id: 'variants.link',
    defaultMessage: 'Browse all',
  },
  description: {
    id: 'variants.description',
    defaultMessage: `Haven’t found what you’re looking for? Check out other variations of this item to see if it better 
    suits your taste.`,
  },
  title: {
    id: 'variants.title',
    defaultMessage: 'AVAILABLE VARIANTS',
  },
});
