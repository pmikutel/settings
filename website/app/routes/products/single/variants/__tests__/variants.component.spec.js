import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';
import { expect } from 'chai';

import { intlMock } from '../../../../../utils/testing';

import { VariantsComponent as Variants } from '../variants.component';
import { ProductItem } from '../../../../../components/productItem/productItem.component';

describe('<Variants />', () => {
  const defaultProps = {
    variants: [],
    metafields: fromJS({}),
    metafieldsRequest: identity,
    productId: '',
    productType: '',
    title: '',
    intl: intlMock(),
    addItem: identity,
    addProjectItem: identity,
    projectId: '',
    productHandle: 'product-handle',
  };

  const component = (props = {}) => <Variants {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const props = {
      variants: [{
        id: 1,
        title: 'title/size',
        variantId: '123',
        image: { originalSrc: 'src' },
      },
      ],
    };

    const wrapper = render(props);

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('when variants exists', () => {
    it('should render component ', () => {
      const props = { variants: [{ id: 1, image: { originalSrc: 'src' }, title: 'title/size' }] };
      const wrapper = render(props);

      expect(wrapper.find(ProductItem)).to.have.length(1);
    });
  });

  describe('when variants does not exist', () => {
    it('should not render component', () => {
      const wrapper = render();

      expect(wrapper.find(ProductItem)).to.have.length(0);
    });
  });
});
