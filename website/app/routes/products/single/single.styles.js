import styled, { css } from 'styled-components';

export const sliderWrapperStyles = css`
  overflow: hidden;
`;

export const EmptyTitle = styled.h2`
  text-align: center;
  padding: 100px 0;
`;
