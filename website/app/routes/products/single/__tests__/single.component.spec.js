import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';
import { expect } from 'chai';

import { intlMock } from '../../../../utils/testing';

import { SingleComponent as Single } from '../single.component';
import { CustomNewsletter } from '../customNewsletter/customNewsletter.component';
import { ProductSlider } from '../../../../components/productSlider/productSlider.component';
import { EmptyTitle } from '../single.styles';

describe('<Single />', () => {
  const defaultProps = {
    match: {
      params: {
        id: 1,
        activeVariantId: 1,
      },
    },
    productRequest: identity,
    metafieldsRequest: identity,
    activeVariant: fromJS({
      selectedOptions: [{ name: 'options', value: 'size; delivery' }],
    }),
    activeVariantId: '',
    variants: fromJS({}),
    product: fromJS({
      id: '1',
      title: '',
      handle: 'handle',
    }),
    metafields: fromJS({}),
    customNewsletterReset: identity,
    customNewsletterSuccess: true,
    customNewsletterError: true,
    customNewsletterFetching: false,
    customNewsletterRequest: identity,
    collectionTitle: '',
    collections: fromJS({}),
    addItem: identity,
    addProjectItem: identity,
    isFetching: false,
    projectId: '',
    isProductInProject: fromJS({}),
    fetchProject: identity,
    removeProjectItem: identity,
    intl: intlMock(),
  };

  const component = (props = {}) => <Single {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const props = { collections: fromJS({ id: 1 }), metafields: fromJS({ id: 1 }) };

    const wrapper = render(props);

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('when data is fetching', () => {
    it('should not render page', () => {
      const props = { isFetching: true };
      const wrapper = render(props);

      expect(wrapper.find(CustomNewsletter)).to.have.length(0);
    });
  });

  describe('when metafields exist', () => {
    it('should render page', () => {
      const props = { metafields: fromJS({ id: 1 }) };
      const wrapper = render(props);

      expect(wrapper.find(CustomNewsletter)).to.have.length(1);
    });
  });

  describe('when metafields do not exist', () => {
    it('should render empty page', () => {
      const props = { metafields: {} };
      const wrapper = render(props);

      expect(wrapper.find(EmptyTitle)).to.have.length(0);
    });
  });

  describe('when collections exist', () => {
    it('should render slider', () => {
      const props = { collections: fromJS({ id: 1 }), metafields: fromJS({ id: 1 }) };
      const wrapper = render(props);

      expect(wrapper.find(ProductSlider)).to.have.length(1);
    });
  });

  describe('when collections do not exist', () => {
    it('should not render slider', () => {
      const props = { collections: {}, metafields: fromJS({ id: 1 }) };
      const wrapper = render(props);

      expect(wrapper.find(ProductSlider)).to.have.length(0);
    });
  });
});
