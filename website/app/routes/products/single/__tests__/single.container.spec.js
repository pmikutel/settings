import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../single.container';
import { ProductActions } from '../../../../modules/product/product.redux';
import { CartActions } from '../../../../modules/cart/cart.redux';
import { CheckoutActions } from '../../../../modules/checkout/checkout.redux';

describe('Single: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call ProductActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).productRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ProductActions.request());
    });

    it('should call ProductActions.metafieldsRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).metafieldsRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ProductActions.metafieldsRequest());
    });

    it('should call CartActions.addItem', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CartActions.addItem());
    });


    it('should call ProductTypesActions.customNewsletterReset', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).customNewsletterReset();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ProductActions.customNewsletterReset());
    });

    it('should call CheckoutActions.customerProjectLineItemAddRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).addProjectItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.customerProjectLineItemAddRequest());
    });

    it('should call CheckoutActions.customerProjectLineItemRemoveRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).removeProjectItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.customerProjectLineItemRemoveRequest());
    });

    it('should call CheckoutActions.fetchRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).fetchProject();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.fetchRequest());
    });
  });
});
