import React, { PureComponent } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Helmet from 'react-helmet';

import All from './all/all.container';
import Single from './single/single.container';

import Header from '../../components/header';
import { Footer } from '../../components/footer/footer.component';

export class Products extends PureComponent {
  static propTypes = {};

  renderRoutes = () => (
    <Switch>
      <Route path="/products/all" component={All} />
      <Route path="/products/:handle/:id/variant/:activeVariantId" component={Single} />
      <Route path="/products/:id" component={Single} />

      <Redirect from="/products" to="/products/all" />
    </Switch>
  );

  render() {
    return (
      <div>
        <Helmet
          title="Products"
        />

        <Header />

        {this.renderRoutes()}

        <Footer />
      </div>
    );
  }
}
