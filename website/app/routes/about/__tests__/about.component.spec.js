import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import 'jest-styled-components';
import { identity } from 'ramda';

import { intlMock } from '../../../utils/testing';
import { AboutComponent as About } from '../about.component';

describe('<About />', () => {
  const defaultProps = {
    intl: intlMock(),
    blogRequest: identity,
    blogData: fromJS([{
      contentHtml: 'test content',
    }]),
  };

  const component = (props = {}) => <About {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
