import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { BlogActions } from '../../modules/blog/blog.redux';
import { About } from './about.component';
import { selectArticles } from '../../modules/blog/blog.selectors';

const mapStateToProps = createStructuredSelector({
  blogData: selectArticles,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  blogRequest: BlogActions.request,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(About));
