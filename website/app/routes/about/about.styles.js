import styled, { css } from 'styled-components';
import { Media, Variables } from '../../theme';

export const sectionDesktopExtraStyles = css`
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    text-align: left;
  }

`;

export const sectionTabletExtraStyles = css`
  margin-bottom: 250px;
  margin-top: 0px;

  p {
    max-width: 795px;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    letter-spacing: 45px;
    padding-left: 75px;
  }

  img {
    margin: 0 120px 120px 0;
  }

  .images {
    text-align: initial;

    img {
      width: auto;
    }
  }

  .partners-desc {
    padding-left: 75px;
  }

  .showroom-image {
    margin-left: 0;
    padding-left: 75px;
  }
`;

//global styles as whole page is populated by admin panel
export const sectionExtraStyles = css`
  margin-bottom: 150px;
  margin-top: 120px;

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    text-transform: uppercase;
    letter-spacing: 13px;
    margin-bottom: 50px;
    word-wrap: break-word;
  }

  p {
    font-family: ${Variables.lyonLightFontFamily};
    color: ${Variables.colorWarmGrey};
    padding-left: 70px;
  }

  img {
    margin-bottom: 40px;
  }

  .images {
    text-align: center;
    max-width: 795px;
    margin: 0 auto;

    img {
      width: 80%;
    }
  }

  .partners-desc {
    padding-left: 15px;
  }

  .showroom-image {
    margin-left: -15px;
    padding-left: 0;

    img {
      width: 100%;
    }
  }

  ${Media.tablet(sectionTabletExtraStyles)}
`;

export const Copy = styled.div`
  max-width: 1400px;
  margin: 140px auto 45px;
  text-align: justify;
  width: 100%;
`;

export const titleExtraStyles = css`
  margin-top: 80px;
`;
