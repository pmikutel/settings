import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'about.title',
    defaultMessage: 'About us',
  },
});
