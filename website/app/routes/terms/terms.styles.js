import styled, { css } from 'styled-components';
import { Media } from '../../theme';

export const Copy = styled.p`
  max-width: 795px;
  margin: 15px auto 45px;
  text-align: justify;
`;

const tabletTitleExtraStyles = css`
  margin-top: 80px;
`;

export const titleExtraStyles = css`
  margin-top: 130px;
  @media (max-width: 380px) {
    h1 {
      font-size: 23px;
      letter-spacing: 13px;
    }
  }
  ${Media.desktop(tabletTitleExtraStyles)}
`;
