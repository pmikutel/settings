import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { UnderlineTitle } from '../../components/underlineTitle/underlineTitle.component';
import { Copy, titleExtraStyles } from './terms.styles';
import { Grid } from '../../theme';
import messages from './terms.messages';
import Header from '../../components/header';
import { Footer } from '../../components/footer/footer.component';

export class TermsComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    blogRequest: PropTypes.func.isRequired,
    blogData: PropTypes.object.isRequired,
  };

  componentDidMount() {
    this.props.blogRequest('terms');
  }

  get content() {
    return this.props.blogData.getIn([0, 'contentHtml']);
  }

  render = () => (
    <div>
      <Header />

      <Grid.Section>
        <Helmet title="Terms and Conditions" />

        <Grid.Row extraStyles={titleExtraStyles}>
          <Grid.Column offset={1} columns={15}>
            <UnderlineTitle title={this.props.intl.formatHTMLMessage(messages.title)} />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Copy
            // eslint-disable-next-line
            dangerouslySetInnerHTML={{__html: this.content }}
          />
        </Grid.Row>
      </Grid.Section>

      <Footer />
    </div>
  );
}

export const Terms = injectIntl(TermsComponent);

