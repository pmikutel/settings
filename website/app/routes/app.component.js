import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { IntlProvider } from 'react-intl';
import { get } from 'lodash';

import { appLocales, translationMessages } from '../i18n';
import { DEFAULT_LOCALE } from '../modules/locales/locales.redux';
import { Modals } from '../components/modals/modals.component';
import { CookieBanner } from '../components/cookieBanner/cookieBanner.component';
import { ScrollToTop } from '../components/scrollToTop/scrollToTop.component';
import CartNotification from '../components/cartNotification/cartNotification.container';
import logoImage from '../images/logo.png';

const defaultMetaData = {
  title: 'White Bear',
  description: `White Bear is a UK wide workplace products company that offers curated furniture and accessories. We
  believe everyone should work in a workplace that they love. We can be found in the heart of the Shoreditch Design
   Triangle in East London.`,
  image: `${window.location.origin}${logoImage}`,
};

export class App extends PureComponent {
  static propTypes = {
    language: PropTypes.string,
    modal: PropTypes.string,
    isPurchaseFlow: PropTypes.bool,
    isProjectFlow: PropTypes.bool,
    setLanguage: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    children: PropTypes.node,
    match: PropTypes.object.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    location: PropTypes.object.isRequired,
    checkSession: PropTypes.func.isRequired,
    showCartNotification: PropTypes.bool.isRequired,
  };

  componentWillMount() {
    const language = get(this.props.match, 'params.lang', DEFAULT_LOCALE);

    if (appLocales.indexOf(language) === -1) {
      this.props.setLanguage(DEFAULT_LOCALE);
      this.props.history.push('/404');
    } else {
      this.props.setLanguage(language);
    }
  }

  componentDidMount() {
    this.props.checkSession();
  }

  render() {
    if (!this.props.language) {
      return null;
    }

    return (
      <div className="app">
        <Helmet
          titleTemplate="%s - White Bear"
          defaultTitle="White Bear"
          meta={[
            { name: 'description', content: defaultMetaData.description },
            { property: 'og:title', content: defaultMetaData.title },
            { property: 'og:image', content: defaultMetaData.image },
            { property: 'og:description', content: defaultMetaData.description },
            { itemprop: 'name', content: defaultMetaData.title },
            { itemprop: 'image', content: defaultMetaData.image },
            { itemprop: 'description', content: defaultMetaData.description },
            { name: 'twitter:card', content: 'summary_large_image' },
          ]}
        />
        <IntlProvider
          locale={this.props.language}
          messages={translationMessages[this.props.language]}
          location={this.props.location}
        >
          <div>
            <Modals
              type={this.props.modal}
              isPurchaseFlow={this.props.isPurchaseFlow}
              isProjectFlow={this.props.isProjectFlow}
              closeModal={this.props.closeModal}
              openModal={this.props.openModal}
            />

            {
              this.props.showCartNotification &&
              <CartNotification />
            }

            {React.Children.only(this.props.children)}

            <CookieBanner />

            <ScrollToTop location={this.props.location} />
          </div>
        </IntlProvider>
      </div>
    );
  }
}
