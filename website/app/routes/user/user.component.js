import React, { PureComponent } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';

import UserRegister from './register/register.container';
import Account from './account/account.container';

import Header from '../../components/header';
import { Footer } from '../../components/footer/footer.component';

export class User extends PureComponent {
  static propTypes = {
    location: PropTypes.object,
    openModal: PropTypes.func,
  };

  renderRoutes = () => (
    <Switch>
      <Route path="/user/register" component={UserRegister} />
      <Route path="/user/account" component={Account} />

      <Redirect from="/user" to="/user/account" />
    </Switch>
  );

  render() {
    return (
      <div>
        <Helmet
          title="User Panel"
        />
        <Header openModal={this.props.openModal} />
        {this.renderRoutes()}
        <Footer />
      </div>
    );
  }
}
