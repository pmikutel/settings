import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { PropTypes } from 'prop-types';
import { Tabs, Tab, TabPanel, TabList } from 'react-tabs';
import { ifElse, isNil, always } from 'ramda';

import { UnderlineTitle } from '../../../components/underlineTitle/underlineTitle.component';
import { DetailsTab } from './components/detailsTab/detailsTab.component';
import { OrdersTab } from './components/ordersTab/ordersTab.component';

import messages from './account.messages';

import { Grid } from '../../../theme';
import { TabsContainer, rowStyles, rowTabListStyles } from './account.styles';


export class Account extends PureComponent {
  static propTypes = {
    profileRequest: PropTypes.func.isRequired,
    fetchProject: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired,
    updateAttributes: PropTypes.func.isRequired,
    profileData: PropTypes.object,
    customAttributes: PropTypes.object,
    projectId: PropTypes.string,
    attributesFetching: PropTypes.bool.isRequired,
    attributesSuccess: PropTypes.bool.isRequired,
    updateUserFetching: PropTypes.bool.isRequired,
    updateUserSuccess: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    this.props.profileRequest();
    if (this.props.customAttributes || !this.props.projectId) {
      return;
    }
    this.props.fetchProject(this.props.projectId, true);
  }

  renderOrdersTab = () => ifElse(
    isNil,
    always(null),
    () => <OrdersTab orders={this.props.profileData.getIn(['orders', 'edges'])} />,
  )(this.props.profileData.get('orders'));

  render = () => (
    <div>
      <Grid.Section>
        <Grid.Row extraStyles={rowStyles}>
          <Grid.Column offset={1} columns={15}>
            <UnderlineTitle title={this.props.profileData.get('lastName')} />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
      <TabsContainer>
        <Tabs>
          <TabList>
            <Grid.Section>
              <Grid.Row extraStyles={rowTabListStyles}>
                <Tab><FormattedMessage {...messages.orders} /></Tab>
                <Tab><FormattedMessage {...messages.details} /></Tab>
              </Grid.Row>
            </Grid.Section>
          </TabList>
          <TabPanel>
            {this.renderOrdersTab()}
          </TabPanel>
          <TabPanel>
            <DetailsTab
              user={this.props.profileData}
              updateUser={this.props.updateUser}
              updateAttributes={this.props.updateAttributes}
              customAttributes={this.props.customAttributes}
              attributesFetching={this.props.attributesFetching}
              attributesSuccess={this.props.attributesSuccess}
              updateUserFetching={this.props.updateUserFetching}
              updateUserSuccess={this.props.updateUserSuccess}
            />
          </TabPanel>
        </Tabs>
      </TabsContainer>
    </div>
  );
}

