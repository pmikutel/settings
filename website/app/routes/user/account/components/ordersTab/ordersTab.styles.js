import styled, { css } from 'styled-components';
import { Variables, Media, Typography } from '../../../../../theme';
import Sprite from '../../../../../utils/sprites';

const desktopTitleStyles = css`
  padding-bottom: 87px;
  letter-spacing: 24px;
`;

export const Title = styled.h2`
  letter-spacing: 13px;
  text-transform: uppercase;
  padding-bottom: 68px;

  ${Media.desktop(desktopTitleStyles)};
`;

const desktopWideSubTitleStyles = css`
  font-size: 22px;
  letter-spacing: 12px;
`;
const desktopSubTitleStyles = css`
  font-size: 16px;
  letter-spacing: 9px;
`;

export const SubTitle = styled.h3`
  letter-spacing: 12px;
  text-transform: uppercase;
  padding-bottom: 20px;
  color: ${Variables.colorWarmGrey};

  ${Media.desktop(desktopSubTitleStyles)};
  ${Media.desktopWide(desktopWideSubTitleStyles)};
`;

const desktopWideCopyStyles = css`
  font-size: 22px;
`;

const desktopCopyStyles = css`
  font-size: 16px;
  line-height: normal;
  margin-bottom: 0;
`;

export const Copy = styled.div`
  font-size: 22px;
  line-height: 46px;
  margin-bottom: 26px;
  color: ${Variables.colorPureBlack};
  ${Typography.fontLyonLight};

  ${Media.desktop(desktopCopyStyles)};
  ${Media.desktopWide(desktopWideCopyStyles)};
`;

const desktopNumberStyles = css`
  letter-spacing: 12px;
`;

export const Number = Copy.extend`
  ${Typography.fontGothamBook};
  letter-spacing: 8px;
  font-size: 14px;

  ${Media.desktop(desktopNumberStyles)};
`;

const rowDesktopStyles = css`
  flex-direction: row;
  padding: 44px 57px 66px 60px;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 51px 16px 26px 60px;
  background-color: ${Variables.colorPureWhite};
  border-top: 1px solid ${Variables.colorBorderGray};

  &:first-child {
    background-color: ${Variables.colorBackgroundGray};
  }

  &:last-child {
    border-bottom: 1px solid ${Variables.colorBorderGray};
  }

  ${Media.desktop(rowDesktopStyles)};
`;

const copyLineDesktopStyles = css`
  display: inline;
`;

export const CopyLine = styled.span`
  display: none;

  ${Media.desktop(copyLineDesktopStyles)};
`;

const copyElementDesktopStyles = css`
  display: inline;
`;

export const CopyElement = styled.span`
  display: block;

  ${Media.desktop(copyElementDesktopStyles)};
`;

const columnDesktopStyles = css`
  padding-right: 20px;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;

  ${Media.desktop(columnDesktopStyles)};
`;

const lastColumnDesktopStyles = css`
  min-width: 295px;
`;

export const LastColumn = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  max-width: 100%;

  ${Media.desktop(lastColumnDesktopStyles)};
`;

const wrapperDesktopStyles = css`
  padding-top: 162px;
`;
export const Wrapper = styled.div`
  padding: 80px 0 65px;

  ${Media.desktop(wrapperDesktopStyles)};
`;

const downloadDesktopStyles = css`
  margin: 26px 0 2px 30px;
`;

const downloadDestkopWideStyles = css`
  margin-top: 37px;
`;

export const Download = styled.a`
  ${Sprite.responsive('download')};
  margin: 21px 23px 4px 10px;

  ${Media.desktop(downloadDesktopStyles)};
  ${Media.desktopWide(downloadDestkopWideStyles)};
`;

export const RowsWrapper = styled.div`
  padding-bottom: 94px;
`;

export const Description = styled.p`
  margin: 0;
  text-align: center;
`;
