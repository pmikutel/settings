
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'ordersTab.title',
    defaultMessage: 'All orders',
  },
  empty: {
    id: 'ordersTab.empty',
    defaultMessage: 'You have no orders to show.',
  },
  order: {
    id: 'ordersTab.order',
    defaultMessage: 'ORDER',
  },
  address: {
    id: 'ordersTab.address',
    defaultMessage: 'DELIVERY ADDRESS',
  },
  total: {
    id: 'ordersTab.total',
    defaultMessage: 'TOTAL',
  },
  number: {
    id: 'ordersTab.number',
    defaultMessage: 'ORDER Nº',
  },
  currency: {
    id: 'ordersTab.currency',
    defaultMessage: '£{ price }',
  },
  hash: {
    id: 'ordersTab.hash',
    defaultMessage: '#{ copy }',
  },
});
