import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import { intlMock } from '../../../../../../utils/testing';

import { OrdersTabComponent as OrdersTab } from '../ordersTab.component';


describe('<OrdersTab />', () => {
  const defaultProps = {
    orders: fromJS({}),
    intl: intlMock(),
  };

  const component = (props = {}) => <OrdersTab {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
