import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { PropTypes } from 'prop-types';
import moment from 'moment';
import { ifElse, prop, always, isEmpty } from 'ramda';

import messages from './ordersTab.messages';

import {
  Wrapper,
  Title, Row,
  Column,
  SubTitle,
  Copy,
  Download,
  LastColumn,
  RowsWrapper,
  CopyLine,
  CopyElement,
  Number,
  Description,
} from './ordersTab.styles';
import { Grid } from '../../../../../theme';
import { ChatButton } from '../../../../../components/chatButton/chatButton.component';
import { priceFormat } from '../../../../../utils/numbers';


export class OrdersTabComponent extends PureComponent {
  static propTypes = {
    orders: PropTypes.object,
    intl: PropTypes.object.isRequired,
  };

  getXeroLink = item => {
    var result = [];
    var addressItems = item.getIn(['shippingAddress', 'address2']);
    if (addressItems) {
      result = addressItems.split(';').filter(item => {
        return item.indexOf('xero') >= 0;
      });
    }

    return result.length > 0 ? result[0].trim() : '';
  };

  renderRows = () => this.props.orders.map((data, index) => {
    const items = data.get('node');
    const address = items.get('shippingAddress');
    const xero = this.getXeroLink(items);

    return (
      <Row key={index}>
        <Column>
          <SubTitle><FormattedMessage {...messages.order} /></SubTitle>
          <Copy>{moment(items.get('processedAt')).format('DD MMMM YYYY')}</Copy>
        </Column>
        <Column>
          <SubTitle><FormattedMessage {...messages.address} /></SubTitle>
          <Copy>
            <CopyElement>{address.get('address1')}</CopyElement>
            <CopyLine>-</CopyLine>
            <CopyElement>{address.get('zip')},</CopyElement>
            <CopyElement>{address.get('city')}</CopyElement>
          </Copy>
        </Column>
        <Column>
          <SubTitle><FormattedMessage {...messages.total} /></SubTitle>
          <Number>
            <FormattedMessage{...messages.currency} values={{ price: priceFormat(items.get('totalPrice')) }} />
          </Number>
        </Column>
        <LastColumn>
          <Column>
            <SubTitle><FormattedMessage {...messages.number} /></SubTitle>
            <Number><FormattedMessage{...messages.hash} values={{ copy: items.get('orderNumber') }} /></Number>
          </Column>
          <Column>
            { this.renderDownloadBtn(xero) }
          </Column>
        </LastColumn>
      </Row>
    );
  });

  renderEmptyCopy = () => ifElse(
    prop('size'),
    always(null),
    () => <Description><FormattedMessage {...messages.empty} /></Description>,
  )(this.props.orders);

  renderDownloadBtn = (url) => ifElse(
    isEmpty,
    always(null),
    () => <Download href={url} target="_blank" />,
  )(url);

  render = () => (
    <Wrapper>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={3} columns={11}>
            <Title><FormattedMessage {...messages.title} /></Title>
            {this.renderEmptyCopy()}
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
      <RowsWrapper>
        {this.renderRows()}
      </RowsWrapper>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={2} columns={11}>
            <ChatButton />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    </Wrapper>
  );
}

export const OrdersTab = injectIntl(OrdersTabComponent);
