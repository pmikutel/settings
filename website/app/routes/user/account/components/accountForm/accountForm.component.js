import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { Field, reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { always, cond, propEq, T } from 'ramda';

import { DotsLoader } from '../../../../../components/dotsLoader/dotsLoader.component';

import messages from './accountForm.messages';

import {
  inputStyles,
  FormField,
  Form,
  submitStyles,
  InputContainer,
  passwordStyles,
  editStyles,
  labelStyles,
} from './accountForm.styles';

import { Forms, Components } from '../../../../../theme';


export class AccountFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    invalid: PropTypes.bool.isRequired,
    change: PropTypes.func.isRequired,
    updateUserSuccess: PropTypes.bool.isRequired,
    updateUserFetching: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
  };

  state = {
    enableEdit: false,
  };

  componentDidMount() {
    this.props.change('email', this.props.user.get('email'));
    this.props.change('name', this.props.user.get('firstName'));
    this.props.change('company', this.props.user.get('lastName'));
  }

  toggleEnableEdit = (error) => {
    if (error) {
      this.props.change('password', null);
    }

    this.setState({ enableEdit: !this.state.enableEdit });
  };

  get placeholder() {
    return this.state.enableEdit ? '' : this.props.intl.formatMessage(messages.passwordPlaceholder);
  }

  copyButton = (error) => {
    if (!this.state.enableEdit) {
      return messages.edit;
    } else if (error) {
      return messages.cancel;
    }
    return messages.save;
  };

  renderCopySubmitButton = () => cond([
    [propEq('updateUserSuccess', true), always(<FormattedMessage {...messages.saved} />)],
    [propEq('updateUserFetching', true), always(<DotsLoader isColorful={true} relative />)],
    [T, always(<FormattedMessage {...messages.save} />)],
  ])(this.props);

  renderTextField = ({ message, input, meta: { touched, error }, errorText }) => (
    <FormField>
      <Forms.Label extraStyles={labelStyles}>
        <FormattedMessage {...message} />
      </Forms.Label>

      <Forms.Input
        errorStyle={touched && !!error}
        {...input}
        extraStyles={inputStyles}
      />

      {touched && error && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
    </FormField>
  );

  renderPasswordField = ({ message, input, edit, type, meta: { touched, error, invalid, dirty }, errorText }) => {
    const errorPassword = edit && invalid || edit && !dirty;
    return (
      <FormField>
        <Forms.Label extraStyles={labelStyles}>
          <FormattedMessage {...message} />
        </Forms.Label>
        <InputContainer>
          <Forms.Input
            errorStyle={touched && !!error}
            type={type}
            {...input}
            placeholder={this.placeholder}
            extraStyles={passwordStyles}
            disabled={!edit}
          />
          <Components.Button
            type="button"
            extraStyles={editStyles}
            onClick={() => this.toggleEnableEdit(errorPassword)}
            noActive
            error={errorPassword}
          >
            {this.props.intl.formatMessage(this.copyButton(errorPassword))}
          </Components.Button>
        </InputContainer>

        {touched && error && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
      </FormField>
    );
  };

  render = () => (
    <Form onSubmit={this.props.handleSubmit} noValidate>
      <Field
        name="name"
        type="text"
        message={messages.name}
        component={this.renderTextField}
        errorText={messages.nameError}
      />
      <Field
        name="company"
        type="text"
        message={messages.company}
        component={this.renderTextField}
        errorText={messages.companyError}
      />
      <Field
        name="email"
        type="email"
        message={messages.email}
        component={this.renderTextField}
        errorText={messages.emailError}
      />
      <Field
        name="password"
        type="password"
        message={messages.password}
        edit={this.state.enableEdit}
        component={this.renderPasswordField}
        errorText={messages.passwordError}
      />
      <Components.Button
        type="submit"
        extraStyles={submitStyles}
        disabled={this.state.enableEdit || this.props.invalid}
        noActive
        noEvents={this.props.updateUserSuccess || this.props.updateUserFetching}
      >
        {this.renderCopySubmitButton()}
      </Components.Button>
    </Form>
  );
}

export const AccountForm = injectIntl(reduxForm({
  form: 'accountForm',
  validate: (values) => validate(values.toJS(), {
    name: {
      presence: {
        allowEmpty: false,
      },
    },
    company: {
      presence: {
        allowEmpty: false,
      },
    },
    email: {
      email: true,
      presence: {
        allowEmpty: false,
      },
    },
    password: {
      format: {
        pattern: '.*[0-9].*',
      },
      length: {
        minimum: 8,
      },
    },
  }),
})(AccountFormComponent));
