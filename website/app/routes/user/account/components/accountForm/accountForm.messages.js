import { defineMessages } from 'react-intl';

export default defineMessages({
  name: {
    id: 'accountForm.name',
    defaultMessage: 'Your name',
  },
  nameError: {
    id: 'accountForm.nameError',
    defaultMessage: 'Please enter your full name',
  },
  company: {
    id: 'accountForm.company',
    defaultMessage: 'Company name',
  },
  companyError: {
    id: 'accountForm.companyError',
    defaultMessage: 'Please enter your Company name',
  },
  email: {
    id: 'accountForm.email',
    defaultMessage: 'Email address',
  },
  emailError: {
    id: 'accountForm.emailError',
    defaultMessage: 'Please enter a valid email address',
  },
  password: {
    id: 'accountForm.password',
    defaultMessage: 'Password',
  },
  passwordPlaceholder: {
    id: 'accountForm.passwordPlaceholder',
    defaultMessage: '**********',
  },
  passwordError: {
    id: 'accountForm.passwordError',
    defaultMessage: 'Your password must be at least 8 characters long and contain at least one number',
  },
  edit: {
    id: 'accountForm.edit',
    defaultMessage: 'Edit',
  },
  save: {
    id: 'accountForm.save',
    defaultMessage: 'Save',
  },
  cancel: {
    id: 'accountForm.cancel',
    defaultMessage: 'Cancel',
  },
  saveDetails: {
    id: 'accountForm.saveDetails',
    defaultMessage: 'Save Details',
  },
  saved: {
    id: 'accountForm.saved',
    defaultMessage: 'Saved',
  },
});
