import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../../../theme';

const inputDesktopStyles = css`
  max-width: 400px;
`;

export const inputStyles = css`
  max-width: 100%;
  
  ${Media.desktop(inputDesktopStyles)}
`;

const formFieldDesktopStyles = css`
  max-width: 400px;
`;

export const FormField = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-bottom: 41px;
  
  ${Media.desktop(formFieldDesktopStyles)}
`;

export const Error = styled.div`
  color: ${Variables.colorRed};
  position: absolute;
  text-align: left;
  top: 73px;
  width: 100%;
`;

const submitDesktopStyles = css`
  width: 276px;
  margin: 96px 0 0 0;
`;

export const submitStyles = css`
  width: 100%;
  margin: 61px auto 0;
  
  ${Media.desktop(submitDesktopStyles)}
`;

const formDesktopStyles = css`
  margin-bottom: 136px;
`;

export const Form = styled.form`
  margin-bottom: 77px;
  
  ${Media.desktop(formDesktopStyles)}
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 400px;
  max-width: 100%;
`;

const passwordDesktopStyles = css`
  margin-right: 25px;
`;

export const passwordStyles = css`
  max-width: 100%;
  margin-right: 20px;
  
  ${Media.desktop(passwordDesktopStyles)}
`;

export const editStyles = css`
  max-width: 101px;
`;

export const labelStyles = css`
  margin: 17px 0;
`;
