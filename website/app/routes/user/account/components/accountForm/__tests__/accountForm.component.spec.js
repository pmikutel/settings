import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { AccountFormComponent as AccountForm } from '../accountForm.component';

describe('<AccountForm />', () => {
  const defaultProps = {
    handleSubmit: identity,
    user: fromJS({}),
    invalid: false,
    change: identity,
    updateUserSuccess: false,
    updateUserFetching: false,
    intl: {
      formatMessage: () => {},
    },
  };

  const component = (props = {}) => <AccountForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
