import messages from './companyForm.messages';

export const COMPANY_DESCRIPTIONS_RADIO_OPTIONS = [
  { id: 'start', message: messages.radioItemStart },
  { id: 'established', message: messages.radioItemEstablished },
  { id: 'industry', message: messages.radioItemIndustry },
];
