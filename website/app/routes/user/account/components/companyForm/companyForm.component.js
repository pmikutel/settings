import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { Field, reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { always, cond, propEq, T } from 'ramda';

import { RadioButtons } from '../../../../../components/radioButtons/radioButtons.component';
import { Counter } from '../../../../../components/counter/counter.component';
import { DotsLoader } from '../../../../../components/dotsLoader/dotsLoader.component';

import messages from './companyForm.messages';

import {
  inputStyles,
  FormField,
  Form,
  submitStyles,
  labelStyles,
} from './companyForm.styles';

import { Forms, Components } from '../../../../../theme';
import { COMPANY_DESCRIPTIONS_RADIO_OPTIONS } from './companyForm.helpers';


export class CompanyFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    change: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    customAttributes: PropTypes.object,
    attributesFetching: PropTypes.bool.isRequired,
    attributesSuccess: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
  };

  componentDidMount() {
    const attr = this.props.customAttributes;
    this.props.change('whatDoes', attr.get('whatDoes'));
    this.props.change('peopleCount', attr.get('peopleCount'));
    this.props.change('companyDescription', attr.get('companyDescription'));
  }

  renderTextField = ({ message, input, placeholder, meta: { touched, error }, errorText }) => (
    <FormField>
      <Forms.Label extraStyles={labelStyles}>
        <FormattedMessage {...message} />
      </Forms.Label>

      <Forms.Input
        errorStyle={touched && !!error}
        {...input}
        placeholder={placeholder}
        extraStyles={inputStyles}
      />

      {touched && error && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
    </FormField>
  );

  renderCounterField = ({ message, input, meta: { touched, invalid, dirty }, errorText }) => (
    <FormField>
      <Forms.Label>
        <FormattedMessage {...message} />
      </Forms.Label>

      <Counter {...input} max={9999} />

      {(dirty || touched) && invalid && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
    </FormField>
  );

  renderRadioField = ({ message, input, items }) => (
    <FormField>
      <Forms.Label>
        <FormattedMessage {...message} />
      </Forms.Label>

      <RadioButtons
        items={items}
        {...input}
      />
    </FormField>
  );

  renderCopyButton = () => cond([
    [propEq('attributesSuccess', true), always(<FormattedMessage {...messages.saved} />)],
    [propEq('attributesFetching', true), always(<DotsLoader isColorful={true} relative />)],
    [T, always(<FormattedMessage {...messages.save} />)],
  ])(this.props);

  render = () => (
    <Form onSubmit={this.props.handleSubmit} noValidate>
      <Field
        name="whatDoes"
        type="text"
        placeholder={this.props.intl.formatMessage(messages.design)}
        message={messages.whatDoes}
        component={this.renderTextField}
        errorText={messages.whatDoesError}
      />
      <Field
        name="peopleCount"
        message={messages.people}
        component={this.renderCounterField}
        errorText={messages.peopleError}
      />
      <Field
        name="companyDescription"
        message={messages.describe}
        component={this.renderRadioField}
        items={COMPANY_DESCRIPTIONS_RADIO_OPTIONS}
      />
      <Components.Button
        type="submit"
        extraStyles={submitStyles}
        disabled={this.props.invalid}
        noActive
        noEvents={this.props.attributesFetching || this.props.attributesSuccess}
      >
        {this.renderCopyButton()}
      </Components.Button>
    </Form>
  );
}

export const CompanyForm = injectIntl(reduxForm({
  form: 'companyForm',
  validate: (values) => validate(values.toJS(), {
    whatDoes: {
      presence: {
        allowEmpty: false,
      },
    },
    peopleCount: {
      presence: {
        allowEmpty: false,
      },
      numericality: {
        greaterThan: 0,
      },
    },
  }),
})(CompanyFormComponent));
