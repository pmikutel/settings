import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { CompanyFormComponent as CompanyForm } from '../companyForm.component';


describe('<CompanyForm />', () => {
  const defaultProps = {
    handleSubmit: identity,
    change: identity,
    invalid: false,
    customAttributes: fromJS({}),
    attributesFetching: false,
    attributesSuccess: false,
    intl: {
      formatMessage: () => {},
    },
  };

  const component = (props = {}) => <CompanyForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
