import { defineMessages } from 'react-intl';

export default defineMessages({
  email: {
    id: 'companyForm.email',
    defaultMessage: 'Your email*',
  },
  save: {
    id: 'companyForm.save',
    defaultMessage: 'Save Details',
  },
  saved: {
    id: 'companyForm.saved',
    defaultMessage: 'Saved',
  },
  radioItemStart: {
    id: 'companyForm.start',
    defaultMessage: 'Early days start up',
  },
  radioItemEstablished: {
    id: 'companyForm.established',
    defaultMessage: 'Established',
  },
  radioItemIndustry: {
    id: 'companyForm.industry',
    defaultMessage: 'Industry leader',
  },
  people: {
    id: 'companyForm.people',
    defaultMessage: 'How many people are using the space?',
  },
  peopleError: {
    id: 'companyForm.peopleError',
    defaultMessage: 'People count should be greater than 0',
  },
  whatDoesError: {
    id: 'companyForm.whatDoesError',
    defaultMessage: 'Please provide what does your company do',
  },
  design: {
    id: 'companyForm.design',
    defaultMessage: 'Design',
  },
  whatDoes: {
    id: 'companyForm.whatDoes',
    defaultMessage: 'What does your company do?',
  },
  describe: {
    id: 'companyForm.describe',
    defaultMessage: 'How would you describe your company?',
  },
});
