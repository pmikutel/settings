import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import { identity } from 'ramda';

import { DetailsTab } from '../detailsTab.component';

describe('<DetailsTab />', () => {
  const defaultProps = {
    user: fromJS({}),
    customAttributes: fromJS({}),
    updateUser: identity,
    updateAttributes: identity,
    attributesFetching: false,
    attributesSuccess: false,
    updateUserSuccess: false,
    updateUserFetching: false,
  };

  const component = (props = {}) => <DetailsTab {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
