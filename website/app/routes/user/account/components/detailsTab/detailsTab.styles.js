import styled, { css } from 'styled-components';
import { Media, Typography, Variables } from '../../../../../theme';

const titleDesktopStyles = css`
  font-size: 40px;
  letter-spacing: 24px;
  padding-bottom: 22px;
  max-width: 100%;
`;

export const Title = styled.h2`
  ${Typography.fontGothamBook};
  font-size: 22px;
  letter-spacing: 13px;
  padding-bottom: 30px;
    
  ${Media.desktop(titleDesktopStyles)}
`;

const wrapperDesktopStyles = css`
  padding: 152px 0 38px;
`;

export const wrapperStyles = css`
  padding-top: 100px;
  
  ${Media.desktop(wrapperDesktopStyles)}
`;

const descriptionDesktopStyles = css`
  padding: 0;
`;

export const Description = styled.p`
  color: ${Variables.colorWarmGrey};
  padding: 10px 0 100px;
  
  ${Media.desktop(descriptionDesktopStyles)}
`;
