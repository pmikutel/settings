import { defineMessages } from 'react-intl';

export default defineMessages({
  account: {
    id: 'detailsTab.account',
    defaultMessage: 'ACCOUNT DETAILS',
  },
  company: {
    id: 'detailsTab.company',
    defaultMessage: 'COMPANY DETAILS',
  },
  space: {
    id: 'detailsTab.space',
    defaultMessage: 'SPACE DETAILS',
  },
  create: {
    id: 'detailsTab.create',
    defaultMessage: 'You haven’t created any project yet. ',
  },
  link: {
    id: 'detailsTab.link',
    defaultMessage: 'Create project',
  },
});
