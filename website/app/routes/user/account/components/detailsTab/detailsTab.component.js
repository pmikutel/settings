import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { ifElse, prop, always } from 'ramda';
import { AccountForm } from '../accountForm/accountForm.component';
import { CompanyForm } from '../companyForm/companyForm.component';
import { SpaceForm } from '../spaceForm/spaceForm.component';

import { Title, wrapperStyles, Description } from './detailsTab.styles';
import { Grid, Components } from '../../../../../theme';

import messages from './detailsTab.messages';

export class DetailsTab extends PureComponent {
  static propTypes = {
    user: PropTypes.object,
    customAttributes: PropTypes.object,
    updateUser: PropTypes.func.isRequired,
    updateAttributes: PropTypes.func.isRequired,
    attributesFetching: PropTypes.bool.isRequired,
    attributesSuccess: PropTypes.bool.isRequired,
    updateUserSuccess: PropTypes.bool.isRequired,
    updateUserFetching: PropTypes.bool.isRequired,
  };

  renderCompanyForm = () => ifElse(
    prop('customAttributes'),
    () =>
      <CompanyForm
        customAttributes={this.props.customAttributes}
        onSubmit={(data) => this.props.updateAttributes(data, false)}
        attributesFetching={this.props.attributesFetching}
        attributesSuccess={this.props.attributesSuccess}
      />,
    () =>
      <Description>
        <FormattedMessage {...messages.create} />
        <Components.Link to="/project/step1">
          <FormattedMessage {...messages.link} />
        </Components.Link>
      </Description>
  )(this.props);

  renderSpaceForm = () => ifElse(
    prop('customAttributes'),
    () =>
      <div>
        <Grid.Row>
          <Grid.Column columns={10} offset={3}>
            <Title>
              <FormattedMessage {...messages.space} />
            </Title>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column columns={8} offset={5}>
            <SpaceForm
              onSubmit={(data) => this.props.updateAttributes(data, false)}
              floorPlanName={this.props.customAttributes.get('floorPlanName')}
            />
          </Grid.Column>
        </Grid.Row>
      </div>,
    always(null),
  )(this.props);

  render = () => (
    <Grid.Section extraStyles={wrapperStyles}>
      <Grid.Row>
        <Grid.Column columns={10} offset={3}>
          <Title>
            <FormattedMessage {...messages.account} />
          </Title>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column columns={8} offset={5}>
          <AccountForm
            user={this.props.user}
            onSubmit={this.props.updateUser}
            updateUserFetching={this.props.updateUserFetching}
            updateUserSuccess={this.props.updateUserSuccess}
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column columns={10} offset={3}>
          <Title>
            <FormattedMessage {...messages.company} />
          </Title>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column columns={8} offset={5}>
          {this.renderCompanyForm()}
        </Grid.Column>
      </Grid.Row>
      {this.renderSpaceForm()}
    </Grid.Section>
  );
}

