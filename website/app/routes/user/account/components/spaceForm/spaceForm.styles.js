import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Media, Variables } from '../../../../../theme';

const inputDesktopStyles = css`
  max-width: 305px;
`;

export const inputStyles = css`
  max-width: 100%;
  
  ${Media.desktop(inputDesktopStyles)}
`;

export const FormField = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const Error = styled.div`
  color: ${Variables.colorRed};
  position: absolute;
  text-align: left;
  top: 73px;
  width: 100%;
`;

export const UploadDescription = styled.p`
  font-size: 18px;
  opacity: 0.9;
  line-height: 2;
  max-width: 520px;
  margin-bottom: 20px;
  color: ${Variables.colorWarmGrey};
`;

export const uploadFileLabelStyles = css`
  margin: 17px 0;
`;

export const filesListStyles = css`
  margin-top: ${ifElse(propEq('children', []), always('0'), always('30px'))};
`;

const formDesktopStyles = css`
  margin-bottom: 136px;
`;

export const Form = styled.form`
  margin-bottom: 77px;
  
  ${Media.desktop(formDesktopStyles)}
`;
