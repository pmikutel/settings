import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { SpaceFormComponent as SpaceForm } from '../spaceForm.component';

describe('<SpaceForm />', () => {
  const defaultProps = {
    handleSubmit: identity,
    floorPlanName: '',
    intl: {
      formatMessage: () => {},
    },
  };

  const component = (props = {}) => <SpaceForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
