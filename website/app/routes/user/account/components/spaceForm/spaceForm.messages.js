import { defineMessages } from 'react-intl';

export default defineMessages({
  uploadInputTitle: {
    id: 'spaceForm.uploadInputTitle',
    defaultMessage: 'Upload a floor plan',
  },
  uploadLabel: {
    id: 'step2.uploadLabel',
    defaultMessage: 'jpeg, png, eps',
  },
  uploadError: {
    id: 'spaceForm.uploadError',
    defaultMessage: 'Please upload floor plan (max 2MB)',
  },
  uploadDescription: {
    id: 'spaceForm.uploadDescription',
    defaultMessage: 'We offer bespoke planning services.  Upload your flooplan and our architects will get in touch ' +
    'with you with a bespoke solution.',
  },
});
