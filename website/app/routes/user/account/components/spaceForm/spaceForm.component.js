import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { Field, reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { always, anyPass, ifElse, is, isEmpty, isNil } from 'ramda';

import { FilesList } from '../../../../../components/filesList/filesList.component';
import { Upload } from '../../../../../components/upload/upload.component';

import messages from './spaceForm.messages';

import {
  Form,
  FormField,
  UploadDescription,
  uploadFileLabelStyles,
  filesListStyles,
} from './spaceForm.styles';

import { Forms } from '../../../../../theme';


export class SpaceFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    floorPlanName: PropTypes.string,
    intl: PropTypes.object.isRequired,
  };

  static defaultProps = {
    floorPlanName: null,
  };

  renderUploadField = ({ message, input, fileName, supportedFiles, meta: { touched, error }, errorText }) => {
    const name = fileName ? [{ name: fileName }] : [];
    return (
      <FormField>
        <FilesList
          items={ifElse(
            anyPass([isNil, isEmpty, is(Boolean)]),
            always(name),
            always([input.value]),
          )(input.value)}
          listStyles={filesListStyles}
        />

        <Forms.Label extraStyles={uploadFileLabelStyles}>
          <FormattedMessage {...message} />
        </Forms.Label>

        <Upload {...input} supportedFiles={supportedFiles} />

        <UploadDescription>
          {this.props.intl.formatMessage(messages.uploadDescription)}
        </UploadDescription>

        {touched && error && <Forms.Error>{this.props.intl.formatMessage(errorText)}</Forms.Error>}
      </FormField>
    );
  };

  render = () => (
    <Form onSubmit={this.props.handleSubmit} noValidate>
      <Field
        name="floorPlan"
        message={messages.uploadInputTitle}
        supportedFiles={['jpg', 'jpeg', 'png']}
        component={this.renderUploadField}
        errorText={messages.uploadError}
        fileName={this.props.floorPlanName}
      />
    </Form>
  );
}

export const SpaceForm = injectIntl(reduxForm({
  form: 'spaceForm',
  onChange: (values, dispatch, props) => {
    setTimeout(props.submit, 0);
  },
  validate: (values) => validate(values.toJS(), {
    floorPlan: {
      presence: {
        allowEmpty: false,
      },
      exclusion: {
        within: [false],
      },
    },
  }),
})(SpaceFormComponent));
