
import { defineMessages } from 'react-intl';

export default defineMessages({
  orders: {
    id: 'account.orders',
    defaultMessage: 'My Orders',
  },
  details: {
    id: 'account.details',
    defaultMessage: 'My Details',
  },
});
