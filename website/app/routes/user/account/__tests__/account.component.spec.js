import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { Account } from '../account.component';


describe('<Account />', () => {
  const defaultProps = {
    profileRequest: identity,
    fetchProject: identity,
    updateUser: identity,
    updateAttributes: identity,
    profileData: fromJS({}),
    customAttributes: fromJS({}),
    projectId: '',
    attributesFetching: false,
    attributesSuccess: false,
    updateUserFetching: false,
    updateUserSuccess: false,
  };

  const component = (props = {}) => <Account {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
