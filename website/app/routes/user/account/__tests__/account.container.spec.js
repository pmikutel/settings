import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../account.container';
import { UserActions } from '../../../../modules/user/user.redux';
import { CheckoutActions } from '../../../../modules/checkout/checkout.redux';

describe('Account: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call UserActions.profileRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).profileRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(UserActions.profileRequest());
    });

    it('should call UserActions.updateUserRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateUser();

      expect(dispatch.firstCall.args[0]).to.deep.equal(UserActions.updateUserRequest());
    });

    it('should call CheckoutActions.fetchRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).fetchProject();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.fetchRequest());
    });

    it('should call CheckoutActions.profileRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateAttributes();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CheckoutActions.updateAttributesRequest());
    });
  });
});
