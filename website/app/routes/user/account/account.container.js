import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Account } from './account.component';
import { UserActions } from '../../../modules/user/user.redux';
import {
  selectProfileData,
  selectProjectId,
  selectUpdateUserFetching,
  selectUpdateUserSuccess,
} from '../../../modules/user/user.selectors';
import {
  selectCustomAttributes,
  selectUpdateAttributesFetching,
  selectUpdateAttributesSuccess,
} from '../../../modules/checkout/checkout.selectors';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';

const mapStateToProps = createStructuredSelector({
  profileData: selectProfileData,
  customAttributes: selectCustomAttributes,
  projectId: selectProjectId,
  attributesFetching: selectUpdateAttributesFetching,
  attributesSuccess: selectUpdateAttributesSuccess,
  updateUserFetching: selectUpdateUserFetching,
  updateUserSuccess: selectUpdateUserSuccess,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  profileRequest: UserActions.profileRequest,
  updateUser: UserActions.updateUserRequest,
  fetchProject: CheckoutActions.fetchRequest,
  updateAttributes: CheckoutActions.updateAttributesRequest,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Account));
