import styled, { css } from 'styled-components';
import { Components, Variables, Media } from '../../../theme';

const tabsContainerDesktopStyles = css`
  .react-tabs__tab {
    margin: 0 16px;
  }

  .react-tabs__tab-list {
    padding-top: 59px;
  }
`;


const tabsContainerTabletStyles = css`
  .react-tabs__tab-list {
    padding-top: 100px;
  }
`;

export const TabsContainer = styled.div`
  position: relative;
  z-index: 1;

  .react-tabs__tab-list {
    list-style: none;
    padding-top: 387px;
  }

  .react-tabs__tab {
    ${Components.buttonStyles};
    outline: none;
    width: 204px;
    justify-content: center;

    &:first-child {
      margin-right: 19px;
    }

    &:focus {
      color: ${Variables.colorMintyGreen};
      background-image: inherit;
      box-shadow: inherit;
    }
  }

  .react-tabs__tab--selected {
    background-image: ${Variables.gradientButtonActive};
    box-shadow: ${Variables.boxShadowRadioButton};
    color: ${Variables.colorPureWhite};
  }

  ${Media.tablet(tabsContainerTabletStyles)}
  ${Media.desktop(tabsContainerDesktopStyles)};
`;

const rowDesktopStyles = css`
  padding-top: 165px;
`;

export const rowStyles = css`
  padding-top: 228px;

  ${Media.desktop(rowDesktopStyles)};
`;

export const rowTabListStyles = css`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;
