
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'register.title',
    defaultMessage: 'register',
  },
  description: {
    id: 'register.title',
    defaultMessage: `Create an account with White Bear to save your details and start building your space.\n
    Already have an account?`,
  },
  link: {
    id: 'registers.link',
    defaultMessage: 'Sign in here',
  },
});
