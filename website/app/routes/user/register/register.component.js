import React, { PureComponent } from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';

import { UnderlineTitle } from '../../../components/underlineTitle/underlineTitle.component';
import { RegisterForm } from '../../../components/registerForm/registerForm.component';

import messages from './register.messages';

import { wrapperStyles } from './register.styles';

import { Grid } from '../../../theme';

export class RegisterComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    openModal: PropTypes.func,
    register: PropTypes.func.isRequired,
    registerError: PropTypes.bool.isRequired,
    registerIsProcessing: PropTypes.bool.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    clearError: PropTypes.func.isRequired,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  componentWillMount() {
    if (this.props.isLoggedIn) {
      this.context.router.history.push('/');
    }

    this.props.clearError();
  }

  componentWillUpdate() {
    if (this.props.isLoggedIn) {
      this.context.router.history.push('/');
    }
  }

  render = () => (
    <div>
      <Grid.Section extraStyles={wrapperStyles}>
        <Grid.Column offset={1} columns={15}>
          <Grid.Row>
            <UnderlineTitle
              title={this.props.intl.formatMessage(messages.title)}
              description={this.props.intl.formatMessage(messages.description)}
              linkCopy={this.props.intl.formatMessage(messages.link)}
              linkFunc={this.props.openModal}
            />
          </Grid.Row>
        </Grid.Column>
      </Grid.Section>

      <RegisterForm
        onSubmit={(data) => this.props.register(data, '/')}
        openModal={this.props.openModal}
        registerIsProcessing={this.props.registerIsProcessing}
        registerError={this.props.registerError}
      />
    </div>
  );
}

export const Register = injectIntl(RegisterComponent);
