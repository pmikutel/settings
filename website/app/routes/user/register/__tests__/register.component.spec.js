import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { RegisterComponent as Register } from '../register.component';
import { intlMock } from '../../../../utils/testing';


describe('<Register />', () => {
  const defaultProps = {
    intl: intlMock(),
    clearError: identity,
    openModal: identity,
    register: identity,
    registerError: false,
    isLoggedIn: false,
    registerIsProcessing: false,
  };

  const component = (props = {}) => <Register {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
