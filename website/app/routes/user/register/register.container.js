import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Register } from './register.component';

import { ModalsActions } from '../../../modules/modals/modals.redux';
import { RegisterActions } from '../../../modules/register/register.redux';
import { selectRegisterError, selectRegisterProcessing } from '../../../modules/register/register.selectors';
import { selectIsLoggedIn } from '../../../modules/user/user.selectors';

const mapStateToProps = createStructuredSelector({
  registerError: selectRegisterError,
  registerIsProcessing: selectRegisterProcessing,
  isLoggedIn: selectIsLoggedIn,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  openModal: ModalsActions.openModal,
  register: RegisterActions.request,
  clearError: RegisterActions.clearError,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Register));
