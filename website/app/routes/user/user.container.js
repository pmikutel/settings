import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { User } from './user.component';
import { ModalsActions } from '../../modules/modals/modals.redux';

const mapStateToProps = createStructuredSelector({});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  openModal: ModalsActions.openModal,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(User));
