import styled, { css } from 'styled-components';
import Media from '../../theme/media';

export const Copy = styled.p`
  max-width: 795px;
  margin: 15px auto 45px;
  text-align: justify;
`;

const tabletTitleExtraStyles = css`
  margin-top: 80px;
`;

export const titleExtraStyles = css`
  margin-top: 130px;
  ${Media.desktop(tabletTitleExtraStyles)}
`;
