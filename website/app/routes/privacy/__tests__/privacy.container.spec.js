import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../privacy.container';
import { BlogActions } from '../../../modules/blog/blog.redux';


describe('Privacy: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call BlogActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).blogRequest();

      expect(dispatch.firstCall.args[0]).to.deep.equal(BlogActions.request());
    });
  });
});
