import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'cart.title',
    defaultMessage: 'Products list',
  },
  cta: {
    id: 'cart.cta',
    defaultMessage: 'Calculate Delivery Cost',
  },
  item: {
    id: 'cart.item',
    defaultMessage: 'Item',
  },
  delivery: {
    id: 'cart.delivery',
    defaultMessage: 'Delivery',
  },
  qty: {
    id: 'cart.qty',
    defaultMessage: 'Qty',
  },
  price: {
    id: 'cart.price',
    defaultMessage: 'Price',
  },
  subtotal: {
    id: 'cart.subtotal',
    defaultMessage: 'Subtotal',
  },
  currency: {
    id: 'cart.currency',
    defaultMessage: '£{ price }',
  },
  empty: {
    id: 'cart.empty',
    defaultMessage: 'Your shopping cart is empty',
  },
  more: {
    id: 'cart.more',
    defaultMessage: 'Check our',
  },
  allProducts: {
    id: 'cart.all',
    defaultMessage: 'all products',
  },
});
