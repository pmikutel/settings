import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { ifElse, prop } from 'ramda';

import messages from './cart.messages';

import {
  ItemsWrapper,
  Bin,
  Image,
  Header,
  ProductTitle,
  VariantTitle,
  ProductSize,
  ItemInfo,
  Price,
  priceMobileStyles,
  deliveryMobileStyles,
  EmptyWrapper,
  Link,
  headerRowStyles,
  rowLabelsStyles,
  mobileRowStyles,
  desktopRowStyles,
  columnStyles,
  columnPositionStyles,
  columnCounterStyles,
  columnRemovePositionStyles,
  priceBoxStyles,
  actionButtonStyles,
  billingsRowStyles,
} from './cart.styles';

import { Grid, Components } from '../../../theme';
import { UnderlineTitle } from '../../../components/underlineTitle/underlineTitle.component';
import { Counter } from '../../../components/counter/counter.component';
import { ChatButton } from '../../../components/chatButton/chatButton.component';
import { decode } from '../../../utils/shopifyUrls';
import { numeralPriceFormat } from '../../../utils/numbers';
import { WindowResize } from '../../../components/windowResize/windowResize.component';

import { SETTINGS } from './cart.helpers';

const MOBILE_BREAKPOINT = 1024;

export class CartComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    cartItems: PropTypes.object.isRequired,
    removeItem: PropTypes.func.isRequired,
    updateItem: PropTypes.func.isRequired,
    createCheckout: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    isUserLogged: PropTypes.bool.isRequired,
    openModal: PropTypes.func.isRequired,
  };

  static contextTypes = {
    router: PropTypes.object.isRequired,
  };

  state = {
    mobile: window.innerWidth < MOBILE_BREAKPOINT,
  };

  onResizeHandler = (evt) => {
    this.setState({
      mobile: evt.currentTarget.innerWidth < MOBILE_BREAKPOINT,
    });
  };

  onCheckoutHandler = () => this.props.createCheckout({ lineItems: this.items });

  getProductUrl = item => {
    return `/products/${item.get('productHandle')}/${item.get('id')}/variant/${decode(item.get('variantId'))}`;
  };

  get price() {
    let result = 0;

    this.props.cartItems.map(item => {
      result = result + item.get('quantity') * item.get('price');
    });

    return numeralPriceFormat(result);
  }

  get items() {
    return this.props.cartItems.toArray().map(item => {
      return {
        variantId: item.get('variantId'),
        quantity: item.get('quantity'),
      };
    });
  }

  updateItemHandler = (item, quantity) => {
    const updatedItem = item.set('quantity', quantity);
    this.props.updateItem(updatedItem.toJS());
  };

  renderGridHeader = () => (
    <Grid.Row extraStyles={rowLabelsStyles}>
      <Grid.Column offset={SETTINGS.image.offset} columns={SETTINGS.image.cols}>
      </Grid.Column>
      <Grid.Column extraStyles={columnStyles} offset={SETTINGS.item.offset} columns={SETTINGS.item.cols}>
        <Header><FormattedMessage {...messages.item} /></Header>
      </Grid.Column>
      <Grid.Column
        extraStyles={columnStyles}
        offset={SETTINGS.delivery.offset}
        columns={SETTINGS.delivery.cols}
      >
        <Header><FormattedMessage {...messages.delivery} /></Header>
      </Grid.Column>
      <Grid.Column extraStyles={columnStyles} offset={SETTINGS.qty.offset} columns={SETTINGS.qty.cols}>
        <Header><FormattedMessage {...messages.qty} /></Header>
      </Grid.Column>
      <Grid.Column extraStyles={columnStyles} offset={SETTINGS.price.offset} columns={SETTINGS.price.cols}>
        <Header><FormattedMessage {...messages.price} /></Header>
      </Grid.Column>
      <Grid.Column offset={SETTINGS.remove.offset} columns={SETTINGS.remove.cols}>
      </Grid.Column>
    </Grid.Row>
  );

  renderGridItemsMobile = () => this.props.cartItems.valueSeq().map((item, index) => (
    <Grid.Row extraStyles={mobileRowStyles} zIndex={this.props.cartItems.size - index} key={index}>
      <Grid.Column
        offset={SETTINGS.image.offset}
        columns={SETTINGS.image.cols}
        mobileColumns={2}
      >
        <Link to={this.getProductUrl(item)}>
          <Image url={item.get('image')} />
        </Link>
      </Grid.Column>
      <Grid.Column
        mobileColumns={4}
      >
        <Grid.Row>
          <Grid.Column
            offset={SETTINGS.item.offset}
            columns={SETTINGS.item.cols}
            mobileColumns={6}
            extraStyles={columnStyles}
          >
            <ProductTitle>{item.get('title')}</ProductTitle>
            <VariantTitle>
              <Link to={this.getProductUrl(item)}>{item.get('variantTitle')}</Link>
            </VariantTitle>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column
            offset={SETTINGS.delivery.offset}
            columns={SETTINGS.delivery.cols}
            mobileColumns={6}
            extraStyles={columnPositionStyles}
          >
            <ItemInfo extraStyles={deliveryMobileStyles}>{item.get('delivery')}</ItemInfo>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row extraStyles={billingsRowStyles}>
          <Grid.Column
            offset={SETTINGS.qty.offset}
            columns={SETTINGS.qty.cols}
            mobileColumns={3}
            extraStyles={columnCounterStyles}
          >
            <Counter
              center
              onChange={(quantity) => this.updateItemHandler(item, quantity)}
              max={9999}
              min={0}
              value={item.toJS().quantity}
            />
          </Grid.Column>
          <Grid.Column
            offset={SETTINGS.remove.offset}
            columns={SETTINGS.remove.cols}
            mobileColumns={1}
            extraStyles={columnRemovePositionStyles}
          >
            <button onClick={() => this.props.removeItem(item.get('variantId'))}>
              <Bin />
            </button>
          </Grid.Column>
          <Grid.Column
            offset={SETTINGS.price.offset}
            columns={SETTINGS.price.cols}
            mobileColumns={2}
            extraStyles={columnPositionStyles}
          >
            <ItemInfo extraStyles={priceMobileStyles}>
              <FormattedMessage
                {...messages.currency}
                values={{ price: numeralPriceFormat(item.get('price')) }}
              />
            </ItemInfo>
          </Grid.Column>
        </Grid.Row>
      </Grid.Column>
    </Grid.Row>
  ));

  renderGridItemsDesktop = () => this.props.cartItems.valueSeq().map((item, index) => (
    <Grid.Row extraStyles={desktopRowStyles} key={index} zIndex={this.props.cartItems.size - index}>
      <Grid.Column
        offset={SETTINGS.image.offset}
        columns={SETTINGS.image.cols}
      >
        <Link to={this.getProductUrl(item)}>
          <Image url={item.get('image')} />
        </Link>
      </Grid.Column>
      <Grid.Column
        offset={SETTINGS.item.offset}
        columns={SETTINGS.item.cols}
        extraStyles={columnStyles}
      >
        <ProductTitle>{item.get('title')}</ProductTitle>
        <VariantTitle>
          <Link to={this.getProductUrl(item)}>{item.get('variantTitle')}</Link>
        </VariantTitle>
        <ProductSize>{item.get('size')}</ProductSize>
      </Grid.Column>
      <Grid.Column
        offset={SETTINGS.delivery.offset}
        columns={SETTINGS.delivery.cols}
        extraStyles={columnPositionStyles}
      >
        <ItemInfo>{item.get('delivery')}</ItemInfo>
      </Grid.Column>
      <Grid.Column
        offset={SETTINGS.qty.offset}
        columns={SETTINGS.qty.cols}
        extraStyles={columnPositionStyles}
      >
        <Counter
          center
          onChange={(quantity) => this.updateItemHandler(item, quantity)}
          max={9999}
          min={0}
          value={item.toJS().quantity}
        />
      </Grid.Column>
      <Grid.Column
        offset={SETTINGS.price.offset}
        columns={SETTINGS.price.cols}
        extraStyles={columnPositionStyles}
      >
        <ItemInfo>
          <FormattedMessage
            {...messages.currency}
            values={{ price: numeralPriceFormat(item.get('price')) }}
          />
        </ItemInfo>
      </Grid.Column>
      <Grid.Column
        columns={SETTINGS.remove.cols}
        extraStyles={columnPositionStyles}
      >
        <button onClick={() => this.props.removeItem(item.get('variantId'))}>
          <Bin />
        </button>
      </Grid.Column>
    </Grid.Row>
  ));

  renderGridItems = () => ifElse(
    prop('mobile'),
    () => this.renderGridItemsMobile(),
    () => this.renderGridItemsDesktop(),
  )(this.state);

  renderGrid = () => (
    <ItemsWrapper>
      {this.renderGridHeader()}
      {this.renderGridItems()}
    </ItemsWrapper>
  );

  renderEmpty = () => (
    <EmptyWrapper>
      <p><FormattedMessage {...messages.empty} /></p>
      <p>
        <FormattedMessage {...messages.more} /> -
        <Components.Link to="/products/all"> <FormattedMessage {...messages.allProducts} /></Components.Link>
      </p>
    </EmptyWrapper>
  );

  renderCartContent = () => ifElse(
    prop('size'),
    () => this.renderGrid(),
    () => this.renderEmpty(),
  )(this.props.cartItems);

  render = () => (
    <div>
      <WindowResize onResize={this.onResizeHandler} />
      <Grid.Section>
        <Grid.Row extraStyles={headerRowStyles}>
          <Grid.Column offset={1} columns={1}>
            <Components.Button extraStyles={Components.buttonArrowStyles} onClick={this.context.router.history.goBack}>
              <Components.Arrow left />
            </Components.Button>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column offset={1} columns={15}>
            <UnderlineTitle title={this.props.intl.formatMessage(messages.title)} dotsPattern={false} />
          </Grid.Column>
        </Grid.Row>

        {this.renderCartContent()}

        <Grid.Row>
          <Grid.Column offset={1} columns={3} extraStyles={actionButtonStyles}>
            <ChatButton />
          </Grid.Column>

          <Grid.Column offset={4} columns={8} extraStyles={priceBoxStyles}>
            <Header>
              <FormattedMessage {...messages.subtotal} />
            </Header>
            <Price>
              <FormattedMessage
                {...messages.currency}
                values={{ price: this.price }}
              />
            </Price>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>

      <Components.SubmitButton
        onClick={this.onCheckoutHandler}
        disabled={!this.props.cartItems.size}
      >
        <FormattedMessage {...messages.cta} />
      </Components.SubmitButton>
    </div>
  );
}

export const Cart = injectIntl(CartComponent);
