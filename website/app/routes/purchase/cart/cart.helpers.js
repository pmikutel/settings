export const SETTINGS = {
  image: {
    offset: 0,
    cols: 3,
  },
  item: {
    offset: 1,
    cols: 4,
  },
  delivery: {
    offset: 1,
    cols: 1,
  },
  qty: {
    offset: 1,
    cols: 2,
  },
  price: {
    offset: 1,
    cols: 2,
  },
  remove: {
    cols: 1,
  },
};
