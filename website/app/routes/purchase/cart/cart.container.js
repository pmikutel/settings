import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Cart } from './cart.component';
import { CartActions } from '../../../modules/cart/cart.redux';
import { ModalsActions } from '../../../modules/modals/modals.redux';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { selectCartItems } from '../../../modules/cart/cart.selectors';
import { selectIsLoggedIn } from '../../../modules/user/user.selectors';

const mapStateToProps = createStructuredSelector({
  cartItems: selectCartItems,
  isUserLogged: selectIsLoggedIn,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  removeItem: CartActions.removeItem,
  updateItem: CartActions.updateItem,
  createCheckout: CheckoutActions.createRequest,
  openModal: ModalsActions.openModal,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Cart));
