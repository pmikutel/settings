import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { fromJS } from 'immutable';
import { CartComponent as Cart } from '../cart.component';
import { intlMock } from '../../../../utils/testing';


describe('<Cart />', () => {
  const defaultProps = {
    intl: intlMock(),
    cartItems: fromJS([]),
    removeItem: identity,
    updateItem: identity,
    createCheckout: identity,
    history: {},
    isUserLogged: true,
    openModal: identity,
  };

  const component = (props = {}) => <Cart {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props), { context: { router: { history: { goBack: identity } } } });

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
