import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../cart.container';
import { CartActions } from '../../../../modules/cart/cart.redux';
import { ModalsActions } from '../../../../modules/modals/modals.redux';

describe('Cart: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call CartActions.removeItem', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).removeItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CartActions.removeItem());
    });

    it('should call CartActions.updateItem', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateItem();

      expect(dispatch.firstCall.args[0]).to.deep.equal(CartActions.updateItem());
    });

    it('should call ModalsActions.openModal', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).openModal();

      expect(dispatch).to.have.been.calledWith(ModalsActions.openModal());
    });
  });
});
