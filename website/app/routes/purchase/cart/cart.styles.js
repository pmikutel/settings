import styled, { css } from 'styled-components';
import { prop } from 'ramda';
import * as RRD from 'react-router-dom';
import Sprite from '../../../utils/sprites';
import { Media, Variables, Typography } from '../../../theme';

const mobileHiddenSectionDesktopStyles = css`
  display: flex;
`;

export const mobileHiddenSectionStyles = css`
  display: none;
  ${Media.desktop(mobileHiddenSectionDesktopStyles)};
`;

const headerRowDesktopStyles = css`
  display: block;
  margin: 30px 0 125px;
`;

export const headerRowStyles = css`
  margin-top: 170px;
  ${Media.desktop(headerRowDesktopStyles)};
`;

const itemsWrapperDesktopStyles = css`
  margin: 75px 0 50px;
`;

export const ItemsWrapper = styled.section`
  margin: 215px 0 90px;
  position: relative;

  ${Media.desktop(itemsWrapperDesktopStyles)}
`;

export const desktopRowStyles = css`
  border-bottom: 1px solid ${Variables.colorBorderGray};

  ${Media.desktop('display: flex;')}
`;

export const mobileRowStyles = css`
  border-bottom: 1px solid ${Variables.colorBorderGray};
  width: 100%;

  ${Media.desktop('display: none;')}
`;

export const rowLabelsStyles = css`
  ${desktopRowStyles};
  ${mobileHiddenSectionStyles};
`;

const columnDesktopStyles = css`
  padding-bottom: 30px;
`;

export const columnStyles = css`
  align-self: center;
  padding-bottom: 15px;

  ${Media.desktop(columnDesktopStyles)}
`;

const columnPositionDesktopStyles = css`
  position: relative;
  top: 30px;
`;

export const columnPositionStyles = css`
  ${columnStyles};

  ${Media.desktop(columnPositionDesktopStyles)};
`;

const columnCounterStylesBigMobileStyles = css`
  width: 50%;
`;

export const columnCounterStyles = css`
  width: 100%;
  margin-bottom: 15px;
  
  ${Media.bigMobile(columnCounterStylesBigMobileStyles)};
`;

export const columnRemovePositionStyles = css`
  ${columnPositionStyles};
`;

export const Bin = styled.i`
  display: block;
  ${Sprite.responsive('bin')};
`;

const imageDesktopStyles = css`
  background-position: center;
`;

export const Image = styled.div`
  background-image: url(${props => props.url});
  background-size: cover;
  background-position: center;
  display: block;
  height: 100%;
  min-height: 220px;
  width: 100%;

  ${Media.desktop(imageDesktopStyles)};
`;

export const Header = styled.h3`
  font-size: 22px;
  ${Typography.fontLyonRegular};
`;

export const productTitleDesktopStyles = css`
  margin-top: 45px;
  margin-bottom: 30px;
  letter-spacing: 12px;
`;

export const ProductTitle = styled.h3`
  text-transform: uppercase;
  margin-top: 44px;
  margin-bottom: 10px;
  color: ${Variables.colorWarmGrey};

  ${Media.desktop(productTitleDesktopStyles)};
`;

const variantTitleDesktopStyles = css`
  margin-bottom: 25px;
  letter-spacing: 24px;
`;
export const VariantTitle = styled.h2`
  text-transform: uppercase;
  margin-bottom: 0;
  overflow-wrap: break-word;

  ${Media.desktop(variantTitleDesktopStyles)};
`;

export const ProductSize = styled.p`
  margin: 0;
  color: ${Variables.colorWarmGrey};
`;

export const priceMobileStyles = css`
  padding-left: 16px;
`;

export const deliveryMobileStyles = css`
  font-size: 14px;
  letter-spacing: 8px;
`;

export const ItemInfo = styled.h3`
  font-size: 22px;
  white-space: nowrap;

  ${prop('extraStyles')}
`;

export const Price = styled.h2`
  margin: 35px -25px 95px 0;
  letter-spacing: 24px;
`;

export const priceBoxStyles = css`
  padding-right: 4%;
  text-align: right;
`;

export const actionButtonStyles = css`
  align-items: start;

  ${mobileHiddenSectionStyles}
`;

export const EmptyWrapper = styled.div`
  position: relative;
  margin-top: 40px;
  margin-bottom: 20px;
  text-align: center;
`;

export const Link = styled(RRD.Link)`
  color: ${Variables.colorBlack};
  display: block;
  height: 100%;
  width: 100%;
`;

const billingDesktopRowStyles = css`
  flex-wrap: nowrap;
`;

export const billingsRowStyles = css`
  flex-wrap: wrap;
  ${Media.desktop(billingDesktopRowStyles)};
`;
