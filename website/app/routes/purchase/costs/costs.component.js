import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';

import messages from './costs.messages';

import { StaticHeroImage } from '../../../components/staticHeroImage/staticHeroImage.component';
import { GradientBox } from '../../../components/gradientBox/gradientBox.component';
import { Form } from './components/form/form.component';
import { ChatButton } from '../../../components/chatButton/chatButton.component';
import { calulateVat } from '../../../utils/vat';
import { getLastDelivery } from '../../../utils/deliveryData';

import { rowStyles, EstimatedTitle, Description, Delivery, Change, columnStyles } from './costs.styles';
import { Grid } from '../../../theme';


export class CostsComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    articleImage: PropTypes.string,
    blogRequest: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    updateData: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    cartItems: PropTypes.object.isRequired,
    checkoutData: PropTypes.object,
    updateAddress: PropTypes.func.isRequired,
    updateAttributes: PropTypes.func.isRequired,
  };

  static defaultProps = {
    checkoutData: {},
  };

  componentDidMount() {
    this.props.blogRequest('costs');
  }

  handleSubmit = data => {
    this.props.updateData(data);

    this.props.updateAddress(data, this.props.checkoutData.get('id'));
    this.props.updateAttributes(data, this.props.checkoutData.get('id'));
  };

  get delivery() {
    return getLastDelivery(this.props.cartItems.toJS());
  }

  render = () => (
    <div>
      <StaticHeroImage
        heroImage={this.props.articleImage}
        title={this.props.intl.formatMessage(messages.heroTitle)}
        subtitle={this.props.intl.formatMessage(messages.heroSubtitle)}
        shouldShowStatus
        showBackIcon
        status="1/2"
      />

      <Grid.Section>
        <Grid.Row extraStyles={rowStyles}>
          <Grid.Column offset={3} columns={6} mobileOffset={0} mobileColumns={6}>
            <EstimatedTitle><FormattedMessage {...messages.estimated} /></EstimatedTitle>
            <Description>
              <Delivery><FormattedMessage {...messages.delivery} values={{ delivery: this.delivery }} /></Delivery>
              <Change>
                <FormattedMessage {...messages.change} />
                <ChatButton link />
              </Change>
            </Description>
          </Grid.Column>
          <Grid.Column offset={1} columns={6} mobileOffset={0} mobileColumns={6} extraStyles={columnStyles}>
            <GradientBox
              subtotal={parseFloat(this.props.checkoutData.get('totalPrice'))}
              vat={parseFloat(calulateVat(this.props.checkoutData.get('totalPrice')))}
              order
            />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>

      <Form
        onSubmit={this.handleSubmit}
        initialValues={this.props.data}
      />
    </div>
  );
}

export const Costs = injectIntl(CostsComponent);
