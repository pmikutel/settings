import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Costs } from './costs.component';
import { selectImage } from '../../../modules/blog/blog.selectors';
import { BlogActions } from '../../../modules/blog/blog.redux';
import { CheckoutDetailsActions } from '../../../modules/checkoutDetails/checkoutDetails.redux';
import { selectData } from '../../../modules/checkoutDetails/checkoutDetails.selectors';
import { selectCartItems } from '../../../modules/cart/cart.selectors';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { selectCheckoutData } from '../../../modules/checkout/checkout.selectors';

const mapStateToProps = createStructuredSelector({
  data: selectData,
  checkoutData: selectCheckoutData,
  articleImage: selectImage,
  cartItems: selectCartItems,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  blogRequest: BlogActions.request,
  updateData: CheckoutDetailsActions.updateData,
  updateAddress: CheckoutActions.customerAddressUpdateRequest,
  updateAttributes: CheckoutActions.customerAttributesUpdateRequest,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Costs);
