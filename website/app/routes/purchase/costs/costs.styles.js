import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../theme';


const rowDesktoptyles = css`
  flex-direction: row;
`;

export const rowStyles = css`
  flex-direction: column-reverse;
  position: relative;
  z-index: 1;

  ${Media.desktop(rowDesktoptyles)}
`;

const estimatedTitleDesktopWideStyles = css`
  font-size: 40px;
  letter-spacing: 24px;
`;

const estimatedTitleDesktopStyles = css`
  letter-spacing: 16px;
  font-size: 30px;
  line-height: 48px;
`;

export const EstimatedTitle = styled.h2`
  letter-spacing: 13px;
  padding: 153px 0 52px;
  
  ${Media.desktop(estimatedTitleDesktopStyles)};
  ${Media.desktopWide(estimatedTitleDesktopWideStyles)};
`;

export const descriptionDesktopStyles = css`
  margin-left: 38%;
`;

export const Description = styled.div`
  color: ${Variables.colorWarmGrey};
  display: flex;
  flex-direction: column;
  
  ${Media.desktop(descriptionDesktopStyles)};
`;

const deliveryDesktopStyles = css`
  padding-bottom: 0;
  line-height: 50px;
`;

export const Delivery = styled.div`
  font-size: 22px;
  line-height: 46px;
  padding-bottom: 15px;
  
  ${Media.desktop(deliveryDesktopStyles)};
`;

export const Change = styled.div`
  font-size: 18px;
  line-height: 36px;
`;

export const columnStyles = css`
  margin-right: 61px;
`;
