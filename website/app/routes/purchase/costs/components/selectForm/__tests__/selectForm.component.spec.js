import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { SelectFormComponent as SelectForm } from '../selectForm.component';
import { intlMock } from '../../../../../../utils/testing';

describe('<SelectForm />', () => {
  const defaultProps = {
    onChange: identity,
    options: [{
      value: 1, label: {
        id: 'costs.firstFloor',
        defaultMessage: '1st floor',
      },
    }],
    placeholder: 'placeholder',
    name: 'name',
    value: 'name',
    intl: intlMock(),

  };

  const component = (props = {}) => <SelectForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
