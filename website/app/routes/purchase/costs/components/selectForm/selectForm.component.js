import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import { Wrapper } from './selectForm.styles';


export class SelectFormComponent extends PureComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    intl: PropTypes.object.isRequired,
  };

  state = {
    option: this.props.value,
  };

  get options() {
    return this.props.options.map(({ value, label }) => {
      return { value, label: this.props.intl.formatMessage(label) };
    });
  }

  handleChange = (selectedOption) => {
    this.setState({
      option: selectedOption,
    });

    const value = selectedOption ? selectedOption.value : '';
    this.props.onChange(value);
  };

  render = () => (
    <Wrapper
      name={this.props.name}
      onChange={this.handleChange}
      value={this.state.option}
      searchable={false}
      clearable={false}
      placeholder={this.props.placeholder}
      options={this.options}
    />
  );
}

export const SelectForm = injectIntl(SelectFormComponent);
