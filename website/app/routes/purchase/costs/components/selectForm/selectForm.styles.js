import styled from 'styled-components';
import ReactSelect from 'react-select';
import { Variables } from '../../../../../theme';


export const Wrapper = styled(ReactSelect)`
  position: relative;
  font-size: 18px;
  z-index: 10;

  .Select-input {
    outline: none;
  }
  
  .Select-value {
    width: 100%;
  }
  
  .Select-value-label {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: block;
  }
  
  .Select-control,
  .Select-menu-outer {
    color: ${Variables.colorWarmGrey};
    width: 100%;
    box-sizing: border-box;
    padding: 12px 40px 12px 20px;
  }
  
  .Select-multi-value-wrapper {
    display: flex;
  }

  .Select-control {
    line-height: 36px;
    position: relative;
    cursor: pointer;
    border: 2px solid ${Variables.colorPureBlack};

    &:after {
      display: inline-block;
      content: '';
      position: absolute;
      right: 22px;
      top: 23px;
      transform: rotateZ(45deg) translateY(-50%);
      border: solid black;
      border-width: 0 2px 2px 0;
      padding: 6px;
      transition: 0.2s transform;
    }
  }

  .Select-menu {
    z-index: 10;
  }

  .Select-menu-outer {
    background-color: ${Variables.colorPureWhite};
    border: 2px solid ${Variables.colorWarmGrey};
    border-top: none;
    position: absolute;
    top: 64px;
    left: 0;
    padding: 20px 20px 0;
  }

  .Select-option {
    cursor: pointer;
    margin-bottom: 20px;

    &:hover {
      color: ${Variables.colorPureBlack};
    }
  }

  &.is-open {
    .Select-control {
      &:after {
        top: 21px;
        transform: rotate(-135deg) translate(-50%, 0%);
      }
  }
`;
