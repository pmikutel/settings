import styled, { css } from 'styled-components';
import { prop } from 'ramda';

import { Media, Variables } from '../../../../../theme/index';


export const wrapperDesktopStyles = css`
  margin-bottom: 127px;
`;

export const wrapper = css`
  margin-bottom: 151px;
    
  ${Media.desktop(wrapperDesktopStyles)}
`;

export const chatDesktopStyles = css`
  padding: 3px 0 92px;
`;

export const chatStyles = css`
  padding: 30px 0 50px;
  
  ${Media.desktop(chatDesktopStyles)}
`;

export const Title = styled.h2`
  letter-spacing: 24px;
  padding-bottom: 21px;
  max-width: 300px;
`;

const rowDesktopStyles = css`
  padding-top: 120px;
`;

export const rowStyles = css`
  z-index: 2;
  padding-top: 100px;
  
  ${Media.desktop(rowDesktopStyles)};
`;

export const inputStyles = css`
  margin: 10px 0;
  display: inline-flex;
`;

export const numberStyles = css`
  width: 30%;
`;

export const streetStyles = css`
  width: 65%;
`;


export const InputRow = styled.div`
  ${inputStyles};
  
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const labelAutoCompleteStyles = css`
  position: relative;
  z-index: 3;
`;

export const Label = styled.div`
  padding-bottom: 20px;
`;

export const autoCompleteStyles = {
  input: {
    padding: '0 20px',
    fontFamily: Variables.lyonLightFontFamily,
  },
  autocompleteItem: {
    fontSize: '18px',
    padding: '5px 20px',
  },
  autocompleteItemActive: {
    color: Variables.colorPureBlack,
    backgroundColor: 'transparent',
    cursor: 'pointer',
  },
};

export const selectFloorDesktopStyles = css`
  max-width: 276px;
`;

export const selectFloorStyles = css`
  max-width: 340px;
  width: 100%;
  
  ${Media.desktop(selectFloorDesktopStyles)}
`;

export const selectWhenStyles = css`
  max-width: 428px;
`;

export const FormElement = styled.div`
  padding-bottom: 50px;
  
  ${prop('extraStyles')};
`;

const contentDesktopStyles = css`
  margin-left: 21%;
`;

export const Content = styled.div`
  margin-left: 0;
  max-width: 500px;
  
  ${Media.desktop(contentDesktopStyles)};
`;
