
import { defineMessages } from 'react-intl';

export default defineMessages({
  location: {
    id: 'costs.location',
    defaultMessage: 'SPACE LOCATION',
  },
  address: {
    id: 'costs.address',
    defaultMessage: 'What is your address?',
  },
  addressPlaceholder: {
    id: 'costs.addressPlaceholder',
    defaultMessage: 'Type postcode to find address',
  },
  find: {
    id: 'costs.find',
    defaultMessage: 'Find address',
  },
  manually: {
    id: 'costs.manually',
    defaultMessage: 'or enter your adress manually',
  },
  details: {
    id: 'costs.details',
    defaultMessage: 'SPACE DETAILS',
  },
  floor: {
    id: 'costs.floor',
    defaultMessage: 'What floor is your office on?',
  },
  lift: {
    id: 'costs.lift',
    defaultMessage: 'Does your floor have a lift access? <br /> (If on ground floor click yes)',
  },
  when: {
    id: 'costs.when',
    defaultMessage: 'When would you prefer installation to occur?',
  },
  yes: {
    id: 'costs.yes',
    defaultMessage: 'Yes',
  },
  no: {
    id: 'costs.no',
    defaultMessage: 'No',
  },
  working: {
    id: 'costs.working',
    defaultMessage: 'During working hours (9am-5am/Mon-Friday)',
  },
  out: {
    id: 'costs.working',
    defaultMessage: 'Out of hours (Before 9am-after 6pm/Sat-Sunday)',
  },
  next: {
    id: 'costs.next',
    defaultMessage: 'Continue to Payment',
  },
  number: {
    id: 'costs.number',
    defaultMessage: 'Number',
  },
  route: {
    id: 'costs.route',
    defaultMessage: 'Road',
  },
  city: {
    id: 'costs.city',
    defaultMessage: 'City',
  },
  postCode: {
    id: 'costs.postCode',
    defaultMessage: 'Post code',
  },
  firstFloor: {
    id: 'costs.firstFloor',
    defaultMessage: '1st floor',
  },
  ground: {
    id: 'costs.ground',
    defaultMessage: 'ground floor',
  },
  secondFloor: {
    id: 'costs.secondFloor',
    defaultMessage: '2nd floor',
  },
  thirdFloor: {
    id: 'costs.thirdFloor',
    defaultMessage: '3rd floor',
  },
  fourthFloor: {
    id: 'costs.thirdFloor',
    defaultMessage: '4th+ floor',
  },
  selectPlaceholder: {
    id: 'costs.selectPlaceholder',
    defaultMessage: 'Please select',
  },
});
