import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { FormComponent as Form } from '../form.component';
import { intlMock } from '../../../../../../utils/testing';


describe('<Form />', () => {
  const defaultProps = {
    handleSubmit: identity,
    change: identity,
    invalid: false,
    intl: intlMock(),
  };

  const component = (props = {}) => <Form {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
