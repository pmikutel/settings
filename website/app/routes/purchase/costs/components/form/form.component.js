import React, { PureComponent } from 'react';
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import validate from 'validate.js';
import { defaultTo } from 'ramda';

import PlacesAutoComplete, { geocodeByAddress } from 'react-places-autocomplete';

import messages from './form.messages';

import { ChatButton } from '../../../../../components/chatButton/chatButton.component';
import { parseAddress, RADIO_OPTIONS, WHEN_SELECT_OPTIONS, FLOOR_SELECT_OPTIONS } from './form.helpers';
import { RadioSquares } from '../../../../../components/radioSquares/radioSquares.component';
import { SelectForm } from '../selectForm/selectForm.component';

import {
  chatStyles,
  InputRow,
  Title,
  rowStyles,
  inputStyles,
  numberStyles,
  streetStyles,
  labelAutoCompleteStyles,
  selectFloorStyles,
  selectWhenStyles,
  Content,
  autoCompleteStyles,
  FormElement,
  Label,
} from './form.styles';
import { Forms, Grid, Components } from '../../../../../theme/index';


export class FormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    change: PropTypes.func,
    invalid: PropTypes.bool,
    intl: PropTypes.object.isRequired,
  };

  state = {
    address: '',
  };

  onChangeAddress = (address) => {
    this.setState({ address });
  };

  handleAddress = (address) => {
    this.setState({ address });
    geocodeByAddress(address)
      .then(results => {
        const object = parseAddress(results);
        Object.keys(object).forEach((key) => {
          this.props.change(key, object[key]);
        });
      });
  };

  renderInput = ({ input, extraStyles, placeholder, meta: { touched, error } }) => (
    <Forms.Input
      errorStyle={touched && !!error}
      extraStyles={defaultTo(inputStyles, extraStyles)}
      placeholder={placeholder}
      {...input}
    />
  );

  renderAddress = () => (
    <div>
      <InputRow>
        <Field
          name="number"
          type="number"
          extraStyles={numberStyles}
          placeholder={this.props.intl.formatMessage(messages.number)}
          component={this.renderInput}
        />
        <Field
          name="route"
          type="text"
          extraStyles={streetStyles}
          placeholder={this.props.intl.formatMessage(messages.route)}
          component={this.renderInput}
        />
      </InputRow>
      <Field
        name="postal_town"
        type="text"
        placeholder={this.props.intl.formatMessage(messages.city)}
        component={this.renderInput}
      />
      <Field
        name="postal_code"
        type="text"
        placeholder={this.props.intl.formatMessage(messages.postCode)}
        component={this.renderInput}
      />
    </div>
  );

  renderRadioField = ({ input, items }) => (
    <RadioSquares
      {...input}
      items={items}
    />
  );

  renderSelectField = ({ input, options, extraStyles, copy }) => (
    <FormElement extraStyles={extraStyles}>
      <Forms.Label>{copy}</Forms.Label>
      <SelectForm
        {...input}
        placeholder={this.props.intl.formatMessage(messages.selectPlaceholder)}
        options={options}
      />
    </FormElement>
  );

  render = () => {
    const { intl: { formatMessage }, handleSubmit, invalid } = this.props;
    const inputOptions = {
      onChange: this.onChangeAddress,
      value: this.state.address,
      placeholder: formatMessage(messages.addressPlaceholder),
    };

    return (
      <form onSubmit={handleSubmit} noValidate>
        <Grid.Section>
          <Grid.Row extraStyles={rowStyles}>
            <Grid.Column offset={3} columns={10}>
              <Title><FormattedMessage {...messages.location} /></Title>
              <Content>
                <Forms.Label extraStyles={labelAutoCompleteStyles}>
                  <Label><FormattedMessage {...messages.address} /></Label>
                  <PlacesAutoComplete
                    inputProps={inputOptions}
                    highlightFirstSuggestion
                    onEnterKeyDown={this.handleAddress}
                    onSelect={this.handleAddress}
                    styles={autoCompleteStyles}
                    options={{ componentRestrictions: { country: 'uk' } }}
                  />
                </Forms.Label>
                {this.renderAddress()}
              </Content>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row extraStyles={rowStyles}>
            <Grid.Column offset={3} columns={10}>
              <Title><FormattedMessage {...messages.details} /></Title>
              <Content>
                <Field
                  name="floor"
                  extraStyles={selectFloorStyles}
                  copy={this.props.intl.formatMessage(messages.floor)}
                  options={FLOOR_SELECT_OPTIONS}
                  component={this.renderSelectField}
                />
                <FormElement>
                  <Forms.Label>
                    <FormattedHTMLMessage {...messages.lift} />
                  </Forms.Label>
                  <Field
                    name="lift"
                    items={RADIO_OPTIONS}
                    component={this.renderRadioField}
                  />
                </FormElement>
                <Field
                  name="when"
                  extraStyles={selectWhenStyles}
                  options={WHEN_SELECT_OPTIONS}
                  copy={this.props.intl.formatMessage(messages.when)}
                  component={this.renderSelectField}
                />
              </Content>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column offset={1} columns={14} extraStyles={chatStyles}>
              <ChatButton />
            </Grid.Column>
          </Grid.Row>
        </Grid.Section>
        <Components.SubmitButton
          type="submit"
          disabled={invalid}
        >
          {this.props.intl.formatMessage(messages.next)}
        </Components.SubmitButton>
      </form>
    );
  };
}

export const Form = injectIntl(reduxForm({
  form: 'costsForm',
  validate: (values) => validate(values.toJS(), {
    number: {
      presence: {
        allowEmpty: false,
      },
    },
    route: {
      presence: {
        allowEmpty: false,
      },
    },
    'postal_town': {
      presence: {
        allowEmpty: false,
      },
    },
    'administrative_area_level_2': {
      presence: {
        allowEmpty: false,
      },
    },
    'postal_code': {
      presence: {
        allowEmpty: false,
      },
    },
    lift: {
      presence: {
        allowEmpty: false,
      },
    },
    floor: {
      presence: {
        allowEmpty: false,
      },
    },
    when: {
      presence: {
        allowEmpty: false,
      },
    },
  }),
})(FormComponent));
