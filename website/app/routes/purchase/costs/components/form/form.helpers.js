import { path, pipe, map, mergeAll, pick } from 'ramda';
import messages from './form.messages';

const pickFields = ['postal_code', 'route', 'postal_town'];

export const parseAddress = pipe(
  path([0, 'address_components']),
  map(({ long_name: name, types }) => ({ [types[0]]: name })),
  mergeAll,
  pick(pickFields),
);

export const RADIO_OPTIONS = [
  { id: 'yes', message: messages.yes },
  { id: 'no', message: messages.no },
];

export const WHEN_SELECT_OPTIONS = [
  { value: 'working', label: messages.working },
  { value: 'out', label: messages.out },
];


export const FLOOR_SELECT_OPTIONS = [
  { value: 'Ground', label: messages.ground },
  { value: '1st', label: messages.firstFloor },
  { value: '2nd', label: messages.secondFloor },
  { value: '3rd+', label: messages.thirdFloor },
  { value: '4th+', label: messages.fourthFloor },
];
