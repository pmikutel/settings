
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'costs.title',
    defaultMessage: 'costs page',
  },
  heroTitle: {
    id: 'costs.heroTitle',
    defaultMessage: 'DELIVERY DETAILS & COST',
  },
  heroSubtitle: {
    id: 'costs.heroSubtitle',
    defaultMessage: 'Calculate your',
  },
  estimated: {
    id: 'costs.estimated',
    defaultMessage: 'ESTIMATED DELIVERY SCHEDULE',
  },
  delivery: {
    id: 'costs.delivery',
    defaultMessage: 'Delivery on {delivery}',
  },
  change: {
    id: 'costs.change',
    defaultMessage: 'Need to change delivery dates? ',
  },
  contact: {
    id: 'costs.contact',
    defaultMessage: 'Contact us',
  },
});
