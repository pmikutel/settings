import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { CostsComponent as Costs } from '../costs.component';
import { intlMock } from '../../../../utils/testing';


describe('<Costs />', () => {
  const defaultProps = {
    intl: intlMock(),
    articleImage: '',
    blogRequest: identity,
    history: {},
    updateData: identity,
    updateAddress: identity,
    data: fromJS({}),
    cartItems: fromJS({
      123: {
        delivery: '1-2 weeks',
      },
    }),
    checkoutData: fromJS({
      totalPrice: 10,
    }),
    updateAttributes: identity,
  };

  const component = (props = {}) => <Costs {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
