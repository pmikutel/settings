import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../costs.container';
import { BlogActions } from '../../../../modules/blog/blog.redux';
import { CheckoutDetailsActions } from '../../../../modules/checkoutDetails/checkoutDetails.redux';
import { CheckoutActions } from '../../../../modules/checkout/checkout.redux';

describe('Costs: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call BlogActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).blogRequest();

      expect(dispatch).to.have.been.calledWith(BlogActions.request());
    });

    it('should call CheckoutDetailsActions.updateData', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateData();

      expect(dispatch).to.have.been.calledWith(CheckoutDetailsActions.updateData());
    });

    it('should call CheckoutActions.customerAddressUpdateRequest,', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateAddress();

      expect(dispatch).to.have.been.calledWith(CheckoutActions.customerAddressUpdateRequest());
    });

    it('should call CheckoutActions.updateAttributes,', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateAttributes();

      expect(dispatch).to.have.been.calledWith(CheckoutActions.customerAttributesUpdateRequest());
    });
  });
});
