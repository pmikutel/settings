
import { defineMessages } from 'react-intl';

export default defineMessages({
  next: {
    id: 'payment.next',
    defaultMessage: 'Buy now',
  },
  heroTitle: {
    id: 'payment.heroTitle',
    defaultMessage: 'REVIEW AND PAY',
  },
  heroSubtitle: {
    id: 'payment.heroSubtitle',
    defaultMessage: 'Payment',
  },
  contact: {
    id: 'payment.contact',
    defaultMessage: 'CONTACT DETAILS',
  },
  phone: {
    id: 'payment.phone',
    defaultMessage: 'Phone number :',
  },
  prefix: {
    id: 'payment.prefix',
    defaultMessage: '+44',
  },
});
