import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../payment.container';
import { BlogActions } from '../../../../modules/blog/blog.redux';
import { CheckoutActions } from '../../../../modules/checkout/checkout.redux';
import { CartActions } from '../../../../modules/cart/cart.redux';

describe('Payment: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call BlogActions.request', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).blogRequest();

      expect(dispatch).to.have.been.calledWith(BlogActions.request());
    });

    it('should call CheckoutActions.customerEmailUpdateRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateEmail();

      expect(dispatch).to.have.been.calledWith(CheckoutActions.customerEmailUpdateRequest());
    });

    it('should call CheckoutActions.customerShippingLineUpdateRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateShippingLines();

      expect(dispatch).to.have.been.calledWith(CheckoutActions.customerShippingLineUpdateRequest());
    });

    it('should call CheckoutActions.customerAddressUpdateRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).updateAddress();

      expect(dispatch).to.have.been.calledWith(CheckoutActions.customerAddressUpdateRequest());
    });

    it('should call CartActions.clearCart', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).clearCart();

      expect(dispatch).to.have.been.calledWith(CartActions.clearCart());
    });
  });
});
