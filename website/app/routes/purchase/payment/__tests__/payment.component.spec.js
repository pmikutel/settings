import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { PaymentComponent as Payment } from '../payment.component';
import { intlMock } from '../../../../utils/testing';


describe('<Payment />', () => {
  const defaultProps = {
    intl: intlMock(),
    articleImage: '',
    blogRequest: identity,
    history: {},
    data: fromJS({}),
    user: fromJS({}),
    clearCart: identity,
    updateEmail: identity,
    updateAddress: identity,
    updateShippingLines: identity,
    checkoutData: fromJS({
      totalPrice: 20,
      totalTax: 4,
    }),
  };

  const component = (props = {}) => <Payment {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
