import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';

import messages from './payment.messages';
import { StaticHeroImage } from '../../../components/staticHeroImage/staticHeroImage.component';
import { ChatButton } from '../../../components/chatButton/chatButton.component';
import { GradientBox } from '../../../components/gradientBox/gradientBox.component';
import { EmailForm } from './components/emailForm/emailForm.component';

import { Grid, Components, Forms } from '../../../theme';
import {
  chatStyles,
  Title,
  rowStyles,
  columnStyles,
  Description,
  InputContainer,
  Prefix,
  inputStyles,
} from './payment.styles';


export class PaymentComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    articleImage: PropTypes.string,
    blogRequest: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    updateEmail: PropTypes.func.isRequired,
    updateAddress: PropTypes.func.isRequired,
    updateShippingLines: PropTypes.func.isRequired,
    checkoutData: PropTypes.object.isRequired,
    clearCart: PropTypes.func.isRequired,
  };

  state = {
    email: this.props.user.get('email'),
    number: '',
  };

  componentDidMount() {
    this.props.blogRequest('payment');

    const shippingLine = this.props.checkoutData.getIn(['availableShippingRates', 'shippingRates', 0, 'handle']);

    if (shippingLine) {
      this.props.updateShippingLines(this.props.checkoutData.get('id'), shippingLine);
    }
  }

  onSubmitEmail = (data) => {
    const email = data.get('email');
    this.setState({ email });
    this.props.updateEmail(this.props.checkoutData.get('id'), email);
  };

  changeNumber = (e) => this.setState({ number: e.target.value });

  handleNumberChanged = () => {
    const newData = this.props.data.set('phone', this.state.number);

    this.props.updateAddress(newData, this.props.checkoutData.get('id'));
  };

  completeCheckout = () => {
    this.props.clearCart();
    window.location.replace(this.props.checkoutData.get('webUrl'));
  };

  render = () => (
    <div>
      <StaticHeroImage
        heroImage={this.props.articleImage}
        title={this.props.intl.formatMessage(messages.heroTitle)}
        subtitle={this.props.intl.formatMessage(messages.heroSubtitle)}
        shouldShowStatus
        showBackIcon
        status="2/2"
      />

      <Grid.Section>
        <Grid.Row extraStyles={rowStyles}>
          <Grid.Column offset={3} columns={6} mobileOffset={0} mobileColumns={6}>
            <Title><FormattedMessage {...messages.contact} /></Title>
            <Description>
              <EmailForm
                onSubmit={this.onSubmitEmail}
                email={this.props.user.get('email')}
              />
            </Description>
            <Description>
              <Forms.Label htmlFor="phone">
                <FormattedMessage {...messages.phone} />
              </Forms.Label>
              <InputContainer>
                <Prefix htmlFor="phone">
                  <FormattedMessage {...messages.prefix} />
                </Prefix>
                <Forms.Input
                  name="phone"
                  id="phone"
                  type="number"
                  autoComplete="off"
                  onChange={this.changeNumber}
                  onBlur={this.handleNumberChanged}
                  extraStyles={inputStyles}
                />
              </InputContainer>
            </Description>
          </Grid.Column>
          <Grid.Column offset={1} columns={6} mobileOffset={0} mobileColumns={6} extraStyles={columnStyles}>
            <GradientBox
              subtotal={
                parseFloat(
                  this.props.checkoutData.get('totalPrice') - this.props.checkoutData.get('totalTax')
                )
              }
              vat={parseFloat(this.props.checkoutData.get('totalTax'))}
              delivery={
                parseFloat(this.props.checkoutData.getIn(['availableShippingRates', 'shippingRates', 0, 'price']))
              }
              address={this.props.data}
              userEmail={this.state.email}
              userNumber={this.state.number}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>

      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={1} columns={14} extraStyles={chatStyles}>
            <ChatButton />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>

      <Components.SubmitButton onClick={this.completeCheckout}>
        {this.props.intl.formatMessage(messages.next)}
      </Components.SubmitButton>
    </div>
  );
}

export const Payment = injectIntl(PaymentComponent);
