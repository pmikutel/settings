import { defineMessages } from 'react-intl';

export default defineMessages({
  edit: {
    id: 'payment.edit',
    defaultMessage: 'Edit email',
  },
  save: {
    id: 'payment.save',
    defaultMessage: 'Save email',
  },
  email: {
    id: 'register.email',
    defaultMessage: 'Email*',
  },
});
