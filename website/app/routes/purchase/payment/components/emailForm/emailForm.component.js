import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import validate from 'validate.js';
import { ifElse, prop } from 'ramda';
import { Field, reduxForm } from 'redux-form/immutable';

import messages from './emailForm.messages';
import { Components, Forms } from '../../../../../theme/index';
import { inputStyles } from './emailForm.styles';


export class EmailFormComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    change: PropTypes.func.isRequired,
    email: PropTypes.string.isRequired,
  };

  state = {
    toggleEdit: false,
  };

  componentDidMount() {
    this.props.change('email', this.props.email);
  }

  getButton = () => ifElse(
    prop('toggleEdit'),
    () =>
      <Components.Button
        type="button"
        onClick={this.toggleEnableEdit}
        disabled={this.props.invalid}
        noActive
      >
        <FormattedMessage {...messages.save} />
      </Components.Button>,
    () =>
      <Components.Button
        type="submit"
        onClick={this.toggleEnableEdit}
        noActive
      >
        <FormattedMessage {...messages.edit} />
      </Components.Button>,
  )(this.state);


  toggleEnableEdit = () => this.setState(({ toggleEdit }) => ({
    toggleEdit: !toggleEdit,
  }));

  renderEmail = ({ input, disabled, placeholder, meta: { active, invalid } }) => (
    <Forms.Input
      {...input}
      disabled={disabled}
      borderDisabled={disabled}
      autoComplete="off"
      placeholder={placeholder}
      extraStyles={inputStyles}
      errorStyle={!active && invalid}
    />
  );

  render = () => (
    <form onSubmit={this.props.handleSubmit} noValidate>
      <Field
        name="email"
        type="email"
        disabled={!this.state.toggleEdit}
        component={this.renderEmail}
        placeholder={this.props.intl.formatMessage(messages.email)}
      />
      {this.getButton()}
    </form>
  );
}

export const EmailForm = injectIntl(reduxForm({
  form: 'emailEditForm',
  validate: (values) => validate(values.toJS(), {
    email: {
      email: true,
      presence: {
        allowEmpty: false,
      },
    },
  }),
})(EmailFormComponent));
