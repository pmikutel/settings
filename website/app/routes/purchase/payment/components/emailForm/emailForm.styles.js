import { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';

export const inputStyles = css`
  margin-bottom: 20px;
  font-size: 22px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding:  ${ifElse(propEq('disabled', true), always(0), always('0 20px'))};
  transition: padding 0.2s ease-in-out, border 0.2s ease-in-out;
`;
