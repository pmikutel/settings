import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { EmailFormComponent as EmailForm } from '../emailForm.component';
import { intlMock } from '../../../../../../utils/testing';


describe('<EmailForm />', () => {
  const defaultProps = {
    intl: intlMock(),
    handleSubmit: identity,
    invalid: false,
    change: identity,
    email: '',
  };

  const component = (props = {}) => <EmailForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
