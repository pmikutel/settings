import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Payment } from './payment.component';
import { selectImage } from '../../../modules/blog/blog.selectors';
import { BlogActions } from '../../../modules/blog/blog.redux';
import { selectData } from '../../../modules/checkoutDetails/checkoutDetails.selectors';
import { selectProfileData } from '../../../modules/user/user.selectors';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { selectCheckoutData } from '../../../modules/checkout/checkout.selectors';
import { CartActions } from '../../../modules/cart/cart.redux';

const mapStateToProps = createStructuredSelector({
  data: selectData,
  user: selectProfileData,
  articleImage: selectImage,
  checkoutData: selectCheckoutData,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  blogRequest: BlogActions.request,
  updateEmail: CheckoutActions.customerEmailUpdateRequest,
  updateShippingLines: CheckoutActions.customerShippingLineUpdateRequest,
  updateAddress: CheckoutActions.customerAddressUpdateRequest,
  clearCart: CartActions.clearCart,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Payment);
