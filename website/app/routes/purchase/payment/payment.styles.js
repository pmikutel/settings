import styled, { css } from 'styled-components';
import { Media, Variables } from '../../../theme';


export const chatStyles = css`
  margin: 63px 0 92px;
`;

const rowDesktoptyles = css`
  flex-direction: row;
`;

export const rowStyles = css`
  flex-direction: column-reverse;
  position: relative;
  z-index: 1;

  ${Media.desktop(rowDesktoptyles)}
`;

export const columnStyles = css`
  margin-right: 61px;
`;

const titleDesktopWideStyles = css`
  font-size: 40px;
  letter-spacing: 24px;
`;

const titleDesktopStyles = css`
  letter-spacing: 16px;
  font-size: 30px;
  line-height: 48px;
  padding: 85px 0 52px;
`;

export const Title = styled.h2`
  letter-spacing: 13px;
  padding: 150px 0 22px;

  ${Media.desktop(titleDesktopStyles)};
  ${Media.desktopWide(titleDesktopWideStyles)};
`;

export const descriptionDesktopStyles = css`
  margin-left: 19%;
  max-width: 276px;
  margin-bottom: 40px;
`;

export const Description = styled.div`
  color: ${Variables.colorWarmGrey};
  display: flex;
  flex-direction: column;

  ${Media.desktop(descriptionDesktopStyles)};
`;

export const InputContainer = styled.div`
  position: relative;
`;

export const Prefix = styled.div`
  position: absolute;
  left: 20px;
  top: 50%;
  transform: translateY(-50%);
`;

export const inputStyles = css`
  padding-left: 65px;
`;
