import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { ThankYou } from './thankYou.component';
import { selectCompletedCartItems } from '../../../modules/cart/cart.selectors';

const mapStateToProps = createStructuredSelector({
  cartItems: selectCompletedCartItems,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(ThankYou));
