import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'thankYou.title',
    defaultMessage: 'THANK YOU',
  },
  description: {
    id: 'thankYou.description',
    defaultMessage: 'Your payment was successful.',
  },
  description2: {
    id: 'thankYou.description2',
    defaultMessage: `Thank you for your order. We’ve sent an email confirmation. You can now track your order status
     from the timeline below.`,
  },
  status: {
    id: 'thankYou.status',
    defaultMessage: 'PROJECT STATUS',
  },
  order: {
    id: 'thankYou.order',
    defaultMessage: 'ORDER',
  },
  fabrication: {
    id: 'thankYou.fabrication',
    defaultMessage: 'PRODUCTION',
  },
  delivery: {
    id: 'thankYou.delivery',
    defaultMessage: 'DELIVERY FROM',
  },
  installation: {
    id: 'thankYou.installation',
    defaultMessage: 'INSTALLATION',
  },
  download: {
    id: 'thankYou.download',
    defaultMessage: 'Download Receipt',
  },
  question: {
    id: 'thankYou.question',
    defaultMessage: 'Need to change delivery dates or have a question?',
  },
  chat: {
    id: 'thankYou.chat',
    defaultMessage: 'Chat With Us',
  },
  call: {
    id: 'thankYou.call',
    defaultMessage: 'Call us',
  },
  today: {
    id: 'thankYou.today',
    defaultMessage: 'TODAY',
  },
  confirm: {
    id: 'thankYou.confirm',
    defaultMessage: 'WHITE BEAR TEAM TO CONFIRM DATE',
  },
  number: {
    id: 'thankYou.number',
    defaultMessage: '+442012346578',
  },
});
