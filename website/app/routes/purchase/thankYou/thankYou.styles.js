import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Media, Variables, Components } from '../../../theme';

export const DescriptionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 39px 0 79px;
`;

export const Description = styled.div`
  line-height: 46px;
  max-width: 658px;
  font-size: 22px;
  color: ${Variables.colorWarmGrey};
`;

const titleDesktopStyles = css`
  letter-spacing: 24px;
`;

export const Title = styled.h2`
  letter-spacing: 12px;
  max-width: 460px;
  padding: 20px 0 42px;
  color: ${Variables.colorPureBlack};

  ${Media.desktop(titleDesktopStyles)};
`;

const lineDesktopStyles = css`
  margin-bottom: 139px;
`;

export const Line = styled.div`
  background-color: ${Variables.colorBorderGray};
  height: 1px;
  width: 100%;
  margin-bottom: 50px;

  ${Media.desktop(lineDesktopStyles)};
`;

const subTitleDesktopStyles = css`
  padding: 0 0 24px 0;
  letter-spacing: 12px;
  font-size: 22px;
`;

export const SubTitle = styled.h3`
  color: ${Variables.colorPureBlack};
  letter-spacing: 7px;
  font-size: 20px;
  padding: ${ifElse(propEq('noPaddingTop', true), always('0 0 3px'), always('120px 0 3px'))};

  ${Media.desktop(subTitleDesktopStyles)}
`;

const dateDesktopStyles = css`
  padding-bottom: 120px;
  letter-spacing: 12px;
  font-size: 18px;
`;

export const Date = styled.h3`
  color: ${Variables.colorWarmGrey};
  padding-bottom: 0;
  letter-spacing: 10px;
  font-size: 14px;
  line-height: 1.6;

  ${Media.desktop(dateDesktopStyles)};
`;

const buttonsContainerDesktopStyles = css`
  flex-direction: row;
  padding-top: 33px;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-top: 50px;

  ${Media.desktop(buttonsContainerDesktopStyles)};
`;

const buttonStylesDesktop = css`
  max-width: 186px;
`;

const buttonStyles = css`
  max-width: 250px;
  width: 100%;

  ${Media.desktop(buttonStylesDesktop)}
`;

export const InlineButton = styled.div`
  ${buttonStyles};
`;

export const InlineLink = Components.SimpleLink.extend`
  ${buttonStyles};
`;

export const questionColumnStyles = css`
  padding: 30px 60px 0 0;
`;

const timeLineColumnDesktopStyles = css`
  padding: 0 0 0 58px;
`;

export const timeLineColumnStyles = css`
  padding: 0 0 121px 42px;
  position: relative;

  ${Media.desktop(timeLineColumnDesktopStyles)};
`;

const rowDesktopStyles = css`
  flex-direction: row;
`;

export const rowStyles = css`
  flex-direction: column;

  ${Media.desktop(rowDesktopStyles)}
`;


const titleColumnDesktopStyles = css`
  padding-top: 0;
`;

export const titleColumnStyles = css`
  padding-top: 161px;

  ${Media.desktop(titleColumnDesktopStyles)}
`;
