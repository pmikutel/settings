import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';

import { UnderlineTitle } from '../../../components/underlineTitle/underlineTitle.component';
import { getLastDelivery } from '../../../utils/deliveryData';

import messages from './thankYou.messages';
import { ChatButton } from '../../../components/chatButton/chatButton.component';

import { Grid, Components } from '../../../theme';
import {
  Description,
  DescriptionWrapper,
  Title,
  SubTitle,
  Date,
  Line,
  ButtonsContainer,
  InlineButton,
  InlineLink,
  questionColumnStyles,
  timeLineColumnStyles,
  rowStyles,
  titleColumnStyles,
} from './thankYou.styles';

import { getTelLink } from '../../../utils/telephone';

export class ThankYouComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    cartItems: PropTypes.object.isRequired,
  };

  get phoneNumber() {
    return `tel:${this.props.intl.formatMessage(messages.number)}`;
  }

  render = () => (
    <div>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={1} columns={15} extraStyles={titleColumnStyles}>
            <UnderlineTitle
              title={this.props.intl.formatMessage(messages.title)}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column offset={3} columns={12} mobileOffset={1} mobileColumns={5}>
            <DescriptionWrapper>
              <Description>
                <FormattedMessage {...messages.description} />
              </Description>
              <Description>
                <FormattedMessage {...messages.description2} />
              </Description>
            </DescriptionWrapper>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column offset={3} columns={12} mobileOffset={1} mobileColumns={5}>
            <Title>
              <FormattedMessage {...messages.status} />
            </Title>
            <Line />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column offset={3} columns={12} extraStyles={timeLineColumnStyles}>
            <Components.VerticalLineContainer step={1}>
              <Grid.Row extraStyles={rowStyles}>
                <Grid.Column columns={8}>
                  <SubTitle noPaddingTop><FormattedMessage {...messages.order} /></SubTitle>
                  <Date><FormattedMessage {...messages.today} /></Date>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column columns={8}>
                  <SubTitle><FormattedMessage {...messages.fabrication} /></SubTitle>
                  <Date><FormattedMessage {...messages.confirm} /></Date>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row extraStyles={rowStyles}>
                <Grid.Column columns={8}>
                  <SubTitle><FormattedMessage {...messages.delivery} /></SubTitle>
                  <Date>{getLastDelivery(this.props.cartItems.toJS())}</Date>
                </Grid.Column>
                <Grid.Column offset={0} columns={7} extraStyles={questionColumnStyles}>
                  <Description>
                    <FormattedMessage {...messages.question} />
                  </Description>
                  <ButtonsContainer>
                    <InlineButton>
                      <ChatButton fullWidth />
                    </InlineButton>
                    <InlineLink href={getTelLink}>
                      <FormattedMessage {...messages.call} />
                    </InlineLink>
                  </ButtonsContainer>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column columns={8}>
                  <SubTitle><FormattedMessage {...messages.installation} /></SubTitle>
                  <Date><FormattedMessage {...messages.confirm} /></Date>
                </Grid.Column>
              </Grid.Row>
            </Components.VerticalLineContainer>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    </div>
  );
}

export const ThankYou = injectIntl(ThankYouComponent);
