import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';

import { ThankYouComponent as ThankYou } from '../thankYou.component';
import { intlMock } from '../../../../utils/testing';

describe('<ThankYou />', () => {
  const defaultProps = {
    intl: intlMock(),
    cartItems: fromJS({
      123: {
        delivery: '5+ weeks',
      },
    }),
  };

  const component = (props = {}) => <ThankYou {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
