import React, { PureComponent } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Helmet from 'react-helmet';

import PurchaseCosts from './costs';
import PurchaseList from './cart';
import PurchasePayment from './payment';
import PurchaseThankYou from './thankYou';
import StartProject from './startProject';

import Header from '../../components/header';
import { Footer } from '../../components/footer/footer.component';

export class Purchase extends PureComponent {
  static propTypes = {

  };

  renderRoutes = () => (
    <Switch>
      <Route path="/purchase/cart" component={PurchaseList} />
      <Route path="/purchase/costs" component={PurchaseCosts} />
      <Route path="/purchase/payment" component={PurchasePayment} />
      <Route path="/purchase/thanks" component={PurchaseThankYou} />
      <Route path="/purchase/start" component={StartProject} />

      <Redirect from="/purchase" to="/purchase/cart" />
    </Switch>
  );

  render() {
    return (
      <div>
        <Helmet
          title="Purchase"
        />

        <Header />

        { this.renderRoutes() }

        <Footer />
      </div>
    );
  }
}
