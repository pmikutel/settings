import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';
import { intlMock } from '../../../../utils/testing';

import { StartProject } from '../startProject.component';


describe('<StartProject />', () => {
  const defaultProps = {
    cartItems: fromJS([]),
    createCheckout: identity,
    intl: intlMock(),
  };

  const component = (props = {}) => <StartProject {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
