import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { UnderlineTitle } from '../../../components/underlineTitle/underlineTitle.component';

import messages from './startProject.messages';

import { buttonRowStyles, buttonStyles, columnTitleStyles } from './startProject.styles';
import { Grid, Components } from '../../../theme';

export class StartProject extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    cartItems: PropTypes.object.isRequired,
    createCheckout: PropTypes.func.isRequired,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  onCheckoutHandler = () => this.props.createCheckout({ lineItems: this.items });

  get items() {
    return this.props.cartItems.toArray().map(item => {
      return {
        variantId: item.get('variantId'),
        quantity: item.get('quantity'),
      };
    });
  }

  render = () => (
    <div>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column offset={1} columns={15} extraStyles={columnTitleStyles}>
            <UnderlineTitle
              title={this.props.intl.formatMessage(messages.title)}
              description={this.props.intl.formatMessage(messages.description)}
            />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row extraStyles={buttonRowStyles}>
          <Components.Button
            extraStyles={buttonStyles}
            onClick={() => (this.context.router.history.push('/project/step1'))}
          >
            <FormattedMessage {...messages.start} />
          </Components.Button>

          <Components.Button
            extraStyles={buttonStyles}
            onClick={this.onCheckoutHandler}
          >
            <FormattedMessage {...messages.checkout} />
          </Components.Button>
        </Grid.Row>
      </Grid.Section>
    </div>
  );
}
