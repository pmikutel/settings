import { css } from 'styled-components';
import Media from '../../../theme/media';

export const buttonRowDesktopStyles = css`
  flex-direction: row;
  padding-bottom: 224px;
`;

export const buttonRowStyles = css`
  justify-content: center;
  align-items: center;
  padding-bottom: 100px;
  flex-direction: column;
  
  ${Media.desktop(buttonRowDesktopStyles)};
`;

export const buttonStyles = css`
  padding: 20px;
  max-width: 330px;
`;

export const columnTitleStyles = css`
  margin-top: 96px;
`;
