
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'startProject.title',
    defaultMessage: 'START A PROJECT',
  },
  description: {
    id: 'startProject.description',
    defaultMessage: `By telling a bit more about your space and you style we can recommend products to help you build 
    your space.`,
  },
  start: {
    id: 'startProject.start',
    defaultMessage: 'Yes, I want to start a project',
  },
  checkout: {
    id: 'startProject.checkout',
    defaultMessage: 'Not right now, proceed to checkout',
  },
});
