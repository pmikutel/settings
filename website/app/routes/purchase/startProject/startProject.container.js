import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { StartProject } from './startProject.component';
import { CheckoutActions } from '../../../modules/checkout/checkout.redux';
import { selectCartItems } from '../../../modules/cart/cart.selectors';


const mapStateToProps = createStructuredSelector({
  cartItems: selectCartItems,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  createCheckout: CheckoutActions.createRequest,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(StartProject));
