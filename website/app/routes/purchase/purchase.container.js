import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Purchase } from './purchase.component';

const mapStateToProps = createStructuredSelector({});

export const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Purchase));
