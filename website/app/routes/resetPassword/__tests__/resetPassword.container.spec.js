import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../resetPassword.container';
import { ResetPasswordActions } from '../../../modules/resetPassword/resetPassword.redux';


describe('ResetPassword: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call ProductTypesActions.customNewsletterRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).request();

      expect(dispatch.firstCall.args[0]).to.deep.equal(ResetPasswordActions.resetRequest());
    });
  });
});
