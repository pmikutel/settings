import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { ResetPasswordComponent as ResetPassword } from '../resetPassword.component';
import { intlMock } from '../../../utils/testing';

describe('<ResetPassword />', () => {
  const defaultProps = {
    request: identity,
    location: {},
    resetError: false,
    intl: intlMock(),
  };

  const component = (props = {}) => <ResetPassword {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
