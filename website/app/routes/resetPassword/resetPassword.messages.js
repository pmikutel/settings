import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'resetPassword.title',
    defaultMessage: 'Reset password',
  },
  description: {
    id: 'resetPassword.description',
    defaultMessage: 'Please enter new password to change exist.',
  },
});
