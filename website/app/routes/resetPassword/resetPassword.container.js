import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { ResetPassword } from './resetPassword.component';
import { ResetPasswordActions } from '../../modules/resetPassword/resetPassword.redux';
import { selectResetError } from '../../modules/resetPassword/resetPassword.selectors';

const mapStateToProps = createStructuredSelector({
  resetError: selectResetError,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  request: ResetPasswordActions.resetRequest,
}, dispatch);

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(ResetPassword));
