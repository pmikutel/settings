import { css } from 'styled-components';
import { Media } from '../../theme';

export const rowDesktopStyles = css`
  padding-top: 0;
`;

export const rowStyles = css`
  margin-top: 150px;
    
  ${Media.desktop(rowDesktopStyles)}
`;
