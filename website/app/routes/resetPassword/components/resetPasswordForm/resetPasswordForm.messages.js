
import { defineMessages } from 'react-intl';

export default defineMessages({
  password: {
    id: 'resetPassword.password',
    defaultMessage: 'Password*',
  },
  passwordError: {
    id: 'resetPassword.password',
    defaultMessage: 'Your password must be at least 8 characters long and contain at least one number',
  },
  registerButton: {
    id: 'resetPassword.registerButton',
    defaultMessage: 'Reset',
  },
});
