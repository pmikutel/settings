import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { ResetPasswordFormComponent as ResetPasswordForm } from '../resetPasswordForm.component';
import { intlMock } from '../../../../../utils/testing';

describe('<ResetPasswordForm />', () => {
  const defaultProps = {
    handleSubmit: identity,
    invalid: false,
    resetError: false,
    intl: intlMock(),
  };

  const component = (props = {}) => <ResetPasswordForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
