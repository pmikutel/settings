import { css } from 'styled-components';
import { Media } from '../../../../theme';


export const inputContainerDesktop = css`
  margin: 0 auto 60px;
`;


export const inputContainer = css`
  max-width: 400px;
  position: relative;
  margin-bottom: 80px;
  flex-direction: column;
  
  ${Media.desktop(inputContainerDesktop)}
`;


export const wrapperDesktopStyles = css`
  margin-bottom: 127px;
`;

export const wrapper = css`
  margin-bottom: 80px;
    
  ${Media.desktop(wrapperDesktopStyles)}
`;
