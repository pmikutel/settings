import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import validate from 'validate.js';

import { TryAgain } from '../../../../components/tryAgain/tryAgain.component';
import messages from './resetPasswordForm.messages';

import {
  wrapper,
  inputContainer,
} from './resetPasswordForm.styles';

import { Forms, Grid, Components } from '../../../../theme';

export class ResetPasswordFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    resetError: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
  };

  renderField = ({ input, error, placeholder, type, errorText, meta: { touched, invalid, dirty } }) => (
    <Grid.Row extraStyles={inputContainer}>
      <TryAgain isActive={error} />
      <Forms.Input errorStyle={(dirty || touched) && invalid} {...input} placeholder={placeholder} type={type} />
      {(dirty || touched) && invalid && <Forms.Error>{errorText}</Forms.Error>}
    </Grid.Row>
  );

  render = () => {
    const { intl: { formatMessage }, handleSubmit, invalid, resetError } = this.props;

    return (
      <form onSubmit={handleSubmit} noValidate>
        <Grid.Section extraStyles={wrapper}>
          <Field
            name="password"
            type="password"
            component={this.renderField}
            errorText={formatMessage(messages.passwordError)}
            placeholder={formatMessage(messages.password)}
            error={resetError}
          />
        </Grid.Section>

        <Components.SubmitButton disabled={invalid}>
          <FormattedMessage {...messages.registerButton} />
        </Components.SubmitButton>
      </form>
    );
  };
}

export const ResetPasswordForm = injectIntl(reduxForm({
  form: 'resetPasswordForm',
  validate: (values) => validate(values.toJS(), {
    password: {
      format: {
        pattern: '.*[0-9].*',
      },
      length: {
        minimum: 8,
      },
      presence: {
        allowEmpty: false,
      },
    },
  }),
})(ResetPasswordFormComponent));
