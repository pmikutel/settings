import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { parse } from 'query-string';

import Header from '../../components/header';
import { UnderlineTitle } from '../../components/underlineTitle/underlineTitle.component';
import { Footer } from '../../components/footer/footer.component';
import { ResetPasswordForm } from './components/resetPasswordForm/resetPasswordForm.component';
import { encode } from '../../utils/shopifyUrls';
import messages from './resetPassword.messages';

import { Grid } from '../../theme';
import { rowStyles } from './resetPassword.styles';

export class ResetPasswordComponent extends PureComponent {
  static propTypes = {
    request: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
    resetError: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
  };

  handleSubmit = (data) => {
    const query = parse(this.props.location.search);
    this.props.request(data, encode(query.id, 'customer'), query.token);
  };

  render = () => (
    <div>
      <Helmet title="Reset" />
      <Header />
      <Grid.Row extraStyles={rowStyles}>
        <Grid.Column offset={1} columns={15}>
          <UnderlineTitle
            title={this.props.intl.formatMessage(messages.title)}
            description={this.props.intl.formatMessage(messages.description)}
          />
        </Grid.Column>
      </Grid.Row>
      <ResetPasswordForm
        onSubmit={this.handleSubmit}
        resetError={this.props.resetError}
      />
      <Footer />
    </div>
  );
}

export const ResetPassword = injectIntl(ResetPasswordComponent);
