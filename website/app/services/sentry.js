import env from 'env-config';

if (env.sentryUrl) {
  Raven.config(env.sentryUrl).install();
}
