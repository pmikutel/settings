import env from 'env-config';
import axios from 'axios';

export default axios.create({
  baseURL: env.backendUrl,
});
