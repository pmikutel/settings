import { schema, normalize } from 'normalizr';
import { mapIdForProducts } from '../../utils/shopifyUrls';


export const imageScheme = new schema.Entity('image');

export const productsImageScheme = new schema.Entity('products', {
  images: [imageScheme],
});

export const productsScheme = new schema.Array(productsImageScheme);
export const normalizeProducts = (data) => normalize(mapIdForProducts(data), productsScheme);

