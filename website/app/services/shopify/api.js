import ShopifyBuy from 'shopify-buy';
import env from 'env-config';
import axios from 'axios/index';
import { path, isNil, complement, pipe } from 'ramda';

export const shopifyClient = ShopifyBuy.buildClient({
  domain: env.shopifyDomain,
  storefrontAccessToken: env.shopifyStoreFrontApiKey,
});

export const getShopifyItems = ([, ...items]) => [...items];

export const shopifyApi = axios.create({
  baseURL: `https://${env.shopifyDomain}/api/graphql`,
  headers: {
    'Content-Type': 'application/graphql',
    'X-Shopify-Storefront-Access-Token': env.shopifyStoreFrontApiKey,
  },
});

export const hasErrors = pipe(
  path(['data', 'errors']),
  complement(isNil),
);
