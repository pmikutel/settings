import buildConfig from '../utils/buildConfig';

export default buildConfig({
  name: 'development',
  backendUrl: 'https://test.admin.whitebear.com/api',
  shopifyDomain: 'white-bear-store.myshopify.com',
  shopifyStoreFrontApiKey: 'dcf5f86cbcdca1de776377ad05f93cd8',
});
