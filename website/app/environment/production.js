import buildConfig from '../utils/buildConfig';

export default buildConfig({
  name: 'production',
  backendUrl: 'https://test.admin.whitebear.com/api',
  shopifyDomain: 'white-bear-store.myshopify.com',
  shopifyStoreFrontApiKey: 'dcf5f86cbcdca1de776377ad05f93cd8',
  sentryUrl: 'https://cce15aff5ca64ece8ac73ba161409ab1@sentry.io/1270868',
});
