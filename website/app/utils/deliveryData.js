import { pipe, indexOf, map, values, apply } from 'ramda';

const deliveryMapped = ['1-2 weeks', '3-4 weeks', '5+ weeks'];

export const getLastDelivery = (items) => deliveryMapped[
  pipe(
    map(({ delivery }) => { return indexOf(delivery.trim(), deliveryMapped); }),
    values(),
    apply(Math.max),
  )(items)
];
