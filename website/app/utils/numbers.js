import numeral from 'numeral';

export const priceFormat = price => Number(price).toFixed(2);
export const numeralPriceFormat = price => numeral(price).format('0,0.[00]');
