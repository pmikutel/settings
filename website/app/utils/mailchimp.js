import { propEq } from 'ramda';
export const SUCCESS_STATUS = 'success';

export const hasSuccessResult = propEq('result', SUCCESS_STATUS);
