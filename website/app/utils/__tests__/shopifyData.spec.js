import { expect } from 'chai';

import { extractVariantTitle } from '../shopifyData';

describe('ShopifyData', () => {
  describe('extractVariantTitle', () => {
    it('should return variant title without extra options', () => {
      const data = 'title/size/color';

      expect(extractVariantTitle(data)).to.equal('title');
    });
  });
});
