import { expect } from 'chai';

import { getLastDelivery } from '../deliveryData';

describe('DeliveryData', () => {
  describe('getLastDelivery', () => {
    it('should return latest delivery date', () => {
      const data = {
        '1234': {
          'delivery': '3-4 weeks',
        },
        '4211': {
          'delivery': '1-2 weeks',
        },
      };

      expect(getLastDelivery(data)).to.equal('3-4 weeks');
    });
  });
});
