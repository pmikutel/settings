import bezierEasing from 'bezier-easing';

const easing = bezierEasing(0.17, 0.67, 0.38, 1.02);

const animateItemBackground = (element, duration) => {
  new TimelineMax()
    .addLabel('start')
    .to(element, duration, { x: '0%', ease: easing }, 'start');
};

export const animateItemImage = (element, duration, delay, callback = () => {}) => {
  new TimelineMax(({ onComplete: () => callback() }))
    .addLabel('start')
    .to(element, duration, { opacity: '1', ease: easing, delay }, 'start');
};

export const animateItemDiscount = (element, duration, delay) => {
  new TimelineMax()
    .addLabel('start')
    .to(element, duration, { opacity: '1', ease: easing, delay }, 'start');
};

const animateItemTitle = (element, duration, delay, isLeft, callback) => {
  new TimelineMax(({ onComplete: () => callback() }))
    .addLabel('start')
    .to(element, duration, { opacity: 1, marginLeft: 0, ease: easing, delay }, 'start');
};

const animateBackground = (el, duration) => {
  new TimelineMax()
    .addLabel('start')
    .to(el, duration, { width: '100%', ease: easing }, 'start');
};

const animateImage = (el, duration, delay) => {
  new TimelineMax()
    .addLabel('start')
    .to(el, duration, { opacity: 1, ease: easing, delay: delay }, 'start');
};

const animateTitle = (el, duration, delay) => {
  new TimelineMax()
    .addLabel('start')
    .set(el, { y: '30px' })
    .to(el, duration, { opacity: 1, y: '0px', ease: easing, delay: delay }, 'start');
};

export const animateItemEnter = (background, image, title, discount, isLeft, callback) => {
  animateItemBackground(background, 0.8);
  animateItemImage(image, 0.8, 1, callback);
  if (title) {
    animateItemTitle(title, 0.8, 1.5, isLeft, callback);
  }
  animateItemDiscount(discount, 0.8, 2);
};

export const animateHeroImage = (background, image, subtitle, title) => {
  animateBackground(background, 0.5);
  animateImage(image, 0.6, 0.5);
  animateTitle(title, 1, 1);
  animateTitle(subtitle, 0.2, 1.5);
};

export const getProductVarianAnimation = (el, parallaxHeight) => {
  return new TimelineMax({ paused: true })
    .addLabel('start')
    .fromTo(
      el,
      1,
      { scale: 1, x: '-22%', y: 0 },
      { scale: .5, x: '0%', y: parallaxHeight, ease: Power0.easeNone },
      'start'
    );
};
