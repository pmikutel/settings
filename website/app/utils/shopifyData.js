import { map, prop, evolve, pipe, toLower, forEach, reduce, split, is } from 'ramda';

/**
 * Function extracts node prop.
 * @param {Array}
 * @returns {Array}
 */
export const extractNode = map(prop('node'));

/**
 * Function flattens variants prop by removing edges and node levels
 * @param {Array}
 * @returns {Array}
 */
export const flattenVariants = map(evolve({ variants: pipe(prop('edges'), extractNode) }));

/**
 * Function flattens images prop by removing edges and node levels.
 * @param {Array}
 * @returns {Array}
 */
export const flattenImages = map(evolve({ images: pipe(prop('edges'), extractNode) }));

/**
 * Function flattens selected options prop by transforming it from array of objects into object with option names as
 * keys.
 * @param {Array}
 * @returns {Array}
 */
export const flattenSelectedOptions = forEach((product) => {
  product.variants[0] = reduce((result, option) => {
    result[toLower(option.name)] = option.value;
    return result;
  }, product.variants[0], product.variants[0].selectedOptions);

  return product;
});

export const flattenDeepSelectedOptions = (variant) => {
  variant = reduce((result, option) => {
    result[toLower(option.name)] = option.value;
    return result;
  }, variant, variant.selectedOptions);
  const options = split(';', variant.options);

  if (options.length < 2) {
    variant.size = '';
    variant.delivery = options[0];
  } else {
    variant.size = options[0];
    variant.delivery = options[1];
  }

  return variant;
};

export const flattenDeepSelectedProductsOptions = forEach((variant) => {
  return flattenDeepSelectedOptions(variant);
});

/**
 * Function extracts size and delivery options hidden inside options field from selected options prop.
 * Extracted options are placed in the same level as other variant fields.
 * keys.
 * @param {Array}
 * @returns {Array}
 */
export const extractDeepOptions = forEach((product) => {
  if (is(String, product.variants[0].options)) {
    const options = split(';', product.variants[0].options);

    if (options.length < 2) {
      product.variants[0].size = '';
      product.variants[0].delivery = options[0];
    } else {
      product.variants[0].size = options[0];
      product.variants[0].delivery = options[1];
    }
  }
  return product;
});

/**
 * Function splits title merged by Shopify with other options
 * eg title / size / extra data and returns first element only
 * @param {String} item
 * @returns {String}
 */
export const extractVariantTitle = (item) => split('/', item)[0];
