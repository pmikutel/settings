import UAParser from 'ua-parser-js';
import semverCompare from 'semver-compare';
import identity from 'lodash/identity';

const DEFAULT_SUPPORTED_BROWSERS_CONFIG = {
  desktop: [{
    browser: 'firefox', minversion: 57,
  }, {
    browser: 'chrome', minversion: 60,
  }, {
    browser: 'edge', minversion: 15,
  }, {
    os: 'mac os', minos: '10.11.0', browser: 'safari', minversion: 10,
  }],
  tablet: [{
    os: 'ios', minos: '10', browser: 'mobile safari',
  }, {
    os: 'ios', minos: '10.0', browser: 'chrome', minversion: 60,
  }, {
    os: 'android', minos: '7.0', browser: 'chrome', minversion: 60,
  }, {
    browser: 'edge', minversion: 15,
  }],
  mobile: [{
    os: 'ios', minos: '10', browser: 'mobile safari',
  }, {
    os: 'ios', minos: '10.0', browser: 'chrome', minversion: 60,
  }, {
    os: 'android', minos: '7.0', browser: 'chrome', minversion: 60,
  }],
};

export default class BrowserDetection {
  parser = new UAParser();

  constructor(config = DEFAULT_SUPPORTED_BROWSERS_CONFIG, isInAppBrowserSupported = true) {
    this.supportedBrowsersConfig = config;
    this.isInAppBrowserSupported = isInAppBrowserSupported;
  }

  get isPrerenderingBot() {
    return (
      (this.ua.indexOf('prerender') > -1)
    );
  }

  get isInAppBrowser() {
    return (
      (this.ua.indexOf('FBAN') > -1) ||
      (this.ua.indexOf('FBAV') > -1) ||
      (this.ua.indexOf('Twitter') > -1)
    );
  }

  get device() {
    return this.parser.getDevice();
  }

  get ua() {
    return this.parser.getUA();
  }

  get browser() {
    return this.parser.getBrowser();
  }

  get os() {
    return this.parser.getOS();
  }

  get deviceType() {
    const { type = 'desktop' } = this.device;
    return type;
  }

  compareVersions(a, b) {
    if (typeof a === 'string' || a instanceof String) {
      return semverCompare(a, b) <= 0;
    }

    return a <= parseInt(b, 10);
  }

  isSupported() {
    if (this.isPrerenderingBot) {
      return true;
    }

    if (this.isInAppBrowser) {
      return this.isInAppBrowserSupported;
    }

    const { version: browserVersion } = this.browser;

    return !this.supportedBrowsersConfig[this.deviceType]
      .every(options => {
        const { os, minos, browser, minversion, versions } = options;
        const parsedVersion = isNaN(parseInt(browserVersion, 10))
          ? browserVersion.toLocaleLowerCase()
          : parseInt(browserVersion, 10);

        const checked = {
          os: os === this.os.name.toLowerCase(),
          minos: this.compareVersions(minos, this.os.version),
          browser: browser === this.browser.name.toLowerCase(),
          minversion: this.compareVersions(minversion, browserVersion),
          versions: versions ? versions.indexOf(parsedVersion) >= 0 : false,
        };

        return Object.keys(options).map(key => checked[key]).indexOf(false) !== -1;
      });
  }

  checkSupport(successCallback = identity, failureCallback = identity) {
    if (this.isSupported()) {
      successCallback();
    } else {
      failureCallback();
    }
  }
}
