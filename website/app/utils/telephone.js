const NUMBER = '+44 0330 004 0404';

export const getTelLink = `tel:${NUMBER}`;
export const getTelNumber = NUMBER;
