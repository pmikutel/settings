import { split, last, pipe, evolve, map } from 'ramda';

const URLS = {
  variant: 'gid://shopify/ProductVariant/',
  product: 'gid://shopify/Product/',
  customer: 'gid://shopify/Customer/',
};

/**
 * Function decodes human readable id from shopify id.
 * @param {string}
 * @returns {string}
 */
export const decode = pipe(
  atob,
  split('/'),
  last
);

/**
 * Function encodes human readable id to shopify id.
 * @param id {string}
 * @param type {string}
 * @returns {string}
 */
export const encode = (id, type) => btoa(`${URLS[type]}${id}`);

/**
 * Function decodes products ids.
 * @param {Object}
 * @returns {string}
 */
export const mapIdForProducts = map(evolve({ id: decode }));
