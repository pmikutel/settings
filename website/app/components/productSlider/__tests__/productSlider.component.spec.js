import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';

import { ProductSlider } from '../productSlider.component';


describe('<ProductSlider />', () => {
  const defaultProps = {
    title: '',
    items: fromJS({}),
  };

  const component = (props = {}) => <ProductSlider {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
