import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import { FormattedMessage } from 'react-intl';

import { decode } from '../../utils/shopifyUrls';
import { Discount } from '../discount/discount.component';
import { getSliderConfig } from './productSlider.helpers';
import { numeralPriceFormat } from '../../utils/numbers';
import { extractVariantTitle } from '../../utils/shopifyData';

import {
  Wrapper,
  Slide,
  Image,
  Copy,
  Title,
  Price,
  SlideTitle,
  DiscountWrapper,
} from './productSlider.styles';

import messages from '../productSlider/productSlider.messages';


export class ProductSlider extends PureComponent {
  static propTypes = {
    items: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
  };

  renderSlides = () => this.props.items.valueSeq().map((item, index) => {
    const data = item.getIn(['variants', 'edges', 0, 'node']) || item.getIn(['variants', 0]);
    const handle = item.get('handle');

    return (
      <Slide to={`/products/${handle}/${item.get('id')}/variant/${decode(data.get('id'))}`} key={index}>
        <Image src={data.getIn(['image', 'originalSrc']) || data.getIn(['image', 'src'])} />
        <Copy>
          <SlideTitle>{extractVariantTitle(data.get('title'))}</SlideTitle>
          <Price>
            <FormattedMessage
              {...messages.currency}
              values={{ price: numeralPriceFormat(data.get('price')) }}
            />
          </Price>
        </Copy>
        <DiscountWrapper>
          <Discount
            price={data.get('price')}
            compareAtPrice={data.get('compareAtPrice')}
          />
        </DiscountWrapper>
      </Slide>
    );
  });

  render = () => (
    <Wrapper size={this.props.items.size}>
      <Title>{this.props.title}</Title>
      <Slider {...getSliderConfig(this.props.items.size > 2)}>
        {this.renderSlides()}
      </Slider>
    </Wrapper>
  );
}


