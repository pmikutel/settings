import { defineMessages } from 'react-intl';

export default defineMessages({
  currency: {
    id: 'productSlider.currency',
    defaultMessage: '£{ price }',
  },
});
