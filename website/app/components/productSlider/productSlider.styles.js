import styled, { css } from 'styled-components';
import { always, ifElse, lt, prop, pipe } from 'ramda';
import * as RRD from 'react-router-dom';
import { Media, Typography, Variables } from '../../theme';

const slickCustomStyles = css`
  .slick-slider {
    position: relative;
    width: 100%;
    padding-bottom: 68px;

    .slick-slide {
      overflow: hidden;
      height: 434px;
      position: relative;
    }
    
    .slick-dots {
      display: flex !important;
      width: 100%;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      padding-top: 47px;
      
      li {
        line-height: 0.4;
        display: block;
        margin-right: 10px;
        
        &:last-child {
          margin-right: 0;
        }
      }
  
      button {
        border: 2px solid ${Variables.colorPureBlack};
        border-radius: 50%;
        display: block;
        height: 11px;
        width: 11px;
        outline: 0;
        line-height: 0;
        font-size: 0;
        color: transparent;
        padding: 5px;
        cursor: pointer;
      }
    }
  
    .slick-active {
      button {
        background-color: ${Variables.colorPureBlack};
      }
    }
  }
`;

const slickCustomDesktopStyles = css`
  .slick-list {
    overflow: ${pipe(prop('size'), ifElse(lt(2), always('visible'), always('hidden')))};
  }
  
  .slick-slider {  
    width: 78%;
    padding-bottom: 145px;
    
    .slick-dots {
        padding-top: 125px;
        width: 128%;
      }
      
    .slick-slide {
       overflow: hidden;
       height: 642px;
    }
   }
`;


export const Wrapper = styled.section`
  ${slickCustomStyles};
  ${Media.desktop(slickCustomDesktopStyles)};
`;

export const Slide = styled(RRD.Link)`
  display: block;
`;

const imageDesktopStyles = css`
  width: calc(100% - 124px);
`;

export const Image = styled.div`
  width: calc(100% - 77px);
  height: 100%;
  display: block;
  background-image: url(${prop('src')});
  background-size: cover;

  ${Media.desktop(imageDesktopStyles)};
`;

const copyDesktopStyles = css`
  right: 57px;
  width: 642px;
`;

export const Copy = styled.div`
  position: absolute;
  right: 40px;
  transform: rotate(-90deg);
  width: 434px;
  transform-origin: bottom right;
  bottom: 100%;
  display: flex;
  align-items: center;
  color: ${Variables.colorPureBlack};
  
  ${Media.desktop(copyDesktopStyles)};
`;

const titleDesktopStyles = css`
  padding: 212px 0 106px;
  max-width: 693px;
  letter-spacing: 24px;
  line-height: 48px;
`;

export const Title = styled.h2`
  padding: 99px 0 67px;
  width: calc(100% - 36px);
  letter-spacing: 13px;
  line-height: normal;
  margin: 0 auto;
  text-align: center;
  
  ${Media.desktop(titleDesktopStyles)}
`;

const slideTitleDesktopStyles = css`
  font-size: 22px;
  letter-spacing: 12px;
  
   &:after {
    width: 100px;
  }
`;

export const SlideTitle = styled.h2`
  font-size: 14px;
  letter-spacing: 8px;
  display: inline-flex;
  align-items: center;
  
  &:after {
    content: '';
    height: 1px;
    width: 69px;
    margin: 10px 20px;
    display: inline-block;
    background-color: ${Variables.colorPureBlack};
  }
  
  ${Media.desktop(slideTitleDesktopStyles)};
`;

const priceDesktopStyles = css`
  font-size: 48px;
  letter-spacing: 10px;
`;

export const Price = styled.span`
  font-size: 24px;
  letter-spacing: 5px;
  ${Typography.fontLyonRegular};
  
  ${Media.desktop(priceDesktopStyles)};
`;

const discountWrapperDesktopStyles = css`
  top: 20px;
  left: 20px;
`;

export const DiscountWrapper = styled.div`
  position: absolute;
  top: 15px;
  left: 15px;
  
  ${Media.desktop(discountWrapperDesktopStyles)};
`;
