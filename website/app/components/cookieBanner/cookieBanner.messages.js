import { defineMessages } from 'react-intl';

export default defineMessages({
  message: {
    id: 'cookieBanner.message',
    defaultMessage: `Whitebear.com use cookies. By continuing to browse the site you are agreeing to our use of cookies.
     For more details about cookies and how to manage them, see our `,
  },
  link: {
    id: 'cookieBanner.link',
    defaultMessage: 'cookie policy.',
  },
});
