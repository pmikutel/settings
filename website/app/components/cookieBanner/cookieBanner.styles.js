import * as RRD from 'react-router-dom';
import styled, { css } from 'styled-components';
import { ifElse, propEq, always } from 'ramda';
import { Variables, Typography, Media } from '../../theme';

const wrapperDesktopStyles = css`
  line-height: 36px;
  font-size: 16px;
  padding: 25px 0;
`;

export const Wrapper = styled.div`
  background-color: ${Variables.colorBlack};
  color: ${Variables.colorPureWhite};
  font-size: 12px;
  text-align: center;
  line-height: 18px;
  padding: 20px 0;
  ${Typography.fontLyonLight};
  position: fixed;
  bottom: 0;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  z-index: 999;
  transition: transform 300ms ease-in-out, opacity 0s ease-in-out 300ms;
  transform: translateY(${ifElse(propEq('hideCookieBanner', true), always('100%'), always(0))});
  opacity: ${ifElse(propEq('hideCookieBanner', true), always(0), always(1))};
  
  ${Media.desktop(wrapperDesktopStyles)}
`;

const linkDesktopStyles = css`
  margin-right: -35px;
`;

export const Link = styled(RRD.Link)`
  border-bottom: 1px solid ${Variables.colorPureWhite};
  color: ${Variables.colorPureWhite};
  
  ${Media.desktop(linkDesktopStyles)}
`;

export const closeStyles = css`
  transform: scale(0.83) translateX(17%);
  align-items: flex-end;

  &:hover {
    transform: scale(0.83) translateX(17%) rotate(45deg);
  }
`;

const contentColumnDesktopStyles = css`
  padding-left: 25px;
`;

export const contentColumnStyles = css`
  display: block;
  box-sizing: border-box;
  
  ${Media.desktop(contentColumnDesktopStyles)}
`;

const closeColumnDesktopStyles = css`
  padding-right: 6px;
`;

export const closeColumnStyles = css`
  align-items: flex-end;
  justify-content: center;
  box-sizing: border-box;
  width: 60px;
  padding-right: 14px;
  
  ${Media.desktop(closeColumnDesktopStyles)}
`;
