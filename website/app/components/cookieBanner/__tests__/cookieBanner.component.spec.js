import React from 'react';
import { shallow } from 'enzyme';

import { CookieBannerComponent as CookieBanner } from '../cookieBanner.component';

describe('<CookieBanner />', () => {
  const defaultProps = {};

  const component = (props = {}) => <CookieBanner {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly Button', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
