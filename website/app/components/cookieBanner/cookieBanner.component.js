import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import messages from './cookieBanner.messages';

import { Wrapper, Link, closeStyles, contentColumnStyles, closeColumnStyles } from './cookieBanner.styles';
import { Grid, Components } from '../../theme';

export class CookieBannerComponent extends PureComponent {
  state = {
    hideCookieBanner: true,
  };

  componentDidMount() {
    this.setState({ hideCookieBanner: localStorage.getItem('whitebear.hideCookieBanner') === 'true' });
  }

  hideCookieBanner = () => {
    this.setState({ hideCookieBanner: true });
    localStorage.setItem('whitebear.hideCookieBanner', 'true');
  };

  render = () => (
    <Wrapper hideCookieBanner={this.state.hideCookieBanner}>
      <Grid.Section>
        <Grid.Row>
          <Grid.Column columns={13} offset={1} mobileColumns={6} extraStyles={contentColumnStyles}>
            <FormattedMessage {...messages.message} />
            <Link to="/privacy">
              <FormattedMessage {...messages.link} />
            </Link>
          </Grid.Column>
          <Grid.Column columns={1} extraStyles={closeColumnStyles}>
            <Components.CloseButton extraStyles={closeStyles} onClick={this.hideCookieBanner} />
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    </Wrapper>
  );
}

export const CookieBanner = injectIntl(CookieBannerComponent);
