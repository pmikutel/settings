import React from 'react';
import { shallow } from 'enzyme';

import { Loader } from '../loader.component';

describe('<Loader />', () => {
  const defaultProps = {
    isActive: true,
  };

  const component = (props = {}) => <Loader {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
