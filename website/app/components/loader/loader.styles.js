import styled from 'styled-components';
import { always, ifElse, propEq } from 'ramda';

export const Wrapper = styled.div`
  display: ${ifElse(propEq('isActive', true), always('block'), always('none'))};
  width: 100%;
  height: 100%;
  position: relative;
  text-align: center;
  margin: 250px 0;
`;
