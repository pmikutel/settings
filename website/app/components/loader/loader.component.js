import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import loaderGifPath from '../../images/loader.gif';

import {
  Wrapper,
} from './loader.styles';

export class Loader extends PureComponent {
  static propTypes = {
    isActive: PropTypes.bool,
  };

  static defaultProps = {
    isActive: true,
  };

  render = () => (
    <Wrapper isActive={this.props.isActive}>
      <img src={loaderGifPath} />
    </Wrapper>
  );
}
