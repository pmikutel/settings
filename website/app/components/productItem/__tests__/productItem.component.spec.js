import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { identity } from 'ramda';

import { ProductItemComponent as ProductItem } from '../productItem.component';
import { intlMock } from '../../../utils/testing';

describe('<ProductItem />', () => {
  const defaultProps = {
    intl: intlMock(),
    index: 0,
    title: null,
    url: null,
    variantId: '',
    description: '',
    productType: '',
    image: '',
    price: '100',
    compareAtPrice: '120',
    size: '',
    wide: '',
    delivery: '',
    arrayLength: 5,
    id: '',
    isLeft: false,
    projectId: '',
    addCartItem: identity,
    addProjectItem: identity,
  };

  const component = (props = {}) => <ProductItem {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
