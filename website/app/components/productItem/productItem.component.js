import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  always,
  equals,
  ifElse,
  isNil,
  pipe,
  prop,
  toLower,
  toString,
  cond,
  propSatisfies,
  T,
  complement,
} from 'ramda';

import { injectIntl } from 'react-intl';
import inViewport from 'in-viewport';

import { animateItemEnter } from '../../utils/animations';
import { numeralPriceFormat } from '../../utils/numbers';

import {
  Wrapper,
  InnerWrapperStyles,
  Image,
  Title,
  TitleWrapper,
  ImageWrapper,
  ImageLinkWrapper,
  Link,
  Price,
  BoxOverlay,
  ButtonsWrapper,
  ProjectButton,
  CartButton,
  HeartIcon,
  HeartIconHover,
  BinIcon,
  CartIcon,
  CartIconHover,
  VerticalDivider,
  ProductDetailsWrapper,
  ProductDescription,
  HorizontalDivider,
  ProductSize,
  ButtonContentWrapper,
  DiscountWrapper,
  AnimBackground,
} from './productItem.styles';

import messages from './productItem.messages';
import { AnimatedText } from '../animatedText/animatedText.component';
import { Discount } from '../../components/discount/discount.component';
import { encode } from '../../utils/shopifyUrls';


export class ProductItemComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    url: PropTypes.string,
    variantId: PropTypes.string.isRequired,
    title: PropTypes.string,
    description: PropTypes.string,
    size: PropTypes.string,
    compareAtPrice: PropTypes.string,
    price: PropTypes.string,
    wide: PropTypes.string,
    delivery: PropTypes.string,
    variantTitle: PropTypes.string,
    lineItemId: PropTypes.string,
    id: PropTypes.any.isRequired,
    arrayLength: PropTypes.number.isRequired,
    isLeft: PropTypes.bool,
    addCartItem: PropTypes.func.isRequired,
    removeProjectItem: PropTypes.func,
    addProjectItem: PropTypes.func,
    projectId: PropTypes.string,
    loadedMasonry: PropTypes.bool,
    scroll: PropTypes.object,
    showVariantTitle: PropTypes.bool,
    productHandle: PropTypes.string,
  };

  static defaultProps = {
    title: null,
    url: null,
    showVariantTitle: false,
  };

  state = {
    animated: false,
    initialized: false,
    cartUpdated: false,
    projectUpdated: false,
    isProductHovered: false,
    isCartButtonHovered: false,
    isProductButtonHovered: false,
  };

  componentDidMount() {
    this.setState({ initialized: true });
  }

  componentWillReceiveProps(nextProps) {
    if (complement(equals(nextProps.scroll)(this.props.scroll))) {
      const isVisible = inViewport(this.containerRef);
      if (this.state.initialized && isVisible && this.props.loadedMasonry) {
        animateItemEnter(this.backgroundRef, this.imageRef, this.titleRef, this.discountRef, this.props.isLeft, () => {
          this.setState({ animated: true });
        });
      }
    }
  }

  onAddToCartClick = () => {
    const data = {
      id: this.props.id,
      title: this.props.title,
      image: this.props.image,
      size: this.props.size,
      delivery: this.props.delivery,
      variantTitle: this.props.variantTitle,
      quantity: 1,
      price: this.props.price,
      variantId: this.encodedVariantId,
      productHandle: this.props.productHandle,
    };

    this.props.addCartItem(data);
    this.setState({ cartUpdated: true });
  };

  saveItemToProject = () => {
    const data = {
      quantity: 1,
      variantId: this.encodedVariantId,
    };

    this.props.addProjectItem(data);
    this.setState({ projectUpdated: true });
  };

  removeItemFromProject = () => {
    this.props.removeProjectItem(this.props.lineItemId);

    this.setState({ projectUpdated: true });
  };

  get encodedVariantId() {
    return encode(this.props.variantId, 'variant');
  }

  handleProductHover = (event) => {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }

    if (!this.state.isProductHovered) {
      document.addEventListener('touchstart', this.handleClickOutside);
    }

    if (this.state.animated) {
      this.setState({
        isProductHovered: !this.state.isProductHovered,
      });
    }
  };

  handleCartButtonHover = () => {
    this.setState({
      isCartButtonHovered: !this.state.isCartButtonHovered,
    });
  };

  handleProjectButtonHover = () => {
    this.setState({
      isProjectButtonHovered: !this.state.isProjectButtonHovered,
    });
  };


  handleClickOutside = (event) => {
    if (this.itemElement && !this.itemElement.contains(event.target)) {
      this.handleProductHover();
      document.removeEventListener('touchstart', this.handleClickOutside);
    }
  };

  isEven = () => this.props.index % 2 === 0;

  imageRef = null;
  titleRef = null;
  backgroundRef = null;
  containerRef = {};
  discountRef = {};

  handleContainerRef = ref => (this.containerRef = ref);
  handleImageRef = (ref) => (this.imageRef = ref);
  handleBackgroundRef = (ref) => (this.backgroundRef = ref);
  handleTitleRef = (ref) => (this.titleRef = ref);
  handleDiscountRef = (ref) => (this.discountRef = ref);

  renderProjectButton = () => cond([
    [propSatisfies(isNil, 'projectId'), always(null)],
    [propSatisfies(isNil, 'removeProjectItem'),
      () =>
        <ProjectButton
          onClick={this.saveItemToProject}
          onMouseEnter={this.handleProjectButtonHover}
          onMouseLeave={this.handleProjectButtonHover}
        >
          <ButtonContentWrapper>
            <AnimatedText
              text={this.props.intl.formatMessage(messages.saveProject)}
              successText={this.props.intl.formatMessage(messages.savedProject)}
              waiting={this.state.projectUpdated}
              callback={() => this.setState({ projectUpdated: false })}
            />
            <HeartIcon>
              <HeartIconHover isActive={this.state.isProjectButtonHovered} />
            </HeartIcon>
          </ButtonContentWrapper>
        </ProjectButton>,
    ], [T,
      () =>
        <ProjectButton
          onClick={this.removeItemFromProject}
          onMouseEnter={this.handleProjectButtonHover}
          onMouseLeave={this.handleProjectButtonHover}
        >
          {this.props.intl.formatMessage(messages.removeProject)}
          <BinIcon />
        </ProjectButton>,
    ],
  ])(this.props);

  renderWithLink = (isLeft, isEven) => {
    const isWide = equals(toLower(toString(this.props.wide)), '"yes"');
    return (
      <div>
        <InnerWrapperStyles isLeft={isLeft} isEven={isEven}>
          <ImageLinkWrapper
            onMouseEnter={this.handleProductHover}
            onTouchEnd={this.handleProductHover}
            isWide={isWide}
            to={this.props.url}
          >
            <DiscountWrapper innerRef={this.handleDiscountRef}>
              <Discount
                price={this.props.price}
                compareAtPrice={this.props.compareAtPrice}
              />
            </DiscountWrapper>
            <AnimBackground
              isLeft={isLeft}
              innerRef={(ref) => this.handleBackgroundRef(ref)}
            />
            <Image src={this.props.image} alt={this.props.title} isWide={isWide} innerRef={this.handleImageRef} />
          </ImageLinkWrapper>

          <BoxOverlay onMouseLeave={this.handleProductHover} isActive={this.state.isProductHovered}>
            <Link to={this.props.url}>
              <ProductDetailsWrapper>
                <ProductDescription>
                  {this.props.description}
                </ProductDescription>
                <HorizontalDivider />
                <ProductSize>
                  {this.props.size}
                </ProductSize>
              </ProductDetailsWrapper>
            </Link>
            <ButtonsWrapper hideProjectButton={this.props.projectId}>
              {this.renderProjectButton()}
              <VerticalDivider hideProjectButton={this.props.projectId} />
              <CartButton
                onClick={this.onAddToCartClick}
                onMouseEnter={this.handleCartButtonHover}
                onMouseLeave={this.handleCartButtonHover}
                hideProjectButton={this.props.projectId}
              >
                <ButtonContentWrapper>
                  <AnimatedText
                    text={this.props.intl.formatMessage(messages.addCart)}
                    successText={this.props.intl.formatMessage(messages.addedCart)}
                    waiting={this.state.cartUpdated}
                    callback={() => this.setState({ cartUpdated: false })}
                  />
                  <CartIcon>
                    <CartIconHover isActive={this.state.isCartButtonHovered} />
                  </CartIcon>
                </ButtonContentWrapper>
              </CartButton>
            </ButtonsWrapper>
          </BoxOverlay>

          <TitleWrapper isLeft={isLeft} isEven={isEven} innerRef={this.handleTitleRef}>
            <Title>{this.renderTitle()}</Title>
            {this.renderPrice()}
          </TitleWrapper>
        </InnerWrapperStyles>
      </div>
    );
  };

  renderPrice = () => pipe(
    prop('price'),
    ifElse(
      isNil,
      always(null),
      () =>
        <Price>
          {this.props.intl.formatMessage(messages.currency)}
          {numeralPriceFormat(this.props.price)}
        </Price>,
    )
  )(this.props);

  renderTitle = () => ifElse(
    prop('showVariantTitle'),
    () => this.props.variantTitle,
    () => this.props.title,
  )(this.props);

  renderWithoutLink = (isLeft, isEven) => (
    <div>
      <InnerWrapperStyles isLeft={isLeft} isEven={isEven}>
        <ImageWrapper>
          <AnimBackground
            innerRef={(ref) => this.handleBackgroundRef(ref)}
          />
          <DiscountWrapper innerRef={this.handleDiscountRef}>
            <Discount
              price={this.props.price}
              compareAtPrice={this.props.compareAtPrice}
            />
          </DiscountWrapper>
          <Image src={this.props.image} alt={this.props.title} innerRef={this.handleImageRef} />
        </ImageWrapper>
        <TitleWrapper isLeft={isLeft} isEven={isEven} innerRef={this.handleTitleRef}>
          <Title>{this.renderTitle()}</Title>
          {this.renderPrice()}
        </TitleWrapper>
      </InnerWrapperStyles>
    </div>
  );

  render = () => {
    const isLeft = this.props.isLeft;
    const isEven = this.isEven();

    return (
      <Wrapper isLeft={isLeft} isEven={isEven} innerRef={this.handleContainerRef}>
        {pipe(
          prop('url'),
          ifElse(
            isNil,
            () => this.renderWithoutLink(isLeft, isEven),
            () => this.renderWithLink(isLeft, isEven),
          )
        )(this.props)}
      </Wrapper>
    );
  };
}

export const ProductItem = injectIntl(ProductItemComponent);
