import { defineMessages } from 'react-intl';

export default defineMessages({
  addCart: {
    id: 'productItem.addCart',
    defaultMessage: 'Add to Basket',
  },
  addedCart: {
    id: 'productItem.addedCart',
    defaultMessage: 'Item added',
  },
  saveProject: {
    id: 'productItem.saveProject',
    defaultMessage: 'Save to Project',
  },
  savedProject: {
    id: 'productItem.savedProject',
    defaultMessage: 'Item Saved',
  },
  removeProject: {
    id: 'productItem.removeProject',
    defaultMessage: 'Remove from Project',
  },
  currency: {
    id: 'productItem.currency',
    defaultMessage: '£',
  },
});
