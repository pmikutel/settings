import React from 'react';
import * as RRD from 'react-router-dom';
import { always, ifElse, isNil, propEq, propSatisfies } from 'ramda';
import styled, { css } from 'styled-components';
import { Media, Variables, Typography } from '../../theme';
import {
  boxShadowProductHoverButtons,
  colorBackgroundGray,
  colorBorderGray,
  colorGrayWithOpacity,
  colorPureWhite,
  colorWarmGrey,
  lyonLightFontFamily,
} from '../../theme/variables';
import Sprite from '../../utils/sprites';

const wrapperTabletDesktopStyles = css`
  text-align: ${ifElse(propEq('isLeft', true), always('right'), always('left'))};
  padding: 0 10px;
`;

export const Wrapper = styled.div`
  position: relative;
  display: block;
  color: ${Variables.colorBlack};
  line-height: 0;
  height: ${ifElse(propEq('hideItems', true), always('0'), always('auto'))};;
  text-align: ${ifElse(propEq('isEven', true), always('left'), always('right'))};

  ${Media.tablet(wrapperTabletDesktopStyles)}
`;

const imageMobileLandscapeStyles = css`
  ${ifElse(propEq('isWide', true), always('max-width: 600px'), always('max-width: 500px'))};
`;

const imageTabletStyles = css`
  height: auto;
  ${ifElse(propEq('isWide', true), always('max-width: 100%'), always('max-width: 300px'))};
  width: 100%;
  display: block;
  margin: 0 auto;
`;

const imageTabletLandscapeStyles = css`
  ${ifElse(propEq('isWide', true), always('min-width: 630px; max-width: none;'), always('max-width: 350px'))};
`;

const imageDesktopStyles = css`
  max-width: 100%;
  height: auto;
  width: auto;
  margin-left: 0;
  min-width: ${ifElse(propEq('isWide', true), always('630px'), always('0'))};
`;

export const Image = styled.img`
  height: ${ifElse(propEq('isWide', true), always('480px'), always('auto'))};
  max-width: ${ifElse(propEq('isWide', true), always('none'), always('100%'))};
  width: ${ifElse(propEq('isWide', true), always('auto'), always('100%'))};
  opacity: 0;
  display: block;
  position: relative;
  z-index: 1;
  transition: transform 0.3s;

  ${Media.mobileLandscape(imageMobileLandscapeStyles)}
  ${Media.tablet(imageTabletStyles)}
  ${Media.tabletLandscape(imageTabletLandscapeStyles)}
  ${Media.desktopWide(imageDesktopStyles)};
`;

const titleWrapperTabletStyles = css`
  font-size: 22px;
  letter-spacing: 12px;
  min-width: 600px;
  left: ${ifElse(propEq('isLeft', true), always('0'), always('93%'))};
`;

const titleWrapperDesktopWideStyles = css`
  left: ${ifElse(propEq('isLeft', true), always('0'), always('96%'))};
`;

const titleWrapperBigMobileStyles = css`
  left: ${ifElse(propEq('isEven', true), always('95%'), always('11px'))};
`;

export const TitleWrapper = styled.div`
  bottom: 0;
  color: ${Variables.colorPureBlack};
  display: flex;
  align-items: center;
  font-family: ${Variables.gothamBookFontFamily};
  font-size: 14px;
  min-width: 350px;
  letter-spacing: 8px;
  position: absolute;
  text-align: left;
  text-transform: uppercase;
  transform: translateY(100%) rotate(-90deg) translateZ(-1000px);
  transform-origin: top left;
  left: ${ifElse(propEq('isEven', true), always('93%'), always('11px'))};
  margin-left: ${ifElse(propEq('isLeft', true), always('50px'), always('-50px'))};
  opacity: 0;
  
  ${Media.bigMobile(titleWrapperBigMobileStyles)}
  ${Media.tablet(titleWrapperTabletStyles)}
  ${Media.desktopWide(titleWrapperDesktopWideStyles)}
`;

const titleDesktopWideStyles = css`
  max-width: 350px;
`;

export const Title = styled.span`
  white-space: nowrap;
  max-width: 250px;
  text-overflow: ellipsis;
  overflow: hidden;
  line-height: normal;
  
  ${Media.desktopWide(titleDesktopWideStyles)}
`;

const imageWrapperTabletStyles = css`
  max-width: none;
`;

const imageWrapperStyles = css`
  overflow: hidden;
  position: relative;
  display: block;
  ${ifElse(propEq('isWide', true), always('max-width: 520px'), always(null))};
  
  ${Media.tablet(imageWrapperTabletStyles)}
`;

export const ImageWrapper = styled.div`
  ${imageWrapperStyles};
`;

// Hack to prevent passing unnecessary props to Link
// eslint-disable-next-line
export const ImageLinkWrapper = styled(({ isWide, ...rest }) => <RRD.Link {...rest} />)`
  cursor: pointer;
  ${imageWrapperStyles};
`;

const priceDesktopWideStyles = css`
  font-size: 48px;
  letter-spacing: 10px;

   &:before {
    width: 100px;
  }
`;

const priceDesktopStyles = css`
  font-size: 36px;
  letter-spacing: 10px;

   &:before {
    width: 100px;
  }
`;

const priceTabletStyles = css`
   &:before {
    width: 30px;
  }
`;

export const Price = styled.span`
  font-size: 24px;
  ${Typography.fontLyonRegular};
  letter-spacing: 5px;
  display: inline-flex;
  align-items: center;

  &:before {
    content: '';
    height: 1px;
    width: 69px;
    margin: 0 20px;
    display: inline-block;
    background-color: ${Variables.colorPureBlack};
  }

  ${Media.tablet(priceTabletStyles)};
  ${Media.desktop(priceDesktopStyles)};
  ${Media.desktopWide(priceDesktopWideStyles)}
`;

const mobileProductDetailsWrapperStyles = css`
  font-size: 16px; 
  line-height: 30px;
  `;

const tabletProductDetailsWrapperStyles = css`
  display: block;
  font-size: 18px; 
  line-height: 40px;
`;

const desktopProductDetailsWrapperStyles = css`
  font-size: 22px; 
  line-height: 50px;
`;

export const ProductDetailsWrapper = styled.div`
  display: none;
  position: relative;
  top: 100%;
  transform: translateY(-100%);
  padding: 10px 26px 105px;
  font-family: ${lyonLightFontFamily};
  font-size: 22px; 
  line-height: 50px;
  color: ${Variables.colorPureWhite};
  text-align: left;
  
  ${Media.mobile(mobileProductDetailsWrapperStyles)};
  ${Media.tablet(tabletProductDetailsWrapperStyles)};
  ${Media.desktop(desktopProductDetailsWrapperStyles)};
`;
export const ProductDescription = styled.div`

`;

export const HorizontalDivider = styled.div`
  position: relative;
  display: block;
  height: 1px;
  width: 123px;
  background-color: ${colorBorderGray};
  z-index: 1;
  margin: 20px 0 10px 0;
`;

export const ProductSize = styled.div`

`;

const tabletLinkStyles = css`
  background: rgba(0,0,0,0.5);
`;

// Hack to prevent passing unnecessary props to Link
// eslint-disable-next-line
export const Link = styled(({ isLeft, isEven, ...rest }) => <RRD.Link {...rest} />)`
  display: block;
  position: relative;
  height: 100%;
  width: 100%;
  overflow: hidden;
  
  ${Media.tablet(tabletLinkStyles)};
`;

const desktopHeartIconStyles = css`
  display: inline-block;
  margin-left: 7%;
  margin-right: 0;
`;

const desktopCartIconStyles = css`
  display: inline-block;
  margin-left: 12%;
  margin-right: 0;
`;

export const HeartIcon = styled.i`
  display: block;

  ${Sprite.desktop('product-item-hover-heart')};
  
  ${Media.desktop(desktopHeartIconStyles)}
`;

export const HeartIconHover = styled.i`
  display: block;
  transition: opacity 0.4s;
  opacity: ${ifElse(propEq('isActive', true), always('1'), always('0'))};

  ${Sprite.desktop('product-item-hover-heart-hover')};
`;

export const CartIcon = styled.i`
  display: block;
  ${Sprite.desktop('product-item-hover-cart')};
  
  ${Media.desktop(desktopCartIconStyles)}
`;

export const CartIconHover = styled.i`
  display: block;
  transition: opacity 0.4s;
  opacity: ${ifElse(propEq('isActive', true), always('1'), always('0'))};

  ${Sprite.desktop('product-item-hover-cart-hover')};
`;

export const ButtonContentWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

const desktopBinIconStyles = css`
  display: inline-block;
  margin-left: 6%;
  margin-right: 0;
`;

export const BinIcon = styled.i`
  display: block;
  margin: 0 auto;
  min-width: 25px;
  ${Sprite.responsive('bin')};

  ${Media.desktop(desktopBinIconStyles)}
`;

export const VerticalDivider = styled.div`
  border-right: 1px solid ${colorGrayWithOpacity};
  height: 59px;
  width: 1px;
  position: absolute;
  bottom: 12px;
  left: 50%;
  z-index: 1;
  display: ${ifElse(propSatisfies(isNil, 'hideProjectButton'), always('none'), always('inline-block'))};
`;

const tabletHoverStateButtonStyles = css`
  font-size: 18px;
`;

const desktopHoverStateButtonStyles = css`
  font-size: 22px;
`;

const HoverStateButton = css`
  position: relative;
  display: flex;
  width: 50%;
  height: 79px;
  padding: 0 10px 0 23px;
  background-color: ${colorPureWhite};
  color: ${colorWarmGrey};
  font-family: ${lyonLightFontFamily};
  font-size: 16px;
  transition: all 0.3s ease-in;

  :active{
    background-color: ${colorBackgroundGray};
    transform: translateY(10%);
  }

  ${Media.tablet(tabletHoverStateButtonStyles)}
  ${Media.desktop(desktopHoverStateButtonStyles)}
`;

export const ProjectButton = styled.button`
  ${HoverStateButton};
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const CartButton = styled.button`
  ${HoverStateButton};
  width: ${ifElse(propSatisfies(isNil, 'hideProjectButton'), always('100%'), always('50%'))};
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const tabletButtonsWrapperStyles = css`
  display: flex;
`;

export const ButtonsWrapper = styled.div`
  display: none;
  width: ${ifElse(propSatisfies(isNil, 'hideProjectButton'), always('50%'), always('100%'))};
  transform: translateY(-100%);
  text-align: center;
  box-shadow: ${boxShadowProductHoverButtons};
  ${Media.tablet(tabletButtonsWrapperStyles)};
  flex-direction: row;
`;

const tabletBoxOverlayStyles = css`
  display: ${ifElse(propEq('isActive', true), always('block'), always('none'))};  
  pointer-events: ${ifElse(propEq('isActive', true), always('auto'), always('none'))};  
  width: calc(100% - 38px);
`;
const desktopBoxOverlayStyles = css`
  width: calc(100% - 54px);
`;

export const BoxOverlay = styled.div`
  display: block;
  height: 100%;
  width: calc(100% - 37px);
  position: absolute;
  top: 0;
  z-index: 2;

  ${Media.tablet(tabletBoxOverlayStyles)}
  ${Media.desktop(desktopBoxOverlayStyles)}
`;

const mobileInnerWrapperStyle = css`
  padding: ${ifElse(propEq('isEven', false), always('0 0 0 37px'), always('0 37px 0 0'))};
  max-width: 100%;
  box-sizing: border-box;
`;

const tabletInnerWrapperStyle = css`
  padding: ${ifElse(propEq('isLeft', true), always('0 0 0 39px'), always('0 39px 0 0'))};
`;

const desktopInnerWrapperStyle = css`
  padding: ${ifElse(propEq('isLeft', true), always('0 0 0 55px'), always('0 55px 0 0'))};
`;

export const InnerWrapperStyles = styled.div`
  display: inline-block;
  position: relative;

  ${Media.mobile(mobileInnerWrapperStyle)}
  ${Media.tablet(tabletInnerWrapperStyle)}
  ${Media.desktop(desktopInnerWrapperStyle)}
`;

const discountWrapperDesktopStyles = css`
  top: 20px;
  left: 20px;
`;

export const DiscountWrapper = styled.div`
  position: absolute;
  top: 15px;
  left: 15px;
  z-index: 2;
  opacity: 0;
  
  ${Media.desktop(discountWrapperDesktopStyles)};
`;

export const AnimBackground = styled.span`
  background-color: ${Variables.colorProductGrey};
  transform: translateX(${ifElse(propEq('isLeft', true), always('100%'), always('-100%'))});
  position: absolute;
  top: 0;
  right: 0;
  height: 100%;
  width: 100%;
`;

