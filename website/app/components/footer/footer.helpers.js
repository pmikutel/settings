import messages from './footer.messages';

//eslint-disable-next-line max-len
export const GOOGLE_MAPS_LINK = 'https://www.google.pl/maps/place/Rivington+News,+55+Rivington+St,+London+EC2A+3QQ,+Wielka+Brytania/@51.5261489,-0.0830548,17z/data=!3m1!4b1!4m5!3m4!1s0x48761cba97f749b3:0xfbaae3d32bb89c18!8m2!3d51.5261489!4d-0.0808661';

export const ITEMS = [
  {
    href: '/project/step1',
    message: messages.inspired,
  },
  {
    href: '/products/all',
    message: messages.products,
  },
  {
    href: '/about',
    message: messages.about,
  },
  {
    href: '/terms',
    message: messages.terms,
  },
  {
    href: '/privacy',
    message: messages.privacy,
  },
];

export const MOBILE_ITEMS = [
  {
    href: '/terms',
    message: messages.terms,
  },
  {
    href: '/privacy',
    message: messages.privacy,
  },
  {
    href: '/about',
    message: messages.about,
  },
];

export const LINKS = {
  facebook: 'https://www.facebook.com/WhiteBearProductsLtd',
  twitter: 'https://twitter.com/WhiteBear_UK',
  linkedin: 'https://www.linkedin.com/company/11496924/',
  instagram: 'https://www.instagram.com/whitebearproducts/',
};
