import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';

import { intlMock } from '../../../utils/testing';
import { FooterComponent as Footer } from '../footer.component';

describe('<Footer />', () => {
  const defaultProps = {
    intl: intlMock(),
  };

  const component = (props = {}) => <Footer {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
