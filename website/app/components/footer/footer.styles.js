import styled, { css } from 'styled-components';
import * as RRD from 'react-router-dom';
import { Media, Variables, Typography } from '../../theme';
import Sprite from '../../utils/sprites';

const ICON_MARGIN = '25px';

const greyText = css`
  ${Typography.fontLyonLight};
	font-size: 18px;
	line-height: 2.0;
	color: ${Variables.colorWarmGrey}
`;

const socialListDesktopStyles = css`
  margin-bottom: 65px;
  margin-top: 0;
`;

const footerRowDesktopStyles = css`
  flex-direction: row;
`;

export const footerRowStyles = css`
  flex-direction: column;

  ${Media.desktop(footerRowDesktopStyles)}
`;

export const SocialList = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin-top: 35px;
  margin-bottom: 45px;

  ${Media.desktop(socialListDesktopStyles)}
`;

const desktopTopRowStyles = css`
  padding-top: 120px;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const bottomRowDesktopStyles = css`
  margin-top: 80px;
`;

export const bottomRowStyles = css`
  display: flex;
  flex-direction: row;
  justify-content: center;
  background-color: ${Variables.colorPureBlack};
  color: white;
  width: 100%;
  padding: 35px 0;

  ${Media.desktop(bottomRowDesktopStyles)}
`;

const iconHover = css`
  transition: opacity 0.2s;

  &:hover {
    opacity: 0.9;
  }
`;

export const Facebook = styled.a`
  margin-right: ${ICON_MARGIN};

  ${iconHover}
  ${Sprite.responsive('facebook')}
`;

export const Twitter = styled.a`
  margin-right: ${ICON_MARGIN};

  ${iconHover}
  ${Sprite.responsive('twitter')}
`;

export const Instagram = styled.a`
  margin-right: ${ICON_MARGIN};

  ${iconHover}
  ${Sprite.responsive('instagram')}
`;

export const Linkedin = styled.a`

  ${iconHover}
  ${Sprite.responsive('linkedin')}
`;

export const ColumnTitle = styled.h3`
  ${Typography.fontGothamBook};
  font-size: 22px;
  letter-spacing: 12px;
  text-align: left;
  color: ${Variables.colorPureWhite};
  text-transform: uppercase;
  margin-top: 35px;
`;

export const Company = styled.div`
  ${greyText};
`;

const columnListDesktopStyles = css`
  margin-top: 20px;
  margin-bottom: 0;
  align-items: flex-start;
  justify-content: flex-start;
`;

export const ColumnList = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 35px;
  width: 100%;

  ${Media.desktop(columnListDesktopStyles)}
`;

const desktopLinkStyles = css`
  border:0;
  line-height: 2;
  margin-bottom: 0px;
`;

const linkStyles = css`
  border-bottom: 1px solid ${Variables.colorWarmGrey};
  ${greyText};
  line-height: 1;
  margin-bottom: 15px;
  transition: color 0.2s;

  &:hover {
    color: ${Variables.colorPureWhite};
  }

  ${Media.desktop(desktopLinkStyles)}
`;

export const Link = styled(RRD.Link)`
  ${linkStyles};
`;

export const SimpleLink = styled.a`
  ${linkStyles};
`;

const contentStyles = css`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`;

export const ColumnContent = styled.div`
  width: 100%;
  ${contentStyles}
`;

export const LeftColumnContent = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column-reverse;
  justify-content: flex-start;
  align-items: center;

  ${Media.desktop(contentStyles)}
`;

export const Address = styled.p`
  ${greyText};
  margin-bottom: 25px;
`;

export const Logo = styled.div`
  ${Sprite.responsive('logo-bright')}
`;

const mobileHiddenSectionDesktopStyles = css`display: flex;`;
export const mobileHiddenSectionStyles = css`
  display: none;
  ${Media.desktop(mobileHiddenSectionDesktopStyles)}
`;

const desktopHiddenDesktopStyles = css`display: none;`;
export const desktopHiddenStyles = css`
  display: flex;
  ${Media.desktop(desktopHiddenDesktopStyles)}
`;

export const Wrapper = styled.div`
  width: 100%;
  padding-top: 50px;
  align-items: center;
  justify-content: center;
  position: relative;
  z-index: 1;
  background-color: ${Variables.colorBlack};
  color: ${Variables.colorWarmGrey};

  ${Media.desktop(desktopTopRowStyles)}
`;



