import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, FormattedHTMLMessage, injectIntl } from 'react-intl';

import { Grid } from '../../theme';
import {
  SocialList,
  Facebook,
  Twitter,
  Instagram,
  Linkedin,
  ColumnTitle,
  bottomRowStyles,
  footerRowStyles,
  Wrapper,
  Company,
  ColumnList,
  Link,
  SimpleLink,
  ColumnContent,
  Address,
  Logo,
  LeftColumnContent,
  mobileHiddenSectionStyles,
  desktopHiddenStyles,
} from './footer.styles';

import { getTelLink, getTelNumber } from '../../utils/telephone';

import {
  ITEMS,
  GOOGLE_MAPS_LINK,
  MOBILE_ITEMS,
  LINKS,
} from './footer.helpers';

import messages from './footer.messages';

export class FooterComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
  };

  renderList = () => ITEMS.map(({ message, href }, index) => (
    <Link key={index} to={href} title={this.props.intl.formatMessage(message)}>
      {this.props.intl.formatMessage(message)}
    </Link>
  ));

  renderMobileList = () => MOBILE_ITEMS.map(({ message, href }, index) => (
    <Link key={index} to={href} title={this.props.intl.formatMessage(message)}>
      {this.props.intl.formatMessage(message)}
    </Link>
  ));

  render = () => (
    <Wrapper id="footer">
      <Grid.Section>
        <Grid.Row extraStyles={footerRowStyles}>
          <Grid.Column offset={1} columns={3}>
            <LeftColumnContent>
              <SocialList>
                <Facebook
                  href={LINKS.facebook}
                  title={this.props.intl.formatMessage(messages.facebook)}
                  target="_blank"
                />
                <Twitter
                  href={LINKS.twitter}
                  title={this.props.intl.formatMessage(messages.twitter)}
                  target="_blank"
                />
                <Instagram
                  href={LINKS.instagram}
                  title={this.props.intl.formatMessage(messages.instagram)}
                  target="_blank"
                />
                <Linkedin
                  href={LINKS.linkedin}
                  title={this.props.intl.formatMessage(messages.linkedin)}
                  target="_blank"
                />
              </SocialList>

              <Logo />
            </LeftColumnContent>
          </Grid.Column>

          <Grid.Column extraStyles={desktopHiddenStyles}>
            <ColumnContent>
              <ColumnList>
                {this.renderMobileList()}
              </ColumnList>
            </ColumnContent>
          </Grid.Column>

          <Grid.Column extraStyles={mobileHiddenSectionStyles} offset={2} columns={3}>
            <ColumnContent>
              <ColumnTitle>
                <FormattedMessage {...messages.general} />
              </ColumnTitle>

              <ColumnList>
                {this.renderList()}
              </ColumnList>
            </ColumnContent>
          </Grid.Column>

          <Grid.Column extraStyles={mobileHiddenSectionStyles} offset={1} columns={3}>
            <ColumnContent>
              <ColumnTitle>
                <FormattedMessage {...messages.studio} />
              </ColumnTitle>

              <Address>
                <SimpleLink href={getTelLink}>{getTelNumber}</SimpleLink>
                <br />
                <FormattedHTMLMessage {...messages.address} />
              </Address>

              <Link
                to={GOOGLE_MAPS_LINK}
                target="_blank"
                title={this.props.intl.formatMessage(messages.google)}
              >
                <FormattedHTMLMessage {...messages.google} />
              </Link>
            </ColumnContent>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>

      <Grid.Row extraStyles={bottomRowStyles}>
        <Company>
          <FormattedMessage {...messages.number} />
        </Company>
      </Grid.Row>
    </Wrapper>
  );
}

export const Footer = injectIntl(FooterComponent);
