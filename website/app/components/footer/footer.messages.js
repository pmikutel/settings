import { defineMessages } from 'react-intl';

export default defineMessages({
  facebook: {
    id: 'footer.facebook',
    defaultMessage: 'Facebook',
  },
  instagram: {
    id: 'footer.instagram',
    defaultMessage: 'Instagram',
  },
  twitter: {
    id: 'footer.twitter',
    defaultMessage: 'Twitter',
  },
  linkedin: {
    id: 'footer.linkedin',
    defaultMessage: 'Linkedin',
  },
  home: {
    id: 'footer.home',
    defaultMessage: 'Home',
  },
  inspired: {
    id: 'footer.inspired',
    defaultMessage: 'Get inspired',
  },
  products: {
    id: 'footer.products',
    defaultMessage: 'Our products',
  },
  about: {
    id: 'footer.about',
    defaultMessage: 'About us',
  },
  terms: {
    id: 'footer.terms',
    defaultMessage: 'Terms of use',
  },
  privacy: {
    id: 'footer.privacy',
    defaultMessage: 'Privacy policy',
  },
  general: {
    id: 'footer.general',
    defaultMessage: 'General',
  },
  studio: {
    id: 'footer.studio',
    defaultMessage: 'Studio',
  },
  number: {
    id: 'footer.number',
    defaultMessage: 'White Bear Products Ltd | 09524208',
  },
  address: {
    id: 'footer.address',
    defaultMessage: '55 Rivington Street<br />Shoreditch<br />London<br />EC2A 3QQ',

  },
  google: {
    id: 'footer.google',
    defaultMessage: 'Google maps',
  },
});
