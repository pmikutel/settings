import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { CartNotificationComponent as CartNotification } from '../cartNotification.component';
import { intlMock } from '../../../utils/testing';


describe('<CartNotification />', () => {
  const defaultProps = {
    intl: intlMock(),
    disableNotifications: identity,
    itemsNumber: 1,
    cartItems: fromJS({}),
    createCheckout: identity,
    addedItem: fromJS({}),
  };

  const component = (props = {}) => <CartNotification {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly CartNotification', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
