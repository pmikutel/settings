import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';

import messages from './cartNotification.messages';

import { Components } from '../../theme';
import {
  Wrapper,
  NotificationTitle,
  NotificationContent,
  NotificationFooter,
  linkStyles,
  Type,
  Name,
  Size,
  Price,
  IconDone,
  CloseButton,
} from './cartNotification.styles';
import { numeralPriceFormat } from '../../utils/numbers';

export class CartNotificationComponent extends PureComponent {
  static propTypes = {
    disableNotifications: PropTypes.func.isRequired,
    itemsNumber: PropTypes.number.isRequired,
    cartItems: PropTypes.object.isRequired,
    createCheckout: PropTypes.func.isRequired,
    addedItem: PropTypes.object.isRequired,
  };

  componentDidMount() {
    setTimeout(() => {
      this.props.disableNotifications();
    }, 5000);
  }

  onCheckoutHandler = () => this.props.createCheckout({ lineItems: this.items });

  get items() {
    return this.props.cartItems.toArray().map(item => {
      return {
        variantId: item.get('variantId'),
        quantity: item.get('quantity'),
      };
    });
  }

  get price() {
    const counter = this.props.addedItem.get('counter');
    const quantity = counter ? counter : 1;

    return this.props.addedItem.get('price') * quantity;
  }

  render = () => (
    <Wrapper>
      <NotificationTitle>
        <IconDone />
        <FormattedMessage {...messages.title} />
        <CloseButton onClick={this.props.disableNotifications} />
      </NotificationTitle>
      <NotificationContent>
        <Type>{this.props.addedItem.get('title')}</Type>
        <Name>{this.props.addedItem.get('variantTitle')}</Name>
        <Size>{this.props.addedItem.get('size')}</Size>
        <Price>£{numeralPriceFormat(this.price)}</Price>
      </NotificationContent>
      <NotificationFooter>
        <Components.ActionLink extraStyles={linkStyles} to="/purchase/cart">
          <FormattedMessage
            {...messages.view}
            values={{ value: this.props.itemsNumber }}
          />
        </Components.ActionLink>
        <Components.Button desktop extraStyles={linkStyles} onClick={this.onCheckoutHandler}>
          <FormattedMessage {...messages.checkout} />
        </Components.Button>
      </NotificationFooter>
    </Wrapper>
  );
}

export const CartNotification = injectIntl(CartNotificationComponent);
