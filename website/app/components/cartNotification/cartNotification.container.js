import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { CartNotification } from './cartNotification.component';
import { CartActions } from '../../modules/cart/cart.redux';
import { CheckoutActions } from '../../modules/checkout/checkout.redux';
import { selectItemsNumber, selectCartItems, selectLastAddedItem } from '../../modules/cart/cart.selectors';

const mapStateToProps = createStructuredSelector({
  itemsNumber: selectItemsNumber,
  cartItems: selectCartItems,
  addedItem: selectLastAddedItem,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  disableNotifications: CartActions.clearItemAdded,
  createCheckout: CheckoutActions.createRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CartNotification);
