import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'cartNotification.title',
    defaultMessage: 'Added to Bag',
  },
  view: {
    id: 'cartNotification.view',
    defaultMessage: 'View Bag ({value})',
  },
  checkout: {
    id: 'cartNotification.checkout',
    defaultMessage: 'Checkout',
  },
});
