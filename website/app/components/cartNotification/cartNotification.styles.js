import styled, { css } from 'styled-components';
import { Variables, Typography, Media } from '../../theme';
import Sprite from '../../utils/sprites';

export const wrapperDesktopStyles = css`
  top: 111px;
  width: 348px;
  left: auto;
  right: 59px;
`;

export const Wrapper = styled.div`
  background-color: ${Variables.colorPureWhite};
  border: 1px solid #cacaca;
  position: fixed;
  top: 80px;
  left: 17px;
  min-height: 200px;
  width: calc(100% - 36px);
  z-index: 10;
  overflow: hidden;
  ${Media.desktop(wrapperDesktopStyles)}
`;

export const NotificationTitle = styled.header`
  padding: 20px;
  text-align: center;
`;

export const NotificationContent = styled.section`
  background-color: ${Variables.colorBorderGray};
  padding: 20px;
`;

export const NotificationFooter = styled.footer`
  padding: 20px;
  display: flex;
  flex-direction: row;
`;

export const linkDesktopStyles = css`
  max-width: 50%;
`;

export const linkStyles = css`
  max-width: 44%;
  margin-right: 3%;
  flex-direction: column;

  &:nth-child(2) {
    flex-direction: unset;
    margin-right: 0;
  }

  ${Media.desktop(linkDesktopStyles)}
`;

export const Type = styled.h3`
  color: ${Variables.colorWarmGrey};
  text-transform: uppercase;
  font-size: 14px;
  letter-spacing: 7.6px;
  margin-bottom: 5px;
`;

export const Name = styled.h3`
  text-transform: uppercase;
  margin-bottom: 15px;
  font-size: 18px;
  letter-spacing: 9.8px;
`;

export const Size = styled.div`
  color: ${Variables.colorWarmGrey};
  font-size: 18px;
  ${Typography.fontLyonLight};
`;

export const Price = styled.h3`
  font-size: 18px;
  letter-spacing: 9.8px;
  word-wrap: break-word;
  margin-top: 20px;
  text-align: right;
`;

export const priceExtraStyles = css`
  flex-direction: column-reverse;
`;

export const IconDone = styled.div`
  backface-visibility: hidden;
  border: 3px solid;
  border-right: 0;
  border-top: 0;
  border-image-source: ${Variables.gradientButton};
  border-image-slice: 1;
  bottom: 0;
  left: 20px;
  position: absolute;
  top: 21px;
  transform: rotate(-45deg);
  height: 8px;
  width: 20px;
`;

export const CloseButton = styled.button`
  ${Sprite.mobile('close-gray')};
  position: absolute;
  right: 25px;
  transition: transform 0.2s;
  padding: 0;

  &:hover {
    transform: rotate(45deg)
  }
`;
