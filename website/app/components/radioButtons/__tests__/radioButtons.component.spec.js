import React from 'react';
import { shallow } from 'enzyme';
import { spy } from 'sinon';
import { identity } from 'ramda';
import { expect } from 'chai';

import { RadioButtonsComponent as RadioButtons } from '../radioButtons.component';
import { Button } from '../../../theme/components';
import { intlMock } from '../../../utils/testing';

describe('<RadioButtons />', () => {
  const defaultProps = {
    items: [
      { id: 'id1', message: { id: 'id1', defaultMessage: 'id1' } },
      { id: 'id2', message: { id: 'id2', defaultMessage: 'id2' } },
    ],
    onChange: identity,
    intl: intlMock(),
    value: '',
  };

  const component = (props = {}) => <RadioButtons {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('<Button >', () => {
    it('should call onChange with item id when user click', () => {
      const onChange = spy();
      const wrapper = render({ onChange });

      wrapper.find(Button).at(1).simulate('click');

      expect(onChange).to.have.been.calledWith(defaultProps.items[1].id);
    });
  });
});
