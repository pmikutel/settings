import styled, { css } from 'styled-components';
import { Media, Typography } from '../../theme';

const wrapperDesktopStyles = css`
  flex-direction: row;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  
  ${Media.desktop(wrapperDesktopStyles)}
`;

export const Text = styled.span`
  ${Typography.fontLyonLight};
  font-size: 18px;
`;
