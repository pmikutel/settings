import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import { Wrapper, Text } from './radioButtons.styles';
import { Components } from '../../theme';

export class RadioButtonsComponent extends PureComponent {
  static propTypes = {
    items: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    intl: PropTypes.object.isRequired,
    value: PropTypes.string.isRequired,
  };

  renderRadioButtons = () => this.props.items.map(({ id, message }) => (
    <Components.Button
      type="button"
      isActive={id === this.props.value}
      key={id}
      onClick={() => this.props.onChange(id)}
    >
      <Text>{this.props.intl.formatMessage(message)}</Text>
    </Components.Button>
  ));

  render = () => (
    <Wrapper>
      {this.renderRadioButtons()}
    </Wrapper>
  );
}

export const RadioButtons = injectIntl(RadioButtonsComponent);
