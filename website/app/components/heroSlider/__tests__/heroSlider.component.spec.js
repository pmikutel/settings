import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { fromJS } from 'immutable';

import { HeroSlider } from '../heroSlider.component';

describe('<HeroSlider />', () => {
  const defaultProps = {
    data: fromJS({}),
  };

  const component = (props = {}) => <HeroSlider {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
