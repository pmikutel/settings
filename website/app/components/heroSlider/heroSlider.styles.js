import styled, { css } from 'styled-components';
import { prop, always, ifElse, propEq } from 'ramda';
import { Media, Variables, Grid } from '../../theme';

const wrapperDesktopStyles = css`
  margin-bottom: 160px;
  min-height: 800px;
`;

export const Wrapper = styled.div`
  margin-bottom: 40px;
  width: 100%;
  position: relative;
  min-height: 560px;

  ${Media.desktop(wrapperDesktopStyles)};
`;

const slideDesktopStyles = css`
  height: auto;
  overflow: visible;
`;

export const Slide = styled.div`
  position: relative;
  height: 570px;
  overflow: hidden;

  ${Media.desktop(slideDesktopStyles)};
`;

const copyDesktopStyles = css`
  padding: 0;
  justify-content: center;
`;

export const Copy = styled.div`
  ${Grid.getColumn({
    columns: 14,
    offset: 1,
    mobileOffset: 0,
    mobileColumns: 5,
  })};

  position: absolute;
  display: flex;
  height: 100%;
  flex-direction: column;
  padding: 165px 0 0 38px;
  z-index: 1;
  left: 0;

  ${Media.desktop(copyDesktopStyles)};
`;

export const Description = styled.h3`
  font-size: 22px;
  font-family: ${Variables.lyonLightFontFamily};
`;

const titleDesktopStyles = css`
  margin-top: 30px;
`;

export const Title = styled.h1`
  font-family: ${Variables.gothamBookFontFamily};
  mix-blend-mode: difference;
  margin-top: 20px;

  ${Media.desktop(titleDesktopStyles)};
`;

export const CopyAnimWrapper = styled.span`
  opacity: ${ifElse(propEq('first', true), always(0), always(1))};
  position: relative;
`;

export const CopyAnimWrapper2 = styled.span`
  opacity: ${ifElse(propEq('first', true), always(0), always(1))};
  position: relative;
`;

export const imageWrapperDesktopStyles = css`
  ${Grid.getColumn({
    columns: 11,
    offset: 3,
  })};
  height: 800px;
`;

export const ImageWrapper = styled.div`
  ${Grid.getColumn({
    mobileOffset: 1,
    mobileColumns: 5,
  })};

  position: relative;
  height: 560px;

  ${Media.desktop(imageWrapperDesktopStyles)};
`;

export const Image = styled.div`
  background-image: url(${prop('src')});
  width: 100%;
  height: 100%;
  background-size: cover;
  background-position: center;
  position: relative;
  opacity: ${ifElse(propEq('first', true), always(0), always(1))};
`;

export const AnimBackground = styled.span`
  background-color: ${Variables.colorProductGrey};
  position: absolute;
  top: 0;
  right: 0;
  height: 100%;
  width: ${ifElse(propEq('first', true), always(0), always(100))}%;
`;
