import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { MainSlider } from '../slider/slider.component';

import {
  Wrapper,
  AnimBackground,
  CopyAnimWrapper,
  CopyAnimWrapper2,
  Copy,
  Image,
  ImageWrapper,
  Slide,
  Title,
  Description,
} from './heroSlider.styles';
import { Components } from '../../theme';

import { animateHeroImage } from '../../utils/animations';

export class HeroSlider extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,
  };

  state = {
    animatedId: '',
  };

  componentWillReceiveProps(nextProps) {
    const isFresh = this.state.animatedId !== nextProps.data.getIn([0, 'id']);
    const isNew = this.props.data.getIn([0, 'id']) !== nextProps.data.getIn([0, 'id']);
    const newId = nextProps.data.getIn([0, 'id']);
    //if to avoide duplicated anim starters
    if (
      (isFresh && isNew) ||
      this.backgroundElement &&
      this.state.animatedId !== newId &&
      isNew
    ) {
      setTimeout(() => {
        animateHeroImage(this.backgroundElement, this.imageElement, this.headSubtitleElement, this.headTitleElement);
      }, 200);
      this.setState({ animatedId: newId });
    }
  }

  handleBackgroundRef = (id, ref) => {
    if (id === 0 && ref) {
      this.backgroundElement = document.querySelector('.slick-active')
        .querySelector(`.${ref.classList[0]}`);
    }
  };
  handleImageRef = (id, ref) => {
    if (id === 0 && ref) {
      this.imageElement = document.querySelector('.slick-active')
        .querySelector(`.${ref.classList[0]}`);
    }
  };
  handleHeadSubtitleRef = (id, ref) => {
    if (id === 0 && ref) {
      this.headSubtitleElement = document.querySelector('.slick-active')
        .querySelector(`.${ref.classList[0]}`);
    }
  };
  handleTitleRef = (id, ref) => {
    if (id === 0 && ref) {
      this.headTitleElement = document.querySelector('.slick-active')
        .querySelector(`.${ref.classList[0]}`);
    }
  };

  backgroundElement = null;
  imageElement = null;
  headSubtitleElement = null;
  headTitleElement = null;
  slickCloned = null;

  renderSlider = () => this.props.data.map((item, index) => (
    <Slide key={item.get('id')}>
      <Copy>
        <CopyAnimWrapper
          first={index === 0}
          innerRef={(ref) => this.handleHeadSubtitleRef(index, ref)}
        >
          <Description >{item.get('contentHtml')}</Description>
        </CopyAnimWrapper>
        <CopyAnimWrapper2
          innerRef={(ref) => this.handleTitleRef(index, ref)}
          first={index === 0}
        >
          <Title >{item.get('title')}</Title>
        </CopyAnimWrapper2>
      </Copy>

      <ImageWrapper>
        <AnimBackground
          first={index === 0}
          innerRef={(ref) => this.handleBackgroundRef(index, ref)}
        />
        <Image
          first={index === 0}
          innerRef={(ref) => this.handleImageRef(index, ref)}
          src={item.getIn(['image', 'originalSrc'])}
        />
      </ImageWrapper>
    </Slide>
  ));

  render = () => (
    <Wrapper>
      <Components.Dots
        width="50%"
        height="1312"
        top="400"
        mobileTop="298"
        mobileHeight="795"
        mobileWidth="79%"
      />
      <MainSlider>
        {this.renderSlider()}
      </MainSlider>
    </Wrapper>
  );
}


