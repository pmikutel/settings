import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';

import messages from './overwriteProject.messages';

import { Wrapper, Title, buttonStyles, linkStyles } from './overwriteProject.styles';
import { BlackCloseButton } from '../modals.styles';
import { Components } from '../../../theme';

export class OverwriteProject extends PureComponent {
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
    resetStyle: PropTypes.func.isRequired,
    resetData: PropTypes.func.isRequired,
  };

  handleOverwriteButton = () => {
    this.props.closeModal();
    this.props.resetStyle();
    this.props.resetData();
  };

  render = () => (
    <Wrapper>
      <BlackCloseButton onClick={this.props.closeModal} />
      <Title>
        <FormattedMessage {...messages.title} />
      </Title>
      <Components.ActionLink
        to="/project/step1"
        onClick={this.handleOverwriteButton}
        noActive
        extraStyles={buttonStyles}
      >
        <FormattedMessage {...messages.overwrite} />
      </Components.ActionLink>
      <Components.Button
        onClick={this.props.closeModal}
        extraStyles={linkStyles}
        noActive
      >
        <FormattedMessage {...messages.decline} />
      </Components.Button>
    </Wrapper>
  );
}

