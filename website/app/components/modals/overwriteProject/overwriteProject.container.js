import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { OverwriteProject } from './overwriteProject.component';
import { ModalsActions } from '../../../modules/modals/modals.redux';
import { Step1Actions } from '../../../modules/step1/step1.redux';
import { Step2Actions } from '../../../modules/step2/step2.redux';

const mapStateToProps = createStructuredSelector({
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  closeModal: ModalsActions.closeModal,
  resetStyle: Step1Actions.resetStyle,
  resetData: Step2Actions.resetData,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OverwriteProject);
