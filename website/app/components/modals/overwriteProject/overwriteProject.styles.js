import styled, { css } from 'styled-components';
import { Typography, Variables, Media } from '../../../theme';

const wrapperDesktopStyles = css`
  padding: 29px 92px 0;
`;

export const Wrapper = styled.div`
  padding: 40px 30px 0;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  
  ${Media.desktop(wrapperDesktopStyles)};
`;

export const Title = styled.div`
  color: ${Variables.colorPureBlack};
  ${Typography.fontLyonLight};
  line-height: 50px;
  font-size: 22px;
  margin-bottom: 45px;
  text-align: center;
`;

export const actionDesktopStyles = css`
  margin: 0 0 32px;
`;

const actionStyles = css`
  text-align: center;
  margin: 0 0 32px;
  width: 100%;
  justify-content: center;
  max-width: 336px;
   
  ${Media.desktop(actionDesktopStyles)};
`;

export const buttonStyles = css`
  ${actionStyles};
  padding: 23px 0;
`;

export const linkStyles = css`
  ${actionStyles};
`;
