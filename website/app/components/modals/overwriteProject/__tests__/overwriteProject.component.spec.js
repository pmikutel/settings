import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { OverwriteProject } from '../overwriteProject.component';


describe('<OverwriteProject />', () => {
  const defaultProps = {
    resetData: identity,
    closeModal: identity,
    resetStyle: identity,
  };

  const component = (props = {}) => <OverwriteProject {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
