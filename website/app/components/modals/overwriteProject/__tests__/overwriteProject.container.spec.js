import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../overwriteProject.container';
import { ModalsActions } from '../../../../modules/modals/modals.redux';
import { Step1Actions } from '../../../../modules/step1/step1.redux';
import { Step2Actions } from '../../../../modules/step2/step2.redux';

describe('OverwriteProject: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call ModalsActions.closeModal', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).closeModal();

      expect(dispatch).to.have.been.calledWith(ModalsActions.closeModal());
    });

    it('should call Step1Actions.resetStyle', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).resetStyle();

      expect(dispatch).to.have.been.calledWith(Step1Actions.resetStyle());
    });

    it('should call Step2Actions.resetData', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).resetData();

      expect(dispatch).to.have.been.calledWith(Step2Actions.resetData());
    });
  });
});
