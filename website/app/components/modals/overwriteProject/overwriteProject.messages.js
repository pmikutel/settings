
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'overwriteProject.title',
    defaultMessage: 'You are about to overwrite your project',
  },
  overwrite: {
    id: 'overwriteProject.overwrite',
    defaultMessage: 'Overwrite',
  },
  decline: {
    id: 'overwriteProject.decline',
    defaultMessage: 'Decline',
  },
});
