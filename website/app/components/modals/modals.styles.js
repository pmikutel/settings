import styled, { css } from 'styled-components';
import * as RRD from 'react-router-dom';
import { Media, Variables, Typography } from '../../theme';
import Sprite from '../../utils/sprites';

const wrapperDesktopStyles = css`
  padding: 63px 0 230px;
`;

export const Wrapper = styled.div`
  padding: 23px 16px 169px;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;

  ${Media.desktop(wrapperDesktopStyles)};
`;

export const closeButtonDesktopStyles = css`
  right: 82px;
  top: 81px;
`;

export const closeButtonStyles = css`
  position: absolute;
  right: 22px;
  top: 35px;
  z-index: 3;

  ${Media.desktop(closeButtonDesktopStyles)};
`;

export const Section = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Icon = styled(RRD.Link)`
  display: table;
  margin-top: 23px;
  ${Sprite.responsive('logo-white')};
`;

const titleDesktopStyles = css`
  margin: 144px 0 16px;
`;

export const Title = styled.h1`
  margin: 65px 0 16px;
  color: ${Variables.colorPureWhite};
  text-align: center;

  ${Media.desktop(titleDesktopStyles)};
`;

export const Line = styled.div`
  max-width: 605px;
  width: 100%;
  height: 1px;
  margin: 6px auto 0;
  background-color: ${Variables.colorPureWhite};
`;

export const SubTitle = styled.h3`
  margin: 19px 0 13px;
  color: ${Variables.colorPureWhite};
  text-align: center;
  ${Typography.fontLyonLight};
`;

export const Content = styled.div`
  width: 100%;
  max-width: 336px;
  text-align: center;
  margin: 0 auto;
`;

const descriptionDesktopStyles = css`
  padding: 0 0 26px;
`;

export const Description = styled.div`
  font-size: 18px;
  line-height: 36px;
  padding: 0 20px 52px;

  color: ${Variables.colorPureWhite};

  ${Media.desktop(descriptionDesktopStyles)};
`;

export const Link = styled(RRD.Link)`
  display: inline-flex;
  line-height: 1;
  border-bottom: 1px solid ${Variables.colorPureWhite};
  color: ${Variables.colorPureWhite};
`;

export const BlackCloseButton = styled.div`
  position: absolute;
  right: 20px;
  top: 11px;
  cursor: pointer;
  transition: transform 0.2s;

  &:after {
    content: '×';
    font-size: 35px;
    line-height: 1;
    font-weight: 600;
  }

  &:hover {
    transform: rotate(45deg)
  }
`;

export const authStyles = {
  content: {
    top: '0',
    left: '0',
    right: '0',
    bottom: '0',
    padding: 0,
    border: 'none',
    borderRadius: 'none',
    zIndex: 1,
    backgroundImage: `${Variables.gradientLogin}`,
  },
  overlay: {
    position: 'absolute',
    zIndex: 100,
  },
};

export const projectStyles = {
  content: {
    maxWidth: '769px',
    width: '100%',
    height: 'auto',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    padding: 0,
    border: 'none',
    borderRadius: 'none',
    transform: 'translate(-50%, -50%)',
    zIndex: 1,
    backgroundImage: `${Variables.colorPureWhite}`,
  },
  overlay: {
    position: 'absolute',
    backgroundColor: `${Variables.colorBlackWithOpacity}`,
  },
};
