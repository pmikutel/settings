import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { ResetPasswordActions } from '../../../modules/resetPassword/resetPassword.redux';
import { selectRemindError, selectRemindSuccess } from '../../../modules/resetPassword/resetPassword.selectors';

import { Remind } from './remind.component';
import { ModalsActions } from '../../../modules/modals/modals.redux';

const mapStateToProps = createStructuredSelector({
  remindError: selectRemindError,
  remindSuccess: selectRemindSuccess,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  request: ResetPasswordActions.remindRequest,
  closeModal: ModalsActions.closeModal,
  remindReset: ResetPasswordActions.remindReset,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Remind);
