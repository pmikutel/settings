
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'remind.title',
    defaultMessage: 'Forgot password',
  },
  success: {
    id: 'remind.success',
    defaultMessage: 'Incoming!',
  },
  successMessage: {
    id: 'remind.successMessage',
    defaultMessage: 'We just sent you an email.',
  },
  successMessage2: {
    id: 'remind.successMessage',
    defaultMessage: 'Please follow the link in the email to reset your password.',
  },
  information: {
    id: 'remind.information',
    defaultMessage: 'Recover your White Bear account',
  },
});
