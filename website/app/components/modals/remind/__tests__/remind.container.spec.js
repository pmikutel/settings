import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../remind.container';
import { ResetPasswordActions } from '../../../../modules/resetPassword/resetPassword.redux';
import { ModalsActions } from '../../../../modules/modals/modals.redux';

describe('Login: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call ResetPasswordActions.remindRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).request();

      expect(dispatch).to.have.been.calledWith(ResetPasswordActions.remindRequest());
    });

    it('should call ResetPasswordActions.remindReset', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).remindReset();

      expect(dispatch).to.have.been.calledWith(ResetPasswordActions.remindReset());
    });

    it('should call ModalsActions.closeModal', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).closeModal();

      expect(dispatch).to.have.been.calledWith(ModalsActions.closeModal());
    });
  });
});
