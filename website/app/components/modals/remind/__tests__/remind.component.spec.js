import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { Remind } from '../remind.component';


describe('<Remind />', () => {
  const defaultProps = {
    request: identity,
    closeModal: identity,
    remindReset: identity,
    remindError: '',
    remindSuccess: false,
  };

  const component = (props = {}) => <Remind {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly with form', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly with success', () => {
    const remindSuccess = true;
    const wrapper = render({ remindSuccess });

    global.expect(wrapper).toMatchSnapshot();
  });
});
