import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { Field, reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { ifElse, always, isEmpty } from 'ramda';

import messages from './remindForm.messages';

import {
  inputStyles,
  Error,
  InputWrapper,
} from './remindForm.styles';

import { Forms, Components } from '../../../../theme';


export class RemindFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    error: PropTypes.bool,
    remindError: PropTypes.string.isRequired,
    valid: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
  };

  renderField = ({ error, input, placeholder, type, meta: { dirty, active, invalid } }) => (
    <InputWrapper>
      <Forms.Input
        {...input}
        placeholder={placeholder}
        type={type}
        extraStyles={inputStyles}
        errorStyle={!!error || (dirty && !active && invalid)}
      />
      {this.renderError(error)}
    </InputWrapper>
  );

  renderError = (error) => ifElse(
    isEmpty,
    always(null),
    () => <Error>{this.props.remindError}</Error>
  )(error);

  render() {
    const { intl: { formatMessage }, handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit} noValidate>
        <Field
          name="email"
          type="email"
          component={this.renderField}
          placeholder={formatMessage(messages.email)}
          error={this.props.remindError}
        />
        <Components.Button>
          <FormattedMessage {...messages.singIn} />
        </Components.Button>
      </form>
    );
  }
}

export const RemindForm = injectIntl(reduxForm({
  form: 'remindForm',
  validate: (values) => validate(values.toJS(), {
    email: {
      email: true,
      presence: {
        allowEmpty: false,
      },
    },
  }),
})(RemindFormComponent));
