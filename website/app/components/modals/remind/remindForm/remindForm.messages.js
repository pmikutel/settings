import { defineMessages } from 'react-intl';

export default defineMessages({
  email: {
    id: 'remind.email',
    defaultMessage: 'Your email*',
  },
  singIn: {
    id: 'remind.singIn',
    defaultMessage: 'Submit',
  },
});
