import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { RemindFormComponent as RemindForm } from '../remindForm.component';


describe('<RemindForm />', () => {
  const defaultProps = {
    handleSubmit: identity,
    remindError: '',
    error: false,
    valid: false,
    intl: {
      formatMessage: () => {},
    },
  };

  const component = (props = {}) => <RemindForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
