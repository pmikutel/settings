import styled, { css } from 'styled-components';
import { Variables } from '../../../../theme';
import { colorPureWhite } from '../../../../theme/variables';

export const inputStyles = css`
  border-color: ${colorPureWhite};
  color: ${colorPureWhite};
  
  &::placeholder {
    color: ${colorPureWhite};
  }
`;

export const InputWrapper = styled.div`
  position: relative;
  padding-bottom: 50px;
  margin-bottom: 10px;
`;

export const Error = styled.div`
  color: ${Variables.colorRed};
  position: absolute;
  text-align: left;
  top: 73px;
  width: 100%;
`;
