import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import { ifElse, prop } from 'ramda';

import { RemindForm } from './remindForm/remindForm.component';
import messages from './remind.messages';

import { Section, Title, Content, Description, SubTitle, Line } from '../modals.styles';


export class Remind extends PureComponent {
  static propTypes = {
    request: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    remindReset: PropTypes.func.isRequired,
    remindError: PropTypes.string,
    remindSuccess: PropTypes.bool,
  };

  componentWillMount() {
    this.props.remindReset();
  }

  renderSuccess = () => (
    <div>
      <Title>
        <FormattedMessage {...messages.success} />
      </Title>
      <Line />
      <SubTitle>
        <FormattedMessage {...messages.successMessage} />
      </SubTitle>
      <SubTitle>
        <FormattedMessage {...messages.successMessage2} />
      </SubTitle>
    </div>
  );

  renderForm = () => (
    <div>
      <Title>
        <FormattedMessage {...messages.title} />
      </Title>
      <Content>
        <Description>
          <FormattedMessage {...messages.information} />
        </Description>
        <RemindForm
          onSubmit={this.props.request}
          remindError={this.props.remindError}
        />
      </Content>
    </div>
  );

  renderContent = () => ifElse(
    prop('remindSuccess'),
    this.renderSuccess,
    this.renderForm,
  )(this.props);

  render = () => (
    <Section>
      {this.renderContent()}
    </Section>
  );
}

