import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { Modals } from '../modals.component';


describe('<Modals />', () => {
  const defaultProps = {
    openModal: identity,
    closeModal: identity,
    type: '',
    isPurchaseFlow: false,
    isProjectFlow: false,
  };

  const component = (props = {}) => <Modals {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
