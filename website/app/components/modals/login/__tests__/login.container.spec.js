import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../login.container';
import { UserActions } from '../../../../modules/user/user.redux';
import { ModalsActions } from '../../../../modules/modals/modals.redux';

describe('Login: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call UserActions.loginRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).login();

      expect(dispatch).to.have.been.calledWith(UserActions.loginRequest());
    });

    it('should call UserActions.clearError', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).clearError();

      expect(dispatch).to.have.been.calledWith(UserActions.clearError());
    });

    it('should call ModalsActions.openModal', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).openModal();

      expect(dispatch).to.have.been.calledWith(ModalsActions.openModal());
    });

    it('should call ModalsActions.closeModal', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).closeModal();

      expect(dispatch).to.have.been.calledWith(ModalsActions.closeModal());
    });

    it('should call UserActions.loginReset', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).loginReset();

      expect(dispatch).to.have.been.calledWith(UserActions.loginReset());
    });
  });
});
