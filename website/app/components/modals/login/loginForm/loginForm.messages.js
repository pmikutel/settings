import { defineMessages } from 'react-intl';

export default defineMessages({
  email: {
    id: 'register.email',
    defaultMessage: 'Your email*',
  },
  password: {
    id: 'register.password',
    defaultMessage: 'Password*',
  },
  remind: {
    id: 'register.remind',
    defaultMessage: 'Forgot your password?',
  },
  singIn: {
    id: 'register.singIn',
    defaultMessage: 'Sign In',
  },
});
