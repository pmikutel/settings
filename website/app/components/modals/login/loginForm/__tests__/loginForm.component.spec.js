import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { LoginFormComponent as LoginForm } from '../loginForm.component';


describe('<LoginForm />', () => {
  const defaultProps = {
    openModal: identity,
    handleSubmit: identity,
    submitting: false,
    pristine: false,
    error: false,
    valid: false,
    intl: {
      formatMessage: () => {},
    },
  };

  const component = (props = {}) => <LoginForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
