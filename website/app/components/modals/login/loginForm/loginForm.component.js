import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import { Field, reduxForm } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import validate from 'validate.js';

import messages from './loginForm.messages';

import { TryAgain } from '../../../../components/tryAgain/tryAgain.component';

import {
  Link,
  LinkWrapper,
  inputStyles,
} from './loginForm.styles';

import { Forms, Components } from '../../../../theme';

export class LoginFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool,
    error: PropTypes.bool,
    valid: PropTypes.bool,
    intl: PropTypes.object.isRequired,
    openModal: PropTypes.func,
    loginError: PropTypes.bool,
    errorMessage: PropTypes.any,
  };

  renderField = ({ input, placeholder, type, meta: { touched, error } }) => (
    <Forms.Input
      {...input}
      placeholder={placeholder}
      type={type}
      extraStyles={inputStyles}
      errorStyle={touched && !!error}
    />
  );

  render() {
    const { intl: { formatMessage }, openModal, handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit} noValidate>
        <Field
          name="email"
          type="email"
          component={this.renderField}
          placeholder={formatMessage(messages.email)}
        />
        <Field
          name="password"
          type="password"
          component={this.renderField}
          placeholder={formatMessage(messages.password)}
        />
        <LinkWrapper>
          <Link type="button" onClick={() => openModal('remind')}>
            <FormattedMessage {...messages.remind} />
          </Link>
        </LinkWrapper>

        <TryAgain isActive={this.props.loginError} copy={this.props.intl.formatMessage(this.props.errorMessage)} />

        <Components.Button>
          <FormattedMessage {...messages.singIn} />
        </Components.Button>
      </form>
    );
  }
}

export const LoginForm = injectIntl(reduxForm({
  form: 'loginForm',
  validate: (values) => validate(values.toJS(), {
    email: {
      email: true,
      presence: {
        allowEmpty: false,
      },
    },
    password: {
      presence: {
        allowEmpty: false,
      },
    },
  }),
})(LoginFormComponent));
