import styled, { css } from 'styled-components';
import { Variables } from '../../../../theme';
import { colorPureWhite } from '../../../../theme/variables';

export const inputStyles = css`
  border-color: ${colorPureWhite};
  color: ${colorPureWhite};
  margin-bottom: 32px;
  
  &::placeholder {
    color: ${colorPureWhite};
  }
`;

export const Link = styled.button`
  padding: 0;
  font-family: ${Variables.lyonLightFontFamily};
  border-bottom: 1px solid ${Variables.colorPureWhite};
  color: ${Variables.colorPureWhite};
`;

export const LinkWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding-bottom: 5px;
`;
