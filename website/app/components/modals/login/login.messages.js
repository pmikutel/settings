
import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'login.title',
    defaultMessage: 'SIGN IN',
  },
  information: {
    id: 'login.information',
    defaultMessage: 'Please sign in to complete your purchase. ',
  },
  information2: {
    id: 'login.information2',
    defaultMessage: ' to start building your space.',
  },
  description: {
    id: 'login.description',
    defaultMessage: 'Don’t have an account? ',
  },
  signIn: {
    id: 'login.signIn',
    defaultMessage: 'Create one now',
  },
  error: {
    id: 'login.error',
    defaultMessage: 'Incorrect email or password, please try again.',
  },
});
