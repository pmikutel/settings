import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { UserActions } from '../../../modules/user/user.redux';
import { ModalsActions } from '../../../modules/modals/modals.redux';

import { Login } from './login.component';
import { selectLoginError } from '../../../modules/user/user.selectors';

const mapStateToProps = createStructuredSelector({
  loginError: selectLoginError,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  login: UserActions.loginRequest,
  clearError: UserActions.clearError,
  loginReset: UserActions.loginReset,
  openModal: ModalsActions.openModal,
  closeModal: ModalsActions.closeModal,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
