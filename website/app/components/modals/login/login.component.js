import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { ifElse, prop } from 'ramda';
import { LoginForm } from './loginForm/loginForm.component';
import messages from './login.messages';

import {
  Section,
  Title,
  Content,
  Description,
  Link,
} from '../modals.styles';


export class LoginComponent extends PureComponent {
  static propTypes = {
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    loginReset: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
    loginError: PropTypes.bool.isRequired,
    clearError: PropTypes.func.isRequired,
    isPurchaseFlow: PropTypes.bool.isRequired,
    isProjectFlow: PropTypes.bool.isRequired,
    intl: PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.clearError();
    this.props.loginReset();
  }

  renderDescription = () => ifElse(
    prop('isPurchaseFlow'),
    () =>
      <Description>
        <FormattedMessage {...messages.information} />
        <FormattedMessage {...messages.description} />
        <Link to="/user/register" onClick={() => this.props.closeModal()}>
          <FormattedMessage {...messages.signIn} />.
        </Link>
      </Description>,
    () =>
      <Description>
        <div>
          <FormattedMessage {...messages.description} />
        </div>
        <Link to="/user/register" onClick={() => this.props.closeModal()}>
          <FormattedMessage {...messages.signIn} />
        </Link>
        <FormattedMessage {...messages.information2} />
      </Description>,
  )(this.props);

  render = () => (
    <Section>
      <Title>
        <FormattedMessage {...messages.title} />
      </Title>
      <Content>
        {this.renderDescription()}

        <LoginForm
          onSubmit={(data) => this.props.login(data, false, this.props.isProjectFlow)}
          openModal={this.props.openModal}
          loginError={this.props.loginError}
          errorMessage={messages.error}
        />
      </Content>
    </Section>
  );
}

export const Login = injectIntl(LoginComponent);
