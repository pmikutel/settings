import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import { cond, propEq, T, always, ifElse } from 'ramda';
import Login from './login/login.container';
import Remind from './remind/remind.container';
import OverwriteProject from './overwriteProject/overwriteProject.container';

import { Wrapper, closeButtonStyles, Icon, authStyles, projectStyles } from './modals.styles';

import { Components } from '../../theme';

export class Modals extends PureComponent {
  static propTypes = {
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
    type: PropTypes.string,
    isPurchaseFlow: PropTypes.bool,
    isProjectFlow: PropTypes.bool,
  };

  static defaultProps = {
    isPurchaseFlow: false,
    isProjectFlow: false,
  };

  componentWillMount() {
    ReactModal.setAppElement('body');
  }

  setModal = () => cond([
    [propEq('type', 'login'), () =>
      <Wrapper>
        <Icon to="/" onClick={this.props.closeModal} />
        <Components.CloseButton onClick={this.props.closeModal} extraStyles={closeButtonStyles} />
        <Login
          isPurchaseFlow={this.props.isPurchaseFlow}
          isProjectFlow={this.props.isProjectFlow}
        />
      </Wrapper>,
    ],
    [propEq('type', 'remind'), () =>
      <Wrapper>
        <Icon to="/" onClick={this.props.closeModal} />
        <Components.CloseButton onClick={this.props.closeModal} extraStyles={closeButtonStyles} />
        <Remind />
      </Wrapper>,
    ],
    [propEq('type', 'project'), () => <OverwriteProject />],
    [T, always('')],
  ])(this.props);

  get styles() {
    return ifElse(
      propEq('type', 'project'),
      () => projectStyles,
      () => authStyles,
    )(this.props);
  }

  render = () => (
    <ReactModal
      isOpen={!!this.props.type}
      style={this.styles}
      onRequestClose={this.props.closeModal}
    >

      {this.setModal()}
    </ReactModal>
  );
}
