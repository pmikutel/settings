import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import { Wrapper, Text, RadioWrapper } from './radioSquares.styles';
import { Forms } from '../../theme';

export class RadioSquaresComponent extends PureComponent {
  static propTypes = {
    items: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    intl: PropTypes.object.isRequired,
    value: PropTypes.string.isRequired,
  };

  renderRadioButtons = () => this.props.items.map(({ id, message }) => (
    <RadioWrapper key={id}>
      <Text onClick={() => this.props.onChange(id)}>{this.props.intl.formatMessage(message)}</Text>
      <Forms.SquareButton
        type="button"
        isActive={id === this.props.value}
        key={id}
        onClick={() => this.props.onChange(id)}
      />
    </RadioWrapper>
  ));

  render = () => (
    <Wrapper>
      {this.renderRadioButtons()}
    </Wrapper>
  );
}

export const RadioSquares = injectIntl(RadioSquaresComponent);
