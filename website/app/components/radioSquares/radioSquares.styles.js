import styled from 'styled-components';
import { Typography, Variables } from '../../theme';


export const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Text = styled.span`
  ${Typography.fontLyonLight};
  cursor: pointer;
  font-size: 22px;
  padding-right: 9px;
  margin-top: 3px;
  color: ${Variables.colorWarmGrey};
`;

export const RadioWrapper = styled.div`
  padding: 0 18px;
  display: flex;
  align-items: center;  
`;
