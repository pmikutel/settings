import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { RegisterFormComponent as RegisterForm } from '../registerForm.component';
import { intlMock } from '../../../utils/testing';

describe('<RegisterForm />', () => {
  const defaultProps = {
    handleSubmit: identity,
    submitting: false,
    pristine: false,
    error: false,
    valid: false,
    onSubmit: false,
    intl: intlMock(),
  };

  const component = (props = {}) => <RegisterForm {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
