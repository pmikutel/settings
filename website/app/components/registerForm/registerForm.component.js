import React, { PureComponent } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import validate from 'validate.js';
import { always, ifElse, propEq } from 'ramda';

import messages from './registerForm.messages';

import { TryAgain } from '../../components/tryAgain/tryAgain.component';
import { DotsLoader } from '../dotsLoader/dotsLoader.component';

import {
  wrapper,
  inputContainer,
  submitButtonStyles,
} from './registerForm.styles';

import { Forms, Grid, Components } from '../../theme';

export class RegisterFormComponent extends PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    pristine: PropTypes.bool,
    error: PropTypes.bool,
    valid: PropTypes.bool,
    intl: PropTypes.object.isRequired,
    registerError: PropTypes.bool,
    registerIsProcessing: PropTypes.bool,
  };

  state = {
    loading: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.registerError) {
      this.setState({ loading: false });
    }
  }

  submitHandler = (data) => {
    this.setState({ loading: true });
    this.props.handleSubmit(data);
  };

  renderField = ({ input, placeholder, type, errorText, meta: { touched, error } }) => (
    <Grid.Row extraStyles={inputContainer}>
      <Forms.Input
        errorStyle={touched && !!error}
        {...input}
        placeholder={placeholder}
        type={type}
      />
      {touched && !!error && <Forms.Error>{errorText}</Forms.Error>}
    </Grid.Row>
  );

  renderLoader = () => ifElse(
    propEq('loading', true),
    always(<DotsLoader relative={true} />),
    always(''),
  )(this.state);

  render = () => {
    const { intl: { formatMessage }, valid } = this.props;

    return (
      <form onSubmit={this.submitHandler} noValidate>
        <Grid.Section extraStyles={wrapper}>
          <Field
            name="fullName"
            type="text"
            component={this.renderField}
            errorText={formatMessage(messages.fullNameError)}
            placeholder={formatMessage(messages.fullName)}
          />
          <Field
            name="email"
            type="email"
            component={this.renderField}
            errorText={formatMessage(messages.emailError)}
            placeholder={formatMessage(messages.email)}
          />
          <Field
            name="password"
            type="password"
            component={this.renderField}
            errorText={formatMessage(messages.passwordError)}
            placeholder={formatMessage(messages.password)}
          />
          <Field
            name="companyName"
            type="text"
            component={this.renderField}
            errorText={formatMessage(messages.companyNameError)}
            placeholder={formatMessage(messages.companyName)}
          />
        </Grid.Section>

        <TryAgain isActive={this.props.registerError} />

        <Components.SubmitButton extraStyles={submitButtonStyles} disabled={!valid || this.state.loading}>
          <FormattedMessage {...messages.registerButton} />
          {this.renderLoader()}
        </Components.SubmitButton>
      </form>
    );
  };
}

export const RegisterForm = injectIntl(reduxForm({
  form: 'registerForm',
  validate: (values) => validate(values.toJS(), {
    email: {
      email: true,
      presence: {
        allowEmpty: false,
      },
    },
    name: {
      length: {
        minimum: 1,
      },
      presence: {
        allowEmpty: false,
      },
    },
    companyName: {
      length: {
        minimum: 1,
      },
      presence: {
        allowEmpty: false,
      },
    },
    password: {
      format: {
        pattern: '.*[0-9].*',
      },
      length: {
        minimum: 8,
      },
      presence: {
        allowEmpty: false,
      },
    },
  }),
})(RegisterFormComponent));
