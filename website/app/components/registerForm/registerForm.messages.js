
import { defineMessages } from 'react-intl';

export default defineMessages({
  fullName: {
    id: 'register.fullName',
    defaultMessage: 'Your name *',
  },
  fullNameError: {
    id: 'register.fullName',
    defaultMessage: 'Please enter your full name',
  },
  email: {
    id: 'register.email',
    defaultMessage: 'Email*',
  },
  emailError: {
    id: 'register.email',
    defaultMessage: 'Please enter a valid email address',
  },
  password: {
    id: 'register.password',
    defaultMessage: 'Password*',
  },
  passwordError: {
    id: 'register.password',
    defaultMessage: 'Your password must be at least 8 characters long and contain at least one number',
  },
  companyName: {
    id: 'register.companyName',
    defaultMessage: 'Company name*',
  },
  companyNameError: {
    id: 'register.companyName',
    defaultMessage: 'Please enter your Company name*',
  },
  registerButton: {
    id: 'register.registerButton',
    defaultMessage: 'Register',
  },
});
