import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { throttle } from 'lodash';

const HEADER_HEIGHT_DESKTOP = 111;
const HEADER_HEIGHT_MOBILE = 88;
const MOBILE_BREAKPOINT = 1024;

export class WindowScroll extends PureComponent {
  static propTypes = {
    onScroll: PropTypes.func.isRequired,
    header: PropTypes.bool,
    throttle: PropTypes.bool,
  };

  static defaultProps = {
    header: false,
    throttle: true,
  };

  state = {
    windowPosition: 0,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handler);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handler);
  }

  action = e => {
    if (this.props.header) {
      const windowNewPosition = window.pageYOffset;
      const headerHeight = window.innerWidth < MOBILE_BREAKPOINT ? HEADER_HEIGHT_MOBILE : HEADER_HEIGHT_DESKTOP;
      const scrollDirection = windowNewPosition > headerHeight ? this.state.windowPosition > windowNewPosition : true;
      this.setState({ windowPosition: window.pageYOffset });
      this.props.onScroll(scrollDirection, windowNewPosition < 10);
      return;
    }
    this.props.onScroll(e);
  };

  actionThrottled = throttle(this.action, 400);

  handler = e => {
    const { throttle } = this.props;
    if (throttle) {
      this.actionThrottled(e);
    } else {
      this.action(e);
    }
  };

  render() {
    return null;
  }
}
