import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { WindowScroll } from '../windowScroll.component';

describe('<WindowResize />', () => {
  const defaultProps = {
    onScroll: identity,
  };

  const component = (props = {}) => <WindowScroll {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
