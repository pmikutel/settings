import React from 'react';
import { shallow } from 'enzyme';

import { ScrollToTop } from '../scrollToTop.component';


describe('<ScrollToTop />', () => {
  const defaultProps = {
    location: {},
  };

  const component = (props = {}) => <ScrollToTop {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
