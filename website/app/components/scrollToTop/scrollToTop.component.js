import { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class ScrollToTop extends PureComponent {
  static propTypes = {
    location: PropTypes.object.isRequired,
  };

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scroll(0, 0);
    }
  }

  render() {
    return null;
  }
}
