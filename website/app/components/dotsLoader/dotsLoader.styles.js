import styled, { keyframes, css } from 'styled-components';
import { bounceIn } from 'react-animations';
import { always, ifElse, propEq } from 'ramda';

import { Variables, Typography } from '../../theme';

const keyFrame = keyframes`${ bounceIn }`;

const FONT_SIZE = 22;
const DOT_SIZE = 10;

const getPosition = ifElse(propEq('relative', true),
  () => css` position: relative;`,
  () => css`  
    position: absolute;
    right: 15px;
    top: 50%;
    transform: translateY(-50%);`
);

export const Wrapper = styled.div`
  display: inline-block;
  
 ${getPosition};
`;

const colorfulDotsStyles = css`
  background-color: ${Variables.colorFluroGreen};
  &:nth-child(2) {
    background-color: ${Variables.colorTurquoise};
  }
    
  &:nth-child(3) {
    background-color: ${Variables.colorBrightBlue2};
  }
`;

export const Dot = styled.div`
  display: inline-block;
  margin: 5px;
  border-radius: ${DOT_SIZE}px;
  height: ${DOT_SIZE}px;
  width: ${DOT_SIZE}px;
  background-color: ${Variables.colorPureWhite};
  animation: 1s ${keyFrame} alternate infinite;
  &:nth-child(2) {
    animation-delay: 0.5s;
  }    
  &:nth-child(3) {
    animation-delay: 1s;
  }
    
  ${ifElse(propEq('isColorful', true), always(colorfulDotsStyles), always(''))}
`;

export const Label = styled.span`
  line-height: 1;
  ${Typography.fontLyonLight};
  color: ${Variables.colorPureWhite};
  font-size: ${FONT_SIZE}px;
`;
