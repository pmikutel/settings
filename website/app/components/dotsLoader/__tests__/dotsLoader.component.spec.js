import React from 'react';
import { shallow } from 'enzyme';

import { DotsLoaderComponent as DotsLoader } from '../dotsLoader.component';
import { intlMock } from '../../../utils/testing';

describe('<DotsLoader />', () => {
  const defaultProps = {
    intl: intlMock(),
    isActive: false,
    isColorful: false,
    label: '',
    relative: false,
  };

  const component = (props = {}) => <DotsLoader {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
