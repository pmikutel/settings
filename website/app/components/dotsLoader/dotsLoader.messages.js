import { defineMessages } from 'react-intl';

export default defineMessages({
  content: {
    id: 'dotsLoader.content',
    defaultMessage: 'Saving',
  },
});
