import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { always, is, cond, T, equals } from 'ramda';

import messages from './dotsLoader.messages';
import {
  Wrapper,
  Label,
  Dot,
} from './dotsLoader.styles';

export class DotsLoaderComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    isActive: PropTypes.bool,
    isColorful: PropTypes.bool,
    label: PropTypes.any,
    relative: PropTypes.bool,
  };

  static defaultProps = {
    isColorful: false,
    relative: false,
  };

  renderLabel = () => cond([
    [is(String), always(<Label>{this.props.label}</Label>)],
    [equals(true), always(<Label>{this.props.intl.formatMessage(messages.content)}</Label>)],
    [T, always('')],
  ])(this.props.label);

  render = () => (
    <Wrapper relative={this.props.relative}>
      {this.renderLabel()}
      <Dot isColorful={this.props.isColorful}></Dot>
      <Dot isColorful={this.props.isColorful}></Dot>
      <Dot isColorful={this.props.isColorful}></Dot>
    </Wrapper>
  );
}

export const DotsLoader = injectIntl(DotsLoaderComponent);
