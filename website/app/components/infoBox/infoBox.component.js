import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
  Wrapper,
  Circle,
} from './infoBox.styles';

export class InfoBox extends PureComponent {
  static propTypes = {
    copy: PropTypes.string.isRequired,
    smallInfoBox: PropTypes.bool,
  };

  render = () => (
    <Wrapper small={this.props.smallInfoBox}>
      <Circle small={this.props.smallInfoBox}>
        {this.props.copy}
      </Circle>
    </Wrapper>
  );
}
