import styled, { css } from 'styled-components';
import { ifElse, propEq, always } from 'ramda';
import { Media, Variables } from '../../theme';

const wrapperDesktopStyles = css`
  height: 184px;
  width: 184px;
`;

const smallWrapperDesktopStyles = css`
  height: 96px;
  width: 96px;
  line-height: 96px;
`;

const smallWrapperStyles = css`
  ${Media.tablet(smallWrapperDesktopStyles)};
`;

const wrapperStyles = css`
  ${Media.tablet(wrapperDesktopStyles)};
`;

const getWrapperStyles = ifElse(propEq('small', true), always(smallWrapperStyles), always(wrapperStyles));

export const Wrapper = styled.div`
  background-image: linear-gradient(225deg, ${Variables.colorYellow}, ${Variables.colorBarbiePink});
  display: inline-block;
  text-align: center;
  height: 77px;
  width: 77px;
  color: ${Variables.colorPureBlack};
    
  ${getWrapperStyles}
`;

const circleDesktopStyles = css`
  font-size: 47px;
  margin-top: 30px;
  height: 124px;
  line-height: 124px;
  width: 124px;
`;

const smallCircleDesktopStyles = css`
  height: 65px;
  width: 65px;
  line-height: 65px;
  font-size: 28px;
  margin-top: 0;
`;

const getCircleStyles = ifElse(
  propEq('small', true),
  always(smallCircleDesktopStyles),
  always(circleDesktopStyles)
);

export const Circle = styled.span`
  background: ${Variables.colorPureWhite};
  border-radius: 62px;
  display: inline-block;
  margin-top: 14px;
  height: 49px;
  font-size: 18px;
  line-height: 49px;
  width: 49px;
  
  ${props => Media.tablet(getCircleStyles(props))};
`;
