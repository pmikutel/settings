import React from 'react';
import { shallow } from 'enzyme';

import { InfoBox } from '../infoBox.component';


describe('<InfoBox />', () => {
  const defaultProps = {
    copy: '',
    smallInfoBox: false,
  };

  const component = (props = {}) => <InfoBox {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
