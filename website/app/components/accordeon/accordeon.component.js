import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
  Wrapper,
  Title,
  Description,
  Arrow,
} from './accordeon.styles';


export class Accordeon extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
  };

  state = {
    isVisible: false,
  };

  toggleDescription = () => this.setState(({ isVisible }) => ({
    isVisible: !isVisible,
  }));

  render = () => (
    <Wrapper>
      <Title onClick={this.toggleDescription}>
        <span>{this.props.title}</span>
        <Arrow rotateArrow={this.state.isVisible} />
      </Title>
      <Description isActive={this.state.isVisible}>
        {this.props.description}
      </Description>
    </Wrapper>
  );
}

