import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import { Accordeon } from '../accordeon.component';
import { Title } from '../accordeon.styles';


describe('<Accordeon />', () => {
  const defaultProps = {
    title: '',
    description: '',
  };

  const component = (props = {}) => <Accordeon {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should set proper isVisible initial state', () => {
    const wrapper = render();
    const isVisible = wrapper.state('isVisible');

    expect(isVisible).to.be.false;
  });

  describe('click <Title />', () => {
    it('should toggle isVisible state', () => {
      const wrapper = render();

      wrapper.find(Title).at(0).simulate('click');

      const isVisible = wrapper.state('isVisible');

      expect(isVisible).to.be.true;
    });
  });
});
