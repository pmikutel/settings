import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Variables, Media } from '../../theme/index';

export const Wrapper = styled.div``;

const titleDesktopStyles = css`
  letter-spacing: 12px;
  padding: 20px 0 26px;
`;

export const Title = styled.h3`
  cursor: pointer;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  position: relative;
  padding: 10px 0 26px;
  text-transform: uppercase;
  letter-spacing: 8px;
  
  &:after {
    content: '';
    border-bottom: 1px solid ${Variables.colorGray};
    position: absolute;
    opacity: 0.2;
    bottom: 0;
    left: 0;
    width: 100%;
  }
  
  ${Media.desktop(titleDesktopStyles)}
`;

const descriptionDesktopStyles = css`
  padding-bottom: 0;
`;

export const Description = styled.p`
  transition: max-height 0.5s ease-in-out;
  overflow: hidden;
  color: ${Variables.colorWarmGrey};
  max-height: ${ifElse(propEq('isActive', true), always(300), always(0))}px;
  margin-bottom: 0;
  
  ${Media.desktop(descriptionDesktopStyles)}
`;

export const Arrow = styled.div`
  border-left: 2px solid ${Variables.colorPureBlack};
  border-bottom: 2px solid ${Variables.colorPureBlack};
  transition: transform 0.2s ease-in-out;
  transform: rotate(${ifElse(propEq('rotateArrow', true), always(135), always(-45))}deg);
  height: 8px;
  width: 8px;
  margin-right: 2px;
`;
