import React from 'react';
import { shallow } from 'enzyme';
import { spy } from 'sinon';
import { identity } from 'ramda';
import { expect } from 'chai';

import { Counter } from '../counter.component';
import { Button } from '../counter.styles';

describe('<Counter />', () => {
  const defaultProps = {
    value: 1,
    onChange: identity,
    onBlur: identity,
  };

  const component = (props = {}) => <Counter {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('minus <Button >', () => {
    it('should not call onChange with lower value than min', () => {
      const onChange = spy();
      const value = 1;
      const min = 1;
      const wrapper = render({ onChange, value, min });

      wrapper.find(Button).at(0).simulate('mousedown');
      wrapper.find(Button).at(0).simulate('mouseup');

      expect(onChange).to.have.been.calledWith(1);
    });

    it('should call onChange with decreased value when value is greater than 0', () => {
      const onChange = spy();
      const value = 1;
      const wrapper = render({ onChange, value });

      wrapper.find(Button).at(0).simulate('mousedown');
      wrapper.find(Button).at(0).simulate('mouseup');

      expect(onChange).to.have.been.calledWith(value - 1);
    });

    it('should call onChange with 0 when value equals 0', () => {
      const onChange = spy();
      const value = 0;
      const wrapper = render({ onChange, value });

      wrapper.find(Button).at(0).simulate('mousedown');
      wrapper.find(Button).at(0).simulate('mouseup');

      expect(onChange).to.have.been.calledWith(value);
    });
  });

  describe('plus <Button >', () => {
    it('should call onChange with increased value', () => {
      const onChange = spy();
      const value = 1;
      const wrapper = render({ onChange, value });

      wrapper.find(Button).at(1).simulate('mousedown');
      wrapper.find(Button).at(1).simulate('mouseup');

      expect(onChange).to.have.been.calledWith(value + 1);
    });

    it('should not call onChange with greater value than max', () => {
      const onChange = spy();
      const value = 1;
      const max = 1;
      const wrapper = render({ onChange, value, max });

      wrapper.find(Button).at(1).simulate('mousedown');
      wrapper.find(Button).at(1).simulate('mouseup');

      expect(onChange).to.have.been.calledWith(1);
    });

    it('should set enabled on true when is mouse down on button', () => {
      const wrapper = render();

      wrapper.find(Button).at(1).simulate('mousedown');
      const enabled = wrapper.state('enabled');

      expect(enabled).to.equal(true);
    });

    it('should set enabled on true when is mouse up on button', () => {
      const wrapper = render();

      wrapper.find(Button).at(1).simulate('mousedown');
      wrapper.find(Button).at(1).simulate('mouseup');
      const enabled = wrapper.state('enabled');

      expect(enabled).to.equal(false);
    });

    it('should callback be a function', () => {
      const wrapper = render();

      wrapper.find(Button).at(1).simulate('mousedown');
      const callback = wrapper.state('callback');

      expect(callback).to.be.a('function');
    });
  });
});
