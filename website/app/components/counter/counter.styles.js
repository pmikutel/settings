import styled, { css } from 'styled-components';
import { ifElse, prop, always } from 'ramda';
import { Variables, Typography, Media } from '../../theme';

const DESKTOP_SIZE = 64;
const MOBILE_SIZE = 38;

const warpperDesktopStyles = css`
  text-align: left;
`;

export const Wrapper = styled.div`
  display: block;
  text-align: ${ifElse(prop('center'), always('left'), always('center'))};
  
  ${Media.desktop(warpperDesktopStyles)}
`;

const contentDesktopStyles = css`
  height: ${DESKTOP_SIZE}px;
`;

export const Content = styled.div`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  height: ${MOBILE_SIZE}px;
  border: 1px solid ${Variables.colorWarmGrey};
  
  ${Media.desktop(contentDesktopStyles)}
`;

const buttonDesktopStyles = css`
  margin: ${ifElse(prop('right'), always('0 5px 0 0'), always('0 0 0 5px'))};
  width: 44px;
`;

export const Button = styled.button`
  height: 100%;
  width: ${MOBILE_SIZE}px;
  padding: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  
  ${Media.desktop(buttonDesktopStyles)}
`;

export const Minus = styled.i`
  display: block;
  width: 15px;
  height: 2px;
  margin: 0 auto;
  background: ${Variables.colorPureBlack};
`;

export const Plus = styled.i`
  position: relative;
  display: block;
  margin: 0 auto;
  width: 16px;
  height: 2px;
  background: ${Variables.colorPureBlack};
  
  &:after {
    content: '';
    position: absolute;
    top: -7px;
    left: 7px;
    height: 16px;
    width: 2px;
    background: ${Variables.colorPureBlack};
  }
`;

export const Value = styled.input`
  ${Typography.fontGothamBook};
  font-size: 22px;
  color: ${Variables.colorPureBlack};
  width: 60px;
  display: flex;
  justify-content: center;
  text-align: center;
  padding: 0;
  border: none;
  
  &:disabled {
    opacity: 1;
  }
`;
