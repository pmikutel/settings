import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { identity, when, isNil, always, either, isEmpty, dec, inc, gt, lt, pipe, is, cond, T } from 'ramda';
import ReactInterval from 'react-interval';

import {
  Wrapper,
  Button,
  Minus,
  Plus,
  Content,
  Value,
} from './counter.styles';

export class Counter extends PureComponent {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    onChange: PropTypes.func.isRequired,
    min: PropTypes.number,
    max: PropTypes.number,
    center: PropTypes.bool,
  };

  static defaultProps = {
    min: 0,
    max: 999,
    center: false,
  };

  state = {
    callback: null,
    enabled: false,
    timeout: 100,
  };


  onToggleInterval = () => this.setState({ enabled: false });

  onChangeCallback = (type) => {
    this[type]();
    this.setState({ callback: this[type], enabled: true });
  };

  get value() {
    return cond([
      [either(isNil, isEmpty), always(this.props.min)],
      [is(String), parseInt],
      [T, identity],
    ])(this.props.value);
  }

  decreaseValue = pipe(dec, when(gt(this.props.min), always(this.props.min)));
  increaseValue = pipe(inc, when(lt(this.props.max), always(this.props.max)));
  changeValue = pipe(
    when(gt(this.props.min), always(this.props.min)),
    when(lt(this.props.max), always(this.props.max)),
  );

  handleChange = (value) => {
    this.props.onChange(value);
  };

  handleDecrease = () => this.handleChange(this.decreaseValue(this.value));

  handleIncrease = () => this.handleChange(this.increaseValue(this.value));

  render = () => {
    const { callback, enabled, timeout } = this.state;
    const { center, min, max } = this.props;

    return (
      <Wrapper center={center}>
        <ReactInterval {...{ timeout, enabled, callback }} />
        <Content>
          <Button
            type="button"
            onMouseDown={() => this.onChangeCallback('handleDecrease')}
            onMouseUp={this.onToggleInterval}
          >
            <Minus />
          </Button>

          <Value
            type="number"
            onChange={(event) => this.handleChange(this.changeValue(event.target.value))}
            maxLength={max}
            minLength={min}
            value={this.value}
          />

          <Button
            type="button"
            right
            onMouseDown={() => this.onChangeCallback('handleIncrease')}
            onMouseUp={this.onToggleInterval}
          >
            <Plus />
          </Button>
        </Content>
      </Wrapper>
    );
  };
}
