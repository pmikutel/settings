import { defineMessages } from 'react-intl';

export default defineMessages({
  buttonTitle: {
    id: 'chatButton.buttonTitle',
    defaultMessage: 'Chat with us',
  },
  linkTitle: {
    id: 'chatButton.linkTitle',
    defaultMessage: 'Contact us',
  },
});
