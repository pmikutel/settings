import styled, { css, injectGlobal } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Variables } from '../../theme';

export const buttonStyles = css`
  width: ${ifElse(propEq('fullWidth', true), always('100%'), always('184px'))};
`;

export const Link = styled.span`
  cursor: pointer;
  color: ${Variables.colorFluroGreen};
  border-bottom: 1px solid ${Variables.colorFluroGreen};
  display: inline-block;
  line-height: 1;
`;

export const showChat = () => injectGlobal`
  .zopim {
    visibility: visible;
  }
`;
