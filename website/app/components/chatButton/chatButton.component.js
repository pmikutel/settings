import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import { ifElse, prop } from 'ramda';

import messages from './chatButton.messages';

import { buttonStyles, Link, showChat } from './chatButton.styles';
import { Components } from '../../theme';


export class ChatButtonComponent extends PureComponent {
  static defaultProps = {
    link: false,
  };

  static propTypes = {
    link: PropTypes.bool,
    fullWidth: PropTypes.bool,
    intl: PropTypes.object.isRequired,
  };

  renderLink = () => (
    <Link onClick={showChat}><FormattedMessage {...messages.linkTitle} /></Link>
  );

  renderButton = () => (
    <Components.Button
      extraStyles={buttonStyles}
      fullWidth={this.props.fullWidth}
      type="button"
      onClick={showChat}
      noActive
    >
      <FormattedMessage {...messages.buttonTitle} />
    </Components.Button>
  );

  render = () => ifElse(
    prop('link'),
    this.renderLink,
    this.renderButton,
  )(this.props);
}

export const ChatButton = injectIntl(ChatButtonComponent);
