import React from 'react';
import { shallow } from 'enzyme';

import { ChatButtonComponent as ChatButton } from '../chatButton.component';
import { intlMock } from '../../../utils/testing';


describe('<ChatButton />', () => {
  const defaultProps = {
    link: false,
    intl: intlMock(),
  };

  const component = (props = {}) => <ChatButton {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly Button', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly Link', () => {
    const link = true;
    const wrapper = render({ link });

    global.expect(wrapper).toMatchSnapshot();
  });
});
