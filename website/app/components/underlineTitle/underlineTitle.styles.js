import styled, { css } from 'styled-components';
import { Variables, Media } from '../../theme';

const wrapperDesktopStyles = css`
  position: static;
`;

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  flex-direction: column;
  
  ${Media.desktop(wrapperDesktopStyles)};
`;

const titleDesktopStyles = css`
  margin-bottom: 46px;
  line-height: 94px;
`;

export const Title = styled.h1`
  position: relative;
  margin-bottom: 19px;
  line-height: 33px;
  max-width: 1250px;
  overflow-wrap: break-word;
  
  ${Media.desktop(titleDesktopStyles)};
`;

const lineDesktopStyles = css`
  height: 9px;
  width: 87%;
  margin-right: 0;
`;

export const Line = styled.div`
  position: relative;
  background-image: ${Variables.gradientUnderline};
  display: flex;
  align-self: flex-end;
  height: 5px;
  width: 93%;
  margin-right: -16px;
    
  ${Media.desktop(lineDesktopStyles)};
`;

const descriptionDesktopStyles = css`
  padding: 0;
  width: 800px;
`;

const descriptionTabletStyles = css`
  padding: 0 18px 0 32px;
`;

export const Description = styled.div`
  width: auto;
  max-width: 100%;
  margin: 39px auto 79px;
  font-size: 22px;
  line-height: 46px;
  padding-left: 41px;
  
  
  color: ${Variables.colorWarmGrey};
  
  ${Media.tablet(descriptionTabletStyles)};
  ${Media.desktop(descriptionDesktopStyles)};
`;
