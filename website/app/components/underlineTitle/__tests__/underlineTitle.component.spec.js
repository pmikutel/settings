import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { UnderlineTitle } from '../underlineTitle.component';


describe('<UnderlineTitle />', () => {
  const defaultProps = {
    title: 'title',
    description: 'description',
    linkCopy: 'link',
    linkFunc: identity,
    dotsPattern: true,
  };

  const component = (props = {}) => <UnderlineTitle {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
