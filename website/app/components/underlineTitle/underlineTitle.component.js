import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { always, ifElse, prop } from 'ramda';

import {
  Title,
  Line,
  Wrapper,
  Description,
} from './underlineTitle.styles';

import { Components } from '../../theme';

export class UnderlineTitle extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    linkCopy: PropTypes.string,
    linkFunc: PropTypes.func,
    dotsPattern: PropTypes.bool,
  };

  static defaultProps = {
    dotsPattern: true,
  };

  renderDescription = () => ifElse(
    prop('description'),
    () =>
      <Description>
        {this.props.description}
        <Components.LinkButton onClick={() => this.props.linkFunc('login')}>
          {this.props.linkCopy}
        </Components.LinkButton>
      </Description>
    , always(null)
  )(this.props);

  renderDots = () => ifElse(
    prop('dotsPattern'),
    () =>
      <Components.Dots
        width="20%"
        height="1408"
        top="94"
        mobileTop="15"
        mobileHeight="393"
        mobileWidth="79%"
        mobileRight
      />
    , always(null)
  )(this.props);


  render = () => (
    <Wrapper>
      {this.renderDots()}
      <Title
        // eslint-disable-next-line
        dangerouslySetInnerHTML={{ __html: this.props.title }}
      />
      <Line />
      {this.renderDescription()}
    </Wrapper>
  );
}

