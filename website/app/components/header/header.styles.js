import styled, { css } from 'styled-components';
import * as RRD from 'react-router-dom';
import { ifElse, always, propEq, when, prop } from 'ramda';
import Sprite from '../../utils/sprites';
import { Variables, Media, Typography } from '../../theme';
import { colorPureWhite } from '../../theme/variables';

const HEADER_DESKTOP_HEIGHT = '111px';
const HEADER_MOBILE_HEIGHT = '88px';

const getLogoSpriteName = ifElse(propEq('white', true), always('logo-white'), always('logo'));

const getHiddenMenuStyles = when(
  propEq('isVisible', false),
  () => css`
    transform: translateX(100%);
  `,
);

export const columnStyles = css`
  justify-content: center;
`;

const WrapperDesktopDesktopStyles = css`
  display: flex;
  align-items: center;
  transform: ${ifElse(propEq('show', true), always('translateY(0)'), always(`translateY(-${HEADER_DESKTOP_HEIGHT})`))};
`;

const wrapperMobileDesktopStyles = css`
  display: none;
`;

const linkDesktopStyles = css`
  color: ${Variables.colorBlack};
`;

export const WrapperDesktop = styled.div`
  display: none;
  z-index: 11;
  position: fixed;
  width: 100%;
  height: ${HEADER_DESKTOP_HEIGHT};
  background-color: ${ifElse(propEq('top', false), always(Variables.colorPureWhite), always('transparent'))};
  transition: all 1s cubic-bezier(.55,.085,0,.99);

  ${Media.desktop(WrapperDesktopDesktopStyles)}
`;

export const WrapperMobile = styled.div`
  margin-bottom: 30px;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 11;
  width: 100%;
  height: ${HEADER_MOBILE_HEIGHT};
  display: flex;
  align-items: center;
  background-color: ${ifElse(propEq('top', false), always(Variables.colorPureWhite), always('transparent'))};
  transform: ${ifElse(propEq('show', true), always('translateY(0)'), always(`translateY(-${HEADER_MOBILE_HEIGHT})`))};
  transition: all 1s cubic-bezier(.55,.085,0,.99);

  ${Media.desktop(wrapperMobileDesktopStyles)}
`;

export const mobileRowStyles = css`
  justify-content: space-between;
  align-items: center;
`;

export const hamburgerColumnStyles = css`
  align-items: flex-end;
`;

export const Logo = styled.h1`
  transform: scale(0.75) translateX(-15%);
  outline: transparent;
  ${props => Sprite.responsive(getLogoSpriteName(props))}
`;

export const Button = styled.button`
  position: relative;
  color: ${Variables.colorPureWhite};
  padding: 0;
  font-family: ${Variables.lyonLightFontFamily};
  transition: color 0.2s;

  ${Media.desktop(linkDesktopStyles)}

  &:hover {
    color: ${Variables.colorMintyGreen};
  }
`;

export const SubMenu = styled.ul`
  box-shadow: 0 0 5px 1px rgba(0,0,0,0.1);
  list-style: none;
  display: none;
  align-items: center;
  position: absolute;
  width: 184px;
  top: 43px;
  z-index: 3;
  background: #fff;
`;

export const SubMenuLink = styled(RRD.Link)`
  color: ${Variables.colorWarmGrey};
  padding: 22px 18px;
  width: 100%;
  display: block;
  box-sizing: border-box;
  text-align: left;
  transition: color 0.2s;

  &:hover {
    color: ${Variables.colorMintyGreen};
  }
`;

export const SubMenuButton = styled.button`
  color: ${Variables.colorWarmGrey};
  display: block;
  padding: 22px 18px;
  text-align: left;
  width: 100%;
  transition: color 0.2s;

  ${Typography.fontLyonLight}

  &:hover {
    color: ${Variables.colorMintyGreen};
  }
`;

export const SubMenuItem = styled.li`
  display: block;
`;

export const UserLink = styled.span`
  display: inline-block;
  position: relative;
  color: ${Variables.colorBlack};
  margin-top: 25px;
  padding-bottom: 25px;
  transition: color 0.2s;

  &:hover{
    color: ${Variables.colorMintyGreen};
    cursor: pointer;

    ${SubMenu} {
      display: block;
    }
  }
`;

const userLinkInnerTabletStyle = styled.span`
  width: 145px;
`;

export const UserLinkInner = styled.button`
  ${Typography.fontLyonLight};
  cursor: pointer;
  display: block;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 175px;

  ${Media.tabletLandscape(userLinkInnerTabletStyle)}
`;

const getEmptyArrowStyles = when(
  propEq('empty', 'true'),
  () => css`
    left: -1px;
    right: unset;
    transform: rotate(-225deg);
  `,
);

export const Arrow = styled.span`
  display: block;
  width: 18px;
  height: 0;
  border-bottom: 2px solid ${colorPureWhite};
  transform: translateY(-50%);
  position: absolute;
  right: 0px;
  top: 0px;
  transform: rotate(${ifElse(propEq('left', true), always(180), always(0))}deg);

  &:after {
    border-color: ${colorPureWhite};
    border: solid;
    border-width: 0 2px 2px 0;
    display: inline-block;
    padding: 5px;
    content: '';
    position: absolute;
    right: -1px;
    top: -5px;
    transform: rotate(-45deg) translateZ(-1000px);

    ${getEmptyArrowStyles}
  }

  ${prop('extraStyles')}
`;

export const ArrowWrapper = styled.div`
  display: block;
  position: absolute;
  right: 20px;
  transform: translateY(-12px);
`;

export const Link = styled(RRD.Link)`
  position: relative;
  color: ${Variables.colorPureWhite};
  transition: color 0.2s;

  ${Media.desktop(linkDesktopStyles)}

  &:hover {
    color: ${Variables.colorMintyGreen};
  }
`;

export const List = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const listItemRightDesktopStyle = css`
  margin-right: 40px;
`;

const listItemRightDesktopWideStyle = css`
  margin-right: 60px;
`;

export const listItemRightStyle = css`
  flex: 1;
  text-align: right;

  ${Media.desktop(listItemRightDesktopStyle)}
  ${Media.desktopWide(listItemRightDesktopWideStyle)}
`;

export const listItemCartStyle = css`
  flex: 0;
  text-align: right;
`;

export const ListItem = styled.li`
  display: block;
  margin-right: ${ifElse(propEq('noMargin', true), always(0), always(60))}px;
  position: relative;

  ${prop('extraStyles')}
`;

export const Cart = styled.i`
  display: block;
  ${Sprite.responsive('cart')};
`;

export const CartHover = styled.i`
  display: block;
  opacity: 0;
  transition: opacity 0.2s;

  &:hover {
    opacity: 1;
  }

  ${Sprite.responsive('cart-hover')};
`;

export const CloseIcon = styled.i`
  display: ${ifElse(propEq('isActive', true), always('block'), always('none'))};

  ${Sprite.responsive('close-white')};
`;

export const Hamburger = styled.button`
  position: relative;
  width: 30px;
  height: 30px;
  margin-right: 16px;
  border: 0;
  border-top: 2px solid ${Variables.colorBlack};
  transition: border-color 0.2s;

  &:after,
  &:before {
    display: block;
    content: '';
    position: absolute;
    background-color: ${Variables.colorBlack};
    transform: translateY(-50%);
    height: 2px;
    width: 100%;
    top: 100%;
    left: 0;
    transition: background-color 0.2s;
  }

  &:before {
    top: 50%;
  }

  display: ${ifElse(propEq('isActive', true), always('none'), always('block'))};
`;

export const MobileMenu = styled.div`
  display: flex;
  flex-direction: column;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  z-index: 3;
  background-image: linear-gradient(209deg, #fff104, ${Variables.colorBarbiePink});
  transition: transform ease .3s;

  ${getHiddenMenuStyles}
`;

export const MobileMenuItems = styled.div`
  display: flex;
  flex-direction: column;
`;

export const mobileMenuItemStyles = css`
  ${Typography.fontGothamBook};
  border-top: 1px solid rgba(255, 255, 255, 0.9);
  text-transform: uppercase;
  font-size: 20px;
  display: block;
  letter-spacing: 6px;
  padding: 20px;
  color: ${Variables.colorPureWhite};
  width: 100%;
  text-align: left;
`;

export const MobileMenuItemLink = styled(RRD.Link)`
  ${mobileMenuItemStyles}
`;
export const MobileMenuItemButton = styled.button`
  ${mobileMenuItemStyles}
`;

const wrapperDesktopStyles = css`
  padding-bottom: ${HEADER_DESKTOP_HEIGHT};
`;

export const Wrapper = styled.div`
  ${Media.desktop(wrapperDesktopStyles)}
`;

export const Notification = styled.span`
  color: ${Variables.colorPureWhite};
  padding: 4px;
  border-radius: 50%;
  position: absolute;
  top: -6px;
  left: -6px;
  background-color: #fb5401;
  z-index: 1;
  font-size: 10px;
  min-width: 11px;
  min-height: 11px;
  text-align: center;
`;
