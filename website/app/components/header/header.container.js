import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';

import { Header } from './header.component';
import { ModalsActions } from '../../modules/modals/modals.redux';
import { UserActions } from '../../modules/user/user.redux';
import { selectIsLoggedIn, selectProjectId, selectProfileData } from '../../modules/user/user.selectors';
import { selectItemsNumber } from '../../modules/cart/cart.selectors';

const mapStateToProps = createStructuredSelector({
  isLoggedIn: selectIsLoggedIn,
  profileData: selectProfileData,
  projectId: selectProjectId,
  itemsNumber: selectItemsNumber,
});

export const mapDispatchToProps = (dispatch) => bindActionCreators({
  openModal: ModalsActions.openModal,
  logOut: UserActions.logoutRequest,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
