import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl } from 'react-intl';
import { equals, ifElse, prop, propEq, both, complement, isEmpty, pipe } from 'ramda';

import { MOBILE_ITEMS, ITEMS, USER_ITEMS, getUrl } from './header.helpers';
import { WindowScroll } from '../windowScroll/windowScroll.component';
import messages from './header.messages';
import {
  Wrapper,
  mobileRowStyles,
  WrapperDesktop,
  hamburgerColumnStyles,
  WrapperMobile,
  columnStyles,
  Logo,
  Button,
  Arrow,
  ArrowWrapper,
  Link,
  List,
  ListItem,
  listItemRightStyle,
  listItemCartStyle,
  Cart,
  CartHover,
  CloseIcon,
  Hamburger,
  UserLink,
  UserLinkInner,
  SubMenu,
  SubMenuItem,
  SubMenuLink,
  SubMenuButton,
  MobileMenu,
  MobileMenuItemLink,
  MobileMenuItemButton,
  MobileMenuItems,
  Notification,
} from './header.styles';
import { Grid } from '../../theme';


export class HeaderComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    openModal: PropTypes.func,
    logOut: PropTypes.func,
    isLoggedIn: PropTypes.bool.isRequired,
    userInfo: PropTypes.string,
    profileData: PropTypes.object,
    projectId: PropTypes.string,
    itemsNumber: PropTypes.number.isRequired,
  };

  static defaultProps = {
    userInfo: '',
    profileData: {},
    projectId: '',
  };

  state = {
    isVisible: false,
    showHeader: true,
    isTop: true,
  };

  get profileName() {
    return this.props.profileData ? this.props.profileData.get('lastName') : null;
  }

  toggleMenu = () => this.setState(({ isVisible }) => ({
    isVisible: !isVisible,
  }));

  scrollHandler = (showHeader, isTop) => this.setState({ showHeader, isTop });

  renderInspiredButton = () => ifElse(
    both(pipe(prop('projectId'), complement(isEmpty)), propEq('isLoggedIn', true)),
    () =>
      <Button
        onClick={() => this.props.openModal('project')}
        title={this.props.intl.formatMessage(messages.inspired)}
      >
        <FormattedMessage {...messages.inspired} />
      </Button>,
    () =>
      <Link
        to="/project/step1"
        title={this.props.intl.formatMessage(messages.inspired)}
      >
        <FormattedMessage {...messages.inspired} />
      </Link>,
  )(this.props);

  renderInspiredMobileButton = () => ifElse(
    both(pipe(prop('projectId'), complement(isEmpty)), propEq('isLoggedIn', true)),
    () =>
      <MobileMenuItemButton
        onClick={() => this.props.openModal('project')}
        title={this.props.intl.formatMessage(messages.inspired)}
      >
        <FormattedMessage {...messages.inspired} />
        <ArrowWrapper>
          <Arrow />
        </ArrowWrapper>
      </MobileMenuItemButton>,
    () =>
      <MobileMenuItemLink
        to="/project/step1"
        title={this.props.intl.formatMessage(messages.inspired)}
      >
        <FormattedMessage {...messages.inspired} />
        <ArrowWrapper>
          <Arrow />
        </ArrowWrapper>
      </MobileMenuItemLink>,
  )(this.props);

  renderMobileMenuHeader = (isLogoWhite) => (
    <Grid.Row extraStyles={mobileRowStyles}>
      <Grid.Column mobileColumns={2}>
        <Link to="/" title={this.props.intl.formatMessage(messages.home)}>
          <Logo white={isLogoWhite} />
        </Link>
      </Grid.Column>

      <Grid.Column mobileColumns={2} extraStyles={hamburgerColumnStyles}>
        <Hamburger onClick={this.toggleMenu} isActive={this.state.isVisible} />
        <CloseIcon onClick={this.toggleMenu} isActive={this.state.isVisible} />
      </Grid.Column>
    </Grid.Row>
  );

  renderItems = () => ITEMS.map(({ href, message }, index) => (
    <ListItem key={index} noMargin={equals(ITEMS.length - 1)(index)}>
      <Link
        to={href}
        title={this.props.intl.formatMessage(message)}
      >
        <FormattedMessage {...message} />
      </Link>
    </ListItem>
  ));

  renderMobileItems = () => MOBILE_ITEMS.map(({ id, href, message }, index) => (
    <MobileMenuItemLink
      key={index}
      to={getUrl(id, href, this.props.projectId)}
      title={this.props.intl.formatMessage(message)}
    >
      <FormattedMessage {...message} />
      <ArrowWrapper>
        <Arrow />
      </ArrowWrapper>
    </MobileMenuItemLink>
  ));

  renderMobileUserItems = () => (
    <div>
      <MobileMenuItemLink to="/user/account" title={this.props.intl.formatMessage(messages.settings)}>
        <FormattedMessage {...messages.settings} />
        <ArrowWrapper>
          <Arrow />
        </ArrowWrapper>
      </MobileMenuItemLink>
      <MobileMenuItemButton onClick={() => this.props.logOut()} title={this.props.intl.formatMessage(messages.logout)}>
        <FormattedMessage {...messages.logout} />
      </MobileMenuItemButton>
    </div>
  );

  renderMobileLogin = () => (
    <MobileMenuItemButton
      onClick={() => this.props.openModal('login')}
      title={this.props.intl.formatMessage(messages.login)}
    >
      <FormattedMessage {...messages.login} />
    </MobileMenuItemButton>
  );

  renderMobileUserActions = () => ifElse(
    prop('isLoggedIn'),
    this.renderMobileUserItems,
    this.renderMobileLogin,
  )(this.props);

  renderUserMenuItems = () => USER_ITEMS.map(({ id, href, message }, index) => (
    <SubMenuItem key={index}>
      <SubMenuLink
        to={getUrl(id, href, this.props.projectId)}
        title={this.props.intl.formatMessage(message)}
      >
        <FormattedMessage {...message} />
      </SubMenuLink>
    </SubMenuItem>
  ));

  renderUserMenu = () => (
    <ListItem extraStyles={listItemRightStyle}>
      <UserLink>
        <UserLinkInner>
          {this.profileName}
        </UserLinkInner>
        <SubMenu>
          {this.renderUserMenuItems()}
          <SubMenuItem>
            <SubMenuButton
              onClick={() => this.props.logOut()}
              title={this.props.intl.formatMessage(messages.logout)}
            >
              <FormattedMessage {...messages.logout} />
            </SubMenuButton>
          </SubMenuItem>
        </SubMenu>
      </UserLink>
    </ListItem>
  );

  renderLoginButton = () => (
    <ListItem extraStyles={listItemRightStyle}>
      <Button
        onClick={() => this.props.openModal('login')}
        title={this.props.intl.formatMessage(messages.login)}
      >
        <FormattedMessage {...messages.login} />
      </Button>
    </ListItem>
  );

  renderUserActions = () => ifElse(
    prop('isLoggedIn'),
    this.renderUserMenu,
    this.renderLoginButton,
  )(this.props);

  renderDesktopLayout = () => (
    <WrapperDesktop show={this.state.showHeader} top={this.state.isTop}>
      <Grid.Section key="desktop">
        <Grid.Row>
          <Grid.Column offset={1} columns={2}>
            <Link to="/" title={this.props.intl.formatMessage(messages.home)}>
              <Logo />
            </Link>
          </Grid.Column>

          <Grid.Column columns={9} extraStyles={columnStyles}>
            <List>
              <ListItem>
                {this.renderInspiredButton()}
              </ListItem>
              {this.renderItems()}
            </List>
          </Grid.Column>

          <Grid.Column columns={3} extraStyles={columnStyles}>
            <List>
              {this.renderUserActions()}

              <ListItem extraStyles={listItemCartStyle} noMargin title={this.props.intl.formatMessage(messages.cart)}>
                {
                  this.props.itemsNumber > 0 &&
                  <Notification>{this.props.itemsNumber}</Notification>
                }

                <Link to="/purchase/cart">
                  <Cart>
                    <CartHover />
                  </Cart>
                </Link>
              </ListItem>
            </List>
          </Grid.Column>
        </Grid.Row>
      </Grid.Section>
    </WrapperDesktop>
  );

  renderMobileLayout = () => (
    <WrapperMobile show={this.state.showHeader} top={this.state.isTop}>
      <Grid.Section>
        {this.renderMobileMenuHeader()}

        <MobileMenu isVisible={this.state.isVisible}>
          {this.renderMobileMenuHeader(true)}

          <MobileMenuItems onClick={this.toggleMenu}>
            {this.renderInspiredMobileButton()}
            {this.renderMobileItems()}
            {this.renderMobileUserActions()}
          </MobileMenuItems>
        </MobileMenu>
      </Grid.Section>
    </WrapperMobile>
  );

  render = () => (
    <Wrapper>
      <WindowScroll onScroll={this.scrollHandler} header />
      {this.renderDesktopLayout()}
      {this.renderMobileLayout()}
    </Wrapper>
  );
}

export const Header = injectIntl(HeaderComponent);
