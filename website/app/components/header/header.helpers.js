import { ifElse, always, equals } from 'ramda';
import messages from './header.messages';

export const USER_ITEMS = [
  {
    id: 'board',
    href: '/project/board',
    message: messages.myProject,
  },
  {
    id: 'account',
    href: '/user/account',
    message: messages.account,
  },
];

export const ITEMS = [
  {
    id: 'all',
    href: '/products/all',
    message: messages.products,
  },
];

export const MOBILE_ITEMS = [
  {
    id: 'board',
    href: '/project/board',
    message: messages.project,
  },
  {
    id: 'cart',
    href: '/purchase/cart',
    message: messages.basket,
  },
  {
    id: 'all',
    href: '/products/all',
    message: messages.products,
  },
];

export const getUrl = (id, href, projectId) => ifElse(
  equals('board'),
  always(`${href}/${projectId}`),
  always(href),
)(id);
