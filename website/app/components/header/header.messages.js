import { defineMessages } from 'react-intl';

export default defineMessages({
  home: {
    id: 'header.home',
    defaultMessage: 'Home',
  },
  inspired: {
    id: 'header.inspired',
    defaultMessage: 'Get inspired',
  },
  products: {
    id: 'header.products',
    defaultMessage: 'Our products',
  },
  login: {
    id: 'header.login',
    defaultMessage: 'Sign in',
  },
  logout: {
    id: 'header.logout',
    defaultMessage: 'Sign out',
  },
  cart: {
    id: 'header.cart',
    defaultMessage: 'Cart',
  },
  inspiration: {
    id: 'header.inspiration',
    defaultMessage: 'inspiration',
  },
  project: {
    id: 'header.project',
    defaultMessage: 'Your project',
  },
  basket: {
    id: 'header.basket',
    defaultMessage: 'Your basket',
  },
  settings: {
    id: 'header.settings',
    defaultMessage: 'settings',
  },
  myProject: {
    id: 'header.myProject',
    defaultMessage: 'My project',
  },
  account: {
    id: 'header.account',
    defaultMessage: 'My account',
  },
});
