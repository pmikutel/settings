import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { identity } from 'ramda';
import { fromJS } from 'immutable';

import { intlMock } from '../../../utils/testing';
import { HeaderComponent as Header } from '../header.component';
import { Hamburger, SubMenu, Button } from '../header.styles';

describe('<Header />', () => {
  const defaultProps = {
    intl: intlMock(),
    isLoggedIn: false,
    openModal: identity,
    logOut: identity,
    profileData: fromJS({}),
    projectId: '1212',
    itemsNumber: 1,
  };

  const component = (props = {}) => <Header {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render for logged user correctly', () => {
    const wrapper = render({ isLoggedIn: true });

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should render for not logged user correctly', () => {
    const wrapper = render({ isLoggedIn: false });

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('active <Hamburger />', () => {
    it('should toggle isVisible state', () => {
      const wrapper = render();
      const isVisible = wrapper.state('isVisible');

      wrapper.find(Hamburger).at(0).simulate('click');

      expect(isVisible).not.to.equal(wrapper.state('isVisible'));
    });
  });

  describe('inactive <Hamburger />', () => {
    it('should toggle isVisible state', () => {
      const wrapper = render();
      const isVisible = wrapper.state('isVisible');

      wrapper.find(Hamburger).at(1).simulate('click');

      expect(isVisible).not.to.equal(wrapper.state('isVisible'));
    });
  });

  describe('user is not logged in', () => {
    it('should not render <SubMenu />', () => {
      const wrapper = render({ isLoggedIn: false });
      const subMenu = wrapper.find(SubMenu);

      expect(subMenu).to.have.been.length(0);
    });

    it('should render login <Button />', () => {
      const wrapper = render({ isLoggedIn: false });
      const button = wrapper.find(Button);

      expect(button).to.have.been.length(1);
    });
  });

  describe('user is logged in', () => {
    it('should render <SubMenu />', () => {
      const wrapper = render({ isLoggedIn: true });
      const subMenu = wrapper.find(SubMenu);

      expect(subMenu).to.have.been.length(1);
    });

    it('should not render login <Button />', () => {
      const wrapper = render({ isLoggedIn: true, projectId: '' });
      const button = wrapper.find(Button);

      expect(button).to.have.been.length(0);
    });

    it('should render inspired <Button /> when projectId is known', () => {
      const wrapper = render({ isLoggedIn: true });
      const button = wrapper.find(Button);

      expect(button).to.have.been.length(1);
    });
  });
});
