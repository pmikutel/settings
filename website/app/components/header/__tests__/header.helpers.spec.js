import { expect } from 'chai';

import { getUrl } from '../header.helpers';

describe('Header: helpers', () => {
  describe('getUrl', () => {
    it('should return same href as passed when not board passed', () => {
      expect(getUrl('testId', '/test', '123')).to.equal('/test');
    });

    it('should return href with id as passed when board passed', () => {
      expect(getUrl('board', '/test', '123')).to.equal('/test/123');
    });
  });
});
