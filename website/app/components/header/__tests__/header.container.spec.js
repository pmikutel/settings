import { expect } from 'chai';
import { spy } from 'sinon';

import { mapDispatchToProps } from '../header.container';
import { ModalsActions } from '../../../modules/modals/modals.redux';
import { UserActions } from '../../../modules/user/user.redux';

describe('Header: Container', () => {
  describe('mapDispatchToProps', () => {
    it('should call ModalsActions.openModal', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).openModal();

      expect(dispatch).to.have.been.calledWith(ModalsActions.openModal());
    });

    it('should call UserActions.logoutRequest', () => {
      const dispatch = spy();

      mapDispatchToProps(dispatch).logOut();

      expect(dispatch).to.have.been.calledWith(UserActions.logoutRequest());
    });
  });
});
