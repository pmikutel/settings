import React from 'react';
import { shallow } from 'enzyme';

import { FilesList } from '../filesList.component';

describe('<FilesList />', () => {
  const defaultProps = {
    items: [{ name: 'file1.ext' }, { name: 'file2.ext' }],
  };

  const component = (props = {}) => <FilesList {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
