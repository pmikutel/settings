import styled from 'styled-components';
import { prop } from 'ramda';
import Sprites from '../../utils/sprites';
import { Variables, Typography } from '../../theme';

export const Wrapper = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
  padding: 0;
  margin: 0;
  
  ${prop('extraStyles')};
`;

export const File = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  ${Typography.fontLyonLight};
  font-size: 22px;
  line-height: 1;
  color: ${Variables.colorMintyGreen};
  
  &:after {
    margin-left: 15px;
    content: '';
    display: block;
    ${Sprites.responsive('done')}
  }
`;
