import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Wrapper, File } from './filesList.styles';

export class FilesList extends PureComponent {
  static propTypes = {
    items: PropTypes.array.isRequired,
    listStyles: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
    ]),
  };

  static defaultProps = {
    items: [],
    listStyles: '',
  };

  renderItems = () => this.props.items.map(({ name }, index) => (
    <File key={index}>{name}</File>
  ));

  render = () => (
    <Wrapper extraStyles={this.props.listStyles}>
      {this.renderItems()}
    </Wrapper>
  );
}
