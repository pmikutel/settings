import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { StickyButton } from '../stickyButton.component';


describe('<StickyButton />', () => {
  const defaultProps = {
    action: identity,
    copy: 'copy',
    disabled: false,
    loading: false,
  };

  const component = (props = {}) => <StickyButton {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
