import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash/throttle';
import inViewport from 'in-viewport';
import { always, ifElse, propEq } from 'ramda';

import { Components } from '../../theme';
import { BottomElement } from './stickyButton.styles';
import { DotsLoader } from '../dotsLoader/dotsLoader.component';


export class StickyButton extends PureComponent {
  static propTypes = {
    action: PropTypes.func,
    copy: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
  };

  static defaultProps = {
    action: () => {},
    disabled: false,
    loading: false,
  };

  state = {
    sticky: true,
    show: false,
    windowPosition: 0,
  };

  componentDidMount() {
    document.addEventListener('scroll', this.throttleScroll);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.throttleScroll);
  }

  throttleScroll = throttle((e) => this.handleScroll(e), 100);

  containerRef = {};

  handleScroll = () => {
    const isVisible = !inViewport(this.containerRef);
    if (!this.state.show && this.state.windowPosition < window.pageYOffset) {
      this.setState({ show: true, windowPosition: window.pageYOffset });
    }

    if (this.state.sticky !== isVisible) {
      this.setState({ sticky: isVisible, show: true });
    }
  };

  handleContainerRef = ref => (this.containerRef = ref);

  renderLoader = () => ifElse(
    propEq('loading', true),
    always(<DotsLoader relative={true} />),
    always(null),
  )(this.props);

  render = () => (
    <div>
      <Components.StickyButton
        type="submit"
        onClick={this.props.action}
        disabled={this.props.disabled || this.props.loading}
        sticky={this.state.sticky}
        show={this.state.show}
      >
        {this.props.copy}
        {this.renderLoader()}
      </Components.StickyButton>
      <BottomElement innerRef={this.handleContainerRef} />
    </div>
  );
}

