import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { ifElse, always, propEq } from 'ramda';

import { InfoBox } from '../infoBox/infoBox.component';

import { Grid, Components } from '../../theme';
import {
  AnimBackground,
  HeadTitle,
  HeadSubtitle,
  HeroImage,
  BackIcon,
  sectionStyles,
  getImageStyles,
  rowStyles,
  contentColumnStyles,
  stepsRowStyles,
  stepInfoColumnStyles,
  smallButtonExtraStyles,
  InfoBoxWrapper,
} from './staticHeroImage.styles';

import { animateHeroImage } from '../../utils/animations';

export class StaticHeroImage extends PureComponent {
  static defaultProps = {
    shouldShowStatus: false,
    status: '',
    showBackIcon: false,
    heroImage: '',
  };

  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    shouldShowStatus: PropTypes.bool,
    status: PropTypes.string,
    showBackIcon: PropTypes.bool,
    heroImage: PropTypes.string,
  };

  static contextTypes = {
    router: PropTypes.object.isRequired,
  };

  state = {
    animated: false,
  };

  componentWillReceiveProps(nextProps) {
    if (!this.state.animated && nextProps.heroImage.length > 0) {
      const img = new Image();
      img.src = nextProps.heroImage;

      img.onload = () => {
        animateHeroImage(this.backgroundElement, this.imageElement, this.headSubtitleElement, this.headTitleElement);
        this.setState({ animated: true });
      };
    }
  }

  handleBackgroundRef = (ref) => (this.backgroundElement = ref);
  handleImageRef = (ref) => (this.imageElement = ref);
  handleHeadSubtitleRef = (ref) => (this.headSubtitleElement = ref);
  handleTitleRef = (ref) => (this.headTitleElement = ref);

  backgroundElement = null;
  imageElement = null;
  headSubtitleElement = null;
  headTitleElement = null;

  renderStepInfo = () => ifElse(
    propEq('shouldShowStatus', true),
    () => <InfoBox copy={this.props.status} />,
    always(null),
  )(this.props);

  renderBackIcon = () => ifElse(
    propEq('showBackIcon', true),
    () => (
      <Components.Button onClick={this.context.router.history.goBack} extraStyles={smallButtonExtraStyles}>
        <Components.Arrow left />
      </Components.Button>
    ),
    always(null),
  )(this.props);

  render = () => (
    <Grid.Section extraStyles={sectionStyles}>
      <Components.Dots
        width="30%"
        height="1066"
        top="167"
        mobileTop="260"
        mobileHeight="795"
        mobileWidth="79%"
      />
      <Grid.Row extraStyles={rowStyles}>
        <Grid.Column
          extraStyles={getImageStyles(this.props.heroImage)}
          offset={3}
          columns={13}
          mobileOffset={1}
          mobileColumns={5}
        >
          <AnimBackground innerRef={this.handleBackgroundRef} />
          <HeroImage innerRef={this.handleImageRef} src={this.props.heroImage} />
        </Grid.Column>

        <Grid.Column
          extraStyles={contentColumnStyles}
          offset={1}
          columns={15}
          mobileOffset={0}
          mobileColumns={5}
        >
          <HeadSubtitle innerRef={this.handleHeadSubtitleRef}>
            {this.props.subtitle}
          </HeadSubtitle>
          <HeadTitle innerRef={this.handleTitleRef}>
            {this.props.title}
          </HeadTitle>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row extraStyles={stepsRowStyles}>
        <Grid.Column offset={1} columns={15} mobileColumns={1} mobileOffset={0} extraStyles={stepInfoColumnStyles}>
          <BackIcon>
            {this.renderBackIcon()}
          </BackIcon>
          <InfoBoxWrapper>
            {this.renderStepInfo()}
          </InfoBoxWrapper>
        </Grid.Column>
      </Grid.Row>
    </Grid.Section>
  );
}
