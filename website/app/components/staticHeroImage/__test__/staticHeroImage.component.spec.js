import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import 'jest-styled-components';

import { StaticHeroImage } from '../staticHeroImage.component';
import { InfoBox } from '../../infoBox/infoBox.component';

describe('<StaticHeroImage />', () => {
  const defaultProps = {
    shouldShowStatus: false,
    status: 'example status',
    showBackIcon: false,
    backUrl: 'http://example.url',
    heroImage: 'example image',
    title: 'example title',
    subtitle: 'example subtitle',
    copy: 'example copy',
  };

  const component = (props = {}) => <StaticHeroImage {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();
    global.expect(wrapper).toMatchSnapshot();
  });

  it('should render <InfoBox /> if shouldShowStatus is true', () => {
    const wrapper = render({ shouldShowStatus: true });
    const infoBoxWrapper = wrapper.find(InfoBox);

    expect(infoBoxWrapper).to.have.length(1);
  });

  it('should not render <InfoBox /> if shouldShowStatus is false', () => {
    const wrapper = render({ shouldShowStatus: false });
    const infoBoxWrapper = wrapper.find(InfoBox);

    expect(infoBoxWrapper).to.have.length(0);
  });
});
