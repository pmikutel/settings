import styled, { css } from 'styled-components';
import { Media, Variables } from '../../theme';

const stepsRowDesktopStyles = css`
  top: 32px;
  left: 0;
  height: auto;
`;

export const stepsRowStyles = css`
  position: absolute;
  height: 100%;
  top: 0;

  ${Media.desktop(stepsRowDesktopStyles)}
`;

export const rowStyles = css`
  height: 100%;
  position: relative;
  width: 100%;
`;

const contentColumnDesktopStyles = css`
  justify-content: center;
  align-items: flex-start;
  top: 0;
`;

export const contentColumnStyles = css`
  position: absolute;
  top: 155px;
  left: 0;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  ${Media.desktop(contentColumnDesktopStyles)}
`;

export const getImageStyles = () => css`
  height: 100%;
  position: relative;
`;

export const HeroImage = styled.div`
  background: url(${props => props.src}) no-repeat center;
  background-size: cover;
  height: 100%;
  position: relative;
  opacity: 0;
`;

const headTitleDesktopStyles = css`
  line-height: 94px;
  left: 0;
`;

export const HeadTitle = styled.h1`
  position: relative;
  line-height: 33px;
  left: 40px;
  opacity: 0;

  ${Media.desktop(headTitleDesktopStyles)}
`;

const headSubtitleDesktopStyles = css`
  font-size: 22px;
  left: 0;
`;

export const HeadSubtitle = styled.div`
  position: relative;
  font-size: 18px;
  line-height: 50px;
  left: 40px;
  opacity: 0;

  ${Media.desktop(headSubtitleDesktopStyles)}
`;

const backIconDesktopStyles = css`
  position: relative;
  display: inline-block;
  left:0;
  bottom: 0;
  margin-right: 27px;
`;

export const BackIcon = styled.div`
  height: 64px;
  width: 66px;
  position: absolute;
  left: 20px;
  bottom: -30px;
  z-index: 1;

  ${Media.desktop(backIconDesktopStyles)}
`;

const sectionDesktopStyles = css`
  height: 800px;
`;

export const sectionStyles = css`
  height: 560px;

  ${Media.desktop(sectionDesktopStyles)};
`;

const stepInfoColumnDesktopStyles = css`
  flex-direction: row;
`;

export const stepInfoColumnStyles = css`
  flex-direction: column;

  ${Media.desktop(stepInfoColumnDesktopStyles)}
`;

const infoBoxWrapperDesktopStyles = css`
  position: relative;
  left: auto;
  bottom: auto;
`;

const infoBoxWrapperTabletStyles = css`
  bottom: 80px;
`;

export const InfoBoxWrapper = styled.div`
  position: absolute;
  left: 0;
  bottom: 90px;

  ${Media.tablet(infoBoxWrapperTabletStyles)}
  ${Media.desktop(infoBoxWrapperDesktopStyles)}
`;

export const smallButtonExtraStyles = css`
  padding: 0;
  height: 100%;
  width: 100%;
`;

export const AnimBackground = styled.span`
  background-color: ${Variables.colorProductGrey};
  position: absolute;
  top: 0;
  right: 0;
  height: 100%;
  width: 0%;
`;
