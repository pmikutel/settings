import styled from 'styled-components';
import ReactSelect from 'react-select';
import { always, ifElse, propEq } from 'ramda';
import { Variables, Typography } from '../../theme';


export const Select = styled(ReactSelect)`
  position: relative;
  font-size: 22px;
  z-index: ${ifElse(propEq('upper', true), always('11'), always('10'))};
  text-transform: uppercase;
  letter-spacing: 12px;
  ${Typography.fontGothamBook};

  .Select-control,
  .Select-menu-outer {
    border-bottom: 1px solid ${Variables.colorBorderGrayWithOpacity};
    width: 100%;
    box-sizing: border-box;
    padding-bottom: 10px;
  }
  
  .Select-value-label {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: block;
  }

  .Select-control {
    line-height: 44px;
    height: 45px;
    position: relative;

    &:after {
      display: inline-block;
      content: '';
      position: absolute;
      right: 4px;
      top: 20px;
      transform: rotateZ(45deg) translateY(-50%);
      border: solid black;
      border-width: 0 2px 2px 0;
      padding: 4px;
      transition: 0.2s transform;
    }
    .Select-input{
      position: absolute;
    }
  }

  .Select-menu {
    background-color: ${Variables.colorPureWhite};
    z-index: 10;
  }

  .Select-menu-outer {
    background-color: ${Variables.colorPureWhite};
    position: absolute;
    top: 50px;
    left: 0;
    padding: 10px 0;
    max-height: 215px;
    overflow-y: scroll;
  }

  .Select-option {
    cursor: pointer;
    margin-bottom: 5px;

    &:hover {
      color: ${Variables.colorWarmGrey};
    }
  }

  &.is-open {
    .Select-control {
      &:after {
        top: 18px;
        transform: rotate(-135deg) translate(-50%, 0%);
      }
  }
`;
