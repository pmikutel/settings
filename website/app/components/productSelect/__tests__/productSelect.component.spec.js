import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { ProductSelect } from '../productSelect.component';

describe('<ProductSelect />', () => {
  const defaultProps = {
    value: '',
    onUpdate: identity,
    options: [{ value: 1, label: 1 }],
    placeholder: '1',
    upper: false,
  };

  const component = (props = {}) => <ProductSelect {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
