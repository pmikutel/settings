import React, { PureComponent } from 'react';
import { path } from 'ramda';
import PropTypes from 'prop-types';
import { Select } from './productSelect.styles';

export class ProductSelect extends PureComponent {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onUpdate: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    placeholder: PropTypes.string.isRequired,
    upper: PropTypes.bool,
  };

  state = {
    option: this.props.value,
    upper: false,
  };

  componentWillReceiveProps() {
    this.setState({
      option: this.props.value,
    });
  }

  handleChange = (selectedOption) => {
    const option = selectedOption.value === 'All' ? { value: '', label: '' } : selectedOption;

    this.props.onUpdate(option.value);
  };

  get options() {
    const options = this.props.options;
    if (path([0, 'value'])(options) !== 'All') {
      options.unshift({ value: 'All', label: 'All' });
    }
    return options;
  }

  render = () => (
    <Select
      name="product-select"
      upper={this.props.upper}
      onChange={this.handleChange}
      value={this.state.option}
      searchable={false}
      clearable={false}
      placeholder={this.props.placeholder}
      options={this.options}
    />
  );
}
