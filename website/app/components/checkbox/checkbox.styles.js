import styled from 'styled-components';
import { Variables, Typography } from '../../theme';


export const Wrapper = styled.button`
  padding: 0;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

export const Label = styled.span`
  position: relative;
  line-height: 1;
  ${Typography.fontLyonLight};
  color: ${Variables.colorWarmGrey};
  font-size: 22px;
  top: 1px;
`;
