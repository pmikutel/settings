import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { complement, when, always, is } from 'ramda';

import {
  Wrapper,
  Label,
} from './checkbox.styles';
import { Forms } from '../../theme';

export class Checkbox extends PureComponent {
  static propTypes = {
    value: PropTypes.any,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func.isRequired,
  };

  get value() {
    return when(
      complement(is(Boolean)),
      always(false),
    )(this.props.value);
  }

  handleClick = () => {
    this.props.onBlur();
    this.props.onChange(!this.value);
  };

  render = () => (
    <Wrapper type="button" onClick={this.handleClick}>
      <Forms.Square isActive={this.value} />

      <Label>{this.props.label}</Label>
    </Wrapper>
  );
}
