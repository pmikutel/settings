import React from 'react';
import { shallow } from 'enzyme';
import { spy } from 'sinon';
import { identity } from 'ramda';
import { expect } from 'chai';

import { Checkbox } from '../checkbox.component';

describe('<Checkbox />', () => {
  const defaultProps = {
    value: false,
    label: 'Test',
    onChange: identity,
    onBlur: identity,
  };

  const component = (props = {}) => <Checkbox {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should call onChange with inversed value when user click', () => {
    const onChange = spy();
    const value = true;
    const wrapper = render({ onChange, value });

    wrapper.simulate('click');

    expect(onChange).to.have.been.calledWith(!value);
  });

  it('should call onBlur when user click', () => {
    const onBlur = spy();
    const wrapper = render({ onBlur });

    wrapper.simulate('click');

    expect(onBlur).to.have.been.called;
  });
});
