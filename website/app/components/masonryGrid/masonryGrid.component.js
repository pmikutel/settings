import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Masonry from 'react-masonry-component';
import { always, and, equals, ifElse, not, path } from 'ramda';
import { debounce } from 'lodash';

import {
  MasonryItem,
  MasonrySizer,
} from './masonryGrid.styles';

import { masonryProps } from './masonryGrid.helpers';

import { WindowResize } from '../../components/windowResize/windowResize.component';

export class MasonryGrid extends PureComponent {
  static propTypes = {
    children: PropTypes.array.isRequired,
    isShifted: PropTypes.bool,
    ignoreFirstItem: PropTypes.bool,
    columns: PropTypes.number,
    containsElementsWithoutImages: PropTypes.bool,
    scroll: PropTypes.bool,
    nrOfNonImageElements: PropTypes.number,
  };

  static defaultProps = {
    isShifted: true,
    ignoreFirstItem: true,
    columns: 2,
    containsElementsWithoutImages: false,
    nrOfNonImageElements: 0,
    scroll: false,
  };

  state = {
    renderedImages: [],
    loadedMasonry: false,
  };

  debouncedMasonryRefresh = debounce(() => {
    this.masonry.layout();
    this.forceUpdate();
  }, 1000);

  handleImagesLoaded = (loadedImages) => {
    let nrOfImageTypeChildren = ifElse(
      equals(true),
      always(path(['children', 1, 'size'], this.props)),
      always(this.props.children.length - this.props.nrOfNonImageElements)
    )(this.props.containsElementsWithoutImages);

    const currentImages = loadedImages.images.map(path(['img', 'currentSrc']));

    if (and(
      equals(loadedImages.images.length, nrOfImageTypeChildren),
      not(equals(currentImages, this.state.renderedImages))
    )) {
      this.masonry.layout();
      this.setState({
        renderedImages: currentImages,
      });
    }

    this.masonry.on('layoutComplete', () => {
      // Hack to refresh animation after grid loaded
      if (!this.state.loadedMasonry && this.props.scroll) {
        window.scrollBy(0, 1);
      }
      this.setState({
        loadedMasonry: true,
      });
    });
  };

  renderGrid = () => React.Children.map(this.props.children, (child, id) => {
    const isLeft = path(['items', id, 'position', 'x'], this.masonry) === 0;
    const enrichedChild = React.cloneElement(
      child,
      { isLeft, loadedMasonry: this.state.loadedMasonry },
    );

    return (
      <MasonryItem
        className="grid-item"
        left={isLeft}
        key={id}
        loadedMasonry={this.state.loadedMasonry}
      >
        {enrichedChild}
      </MasonryItem>);
  });

  render = () => {
    return (
      <Masonry
        onImagesLoaded={this.handleImagesLoaded}
        ref={(c) => {this.masonry = this.masonry || c.masonry;}}
        className="grid"
        options={masonryProps}
      >
        <WindowResize onResize={this.debouncedMasonryRefresh} />
        <MasonrySizer className="grid-sizer" />
        {this.renderGrid()}
      </Masonry>
    );
  };
}


