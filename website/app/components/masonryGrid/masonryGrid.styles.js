import styled from 'styled-components';

const GAP = '17px';

export const MasonrySizer = styled.div`
  width: 50%;
  display: block;
  height: 1px;
`;

export const MasonryItem = styled.div`
  position: relative;
  display: inline-block;
  min-width: 50%;
  max-width: 100%;
  margin-bottom: ${GAP};
  text-align: center;
`;
