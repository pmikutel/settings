export const masonryProps = {
  horizontalOrder: true,
  itemSelector: '.grid-item',
  columnWidth: '.grid-sizer',
  percentPosition: true,
};
