import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';

import { MasonryGrid } from '../masonryGrid.component';

describe('<MasonryGrid />', () => {
  const defaultProps = {
    isShifted: true,
    ignoreFirstItem: true,
    columns: 2,
    scroll: false,
    children: [],
  };

  const component = (props = {}) => <MasonryGrid {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
