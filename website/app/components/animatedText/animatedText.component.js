import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class AnimatedText extends PureComponent {
  static propTypes = {
    text: PropTypes.string.isRequired,
    successText: PropTypes.string.isRequired,
    waiting: PropTypes.bool,
    timeout: PropTypes.number,
    callback: PropTypes.func.isRequired,
  };

  static defaultProps = {
    waiting: false,
    timeout: 3000,
  };

  state = {
    isWaiting: this.props.waiting,
  };

  componentDidUpdate(prevProps) {
    if (this.props.waiting !== prevProps.waiting) {
      clearTimeout(this.timeoutId);
      const { waiting, timeout } = this.props;

      this.setState({ isWaiting: waiting });

      this.timeoutId = setTimeout(() => {
        this.setState({ isWaiting: false });
        this.props.callback();
      }, timeout);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutId);
  }

  get copy() {
    return this.state.isWaiting ? this.props.successText : this.props.text;
  }

  timeoutId = null;

  render = () => (
    <span>
      {this.copy}
    </span>
  );
}
