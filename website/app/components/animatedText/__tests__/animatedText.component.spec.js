import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { AnimatedText } from '../animatedText.component';

describe('<AnimatedText />', () => {
  const defaultProps = {
    text: 'test',
    successText: 'test success',
    waiting: false,
    timeout: 1000,
    callback: identity,
  };

  const component = (props = {}) => <AnimatedText {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly while waiting', () => {
    const wrapper = render({ waiting: true });

    global.expect(wrapper).toMatchSnapshot();
  });
});
