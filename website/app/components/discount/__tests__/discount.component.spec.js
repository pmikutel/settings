import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import { Discount } from '../discount.component';
import { InfoBox } from '../../infoBox/infoBox.component';


describe('<Accordeon />', () => {
  const defaultProps = {
    price: '',
    compareAtPrice: '',
  };

  const component = (props = {}) => <Discount {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const props = {
      price: '10',
      compareAtPrice: '20',
    };
    const wrapper = render(props);

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('when price and compareAtPrice exist', () => {
    it('should render Infobox', () => {
      const props = {
        price: '10',
        compareAtPrice: '20',
      };
      const wrapper = render(props);

      expect(wrapper.find(InfoBox)).to.have.length(1);
    });

    it('should render Infobox have proper value', () => {
      const props = {
        price: '15',
        compareAtPrice: '20',
      };
      const wrapper = render(props);

      expect(wrapper.find(InfoBox).props().copy).to.equal('-25%');
    });
  });

  describe('when price and compareAtPrice do not exist', () => {
    it('should not render Infobox', () => {
      const wrapper = render();

      expect(wrapper.find(InfoBox)).to.have.length(0);
    });
  });

  describe('when compareAtPrice does not exist', () => {
    it('should not render Infobox', () => {
      const props = {
        price: '15',
      };

      const wrapper = render(props);

      expect(wrapper.find(InfoBox)).to.have.length(0);
    });
  });
});
