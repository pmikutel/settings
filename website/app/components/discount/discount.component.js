import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { always, ifElse, equals, lt, either, gt } from 'ramda';

import { InfoBox } from '../infoBox/infoBox.component';


export class Discount extends PureComponent {
  static propTypes = {
    price: PropTypes.string,
    compareAtPrice: PropTypes.string,
  };

  get discount() {
    return parseInt(100 - this.props.price / this.props.compareAtPrice * 100, 10);
  }

  render = () => ifElse(
    either(
      equals(NaN),
      gt(1),
      lt(100),
    ),
    always(null),
    (value) => <InfoBox copy={`-${value}%`} smallInfoBox />
  )(this.discount);
}


