import { defineMessages } from 'react-intl';

export default defineMessages({
  titleOrder: {
    id: 'gradientBox.titleOrder',
    defaultMessage: 'ORDER SUMMARY',
  },
  titleDelivery: {
    id: 'gradientBox.titleDelivery',
    defaultMessage: 'DELIVERY DETAILS',
  },
  subtotal: {
    id: 'gradientBox.subtotal',
    defaultMessage: 'Subtotal before delivery:',
  },
  delivery: {
    id: 'gradientBox.delivery',
    defaultMessage: 'Delivery charge:',
  },
  vat: {
    id: 'gradientBox.vat',
    defaultMessage: 'Total ex.VAT:',
  },
  totalVat: {
    id: 'gradientBox.totalVat',
    defaultMessage: 'VAT 20%:',
  },
  total: {
    id: 'gradientBox.total',
    defaultMessage: 'Total cost:',
  },
  currency: {
    id: 'gradientBox.currency',
    defaultMessage: '£{ price }',
  },
  country: {
    id: 'gradientBox.country',
    defaultMessage: 'United Kingdom',
  },
});
