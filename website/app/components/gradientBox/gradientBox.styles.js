import styled, { css } from 'styled-components';
import { propEq, ifElse, always } from 'ramda';
import { Variables, Typography, Media } from '../../theme';


export const wrapperDesktopStyles = css`
  margin-top: -160px;
`;

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: 42px;
  
  ${Media.desktop(wrapperDesktopStyles)}
`;

export const WhiteBox = styled.div`
  background-color: ${Variables.colorPureWhite};
  margin: 5px;
  padding: 31px 0 25px;
`;

const titleDesktopWideStyles = css`
  font-size: 40px;
`;

const titleDesktopStyles = css`
  font-size: 30px;
  margin: 0 31px 20px;
  letter-spacing: 24px;
`;

export const Title = styled.h2`
  color: ${Variables.colorPureBlack};
  margin: 0 31px 10px;
  padding-bottom: 30px;
  letter-spacing: 13px;
  border-bottom: 1px solid ${Variables.colorBorderGray};
  
  ${Media.desktop(titleDesktopStyles)};
  ${Media.desktopWide(titleDesktopWideStyles)};
`;

const rowTabletStyles = css`
  flex-direction: row;
  align-items: center;
  padding: ${ifElse(propEq('white', true), always('40px 0'), always('15px 0'))};
`;

const rowDesktopStyles = css`
  font-size: 18px;
`;

const rowDesktopWideStyles = css`
  font-size: 22px;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  margin: 0 31px;
  font-size: 22px;
  padding: 15px 0; 
  color: ${Variables.colorWarmGrey};

  ${Media.tablet(rowTabletStyles)};
  ${Media.desktop(rowDesktopStyles)};
  ${Media.desktopWide(rowDesktopWideStyles)};
`;

export const Copy = styled.div`
  padding-right: 10px; 
  ${Typography.fontLyonLight};
  color: ${ifElse(propEq('white', true), always(Variables.colorPureWhite), always(Variables.colorWarmGrey))};
  font-weight: 300;
`;

export const Price = styled.div`
  letter-spacing: 12px;
  ${Typography.fontGothamBook};
  color: ${ifElse(propEq('white', true), always(Variables.colorPureWhite), always(Variables.colorPureBlack))};
`;

const borderTabletStyles = css`
  margin-bottom: 25px;
`;

export const Border = styled.div`
  background-color: ${Variables.colorGrayWithOpacity};
  height: 1px;
  width: calc(100% - 62px);
  margin: 25px auto 15px;
  
  ${Media.tablet(borderTabletStyles)};
`;

export const BackgroundWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background-image: ${Variables.gradientBox};
  margin-bottom: ${ifElse(propEq('marginBottom', true), always('42px'), always('0'))};
`;
