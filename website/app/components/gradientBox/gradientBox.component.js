import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { ifElse, prop, always, defaultTo, isEmpty } from 'ramda';
import { FormattedMessage } from 'react-intl';

import { priceFormat } from '../../utils/numbers';

import { Wrapper, WhiteBox, Title, Row, Copy, Price, Border, BackgroundWrapper } from './gradientBox.styles';
import messages from './gradientBox.messages';


export class GradientBox extends PureComponent {
  static defaultProps = {
    address: {},
    delivery: null,
  };

  static propTypes = {
    subtotal: PropTypes.number.isRequired,
    vat: PropTypes.number.isRequired,
    delivery: PropTypes.number,
    address: PropTypes.object,
    userEmail: PropTypes.string,
    userNumber: PropTypes.string,
  };

  get fullPrice() {
    return this.props.subtotal + this.props.vat;
  }

  renderDeliveryRow = () => ifElse(
    prop('delivery'),
    () =>
      <Row>
        <Copy>
          <FormattedMessage{...messages.delivery} />
        </Copy>
        <Price>
          <FormattedMessage{...messages.currency} values={{ price: this.props.delivery }} />
        </Price>
      </Row>,
    always(null),
  )(this.props);

  renderOrder = () => (
    <BackgroundWrapper>
      <WhiteBox>
        <Title>
          <FormattedMessage {...messages.titleOrder} />
        </Title>
        <Row>
          <Copy>
            <FormattedMessage {...messages.subtotal} />
          </Copy>
          <Price>
            <FormattedMessage {...messages.currency} values={{ price: priceFormat(this.props.subtotal) }} />
          </Price>
        </Row>
        {this.renderDeliveryRow()}
        <Border />
        <Row>
          <Copy>
            <FormattedMessage {...messages.vat} />
          </Copy>
          <Price>
            <FormattedMessage {...messages.currency} values={{ price: priceFormat(this.props.vat) }} />
          </Price>
        </Row>
        <Row>
          <Copy>
            <FormattedMessage {...messages.totalVat} />
          </Copy>
          <Price>
            <FormattedMessage
              {...messages.currency}
              values={{ price: priceFormat(this.fullPrice) }}
            />
          </Price>
        </Row>
      </WhiteBox>
      <Row white>
        <Copy white>
          <FormattedMessage {...messages.total} />
        </Copy>
        <Price white>
          <FormattedMessage
            {...messages.currency}
            values={{ price: priceFormat(this.fullPrice + defaultTo(0, this.props.delivery)) }}
          />
        </Price>
      </Row>
    </BackgroundWrapper>
  );

  renderNumber = () => ifElse(
    prop('userNumber'),
    () => <Row>{this.props.userNumber}</Row>,
    always(null),
  )(this.props);

  renderDetails = () => (
    <div>
      <BackgroundWrapper marginBottom>
        <WhiteBox>
          <Title>
            <FormattedMessage {...messages.titleDelivery} />
          </Title>
          <Row>
            {this.props.address.get('number')}
            {this.props.address.get('route')}
          </Row>
          <Row>{this.props.address.get('postal_town')}</Row>
          <Row>{this.props.address.get('postal_code')}</Row>
          <Row>
            <FormattedMessage {...messages.country} />
          </Row>
          <Border />
          <Row>{this.props.userEmail}</Row>
          {this.renderNumber()}
        </WhiteBox>
      </BackgroundWrapper>
      {this.renderOrder()}
    </div>
  );

  renderContent = () => ifElse(
    isEmpty,
    this.renderOrder,
    this.renderDetails,
  )(this.props.address);

  render = () => (
    <Wrapper>
      {this.renderContent()}
    </Wrapper>
  );
}
