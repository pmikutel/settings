import React from 'react';
import { shallow } from 'enzyme';

import { GradientBox } from '../gradientBox.component';


describe('<GradientBox />', () => {
  const defaultProps = {
    subtotal: 29400,
    delivery: 546,
    vat: 832,
    order: true,
    totalVat: 23423,
    total: 23423,
    pink: true,
  };

  const component = (props = {}) => <GradientBox {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
