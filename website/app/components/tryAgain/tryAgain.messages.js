import { defineMessages } from 'react-intl';

export default defineMessages({
  oops: {
    id: 'tryAgain.oops',
    defaultMessage: 'Oops! Something went wrong.',
  },
  tryAgain: {
    id: 'tryAgain.tryAgain',
    defaultMessage: 'Please try again.',
  },
});
