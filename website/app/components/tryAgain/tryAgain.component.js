import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { always, ifElse, equals, isNil } from 'ramda';

import messages from './tryAgain.messages';
import {
  Wrapper,
  Label,
} from './tryAgain.styles';

export class TryAgainComponent extends PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    isActive: PropTypes.bool,
    copy: PropTypes.string,
  };

  static defaultProps = {
    isActive: false,
  };

  get copy() {
    return this.props.copy || this.props.intl.formatMessage(messages.content);
  }

  defaultMessage = (
    <Label>
      {this.props.intl.formatMessage(messages.oops)}
      <br />
      {this.props.intl.formatMessage(messages.tryAgain)}
    </Label>
  );

  renderMessage = () => ifElse(
    isNil,
    always(this.defaultMessage),
    always(<Label>{this.props.copy}</Label>)
  )(this.props.copy);

  renderLabel = () => ifElse(
    equals(true), this.renderMessage, always('')
  )(this.props.isActive);

  render = () => (
    <Wrapper>
      {this.renderLabel()}
    </Wrapper>
  );
}

export const TryAgain = injectIntl(TryAgainComponent);
