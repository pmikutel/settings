import React from 'react';
import { shallow } from 'enzyme';

import { TryAgainComponent as TryAgain } from '../tryAgain.component';
import { intlMock } from '../../../utils/testing';

describe('<TryAgain />', () => {
  const defaultProps = {
    intl: intlMock(),
    isActive: false,
  };

  const component = (props = {}) => <TryAgain {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should render correctly with copy from props', () => {
    const copy = 'copy';
    const wrapper = render({ copy });

    global.expect(wrapper).toMatchSnapshot();
  });
});
