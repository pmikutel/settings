import styled from 'styled-components';
import { Variables, Typography } from '../../theme';

const PADDING_SIZE = 11;
const FONT_SIZE = 18;
const WRAPPER_SIZE = 2 * PADDING_SIZE + FONT_SIZE;

export const Wrapper = styled.div`
  padding: ${PADDING_SIZE}px;
  min-height: ${WRAPPER_SIZE}px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Label = styled.span`
  line-height: 1;
  ${Typography.fontLyonLight};
  color: ${Variables.colorRed};
  font-size: ${FONT_SIZE}px;
  text-align: center;
`;
