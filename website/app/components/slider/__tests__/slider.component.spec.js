import React from 'react';
import { shallow } from 'enzyme';
import 'jest-styled-components';
import { fromJS } from 'immutable';

import { MainSlider } from '../slider.component';

describe('<MainSlider />', () => {
  const defaultProps = {
    children: fromJS({}),
    inspire: false,
    noAutoPlay: false,
  };

  const component = (props = {}) => <MainSlider {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
