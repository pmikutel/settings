import styled, { css } from 'styled-components';
import { always, ifElse, propEq } from 'ramda';
import { Grid, Media, Variables } from '../../theme';

const dotsColumnStyles = Grid.getColumn({
  columns: 1,
  offset: 15,
});

const slickCustomStyles = css`
  .slick-slider {
    position: relative;
    padding-left: ${ifElse(propEq('inspire', true), always('0'), always('10px'))};

    .slick-slide {
      overflow: hidden;
    }

    .slick-dots {
      display: flex !important;
      flex-direction: row;
      align-items: center;
      justify-content: center;
      visibility: ${ifElse(propEq('inspire', true), always('visible'), always('hidden'))};
      margin-top: ${ifElse(propEq('inspire', true), always('20px'), always('0'))};

      ${dotsColumnStyles}

      li {
        margin-right: 10px;
        line-height: 0.4;
        display: block;
        
        &:last-child {
            margin-right: 0;
        }
      }
    

      button {
        border: 2px solid ${Variables.colorPureBlack};
        border-radius: 50%;
        display: block;
        height: 11px;
        width: 11px;
        outline: 0;
        line-height: 0;
        font-size: 0;
        color: transparent;
        padding: 5px;
        cursor: pointer;
        transition: border-color 0.2s, background-color 0.4s;

        &:hover {
          border-color: ${Variables.colorMintyGreen};
        }
      }
    }

    .slick-active {
      button {
        background-color: ${Variables.colorPureBlack};

        &:hover {
          border-color: ${Variables.colorPureBlack};
        }
      }
    }
  }
`;

const slickCustomDesktopStyles = css`
  .slick-slider {
  padding-left: 0;
  
    .slick-dots {
        visibility: visible;
        position: absolute;
        top: 0;
        right: 20px;
        height: 100%;
        margin: 0;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
        
        li {
          margin: 10px 0 0;

          &:first-child {
            margin-top: 0;
          }
        }
      }
   }
`;


export const Wrapper = styled.section`
  ${slickCustomStyles};
  ${Media.desktop(slickCustomDesktopStyles)};
`;
