import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

import { Wrapper } from './slider.styles';

const SLIDER_CONFIG = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  pauseOnHover: false,
  pauseOnFocus: false,
  autoplaySpeed: 6000,
};

export class MainSlider extends PureComponent {
  static propTypes = {
    inspire: PropTypes.bool,
    noAutoPlay: PropTypes.bool,
    children: PropTypes.any.isRequired,
  };

  static defaultProps = {
    inspire: false,
    noAutoPlay: false,
  };

  render = () => (
    <Wrapper inspire={this.props.inspire}>
      <Slider {...SLIDER_CONFIG} autoplay={!this.props.noAutoPlay}>
        {this.props.children}
      </Slider>
    </Wrapper>
  );
}


