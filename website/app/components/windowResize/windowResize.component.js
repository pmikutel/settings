import { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class WindowResize extends PureComponent {
  static propTypes = {
    onResize: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.handler = this.props.onResize;
    window.addEventListener('resize', this.handler);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handler);
  }

  handler = null;

  render() {
    return null;
  }
}
