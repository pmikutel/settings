import React from 'react';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { WindowResize } from '../windowResize.component';

describe('<WindowResize />', () => {
  const defaultProps = {
    onResize: identity,
  };

  const component = (props = {}) => <WindowResize {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });
});
