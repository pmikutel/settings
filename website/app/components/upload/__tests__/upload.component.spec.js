import React from 'react';
import { shallow } from 'enzyme';
import { spy } from 'sinon';
import { identity } from 'ramda';
import { expect } from 'chai';

import { Upload } from '../upload.component';
import { InputFile } from '../upload.styles';

describe('<Upload />', () => {
  const defaultProps = {
    supportedFiles: ['jpg', 'png', 'eps'],
    onChange: identity,
    value: null,
  };

  const component = (props = {}) => <Upload {...defaultProps} {...props} />;

  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('<InputFile />', () => {
    it('should call onChange with base64 and file details', () => {
      const onChange = spy();
      const wrapper = render({ onChange });
      const file = { name: 'Test', type: 'image/png', size: 100 };

      wrapper.find(InputFile).simulate('change', { target: { files: [file] } });

      expect(onChange).to.have.been.calledWith({ name: 'Test', type: 'image/png', data: 'base64' });
    });

    it('should call onChange with empty object', () => {
      const onChange = spy();
      const wrapper = render({ onChange });
      const file = { name: 'Test', type: 'image/png' };

      wrapper.find(InputFile).simulate('change', { target: { files: [file] } });

      expect(onChange).to.have.been.calledWith({});
    });
  });
});
