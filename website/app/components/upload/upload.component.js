import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isEmpty, join, always, ifElse, map, pipe, either, isNil } from 'ramda';
import FileAPI from 'fileapi';

import {
  FileUpload,
  SupportedFiles,
  UploadIcon,
  InputFile,
} from './upload.styles';

const maxFileSize = 3000000;

export class Upload extends PureComponent {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
      PropTypes.string,
    ]),
    supportedFiles: PropTypes.array,
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired,
  };

  getInputAcceptString = () => ifElse(
    either(isEmpty, isNil),
    always(''),
    pipe(
      map(value => `image/${value}`),
      join(', ')
    )
  )(this.props.supportedFiles);

  handleInputChange = ({ target: { files: [file] } }) => {
    FileAPI.readAsDataURL(file, ({ result: data }) => {
      const { name, type, size } = file;

      // Loop to find position from where cut will be. Unfortunately replace or split work too long for long strings.
      let positionToCut = 0;
      for (let i = 0; i < data.length; i++) {
        if (data[i] === ',') {
          positionToCut = i + 1;
          break;
        }
      }

      const baseData = data.slice(positionToCut);
      const fileObject = size < maxFileSize ? { name, type, data: baseData } : {};
      this.props.onChange(fileObject);
    });
  };

  renderSupportedFilesString = () => ifElse(
    either(isEmpty, isNil),
    always(''),
    join(', ')
  )(this.props.supportedFiles);

  render = () => (
    <FileUpload>
      <SupportedFiles>
        {this.renderSupportedFilesString()}
      </SupportedFiles>

      <UploadIcon />

      <InputFile
        type="file"
        accept={this.getInputAcceptString()}
        onChange={this.handleInputChange}
      />
    </FileUpload>
  );
}
