import styled, { css } from 'styled-components';
import Sprites from '../../utils/sprites';
import { Variables, Media } from '../../theme';

const fileUploadDesktopStyles = css`
  max-width: 305px;
  width: 100%;
`;

export const FileUpload = styled.div`
  position: relative;
  display: flex;
  height: 64px;
  border: 2px solid ${Variables.colorPureBlack};
  align-items: center;
  justify-content: space-between;
  padding: 0 24px;
  
  ${Media.desktop(fileUploadDesktopStyles)}
`;

export const SupportedFiles = styled.span`
  font-size: 18px;
  opacity: 0.9;
  line-height: 2;
  max-width: 520px;
  color: ${Variables.colorWarmGrey};
`;

export const UploadIcon = styled.i`
  ${Sprites.responsive('upload')}
`;

export const InputFile = styled.input`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
  cursor: pointer;
`;
