import { defineMessages } from 'react-intl';

export default defineMessages({
  subtitle: {
    id: 'header.subtitle',
    defaultMessage: 'Sign up to our newsletter',
  },
  title: {
    id: 'newsletter.title',
    defaultMessage: 'Creating a place for the rare & the brave?',
  },
});
