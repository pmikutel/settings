import styled, { css } from 'styled-components';
import { Typography, Forms, Media } from '../../theme';

const titleDesktopStyles = css`
  margin-bottom: 145px;
  line-height: 48px;
  letter-spacing: 28px;
`;

const inputDesktopStyles = css`
  width: 100%;
`;

export const Title = styled.h2`
  text-transform: uppercase;
  margin-bottom: 110px;
  letter-spacing: 13px;
  line-height: 28px;
    
  ${Media.desktop(titleDesktopStyles)}
`;

export const SubTitle = styled.h4`
  ${Typography.fontLyonLight}
  margin-bottom: 45px;
  font-weight: 300;
`;

export const Input = styled.input`
  ${Forms.input};
  width: 85%;
  margin: 0 auto;
  max-width: 890px;
  
  ${Media.desktop(inputDesktopStyles)}
`;

export const topRowStyles = css`
  flex-direction: column;
  align-items: center;
  text-align: center;
`;

const sectionDesktopStyles = css`
  margin-bottom: 220px;
`;

export const sectionStyles = css`
  margin-bottom: 130px;
  
  ${Media.desktop(sectionDesktopStyles)}
`;
