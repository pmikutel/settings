import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { NewsletterForm } from './form/form.component';
import { TryAgain } from '../tryAgain/tryAgain.component';
import { topRowStyles, sectionStyles, Title, SubTitle } from './newsletter.styles';
import { Grid } from '../../theme';
import messages from './newsletter.messages';

export class Newsletter extends PureComponent {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    isSuccess: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired,
    clearError: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.clearError();
  }

  render = () => (
    <Grid.Section extraStyles={sectionStyles}>
      <Grid.Row>
        <Grid.Column offset={1} columns={14} extraStyles={topRowStyles}>
          <SubTitle><FormattedMessage {...messages.subtitle} /></SubTitle>
          <Title><FormattedMessage {...messages.title} /></Title>
        </Grid.Column>
      </Grid.Row>

      <Grid.Row>
        <Grid.Column offset={3} columns={10} extraStyles={topRowStyles}>
          <NewsletterForm
            onSubmit={this.props.onSubmit}
            isSuccess={this.props.isSuccess}
            isFetching={this.props.isFetching}
            hasError={this.props.isError}
          />
          <TryAgain isActive={this.props.isError} />
        </Grid.Column>
      </Grid.Row>
    </Grid.Section>
  );
}
