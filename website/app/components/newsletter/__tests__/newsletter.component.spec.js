import React from 'react';
import { expect } from 'chai';
import { spy } from 'sinon';
import { shallow } from 'enzyme';
import { identity } from 'ramda';

import { Newsletter } from '../newsletter.component';
import { NewsletterForm } from '../form/form.component';

describe('<Newsletter />', () => {
  const defaultProps = {
    onSubmit: identity,
    isSuccess: true,
    isError: false,
    isFetching: false,
    clearError: identity,
  };

  const component = (props = {}) => <Newsletter {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  describe('<NewsletterForm />', () => {
    it('should call onSubmit when form onSubmit was called', () => {
      const submitSpy = spy();

      const wrapper = render({
        onSubmit: submitSpy,
      });

      wrapper.find(NewsletterForm).simulate('submit');

      expect(submitSpy).to.have.been.called;
    });
  });
});
