import { defineMessages } from 'react-intl';

export default defineMessages({
  placeholder: {
    id: 'newsletter.placeholder',
    defaultMessage: 'Type your email here',
  },
});
