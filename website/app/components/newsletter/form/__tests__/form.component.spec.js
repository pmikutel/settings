import React from 'react';
import { spy } from 'sinon';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { identity } from 'ramda';

import { intlMock } from '../../../../utils/testing';
import { DotsLoader } from '../../../dotsLoader/dotsLoader.component';

import { NewsletterFormBase as NewsletterForm } from '../form.component';
import { Form, SuccessIcon, SubmitButton } from '../form.styles';

describe('<NewsletterForm />', () => {
  const defaultProps = {
    handleSubmit: identity,
    isSuccess: false,
    isFetching: false,
    hasError: false,
    valid: false,
    intl: intlMock(),
  };

  const component = (props = {}) => <NewsletterForm {...defaultProps} {...props} />;
  const render = (props = {}) => shallow(component(props));

  it('should render correctly', () => {
    const wrapper = render();

    global.expect(wrapper).toMatchSnapshot();
  });

  it('should call onSubmit from props when form onSubmit was called', () => {
    const handleSubmit = spy();
    const wrapper = render({ handleSubmit });

    wrapper.find(Form).simulate('submit');

    expect(handleSubmit).to.have.been.called;
  });

  it('should render <SubmitButton /> if isSuccess is false', () => {
    const wrapper = render({ isSuccess: false });
    const submitButton = wrapper.find(SubmitButton);

    expect(submitButton).to.have.been.length(1);
  });

  it('should not render <SubmitButton /> if isSuccess is true', () => {
    const wrapper = render({ isSuccess: true });
    const submitButton = wrapper.find(SubmitButton);

    expect(submitButton).to.have.been.length(0);
  });

  it('should render <SuccessIcon /> if isSuccess is true', () => {
    const wrapper = render({ isSuccess: true });
    const submitButton = wrapper.find(SuccessIcon);

    expect(submitButton).to.have.been.length(1);
  });

  it('should not render <SuccessIcon /> if isSuccess is false', () => {
    const wrapper = render({ isSuccess: false });
    const submitButton = wrapper.find(SuccessIcon);

    expect(submitButton).to.have.been.length(0);
  });

  it('should render <DotsLoader /> when isFetching is true', () => {
    const wrapper = render({ isFetching: true });
    expect(wrapper.find(DotsLoader)).to.have.been.length(1);
  });

  it('should not render <DotsLoader /> when isFetching is false', () => {
    const wrapper = render({ isFetching: false });
    expect(wrapper.find(DotsLoader)).to.have.been.length(0);
  });
});
