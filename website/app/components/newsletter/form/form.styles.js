import styled, { css } from 'styled-components';
import { ifElse, propEq, always } from 'ramda';
import { Media, Variables, Typography } from '../../../theme';

const inputDesktopStyles = css`
  padding-right: 80px;
  width: 100%;
`;

const inputErrorStyles = ifElse(
  propEq('hasError', true),
  () => css`
    border-color: ${Variables.colorRed};
    color: ${Variables.colorRed};
  `,
  always(''),
);

export const Input = styled.input`
  ${Typography.fontLyonLight};  
  padding-right: 85px;
  width: 100%;
  
  ${Media.desktop(inputDesktopStyles)}
  ${inputErrorStyles}
`;

export const Form = styled.form`
  position: relative;
  max-width: 890px;
  width: 85%;
`;

const successIconDesktopStyles = css`
  right: 19px;
`;

export const SuccessIcon = styled.span`
  backface-visibility: hidden;
  border: 3px solid;
  border-right: 0;
  border-top: 0;
  border-image-source: ${Variables.gradientButton};
  border-image-slice: 1;
  height: 14px;
  margin: auto;
  position: absolute;
  right: 27px;
  top: 19px;
  transform: rotate(-45deg);
  width: 25px;
  
  ${Media.desktop(successIconDesktopStyles)}
`;

const submitButtonDesktopStyles = css`
  right: 20px;
`;

export const SubmitButton = styled.button`
  display: block;
  height: 2px;
  padding: 20px;
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: 27px;
  width: 18px;
  
  &:before {
    background: ${Variables.gradientButton};
    content: '';
    display: block;
    height: 2px;
    position: absolute;
    top: 19px;
    right: 1px;
    width: 43px;
  }
  
  &:after {
    border: solid ${Variables.colorFluroGreen};
    border-width: 0 2px 2px 0;
    content: '';
    display: inline-block;
    padding: 5px;
    position: absolute;
    right: 0px;
    top: 14px;
    transform: rotate(-45deg);
  }
  
  ${Media.desktop(submitButtonDesktopStyles)}
 `;
