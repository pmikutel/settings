import styled, { css } from 'styled-components';
import { ifElse, prop, always } from 'ramda';
import Media from '../media';

const desktopStyles = css`
  width: 100%;
`;

export const Row = styled.div`
  display: flex;
  width: calc(100% - 32px);
  margin: 0 auto;
  position: relative;

  ${Media.desktop(desktopStyles)}

  z-index: ${ifElse(prop('zIndex'), always(prop('zIndex')), always('1'))};
  ${prop('extraStyles')}
`;
