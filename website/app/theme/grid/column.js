import styled, { css } from 'styled-components';
import Media from '../media';

const MAX_COLUMNS_COUNT = 16;
const MAX_MOBILE_COLUMNS_COLUMNS = 6;

export const getColumnWidth = (columnsCount) => `${(columnsCount / MAX_COLUMNS_COUNT) * 100}%`;
export const getMobileColumns = (columnsCount) => `${(columnsCount / MAX_MOBILE_COLUMNS_COLUMNS) * 100}%`;

export const getMobileColumn = ({
  mobileColumns = MAX_MOBILE_COLUMNS_COLUMNS,
  mobileOffset = 0,
}) => css`
  width: ${getMobileColumns(mobileColumns)};
  margin-left: ${getMobileColumns(mobileOffset)};
`;

export const getDesktopColumn = ({
  columns = MAX_MOBILE_COLUMNS_COLUMNS,
  offset = 0,
}) => css`
  width: ${getColumnWidth(columns)};
  margin-left: ${getColumnWidth(offset)};
`;

export const getColumn = ({ extraStyles = css``, ...props }) => css`
  ${getMobileColumn(props)}
  ${Media.desktop(getDesktopColumn(props))}
  
  ${extraStyles}
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  ${getColumn};
`;
