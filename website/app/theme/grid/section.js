import styled from 'styled-components';
import { prop } from 'ramda';

const GRID_WIDTH = 1440;

export const Section = styled.section`
  position: relative;
  max-width: ${GRID_WIDTH}px;
  margin: 0 auto;
  width: 100%;
  ${prop('extraStyles')}
`;
