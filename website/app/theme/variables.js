export const colorPureWhite = '#fff';
export const colorPureBlack = '#000';
export const colorBlack = '#1a1a1a';
export const colorDarkerGray = '#272727';
export const colorDarkGray = '#363636';
export const colorWarmGrey = '#7f7f7f';
export const colorProductGrey = '#EEEEEE';
export const colorGray = '#979797';
export const colorBackgroundGray = '#fbfbfb';
export const colorMintyGreen = '#0eed7f';
export const colorFluroGreen = '#14ff00';
export const colorTurquoise = '#0eff00';
export const colorBarbiePink = '#fe51bb';
export const colorRed = '#ff0000';
export const colorYellow = '#fff104';
export const colorBrightBlue = '#2edcee';
export const colorBrightBlue2 = '#0ddbf3';
export const colorGreen = '#28ff00';
export const colorBorderGray = '#d8d8d8';
export const colorGrayWithOpacity = 'rgba(216, 216, 216, 0.6)';
export const colorBorderGrayWithOpacity = 'rgba(151,151,151, 0.2)';
export const colorBlackWithOpacity = 'rgba(0,0,0, 0.6)';

export const gothamBookFontFamily = 'GothamHTF-Book';
export const lyonLightFontFamily = 'LyonDisplay-Light';
export const lyonRegularFontFamily = 'LyonDisplay-Regular';

export const gradientUnderline = `linear-gradient(to left, ${colorYellow}, ${colorBarbiePink})`;
export const gradientButton = `linear-gradient(to right, ${colorBrightBlue2}, ${colorFluroGreen})`;
export const gradientButtonActive = `linear-gradient(to left, ${colorGreen}, ${colorBrightBlue})`;
export const gradientButtonDisabled = `linear-gradient(248deg, ${colorDarkGray}, ${colorDarkerGray})`;
export const gradientLogin = `linear-gradient(235deg, ${colorYellow}, ${colorBarbiePink})`;
export const gradientCustom = `linear-gradient(to bottom, ${colorYellow}, ${colorBarbiePink});`;
export const gradientBox = `linear-gradient(258deg, ${colorYellow}, ${colorBarbiePink});`;

export const shadow = '0 6px 6px 0 rgba(0, 0, 0, 0.12), 0 0 6px 0 rgba(0, 0, 0, 0.08)';
export const boxShadowRadioButton = '0 1px 4px 0 rgba(0, 0, 0, 0.42)';
export const boxShadowFullButton = '0 6px 6px 0 rgba(0, 0, 0, 0.12), 0 0 6px 0 rgba(0, 0, 0, 0.08)';
export const boxShadowProductHoverButtons = '0 2px 4px 2px rgba(0, 0, 0, 0.12), 0 0 4px 0 rgba(0, 0, 0, 0.08)';
