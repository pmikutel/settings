import { css } from 'styled-components';
import { media } from './media';
import { gothamBookFontFamily, lyonLightFontFamily, lyonRegularFontFamily } from './variables';

export const fontGothamBook = css`
  font-family: ${gothamBookFontFamily};
`;

export const fontLyonLight = css`
  font-family: ${lyonLightFontFamily};
`;

export const fontLyonRegular = css`
  font-family: ${lyonRegularFontFamily};
`;

export const hBase = css`
  ${fontGothamBook};
  margin: 0;
  padding: 0;
  line-height: 1;
`;

const h1DesktopStyles = css`
  font-size: 80px;
  letter-spacing: 45px;
  text-transform: uppercase;
`;

const h2DesktopStyles = css`
  font-size: 40px;
`;

const h3DesktopStyles = css`
  font-size: 22px;
`;

export const h1 = css`
  font-size: 28px;
  letter-spacing: 16px;
  font-weight: 100;
  text-transform: uppercase;

  ${media.desktop(h1DesktopStyles)}
`;

export const h2 = css`
  font-size: 22px;
  font-weight: 100;

  ${media.desktop(h2DesktopStyles)}
`;

export const h3 = css`
  font-size: 14px;
  font-weight: 100;

  ${media.desktop(h3DesktopStyles)}
`;

const pDesktopStyles = css`
  line-height: 50px;
`;

export const p = css`
  font-size: 22px;
  line-height: 46px;

  ${media.desktop(pDesktopStyles)}
`;

export const descriptionDesktopStyles = css`
  max-width: 305px;
`;

export const description = css`
  ${p};
  padding-left: 18%;
  padding-right: 18px;

  ${media.desktop(descriptionDesktopStyles)}
`;
