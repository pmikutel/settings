import React from 'react';
import * as RRD from 'react-router-dom';
import styled, { css, keyframes } from 'styled-components';
import { ifElse, prop, propEq, always, when } from 'ramda';

import Media from './media';
import {
  colorPureWhite,
  gradientButton,
  gradientButtonDisabled,
  colorWarmGrey,
  colorBrightBlue,
  colorMintyGreen,
  colorFluroGreen,
  colorBorderGray,
  shadow,
  gradientButtonActive,
  gradientCustom,
  boxShadowRadioButton,
  boxShadowFullButton,
  colorRed,
} from './variables';
import { fontLyonLight } from './typography';
import Sprite from '../utils/sprites';
import backgroundImage from './assets/dot-pattern.png';

const verticalLinePercentage = ['0%', '26%', '53%', '100%'];
const verticalLineMobilePercentage = ['0%', '25%', '43%', '100%'];

const rotate360 = keyframes`
  0% {
    background-position: 0 0;
    background-size: 100% 100%;
  }

  50% {
    background-size: 300% 300%;
  }

  100% {
    background-position: 100% 0;
    background-size: 100% 100%;
  }
`;

const submitButtonTabletStyles = css`
  font-size: 36px;
  height: 100px;
`;

const submitButtonStyles = css`
  bottom: 0;
  font-size: 24px;
  height: 70px;
  padding: 0;
  width: 100%;
  color: ${colorPureWhite};
  background-image: ${gradientButton};
  z-index: 1;
  transition: box-shadow 0.2s;
  ${fontLyonLight};

  &:disabled {
    background-image: ${gradientButtonDisabled};
    color: ${colorWarmGrey};
    cursor: default;
  }

  &:focus {
    background: ${colorBrightBlue} none;
  }

  &:hover {
    box-shadow: ${boxShadowFullButton};
    animation: ${rotate360} 0.4s linear forwards;
  }

  ${Media.tablet(submitButtonTabletStyles)};
  ${prop('extraStyles')}
`;

export const SubmitButton = styled.button`
  ${submitButtonStyles};

  position: relative;
`;

export const SubmitLink = styled(RRD.Link)`
  ${submitButtonStyles};
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`;

export const StickyButton = styled.button`
  ${submitButtonStyles};
  transition: opacity 0.4s ease-in-out;
  z-index: 10;
  
  position: ${ifElse(prop('sticky'), always('fixed'), always('absolute'))};
  opacity: ${ifElse(prop('show'), always('1'), always('0'))};
  
  > div {
    margin-left: 20px;
  }
`;

export const CloseButton = styled.button`
  ${Sprite.mobile('close-white')};
  transition: transform 0.2s;
  padding: 0;

  &:hover {
    transform: rotate(45deg)
  }
  
  ${prop('extraStyles')}
`;

const buttonDesktopStyles = css`
  margin-right: 30px;
  margin-bottom: 0;

  &:last-child {
    margin-right: 0;
  }
`;

const buttonActiveStyles = css`
  background-image: ${gradientButtonActive};
  box-shadow: ${boxShadowRadioButton};
  color: ${colorPureWhite};
`;

const buttonErrorStyles = css`
  color: ${colorRed};
  
  &:before{
    background: ${colorRed};
    box-shadow: none;
  }
`;

const buttonDisabledStyles = css`
  cursor: auto;
  color: ${colorWarmGrey};

  &:before{
    background: ${colorWarmGrey};
    box-shadow: none;
  }
`;

export const buttonStyles = css`
  position: relative;
  background-color: ${colorPureWhite};
  width: 100%;
  color: ${colorMintyGreen};
  display: flex;
  align-items: center;
  text-align: center;
  padding: 20px;
  margin-bottom: 24px;
  cursor: pointer;
  max-width: 400px;
  font-weight: 100;
  ${fontLyonLight};

  &:before {
    content: '';
    position: absolute;
    top: -2px;
    left: -2px;
    height: calc(100% + 4px);
    width: calc(100% + 4px);
    border-image-slice: 1;
    background: ${gradientButton};
    z-index: -1;
    background-size: 100% 100%;
    background-position: 0 0;
    transition: box-shadow 0.2s ease-in-out;
  }

  ${Media.desktop(buttonDesktopStyles)}

  &:focus {
    ${ifElse(propEq('noActive', true), always(null), always(buttonActiveStyles))};
  }

  &:hover {
    &:before {
      height: calc(100% + 8px);
      width: calc(100% + 8px);
      top: -4px;
      left: -4px;
      animation: ${rotate360} 0.4s linear forwards;
      animation-timing-function: cubic-bezier(0.61, 0.51, 0.28, 0.98);
      box-shadow: ${shadow};
    }
  }
`;

// Hack to prevent passing unnecessary props to Link
// eslint-disable-next-line
export const ActionLink = styled(({ noActive, desktop, extraStyles, ...rest }) => <RRD.Link {...rest} />)`
  ${buttonStyles};

  font-size: 18px;
  line-height: 1;
  padding: 23px 20px;

  ${prop('extraStyles')}
`;

export const SimpleLink = styled.a`
  ${buttonStyles};

  max-width: 200px;
  justify-content: center;
  box-sizing: border-box;
`;

export const Button = styled.button`
  ${buttonStyles};

  justify-content: center;
  display: inline-block;
  ${prop('extraStyles')}

  ${ifElse(propEq('isActive', true), always(buttonActiveStyles), always(''))};
  ${ifElse(propEq('disabled', true), always(buttonDisabledStyles), always(''))};
  ${ifElse(propEq('error', true), always(buttonErrorStyles), always(''))};
  pointer-events: ${ifElse(propEq('noEvents', true), always('none'), always('all'))};
`;

export const LinkButton = styled.button`
  margin-left: 5px;
  display: inline-flex;
  line-height: 1;
  padding: 0;
  color: ${colorMintyGreen};
  ${fontLyonLight};
  border-bottom: 1px solid ${colorMintyGreen};
`;


const getEmptyArrowStyles = when(
  propEq('empty', 'true'),
  () => css`
    left: -1px;
    right: unset;
    transform: rotate(-225deg);
  `,
);

export const Arrow = styled.span`
  display: block;
  width: 18px;
  height: 0;
  border-bottom: 2px solid ${colorFluroGreen};
  transform: translateY(-50%);
  position: absolute;
  right: 20px;
  top: 29px;
  transform: rotate(${ifElse(propEq('left', true), always(180), always(0))}deg);

  &:after {
    border-color: ${colorFluroGreen};
    border: solid;
    border-width: 0 2px 2px 0;
    display: inline-block;
    padding: 5px;
    content: '';
    position: absolute;
    right: -1px;
    top: -5px;
    transform: rotate(-45deg) translateZ(-1000px);

    ${getEmptyArrowStyles}
  }

  ${prop('extraStyles')}
`;

export const dotsDesktopStyles = css`
  width: ${prop('width')};
  top: ${prop('top')}px;
  height: ${prop('height')}px;
  left: ${ifElse(prop('right'), always('auto'), always(0))};
  right: ${ifElse(prop('right'), always(0), always('auto'))};
`;

export const Dots = styled.div`
  position: absolute;
  width: ${prop('mobileWidth')};
  top: ${prop('mobileTop')}px;
  bottom: ${prop('mobileBottom')}px;
  height: ${prop('mobileHeight')}px;
  left: ${ifElse(prop('mobileRight'), always('auto'), always(0))};
  right: ${ifElse(prop('mobileRight'), always(0), always('auto'))};
  z-index: 0;
  background: url(${backgroundImage}) repeat left top;
  background-size: auto;
  pointer-events: none;

  ${Media.desktop(dotsDesktopStyles)};
`;

export const Link = styled(RRD.Link)`
  color: ${colorMintyGreen};
  text-decoration: underline;
`;

export const smallButtonExtraStyles = css`
  max-width: 225px;
`;

export const centeredButtonExtraStyles = css`
  justify-content: center;
`;

export const buttonArrowStyles = css`
  padding: 0;
  height: 64px;
  width: 64px;
`;

const verticalLineContainerDesktopStyles = css`
  &:before {
    left: 0;
    height: 76%;
  }

  &:after {
    left: 0;
    height: ${props => verticalLinePercentage[props.step]};
  }
`;

export const VerticalLineContainer = styled.div`
  &:before {
    content: '';
    background-color: ${colorBorderGray};
    width: 5px;
    height: 84%;
    position: absolute;
    left: 16px;
    top: 0;
  }

  &:after {
    content: '';
    background-image: ${gradientCustom};
    left: 16px;
    top: 0;
    width: 5px;
    height: ${props => verticalLineMobilePercentage[props.step]};
    position: absolute;
  }

  ${Media.desktop(verticalLineContainerDesktopStyles)};
`;
