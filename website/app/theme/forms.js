import styled, { css } from 'styled-components';
import { ifElse, prop, propEq, always } from 'ramda';
import { fontLyonLight } from './typography';
import { colorBlack, colorWarmGrey, colorRed, colorPureBlack } from './variables';


const errorStyle = css`
  border-color: ${colorRed};
  color: ${colorRed};
`;

const disabledStyle = css`
  border: 2px solid transparent;
`;

export const Input = styled.input`
  background-color: transparent;
  outline: none;
  ${fontLyonLight};
  border: 2px solid ${colorBlack};
  line-height: normal;
  color: ${colorWarmGrey};
  font-size: 18px;
  padding: 20px;
  box-sizing: border-box;
  display: block;
  border-radius: 0;
  width: 100%;

  ${prop('extraStyles')}
  ${ifElse(propEq('errorStyle', true), always(errorStyle), always(''))}
  ${ifElse(propEq('borderDisabled', true), always(disabledStyle), always(''))}
`;

export const Error = styled.span`
  color: ${colorRed};
  position: absolute;
  top: 100%;
  left: 0;
  padding-top: 10px;
`;

export const Label = styled.label`
  display: block;
  margin: 30px 0;
  font-size: 22px;
  font-weight: 300;
  line-height: 2.09;
  ${fontLyonLight};
  color: ${colorWarmGrey};

  ${prop('extraStyles')}
`;

const squareActiveStyles = css`
  &:after {
    display: block;
  }
`;

const squareStyles = css`
  position: relative;
  width: 18px;
  height: 18px;
  border: solid 2px ${colorPureBlack};
  margin-right: 12px;
  background-color: transparent;
  padding: 0;

  &:after {
    display: none;
    content: '';
    background: ${colorPureBlack};
    position: absolute;
    width: 10px;
    height: 10px;
    top: 2px;
    left: 2px;
  }

  ${ifElse(propEq('isActive', true), always(squareActiveStyles), always(''))};
`;

export const Square = styled.div`
  ${squareStyles};

  &:after {
    width: 14px;
    height: 14px;
  }
`;

export const SquareButton = styled.button`
  ${squareStyles};
`;
