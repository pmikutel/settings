import { injectGlobal } from 'styled-components';
import { h1, h2, h3, p, hBase, fontLyonLight } from './typography';
import { colorPureBlack, colorWarmGrey } from './variables';

/* eslint-disable chai-friendly/no-unused-expressions */
injectGlobal`
  @font-face {
    font-family: 'LyonDisplay-Regular';
    src: url(${require('../fonts/LyonDisplay-Regular.eot')});
    src: url(${require('../fonts/LyonDisplay-Regular.eot')}) format('embedded-opentype'),
        url(${require('../fonts/LyonDisplay-Regular.woff2')}) format('woff2'),
        url(${require('../fonts/LyonDisplay-Regular.woff')}) format('woff'),
        url(${require('../fonts/LyonDisplay-Regular.ttf')}) format('truetype'),
        url(${require('../fonts/LyonDisplay-Regular.svg')}) format('svg');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'LyonDisplay-Light';
    src: url(${require('../fonts/LyonDisplay-Light.eot')});
    src: url(${require('../fonts/LyonDisplay-Light.eot')}) format('embedded-opentype'),
        url(${require('../fonts/LyonDisplay-Light.woff2')}) format('woff2'),
        url(${require('../fonts/LyonDisplay-Light.woff')}) format('woff'),
        url(${require('../fonts/LyonDisplay-Light.ttf')}) format('truetype'),
        url(${require('../fonts/LyonDisplay-Light.svg')}) format('svg');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'GothamHTF-Book';
    src: url(${require('../fonts/GothamHTF-Book.eot')});
    src: url(${require('../fonts/GothamHTF-Book.eot')}) format('embedded-opentype'),
        url(${require('../fonts/GothamHTF-Book.woff2')}) format('woff2'),
        url(${require('../fonts/GothamHTF-Book.woff')}) format('woff'),
        url(${require('../fonts/GothamHTF-Book.ttf')}) format('truetype'),
        url(${require('../fonts/GothamHTF-Book.svg')}) format('svg');
    font-weight: normal;
    font-style: normal;
  }

  h1, h2, h3 {
    ${hBase}
  }

  h1 {
    ${h1}
  }

  h2 {
    ${h2}
  }

  h3 {
    ${h3}
  }

  p {
    ${p}
  }
  
  html {
    height: 100%;
  }

  html, body {
    width: 100%;
    ${fontLyonLight};
    padding: 0;
    margin: 0;
    color: ${colorPureBlack};
    font-size: 18px;
    overflow-x: hidden;
    -webkit-overflow-scrolling: touch;
  }
  
  body.ReactModal__Body--open {
    height: 100%;
  }

  .app {
    overflow: hidden;
  }

  a {
    text-decoration: none;
  }

  ul {
    margin: 0;
    padding: 0;
  }

  input {
    background-color: transparent;
    outline: none;
    border: solid 2px ${colorPureBlack};
    padding: 12px 20px;
    width: 100%;
    display: block;
    height: 64px;
    line-height: 64px;
    color: ${colorWarmGrey};
    box-sizing: border-box;
    font-size: 18px;
    
    &:focus::placeholder { 
      color:transparent;
    }
  }

  /* For Firefox */
  input[type='number'] {
    -moz-appearance: textfield;
  }

  /* Webkit browsers like Safari and Chrome */
  input[type=number]::-webkit-inner-spin-button,
  input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  button {
    background-color: transparent;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 0;
  }

  .ReactModal__Overlay{
    z-index: 3;
  }

  html.unsupported {
    background-color: #f7f7f7;
        
    .unsupported-page {
      display: block !important;
      text-align: center; 
    }
    
    .unsupported-page__logo {
      margin: 0 auto; 
      padding-top: 64px; 
      max-width: 64px;
      display: block;
    }
    
    .unsupported-page__image {
      margin: 0 auto; 
      max-width: 100%;
      padding-top: 124px; 
      display: block;
    }
    
    .unsupported-page__title {
      font-size: 80px; 
      color: #1a1a1a; 
      letter-spacing: 45px; 
      font-weight: 100; 
      padding-top: 57px;
      
      @media (max-width: 1024px) {
        font-size: 40px;
        letter-spacing: 20px;
      }
    }
    
    .unsupported-page__description {
      font-size: 18px; 
      color: #1a1a1a; 
      width: calc(100% - 20px); 
      max-width: 546px; 
      margin: 0 auto; 
      padding: 31px 0 50px;
    }

    #app {
      display: none;
    }
  }
  
  .zopim {
    visibility: hidden;
  }
`;
/* eslint-enable chai-friendly/no-unused-expressions */
