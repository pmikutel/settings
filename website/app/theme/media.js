import { css } from 'styled-components';
const sizes = {
  desktopFull: 1920,
  desktopWide: 1440,
  desktop: 1024,
  tabletLandscape: 1024,
  tablet: 768,
  mobileLandscape: 736,
  bigMobile: 480,
  mobile: 320,
};

const getWindowWidth = () => window.innerWidth;

export const media = Object.keys(sizes).reduce((accumulator, label) => {
  accumulator[label] = (...args) => css`
    @media (min-width: ${sizes[label]}px) {
      ${css(...args)}
    }
  `;
  accumulator[`${label}Landscape`] = (...args) => css`
    @media (min-width: ${sizes[label]}px) and (orientation: landscape) {
      ${css(...args)}
    }
  `;
  return accumulator;
}, {});

export const highDPI = (...args) => css`
  @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
    ${css(...args)}
  }
`;

export const isMobile = () => {
  const width = getWindowWidth();
  return width < sizes.tablet;
};

export const isTablet = () => {
  const width = getWindowWidth();
  return width >= sizes.tablet && width < sizes.tabletLandscape;
};

export const isTabletLandscape = () => {
  const width = getWindowWidth();
  return width >= sizes.tabletLandscape && width < sizes.desktop;
};

export const isDesktop = () => {
  const width = getWindowWidth();
  return width >= sizes.desktop;
};

export const isDesktopWide = () => {
  const width = getWindowWidth();
  return width >= sizes.desktopWide;
};

const desktop = (styles) => media.desktop`${styles}`;
const desktopWide = (styles) => media.desktopWide`${styles}`;
const tablet = (styles) => media.tablet`${styles}`;
const tabletLandscape = (styles) => media.tabletLandscape`${styles}`;
const bigMobile = (styles) => media.bigMobile`${styles}`;
const mobile = (styles) => media.mobile`${styles}`;
const mobileLandscape = (styles) => media.mobileLandscape`${styles}`;

export default { desktop, desktopWide, tablet, tabletLandscape, bigMobile, mobile, mobileLandscape };
