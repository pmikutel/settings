import 'babel-polyfill';
import 'isomorphic-fetch';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
import sinonChai from 'sinon-chai';
import chaiJestDiff from 'chai-jest-diff';

Enzyme.configure({ adapter: new Adapter() });

// Fix issue in tests with "matchMedia not present"
window.matchMedia = window.matchMedia || function () {
  return {
    matches: false,
    addListener: function () {},
    removeListener: function () {},
  };
};

chai.use(chaiEnzyme());
chai.use(sinonChai);
chai.use(chaiJestDiff());
chai.config.includeStack = true;

global.requestAnimationFrame = (callback) => setTimeout(callback, 0);
/* eslint-disable no-undef */
/**
 * This object overrides environment configuration
 */
jest.mock('local-env-config', () => ({
  localEnvConfigLoaded: true,
}));

jest.mock('shopify-buy', () => ({
  buildClient: jest.fn().mockReturnValue({
    product: {
      fetchQuery: jest.fn(),
      fetch: jest.fn(),
      fetchAll: jest.fn(),
    },
  }),
}));

jest.mock('axios', () => ({
  create: jest.fn().mockReturnValue({
    post: jest.fn(),
    get: jest.fn(),
  }),
}));

jest.mock('gsap/TweenMax', () => ({
  set: jest.fn(),
}));


jest.mock('fileapi', () => ({
  readAsDataURL: jest.fn().mockImplementation((file, cb) => cb({ result: 'base64' })),
}));

global.localStorage = { getItem: jest.fn(), setItem: jest.fn() };
/* eslint-enable no-undef */
