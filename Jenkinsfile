#!/usr/bin/env groovy
projectName="whitebear"
slackChannel="white-bear"
currentStage=""
deployCredsPath="whitebear/deployment"
servers = [
	qa: [
		application: 'whitebear',
		environment: 'www-test',
		branches: ['develop'],
		s3: 'whitebear-beanstalk-application-storage',
		vaultSecret: 'whitebear/app/test',
		region: 'eu-west-1',
		healthcheckUrl: 'https://test.www.whitebear.com/status/elb',
		wait: false
	],    stage: [
		application: 'whitebear',
		environment: 'www-stage',
		branches: ['release/', 'master'],
		s3: 'whitebear-beanstalk-application-storage',
		vaultSecret: 'whitebear/app/stage',
		region: 'eu-west-1',
		healthcheckUrl: 'https://stage.www.whitebear.com/status/elb',
		wait: false
	],    prod: [
		application: 'whitebear',
		environment: 'www-prod',
		branches: ['master'],
		s3: 'whitebear-beanstalk-application-storage',
		vaultSecret: 'whitebear/app/prod',
		region: 'eu-west-1',
		healthcheckUrl: 'https://prod.www.whitebear.com/status/elb',
		wait: true
	]    ]
node ('bash') {
	stage('load utils'){
		currentStage=env.STAGE_NAME
		configFileProvider([configFile(fileId: 'utils-loader', variable: 'utilsLoader')]) {
			utils = load(utilsLoader).getUtils(["aws"])
			utils.init()
		}
	}
}
try{
	node('aws-credentials-helper') {
		stage('init') {
			currentStage=env.STAGE_NAME
			utils.performCheckout()
			pushCreds = utils.getVaultSecret(
				secret: deployCredsPath
				)
			buildCreds = utils.getVaultSecret(
				secret: "whitebear/app/build"
				) 
			utils.setBuildVariables()
			changedDirs = utils.checkChangesInDirs([
				'nginx',
				'website'
			])
		}
		stage('build') {
			currentStage=env.STAGE_NAME
			buildImages()
		}
		changedDirs.each { moduleName, toBeRun ->
			if (toBeRun)
				stage("${moduleName} tests") {
					currentStage = env.STAGE_NAME
					runTests(moduleName)
				}
		}
		if (changedDirs['website']) {
            stage('frontend build') {
                currentStage=env.STAGE_NAME
                images = buildWebsite(buildCreds)
            }
        }
		if ( env.IS_DEPLOYING || env.IS_QA_BRANCH ) {
			stage('docker images push') {
				currentStage=env.STAGE_NAME
				utils.pushDockerImages(images, pushCreds)
				utils.pushToStash()
			}
		}
	}
	isBranchInTheEnv.each { env, isInTheEnv ->
		if (isInTheEnv){
			stage("deployment of env: $env"){
				currentStage="deployment of env: $env"
				currentServer = servers[env]
				if (currentServer['wait']){
					milestone 1
					try {
						timeout(time: 8, unit: 'HOURS') { input message: "Deploy $env?" }
					} catch (error) {
						currentBuild.result = 'SUCCESS'
						echo "Aborted? Muting error."
						return
					}
					milestone 2
				}
				utils.deployOnBeanstalkServer(currentServer, pushCreds, slackChannel)
			}
		}
	}
} catch(er) {
	if (utils.isError(er)) {
		if (currentStage.contains('deployment')) {
			utils.sendSlack(
				channel: slackChannel,
				status: "failed",
				actionType: "deployment",
				target: "Job: ${env.JOB_NAME}#${env.BUILD_ID}: env ${currentServer['environment']} in app ${currentServer['application']}"
				)
		} else {
			utils.sendSlack(
				channel: slackChannel,
				status: "failed",
				actionType: "build",
				target: "Job: ${env.JOB_NAME}#${env.BUILD_ID}: branch ${env.BRANCH_NAME}"
				)
		}
	}
}
List buildImages() {
	parallel(
		nginx:{
			dir('nginx'){
				nginxImage = docker.build("${env.GENERIC_IMAGE_NAME}-nginx-without-webapp", "--quiet .")
			}
		},
		website:{
			dir('website'){
				websiteImage = docker.build("${env.GENERIC_IMAGE_NAME}-website", "--quiet .")
			}
		},
		failFast: true
		)
}
def buildWebsite(def buildCreds) {
    def showVersion = ( env.IS_QA_BRANCH ? true : false)
    def command =  "npm run build -- --env.buildVersion ${version} --env.showVersion ${showVersion}"
    sh "rm -rf nginx/dist/*"
    websiteImage.inside("-v ${pwd()}/nginx/website:/app/webapp/dist:rw") {
        ansiColor('xterm') {
            sh "cd /app/webapp && ${command}"
        }
    }
    dir('nginx') {
        if (!fileExists("website/index.html")) {
            error()
        }
        nginxWebsiteImage = docker.build("${env.GENERIC_IMAGE_NAME}-www-nginx")
    }
	return [nginxWebsiteImage]
}
def runTests(String container) {
	switch(container) {
		case "nginx":
			testNginx()
			break
		case "website":
			testFrontend()
			break
	}
}
def testFrontend() {
    websiteImage.inside() {
        ansiColor('xterm') {
            sh "cd /app/webapp && yarn run test"
        }
    }
}
def testNginx() {
    echo "Test application nginx conf"
    nginxImage.inside("--add-host=backend:127.0.0.1") {
        ansiColor('xterm') {
            sh "nginx -t"
        }
    }
}
