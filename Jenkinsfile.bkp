#!/usr/bin groovy
projectName = "whitebear"
slackChannel = "white-bear"
vaultTokenId = "vault-read-token"
dockerRegistry = "145857224620.dkr.ecr.eu-west-1.amazonaws.com/whitebear"
vaultAddr = "https://vault-standalone.appelk.com:8200"
deployCredsPath = "whitebear/deployment"
dockerrunFile = "deployment/Dockerrun.aws.json.j2"
isQaBranch = "QA_BRANCH" in params
currentStage = ""
servers = [
    qa: [
        appName: 'whitebear',
        envName: 'www-test',
        branches: ['develop'],
        s3Bucket: 'whitebear-beanstalk-application-storage',
        vaultPath: 'whitebear/app/test',
        region: 'eu-west-1',
        requestPath: 'https://test.www.whitebear.com/status/elb',
        wait: false
    ],
    stage: [
        appName: 'whitebear',
        envName: 'www-stage',
        branches: ['release/', 'master'],
        s3Bucket: 'whitebear-beanstalk-application-storage',
        vaultPath: 'whitebear/app/stage',
        region: 'eu-west-1',
        requestPath: 'https://stage.www.whitebear.com/status/elb',
        wait: false
    ],
    prod: [
        appName: 'whitebear',
        envName: 'www-prod',
        branches: ['master'],
        s3Bucket: 'whitebear-beanstalk-application-storage',
        vaultPath: 'whitebear/app/prod',
        region: 'eu-west-1',
        requestPath: 'https://prod.www.whitebear.com/status/elb',
        wait: true
    ]
]

node ('bash') {
    stage('load-utils'){
        currentStage=env.STAGE_NAME
        withCredentials([usernamePassword(
            credentialsId: 'jenkins-apptension-bitbucket-user',
            usernameVariable: 'user',
            passwordVariable: 'pass'
        )]) {
            sh  "curl -u ${user}:${pass} -H 'Cache-Control: no-cache' https://bitbucket.org/apptension/jenkins/raw/v5/utils.groovy > utils.groovy"
        }
        utils = load("utils.groovy")
        utils.init()
    }
}

try {
    node('aws-credentials-helper') {
        stage('init') {
            currentStage=env.STAGE_NAME
            checkout scm
            changedDirs = utils.checkChangesInDirs(["website", "nginx"])
            BRANCH_NAME = isQaBranch ? QA_BRANCH - "origin/" : BRANCH_NAME

            version = utils.getGitTagVersion()
            commitMessage = sh returnStdout: true, script: "git log -1 --pretty=%B"
            currentBuild.displayName = "${BRANCH_NAME}-${version}"
            currentBuild.description = "${commitMessage.trim()}"
            awsCreds = utils.getVaultCreds(
                vaultAddr: vaultAddr,
                vaultPath: deployCredsPath,
                vaultTokenId: vaultTokenId,
                version: version)
            genericImageName = "${dockerRegistry.tokenize('/')[1]}:${version}"
        }

        stage('build creds vault query') {
            currentStage=env.STAGE_NAME
            buildCreds = utils.getVaultCreds(
                vaultAddr: vaultAddr,
                vaultPath: "whitebear/app/build",
                vaultTokenId: vaultTokenId,
                version: version
            )
        }

        stage('docker image build') {
            currentStage=env.STAGE_NAME
            images = buildImages(genericImageName)
        }

        changedDirs.each { moduleName, toBeRun ->
            if (toBeRun)
                stage("${moduleName} tests") {
                    currentStage = env.STAGE_NAME
                    runTests(moduleName)
                }
        }

        if (changedDirs['website']) {
            stage('frontend build') {
                currentStage=env.STAGE_NAME
                images = buildWebsite(images, buildCreds, version, genericImageName)
            }
        }

        if (isQaBranch || utils.isDeploymentApplicableBranch(branch: BRANCH_NAME, servers: servers)) {
            stage('docker images push') {
                currentStage=env.STAGE_NAME
                utils.pushDockerImages(
                    dockerRegistry, 
                    images, 
                    awsCreds)
                stashName = "${projectName}-${BRANCH_NAME}-${version}-files".replaceAll('/', '-')
                stash name: stashName, includes: "${dockerrunFile}, .ebextensions/*"
            }
        }
    }

    if (utils.isDeploymentApplicableBranch(branch: BRANCH_NAME, servers: servers, server: 'qa')) {
        stage('deployment') {
            currentStage=env.STAGE_NAME
            currentServer = servers['qa']
            utils.deployBeanstalk(
                envName : currentServer['envName'],
                configFile : dockerrunFile,
                appName : currentServer['appName'],
                requestPath : currentServer['requestPath'],
                stashName : stashName,
                version : version,
                s3Bucket : currentServer['s3Bucket'],
                registry : dockerRegistry,
                vaultPath : currentServer['vaultPath'],
                vaultAddr : vaultAddr,
                slackChannel : slackChannel,
                vaultTokenId : vaultTokenId,
                awsCreds : awsCreds
            )
        }
    }
    if (utils.isDeploymentApplicableBranch(branch: BRANCH_NAME, servers: servers, server: 'stage')) {
        stage('deployment') {
            currentStage=env.STAGE_NAME
            currentServer = servers['stage']
            utils.deployBeanstalk(
                envName : currentServer['envName'],
                configFile : dockerrunFile,
                appName : currentServer['appName'],
                requestPath : currentServer['requestPath'],
                stashName : stashName,
                version : version,
                s3Bucket : currentServer['s3Bucket'],
                registry : dockerRegistry,
                vaultPath : currentServer['vaultPath'],
                vaultAddr : vaultAddr,
                slackChannel : slackChannel,
                vaultTokenId : vaultTokenId,
                awsCreds : awsCreds
            )
            if (utils.isDeploymentApplicableBranch(branch: BRANCH_NAME, servers: servers, server: 'prod')) {
                try {
                    timeout(time: 8, unit: 'HOURS') { input message: 'Deploy production?' }
                } catch (error) {
                    currentBuild.result = 'SUCCESS'
                    echo "Aborted? Muting error."
                    return
                }
                currentServer = servers['prod']
                utils.deployBeanstalk(
                    envName : currentServer['envName'],
                    configFile : dockerrunFile,
                    appName : currentServer['appName'],
                    requestPath : currentServer['requestPath'],
                    stashName : stashName,
                    version : version,
                    s3Bucket : currentServer['s3Bucket'],
                    registry : dockerRegistry,
                    vaultPath : currentServer['vaultPath'],
                    vaultAddr : vaultAddr,
                    slackChannel : slackChannel,
                    vaultTokenId : vaultTokenId,
                    awsCreds : awsCreds
                )
            }
        }
    }
} catch(er) {
    if (utils.isError(er)) {
        if (currentStage == 'deployment') {
            utils.sendSlack(
                channel: slackChannel,
                status: "failed",
                actionType: "deployment",
                target: "env ${currentServer['envName']} in app ${currentServer['appName']}"
            )
        } else {
            utils.sendSlack(
                channel: slackChannel,
                status: "failed",
                actionType: "build",
                target: "branch ${BRANCH_NAME}"
            )
        }
    }
}

def runTests(String module) {
    switch(module) {
        case "nginx":
            testNginx()
            break
        case "website":
            testFrontend()
            break
    }
}

def buildImages(def genericImageName) {
    def images = []
    parallel(
        website: {
            dir('website') {
                websiteImage = docker.build("${genericImageName}-website")
            }
        },
        nginx: {
            dir('nginx') {
                nginxImage = docker.build("${genericImageName}-nginx-without-webapp")
            }
        },
        failFast: true)
    return images
}

def buildWebsite(def images, def buildCreds, def version, def genericImageName) {
    def showVersion = ("QA_BRANCH" in params ? true : false)
    noCdnVersion = "npm run build -- --env.buildVersion ${version} --env.showVersion ${showVersion}"
    def command = noCdnVersion
    sh "rm -rf nginx/dist/*"
    websiteImage.inside("-v ${pwd()}/nginx/website:/app/webapp/dist:rw") {
        ansiColor('xterm') {
            sh "cd /app/webapp && ${command}"
        }
    }
    dir('nginx') {
        if (!fileExists("website/index.html")) {
            error()
        }
        nginxWebsiteImage = docker.build("${genericImageName}-www-nginx")
        images << nginxWebsiteImage
    }
}

def testFrontend() {
    websiteImage.inside() {
        ansiColor('xterm') {
            sh "cd /app/webapp && yarn run test"
        }
    }
}

def testNginx() {
    echo "Test application nginx conf"
    nginxImage.inside("--add-host=backend:127.0.0.1") {
        ansiColor('xterm') {
            sh "nginx -t"
        }
    }
}
